<?php
// let the awesome begin
add_action( 'plugins_loaded', 'init_verifpayments_gateway' );

/**
 * Initiate  gateway and include the gateway code
 */
function init_verifpayments_gateway() {
   include( plugin_dir_path( __FILE__ ) . 'class/class.verifpayments.php');
   include( plugin_dir_path( __FILE__ ) . 'class/class.verifpayments_order_status.php');
}

/**
 * Add the gateway to list of WooCommerce gateways
 *
 */
function add_verifpayments( $methods ) {
    $methods[] = 'WC_Gateway_Verifpayments';
    return $methods;
}

add_filter( 'woocommerce_payment_gateways', 'add_verifpayments' );
