<?php

if (!defined('ABSPATH')) exit; // Exit if accessed directly

function get_ecomm_user($external_id)
{
    $pluginOptions = get_option('woocommerce_verifpayments_gateway_settings');
    $keyVal = get_option('woocommerce_verifpayments_gateway_keys_val');
    $curl = curl_init();

    curl_setopt_array($curl, array(
        CURLOPT_URL => $pluginOptions['api_end_point']."/users/".$external_id."/ecomms/".$keyVal['ecomm_id'],
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "GET",
        CURLOPT_HTTPHEADER => array(
        "cache-control: no-cache",
        "Content-Type:application/json"
        ),
    ));

    $response = curl_exec($curl);

    $jsonObject = json_decode(json_encode($response));
    $result = json_decode($jsonObject, true);

    curl_close($curl);

    return $result;
}

add_action('user_register', 'verifpayments_save_customer', 10, 1);
add_action('profile_update', 'verifpayments_save_customer', 10, 1);
function verifpayments_save_customer($user_id)
{
    $pluginOptions = get_option('woocommerce_verifpayments_gateway_settings');
    $keyVal = get_option('woocommerce_verifpayments_gateway_keys_val');
    $ecommId = $keyVal['ecomm_id'];
    $registeredUser = get_user_by('id', $user_id);

    $post_fields = json_encode(array(
        "displayName" => $registeredUser->data->display_name,
        "firstName" => get_user_meta($registeredUser->ID, 'first_name', true),
        "lastName" => get_user_meta($registeredUser->ID, 'last_name', true),
        "email" => $registeredUser->data->user_email,
        "phoneNumber" => get_user_meta($registeredUser->ID, 'billing_phone', true),
        "postcode" => array(
            "billing" => isset($_POST['billing_postcode']) ? $_POST['billing_postcode'] : get_user_meta($registeredUser->ID, 'billing_postcode', true),
            "shipping" => isset($_POST['shipping_postcode']) ? $_POST['shipping_postcode'] : get_user_meta($registeredUser->ID, 'shipping_postcode', true),
        ),
        "ecommId" => $ecommId,
        "externalId" => $user_id
    ));

    $curl = curl_init();

    curl_setopt_array($curl, array(
        CURLOPT_URL => $pluginOptions['api_end_point'].'/webhooks/woocommerce/customer',
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => $post_fields,
        CURLOPT_HTTPHEADER => array(
        "cache-control: no-cache",
        "Content-Type:application/json"
        ),
    ));

    $response = curl_exec($curl);

    $jsonObject = json_decode(json_encode($response));
    $result = json_decode($jsonObject, true);

    curl_close($curl);
}

add_action('woocommerce_checkout_order_processed', 'verifpayments_save_order',  1, 2);
function verifpayments_save_order($order_id)
{
    $order = new WC_Order($order_id);
    $pluginOptions = get_option('woocommerce_verifpayments_gateway_settings');
    $keyVal = get_option('woocommerce_verifpayments_gateway_keys_val');
    $registeredUser = get_user_by('id', $order->customer_id);
    $ecomm_user = get_ecomm_user($order->customer_id);
    $curl = curl_init();

    $post_fields = json_encode(array(
        "amount" => (float)$order->total * 100,
        "description" => "Woocommerce order_id=".$order_id,
        "userId" => $ecomm_user['data']['id'],
        "orderId" => $order_id,
        "ecommId" => $keyVal['ecomm_id'],
        "paymentMethod" => $order->payment_method,
        "status" => $order->status
    ));

    curl_setopt_array($curl, array(
        CURLOPT_URL => $pluginOptions['api_end_point'].'/webhooks/woocommerce/order',
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => $post_fields,
        CURLOPT_HTTPHEADER => array(
            "cache-control: no-cache",
            "Content-Type:application/json"
        ),
    ));

    $response = curl_exec($curl);

    $jsonObject = json_decode(json_encode($response));
    $result = json_decode($jsonObject, true);

    curl_close($curl);
}

add_action('edit_post_shop_order', 'verifpayments_update_order', 10, 2);
function verifpayments_update_order($order_id)
{
    $order = new WC_Order($order_id);
    $pluginOptions = get_option('woocommerce_verifpayments_gateway_settings');
    $keyVal = get_option('woocommerce_verifpayments_gateway_keys_val');
    $registeredUser = get_user_by('id', $order->customer_id);
    $ecomm_user = get_ecomm_user($order->customer_id);
    $curl = curl_init();

    $post_fields = json_encode(array(
        "amount" => (float)$order->total * 100,
        "description" => "Woocommerce order_id=".$order_id,
        "userId" => $ecomm_user['data']['id'],
        "orderId" => $order_id,
        "ecommId" => $keyVal['ecomm_id'],
        "paymentMethod" => $order->payment_method,
        "status" => $order->status
    ));

    curl_setopt_array($curl, array(
        CURLOPT_URL => $pluginOptions['api_end_point'].'/webhooks/woocommerce/order',
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "PUT",
        CURLOPT_POSTFIELDS => $post_fields,
        CURLOPT_HTTPHEADER => array(
            "cache-control: no-cache",
            "Content-Type:application/json"
        ),
    ));

    $response = curl_exec($curl);

    $jsonObject = json_decode(json_encode($response));
    $result = json_decode($jsonObject, true);

    curl_close($curl);
}

add_action('woocommerce_checkout_process', 'verifpayments_checkout_field_checks');
function verifpayments_checkout_field_checks() {
    global $woocommerce;

    // Check if set, if its not set add an error.
    if (!$_POST['card_number'] && $_POST['payment_method']=='verifpayments_gateway' ) {
        wc_add_notice( 'Please enter your <strong>Credit Card number</strong>.', $notice_type = 'error' );
    }

    if (!$_POST['cvv2'] && $_POST['payment_method']=='verifpayments_gateway'){
        wc_add_notice( 'Please enter your <strong>CVV2 number</strong>', $notice_type = 'error' );
    }
}

/**
 * Custom login/register form(s) shortcode.\
 * https://aceplugins.com/creating-a-login-registration-page-before-checkout/
 */
function ace_wc_login_register() {
	ob_start();
	do_action( 'woocommerce_before_customer_login_form' ); ?>

	<div class="u-columns col2-set" id="customer_login">
		<div class="u-column1 col-1">

			<h3><?php esc_html_e( 'Existing customer', 'woocommerce' ); ?></h3>

			<form class="woocommerce-form woocommerce-form-login login" method="post"><?php
				do_action( 'woocommerce_login_form_start' );

				?><p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
					<label for="username"><?php esc_html_e( 'Email address', 'woocommerce' ); ?>
					<input type="text" class="woocommerce-Input woocommerce-Input--text input-text" name="username" id="username" autocomplete="username" value="<?php echo ( ! empty( $_POST['username'] ) ) ? esc_attr( wp_unslash( $_POST['username'] ) ) : ''; ?>" /><?php // @codingStandardsIgnoreLine ?>
				</p>
				<p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
					<label for="password"><?php esc_html_e( 'Password', 'woocommerce' ); ?>
					<input class="woocommerce-Input woocommerce-Input--text input-text" type="password" name="password" id="password" autocomplete="current-password" />
				</p><?php

				do_action( 'woocommerce_login_form' );

				?><p class="form-row"><?php
					wp_nonce_field( 'woocommerce-login', 'woocommerce-login-nonce' );

					?><button type="submit" class="woocommerce-Button button" name="login" value="<?php esc_attr_e( 'Log in', 'woocommerce' ); ?>"><?php esc_html_e( 'Log in', 'woocommerce' ); ?></button>
					<label class="woocommerce-form__label woocommerce-form__label-for-checkbox inline">
						<input class="woocommerce-form__input woocommerce-form__input-checkbox" name="rememberme" type="checkbox" id="rememberme" value="forever" /> <span><?php esc_html_e( 'Remember me', 'woocommerce' ); ?></span>
					</label>
				</p>
				<p class="woocommerce-LostPassword lost_password">
					<a href="<?php echo esc_url( wp_lostpassword_url() ); ?>"><?php esc_html_e( 'Lost your password?', 'woocommerce' ); ?></a>
				</p><?php

				do_action( 'woocommerce_login_form_end' );
			?></form>

		</div>

		<div class="u-column2 col-2">

			<h3><?php echo sprintf( __( 'New at %s?', 'woocommerce' ), get_option( 'blogname' ) ); ?></h3>

			<form method="post" class="woocommerce-form woocommerce-form-register register" <?php do_action( 'woocommerce_register_form_tag' ); ?> ><?php
				do_action( 'woocommerce_register_form_start' );

				?><p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
					<label for="reg_email"><?php esc_html_e( 'Email address', 'woocommerce' ); ?>
					<input type="email" class="woocommerce-Input woocommerce-Input--text input-text" name="email" id="reg_email" autocomplete="email" value="<?php echo ( ! empty( $_POST['email'] ) ) ? esc_attr( wp_unslash( $_POST['email'] ) ) : ''; ?>" /><?php // @codingStandardsIgnoreLine ?>
				</p><?php

				do_action( 'woocommerce_register_form' );

				?><p class="woocommerce-FormRow form-row"><?php
					wp_nonce_field( 'woocommerce-register', 'woocommerce-register-nonce' ); ?>
					<button type="submit" class="woocommerce-Button button" name="register" value="<?php esc_attr_e( 'Register', 'woocommerce' ); ?>"><?php esc_html_e( 'Continue ordering', 'woocommerce' ); ?></button>
				</p><?php

				do_action( 'woocommerce_register_form_end' );
			?></form>

		</div>
	</div><?php

	do_action( 'woocommerce_after_customer_login_form' );

	return ob_get_clean();
}
add_shortcode( 'wc_login_register', 'ace_wc_login_register' );

/**
 * Strips input of inappropriate characters
 * @param $input
 * @return mixed
 */
function cleanInput($input) {

    $search = array(
        '@<script[^>]*?>.*?</script>@si',   // Strip out javascript
        '@<[\/\!]*?[^<>]*?>@si',            // Strip out HTML tags
        '@<style[^>]*?>.*?</style>@siU',    // Strip style tags properly
        '@<![\s\S]*?--[ \t\n\r]*>@'         // Strip multi-line comments
    );

    $output = preg_replace($search, '', $input);
    return $output;
}

/**
 * Clean input (both arrays and strings)
 * @param $input
 * @return mixed|string
 */
function sanitize($input) {
    if (is_array($input)) {
        foreach($input as $var=>$val) {
            $output[$var] = sanitize($val);
        }
    }
    else {

        $input = stripslashes($input);
        $input  = cleanInput($input);

    }
    return $input;
}


add_filter( 'woocommerce_checkout_fields' , 'default_values_checkout_fields' );
function default_values_checkout_fields( $fields )
{

    $fields['billing']['billing_first_name']['default'] = isset($_GET['billing_first_name']) ? $_GET['billing_first_name'] : '';
    $fields['billing']['billing_last_name']['default'] = isset($_GET['billing_last_name']) ? $_GET['billing_last_name'] : '';
    $fields['billing']['billing_company']['default'] = isset($_GET['billing_company']) ? $_GET['billing_company'] : '' ;
    $fields['billing']['billing_address_1']['default'] = isset($_GET['billing_address_1']) ? $_GET['billing_address_1'] : '';
    $fields['billing']['billing_address_2']['default'] = isset($_GET['billing_address_2']) ? $_GET['billing_address_2'] : '';
    $fields['billing']['billing_city']['default'] = isset($_GET['billing_city']) ? $_GET['billing_city'] : '';
    $fields['billing']['billing_postcode']['default'] = isset($_GET['billing_postcode']) ? $_GET['billing_postcode'] : '';
    $fields['billing']['billing_country']['default'] = isset($_GET['billing_country']) ? $_GET['billing_country'] : '';
    $fields['billing']['billing_email']['default'] = isset($_GET['billing_email']) ? $_GET['billing_email'] : '';
    $fields['billing']['billing_phone']['default'] = isset($_GET['billing_phone']) ? $_GET['billing_phone'] : '';

    return $fields;
}

/**
 * Class WC_Gateway_verifpayments - Handles all Payment Gateway functionality
 */
class WC_Gateway_verifpayments extends WC_Payment_Gateway
{
    public function __construct()
    {

        global $woocommerce;

        // define variables
        $this->id = 'verifpayments_gateway';
        $this->has_fields = true; //  – puts payment form fields into payment options on payment page.
        $this->method_title = __('Verif Payments', 'woocommerce'); //– Title of the payment method shown on the admin page.
        $this->method_description = 'Use verifpayments to process your payments.'; //– Description for the payment method shown on the admin page

        // load the settings
        $this->init_form_fields();
        $this->init_settings();

        // define user set variables
        $this->title = $this->get_option('title');
        $this->description = $this->get_option('description');
        $this->keys_val = get_option('woocommerce_verifpayments_gateway_keys_val');
        $this->http_force = 'no';

        $this->env_error = 'false';
        $this->disablegateway_js ='';
        $this->api_end_point = $this->get_option('api_end_point');
        $this->token = $this->get_option('token');

        $this->micro_transaction_auth_token = null;
        $this->micro_transaction_button_possible = false;

        add_action('woocommerce_update_options_payment_gateways_' . $this->id, array($this, 'process_admin_options'));

        // if force http after checkout is checked don't run SSL checkout page validation
        if ($this->http_force != true) {
            $this->sslcheck = check_ssl_checkoutpage();
        } else {
            $this->sslcheck = 'na';
        }

        // check if force ssl option is checked
        $this->forcesslchecked = force_ssl_checked();

        // check for existing API key signature
        if (is_array($this->keys_val) && $this->keys_val['status'] == 'true'):
            if ($this->token == $this->keys_val['token'] && $this->keys_val['ecomm_id'] != null):
                $this->api_info_valid = 'true';
            else:
                $this->api_info_valid = validate_verifpayments_api($this->api_end_point, $this->token);
            endif;
        else:
            $this->api_info_valid = validate_verifpayments_api($this->api_end_point, $this->token);
        endif;

        if ($this->sslcheck == 'false' || $this->forcesslchecked != 'true' || $this->api_info_valid != 'true' || $this->http_force !='no' ) {

            // disable gateway
            $this->enabled = false;

            // include jquery to uncheck enabled box
            $this->disablegateway_js = '<script type="text/javascript">
            jQuery(document).ready(function ($) {
                $( "#woocommerce_verifpayments_gateway_enabled" ).prop( "checked", false );
            });
            </script>';

            // process errors
            $this->env_error = $this->gen_verifpayments_error();
        }
    }

    /**
     * Create the settings page for the  Payment Gateway.
     *
     * @access public
     */
    public function admin_options()
    {
        ?>

        <script type="text/javascript">
            // validate the form to prevent activating the gateway without the data needed for it to work.
            jQuery(document).ready(function () {
                jQuery("#mainform").validate({
                    rules: {
                        woocommerce_verifpayments_gateway_token: {
                            required: {
                                depends: function(element) {
                                    return jQuery("input[name='woocommerce_verifpayments_gateway_enabled']:checked").length == 1
                                }
                            }
                        }
                    },
                    messages: {
                        woocommerce_verifpayments_gateway_token: {
                            required: "Please enter your token"
                        }
                    }
                });
            });

        </script>
        <?php

        // load disabling jquery if gateway disabled
        echo $this->disablegateway_js;

        ?>
        <h3><?php _e('Verif Payments for WooCommerce', 'woocommerce'); ?></h3>
        <p><?php _e('Verif Payments works by processing the <strong>credit card</strong> payment via your verifpayments account.', 'woocommerce'); ?></p>

            <table class="form-table">
                <?php

                // load error message if errors
                if($this->env_error !='false'){
                    echo $this->env_error;
                }

                // Generate the HTML For the settings form.
                $this->generate_settings_html();
                ?>
            </table><!--/.form-table-->

        <?php
    }

    /**
     * Run gateway error checks and if errors, create error message
     * @return string
     */
    public function gen_verifpayments_error()
    {

        $errortext = '';
        $errors = array();

        // if force http after checkout is checked
        if($this->http_force !='no') {
            $errors[] = 'Please uncheck the "Force HTTP when leaving the checkout" option located on
                     the <a href="' . get_admin_url() . 'admin.php?page=wc-settings&tab=checkout">Checkout Options</a> page.';
        }

        // if force ssl option is not checked
        if($this->forcesslchecked !='true'){
            $errors[] = 'Please check the "Force Secure Checkout" box on the
                <a href="' . get_admin_url() . 'admin.php?page=wc-settings&tab=checkout">Checkout Options</a> settings tab.';
        }

        // if form submitted run additional checks
        if($_POST) {

            // if SSL on checkout page check fails
            if ($this->sslcheck == 'false') {
                $errors[] = 'Please install a valid SSL certificate at your domain, for use with the checkout page';
            }

            // if  API credentials are invalid
            if($this->api_info_valid != 'true'){
                $errors[] = 'Please re-enter your token.';
            }
        }

        // if error(s) found return error message
        if(!empty($errors)){
            $errortext = '<div class="inline error"><p><strong>' . __('Gateway Disabled', 'woocommerce') . '</strong>:<br />';

            foreach($errors as $error){
                $errortext .= __($error, 'woocommerce').'<br />';
            }
            $errortext.='</p></div>';
        }
        return $errortext;
    }

    /**
     * Show the  Gateway Settings Form Fields in the Settings page
     *
     * @access public
     * @return void
     */
    public function init_form_fields()
    {

        $server_name = "https://".$_SERVER['SERVER_NAME'];

        // set the form fields array for the  Payment Gateway settings page.
        $this->form_fields = array(
            'title' => array(
                'title' => __('Title', 'woocommerce'),
                'type' => 'text',
                'description' => __('This controls the title which the user sees during checkout.', 'woocommerce'),
                'default' => __('Credit Card', 'woocommerce'),
            ),
            'api_end_point' => array(
                'title' => __('API Endpoint Url:', 'woocommerce'),
                'type' => 'text',
                'description' => __('API Endpoint.', 'woocommerce'),
                'default' => __('https://dazzling-bose-64fe6e.netlify.app/api', 'woocommerce'),
            ),
            'token' => array(
              'title' => __('Token', 'woocommerce'),
              'type' => 'text',
              'description' => __('*Your token can be found and changed via the VerifSystem dashboard.</a>', 'woocommerce'),
              'default' => ''
            ),
            'enabled' => array(
                'title' => __('Enable/Disable', 'woocommerce'),
                'type' => 'checkbox',
                'label' => __('Enable verifpayments', 'woocommerce'),
                'default' => 'no'
            )
        );
    }

    private function get_user_id()
    {
        $current_user_id = get_current_user_id();

        if($current_user_id == 0) {
            $session_temp_user_id = WC()->session->get( 'micro_temp_user_id' );
            if(strlen($session_temp_user_id) === 30){
                $current_user_id = $session_temp_user_id ;
            } else {
                $current_user_id = $this->random_string(30);
                WC()->session->set( 'micro_temp_user_id' , $current_user_id );
            }
        } else {
            $micro_temp_user_id = get_metadata( 'user', $current_user_id, 'micro_temp_user_id',true );
            if(strlen($micro_temp_user_id) === 30){
                $current_user_id = $micro_temp_user_id;
            }
        }

        return $current_user_id;
    }

    function random_string($length)
    {
        $key = '';
        $keys = array_merge(range(0, 9), range('a', 'z'));

        for ($i = 0; $i < $length; $i++) {
            $key .= $keys[array_rand($keys)];
        }

        return $key;
    }

    /**
     * Initialise the payment fields on the front-end payment form
     *
     * @access public
     */
    public function payment_fields()
    {
        global $woocommerce;

        if (is_user_logged_in() == 1) {
            $current_user = wp_get_current_user();
            $current_user_meta = get_user_meta($current_user->ID);
            $current_user_id = $this->get_user_id();
            $amount =  WC()->cart->total;
            $ecomm_user = $this->get_ecomm_user($current_user_id);

            if (is_null($ecomm_user['data'])) {
                echo "<div>Authentication Failed.</div>";
                return;
            }
            else {
                if ($this->get_latest_veriff($ecomm_user['data']['veriffs'])['decision'] == 'approved') {
                    if (!empty($ecomm_user['data']['cards'])) {
                        echo "<div>Your credit card has already been verified.</div><br>";
                        echo "<div>************".$ecomm_user['data']['cards'][0]['last4']."</div>";
                        echo '<div class="loader_box_column"><div id="delete-card-start"></div></div>';
                        echo '<div id="micro_tran_delete_card">Remove Card?</div>';
                        echo "<br><div>Now you can place an order!</div>";

                        // MAKE FAKE CARD AND CVV FOR PROCESS
                        echo '<div><input type="hidden" name="card_number" id="card_number" value="4242424242424242"/></div>';
                        echo '<div><input type="hidden" size="5" name="cvv2" id="cvv2" value="123" /></div>';

                        ?>
                        <script type="text/javascript">
                            jQuery(document).ready(function () {
                                jQuery("#micro_tran_delete_card").click(function(){
                                    if(confirm("Do you want to continue?")) {
                                        jQuery("#micro_tran_delete_card").attr('disabled','disabled');
                                        jQuery("#delete-card-start").addClass('loader');

                                        jQuery.ajax({
                                            url: '<?php echo $this->api_end_point;?>/users/<?php echo $ecomm_user['data']['id']; ?>/cards/<?php echo $ecomm_user['data']['cards'][0]['id'] ?>',
                                            type: "DELETE",
                                            contentType: 'application/json',
                                            success: function(response) {
                                                jQuery("#delete-card-start").removeClass('loader');
                                                window.location.reload(true);
                                            },
                                            error: function(error){
                                                jQuery("#micro_tran_delete_card").removeAttr('disabled');
                                                jQuery("#delete-card-start").removeClass('loader');
                                                alert(error.responseJSON.error);
                                            }
                                        });
                                    }
                                });
                            });
                        </script>
                        <?php
                    }
                    else {
                        echo'<div class="verifpayments-card">';
                        echo'<div>' . __('Credit Card Number*:', 'woocommerce') . ' <input type="text" name="card_number" id="card_number" class="verifpayments_input_cardnumber"/></div>
                            <div class="verifpayments_input_line">' . __('Expiration Month*: ', 'woocommerce') . $this->verifpayments_select_month_list() .' </div>'.
                            '<div class="verifpayments_input_line">'.__('Expiration Year*: ', 'woocommerce') . $this->verifpayments_select_year_list() . ' </div>
                            <div class="verifpayments_input_line">' . __('Security Code*:', 'woocommerce') . ' <input type="text" size="5" name="cvv2" id="cvv2" /></div>
                            ';

                        echo ' <div class="verifpayments-add_card_message"></div>';
                        echo '<br/>';
                        echo '<div class="loader_box_column"><div id="add-card-start"></div></div>';
                        echo '<div id="micro_tran_add_card">Add Card</div>';
                        echo '</div>';

                        echo ' <div class="verifpayments-powered-by">Powered by Verif Payments</div>';

                        ?>
                        <script type="text/javascript">
                            jQuery(document).ready(function () {
                                var paymentMethod = jQuery("input[name='payment_method']:checked").val();

                                if(paymentMethod == 'verifpayments_gateway'){
                                    jQuery("#place_order").attr('disabled','disabled');
                                } else {
                                    jQuery("#place_order").attr('disabled',false);
                                }

                                jQuery("#card_number").inputmask({"mask": "9999 9999 9999 9999"});

                                jQuery("#micro_tran_add_card").click(function(){
                                    jQuery("#micro_tran_add_card").attr('disabled','disabled');
                                    jQuery("#add-card-start").addClass('loader');

                                    var cardNumber = jQuery("#card_number").val();
                                    var expiryMonth = jQuery("#card_expiry_month").val();
                                    var expiryYear = jQuery("#card_expiry_year").val();
                                    var cvc = jQuery("#cvv2").val();

                                    var billing_first_name = jQuery("#billing_first_name").val() || '<?php echo get_user_meta($current_user->ID, 'first_name', true) ?>' || '<?php echo WC()->customer->get_billing_first_name() ?>';
                                    var billing_last_name = jQuery("#billing_last_name").val() || '<?php echo get_user_meta($current_user->ID, 'last_name', true) ?>' || '<?php echo WC()->customer->get_billing_last_name() ?>';

                                    var billing_company = jQuery("#billing_company").val();
                                    var billing_address_1 = jQuery("#billing_address_1").val();
                                    var billing_address_2 = jQuery("#billing_address_2").val();
                                    var billing_city = jQuery("#billing_city").val();
                                    var billing_postcode = jQuery("#billing_postcode").val();
                                    var billing_country = jQuery("#billing_country").val();
                                    var billing_state = jQuery("#billing_state").val();
                                    var billing_email = jQuery("#billing_email").val();
                                    var billing_phone = jQuery("#billing_phone").val();

                                    var replaceCardNumber = cardNumber.replace("_", "");
                                    var url = '<?php echo $this->api_end_point; ?>/users/<?php echo $ecomm_user['data']['id']; ?>/cards';
                                    var data = {
                                        cardHolderName: billing_first_name+' '+billing_last_name,
                                        billingAddress: {
                                            address1: '<?php echo WC()->customer->get_billing_address_1() ?>',
                                            address2: '<?php echo WC()->customer->get_billing_address_2() ?>',
                                            city: '<?php echo WC()->customer->get_billing_city() ?>',
                                            state: '<?php echo WC()->customer->get_billing_state() ?>',
                                            country: '<?php echo WC()->customer->get_billing_country() ?>',
                                            zipCode: '<?php echo WC()->customer->get_billing_postcode() ?>'
                                        },
                                        description: null,
                                        number: replaceCardNumber,
                                        expYear: expiryYear,
                                        expMonth: expiryMonth,
                                        cvc: cvc
                                    };

                                    jQuery.ajax({
                                        url: url,
                                        type: "POST",
                                        data: JSON.stringify(data),
                                        contentType: "application/json",
                                        success:function(response) {
                                            jQuery(".verifpayments-add_card_message").removeClass("verifpayments-add_card_error");
                                            jQuery(".verifpayments-add_card_message").text("We successfully added your card!");
                                            jQuery("#add-card-start").removeClass('loader');

                                            window.location.href += (window.location.href.indexOf('?') > -1 ? '&' : '?')
                                            + 'billing_first_name='+encodeURIComponent(billing_first_name)
                                            + '&billing_last_name='+encodeURIComponent(billing_last_name)
                                            + '&billing_email='+encodeURIComponent(billing_email)
                                            + '&billing_phone='+encodeURIComponent(billing_phone)
                                            + '&billing_company='+encodeURIComponent(billing_company)
                                            + '&billing_address_1='+encodeURIComponent(billing_address_1)
                                            + '&billing_address_2='+encodeURIComponent(billing_address_2)
                                            + '&billing_city='+encodeURIComponent(billing_city)
                                            + '&billing_postcode='+encodeURIComponent(billing_postcode)
                                            + '&billing_country='+encodeURIComponent(billing_country)
                                            + '&billing_state='+encodeURIComponent(billing_state)
                                        },
                                        error:function(error){
                                            jQuery(".verifpayments-add_card_message").text(error.responseJSON.error);
                                            jQuery(".verifpayments-add_card_message").addClass("verifpayments-add_card_error");
                                            jQuery("#micro_tran_add_card").removeAttr('disabled');
                                            jQuery("#add-card-start").removeClass('loader');
                                            alert(error.responseJSON.error);
                                        }
                                    });
                                });
                            });
                        </script>
                        <?php
                    }
                }
                else {
                    if ($this->get_latest_veriff($ecomm_user['data']['veriffs'])['decision'] == 'submitted') {
                        echo '<div class="verifpayments_veriff_info">';
                        echo '<p>Thank you for completing the verification session. This process might take up to <strong>10 minutes</strong>, and we will send an email to your registered email address once the session is completed.</p>';
                        echo '<p>Please check your inbox or junk/spam/promotion folder shortly to view your results.</p>';
                        echo '<p>If approved, you will be able to use the creditcard payment option on <strong>'.get_bloginfo( 'name' ).'</strong>.</p>';
                        echo '<p>if the email was sent to your <strong>junk/spam/promotion</strong> folder, please remember to click and drag that email into your inbox so you can stay up-to-date on any issues regarding your online payments with <strong>'.get_bloginfo( 'name' ).'</strong></p>';
                        echo '</div>';
                    }
                    else {
                        $processor = $this->pick_processor('verification');

                        if (!is_null($processor['data'])) {
                            if ($this->get_latest_veriff($ecomm_user['data']['veriffs'])['decision'] == 'declined') {
                                echo '<div style="margin-bottom: 5px;">Your verification is declined. Please verify it again.</div>';
                            }

                            echo '<div id="verify_user_submit"></div>';

                            ?>
                            <script type="text/javascript">
                                jQuery(document).ready(function () {
                                    var v = Veriff({
                                        host: 'https://stationapi.veriff.com',
                                        apiKey: '<?php echo $processor['data']['apiKey']; ?>',
                                        parentId: 'verify_user_submit',
                                        onSession(err, response) {
                                            window.veriffSDK.createVeriffFrame({
                                                url: response.verification.url,
                                                onEvent: function(msg) {
                                                    switch(msg) {
                                                        case 'FINISHED':
                                                            window.location.reload();
                                                        break;
                                                    }
                                                }
                                            });
                                        },
                                    });

                                    v.setParams({
                                        person: {
                                            givenName: '<?php echo $ecomm_user['data']['firstName'] ?>',
                                            lastName: '<?php echo $ecomm_user['data']['lastName'] ?>',
                                        },
                                        vendorData: JSON.stringify({
                                            ecommId: '<?php echo $this->keys_val['ecomm_id'] ?>',
                                            externalId: '<?php echo $current_user->ID ?>',
                                        }),
                                    });

                                    v.mount();
                                });
                            </script>
                            <?php
                        }
                        else {
                            echo '<div class="verifpayments_veriff_info">';
                            echo '<p>No Veriff processor found. Contact administrator.</p>';
                            echo '</div>';
                        }
                    }
                }
            }
        }
        else {
            echo '<div>Please <a href="'.get_permalink(get_option('woocommerce_myaccount_page_id')).'">login</a> to continue.</div>';
        }
    }

    /**
     * Generate month selector for payment form
     *
     * @access public
     * @return string
     */
    public function verifpayments_select_month_list()
    {

        $return = '<select name="card_expiry_month" id="card_expiry_month" class="verifpayments_dropdown">';
        for ($i = 1; $i <= 12; $i++) {
            $return .= '<option value="' . $i . '">' . $i . '</option>';
        }
        $return .= '</select>';

        // return full dropdown html
        return $return;
    }

    /**
     * Generate year selector for payment form
     *
     * @access public
     * @return string
     */
    public function verifpayments_select_year_list()
    {

        $return = '<select name="card_expiry_year" id="card_expiry_year" class="verifpayments_dropdown">';
        $year = date('Y');
        $year_end = $year + 10;

        for ($i = $year; $i <= $year_end; $i++) {
            $return .= '<option value="' . $i . '">' . $i . '</option>';
        }

        $return .= '</select>';
        return $return;
    }

    /**
     * Process  Payment
     *
     * @access public
     * @param $order_id
     * @return array
     */
    function process_payment($order_id)
    {
        global $woocommerce;

        // get the order by order_id
        $order = new WC_Order($order_id);

        // get all of the args
        $verifpayments_args = $this->get_verifpayments_args($order);

        // post to verifpayments
        $verifpayments_result = $this->post_to_verifpayments($verifpayments_args);

        if ($verifpayments_result['status'] == 201){
            // mark payment as complete
            $order->payment_complete();

            // Remove cart
            $woocommerce->cart->empty_cart();

            // Return thankyou redirect
            return array(
                'result' => 'success',
                'redirect' => $this->get_return_url($order)
            );
        }
        else {
            $order->update_status( 'failed' );
            $error_message = 'Your payment declined.  Please enter your payment details again.';
            wc_add_notice( '<strong>Payment declined:</strong>Please enter your payment details again', $notice_type = 'error' );

            return;
        }
    }

    /**
     * Build array of args to send to  for processing
     *
     * @access private
     * @return array
     */
    private function get_verifpayments_args($order)
    {
        //  required arguments
        $verifpayments_args = array(
            'first_name' => $order->billing_first_name,
            'last_name' => $order->billing_last_name,
            'company' => $order->billing_company,
            'address1' => $order->billing_address_1,
            'address2' => $order->billing_address_2,
            'city' => $order->billing_city,
            'province' => $order->billing_state,
            'postal_code' => $order->billing_postcode,
            'country' => $order->billing_country,
            'email' => $order->billing_email,
            'amount_tax' => $order->get_total_tax(),
            'amount' => $order->order_total,
            'order_id' => $order->id,
            'currency' => $currency,
            'telephone'=> $order->billing_phone,
            'card_number' => sanitize($_POST['card_number']),
            'card_expiry_month' => sanitize($_POST['card_expiry_month']),
            'card_expiry_year' => sanitize($_POST['card_expiry_year']),
            'cvv2' => sanitize($_POST['cvv2']),
            'description'=>'Order #'.$order->id.' from '.get_bloginfo('url'),
        );

        // return the array of arguments
        return $verifpayments_args;
    }

    /**
     * Open a cURL connection to  API, post values, return the result object
     *
     * @access private
     * @return object
     */
    private function post_to_verifpayments($wooCheckoutOrderArgs)
    {
        $current_user_id = $this->get_user_id();
        $ecomm_user = $this->get_ecomm_user($current_user_id);
        $processor = $this->pick_processor('payment');

        if (is_null($ecomm_user['data'])) {
            return null;
        }

        $verifpayments_args = json_encode(array(
            "currency" => get_woocommerce_currency(),
            "amount" => (float)$wooCheckoutOrderArgs['amount'] * 100,
            "description" => "Woocommerce order_id=".$wooCheckoutOrderArgs['order_id'],
            "user" => $ecomm_user['data'],
            "card" => $ecomm_user['data']['cards'][0],
            "gateway" => $processor['data'],
            "processor" => $processor['data']['processor'],
            "ecommId" => $this->keys_val['ecomm_id']
        ));

        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $processor['data']['processor']['baseUrl']."/gateway-configs/charge",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => $verifpayments_args,
            CURLOPT_HTTPHEADER => array(
                "cache-control: no-cache",
                "Content-Type:application/json"
            ),
        ));

        $response = curl_exec($curl);

        $jsonObject = json_decode(json_encode($response));
        $json_feed_object = json_decode($jsonObject, true);

        $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        curl_close($curl);

        $result = array(
            'status'=> $httpcode,
            'data'=> $json_feed_object
        );

        return $result;
    }

    function get_latest_veriff($veriffs) {
        usort($veriffs, function($a, $b) {
            $datetime1 = strtotime($a['datetime']);
            $datetime2 = strtotime($b['datetime']);

            return $datetime1 < $datetime2 ? -1 : 1;
        });

        return $veriffs[count($veriffs) - 1];
    }

    function get_ecomm_user($external_id)
    {
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => $this->api_end_point."/users/".$external_id."/ecomms/".$this->keys_val['ecomm_id'],
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
            "cache-control: no-cache",
            "Content-Type:application/json"
            ),
        ));

        $response = curl_exec($curl);

        $jsonObject = json_decode(json_encode($response));
        $result = json_decode($jsonObject, true);

        curl_close($curl);

        return $result;
    }

    function pick_processor($processorType)
    {
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => $this->api_end_point."/ecomms/".$this->keys_val['ecomm_id']."/processors/".$processorType,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
            "cache-control: no-cache",
            "Content-Type:application/json"
            ),
        ));

        $response = curl_exec($curl);

        $jsonObject = json_decode(json_encode($response));
        $responseArray = json_decode($jsonObject, true);

        curl_close($curl);

        return $responseArray;
    }
}

