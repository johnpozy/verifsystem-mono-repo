<?php

/*
 * Register a custom order status
 */
function verifpayments_register_dispute_status() {
	register_post_status('wc-dispute', array(
        'label'		=> 'Dispute',
        'public'	=> true,
        'show_in_admin_status_list' => true,
        'label_count'	=> _n_noop( 'Dispute (%s)', 'Dispute (%s)' )
    ));
}
add_action('init', 'verifpayments_register_dispute_status');

// Add registered status to list of WC Order statuses
function verifpayments_add_status_to_list( $order_statuses ) {
	$order_statuses['wc-dispute'] = 'Dispute';

	return $order_statuses;
}
add_filter('wc_order_statuses', 'verifpayments_add_status_to_list');
