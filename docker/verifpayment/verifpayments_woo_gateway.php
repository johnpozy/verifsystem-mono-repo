<?php
/**
 * Plugin Name: Verif Payments for WooCommerce
 * Plugin URI:  https://verifsystem.com
 * Description: Verif Payments
 * Version: 1.1.0
 */


/**
 * Perform plugin environment checks before allowing  Gateway to be activated.
 */
function verifpayments_woo_requires() {

    global $wp_version;

    global $woocommerce;

    $plugin = plugin_basename( __FILE__ );
    $plugin_data = get_plugin_data( __FILE__, false );
    $require_wp = "3.5";

    $plugin_to_check= 'woocommerce/woocommerce.php';
    $req_woocommerce_version ='2.0';

    $verifpayments_curl = verifpayments_curl_installed();

    /**
     * Add custom role
     */
    add_role('approved_customer', __('Approved Customer'), array('read' => true));
    add_role('declined_customer', __('Declined Customer'));

    /**
     * Automatically deactivates  Woo Gateway if WooCommerce is deactivated.
    */
    if ( version_compare( $wp_version, $require_wp, "<" ) ) {
        if( is_plugin_active($plugin) ) {
            deactivate_plugins( $plugin );
            wp_die( "<strong>".$plugin_data['Name']."</strong> requires <strong>WordPress ".$require_wp."</strong> or higher, and has been deactivated! Please upgrade WordPress and try again.<br /><br />Back to the WordPress <a href='".get_admin_url(null, 'plugins.php')."'>Plugins page</a>." );
        }
    }

    /**
     * Automatically deactivates  Woo Gateway if CURL cannot be used.
     */
    if( $verifpayments_curl=='false'){
        deactivate_plugins( $plugin );
        wp_die( "<strong>".$plugin_data['Name']."</strong> requires <strong> cURL</strong> to be active, and has been deactivated! Please activate cURL to use ".$plugin_data['Name']." again.<br /><br />Back to the WordPress <a href='".get_admin_url(null, 'plugins.php')."'>Plugins page</a>." );

    }

    /**
     * Automatically deactivates  Woo Gateway if SSL is not valid.
     */
    if(check_ssl_valid() !='true'){
        deactivate_plugins( $plugin );
        wp_die( "<strong>".$plugin_data['Name']."</strong> requires a <strong> valid SSL certificate</strong> to be active, and has been deactivated! Please install a valid SSL certificate to use ".$plugin_data['Name']." again.<br /><br />Back to the WordPress <a href='".get_admin_url(null, 'plugins.php')."'>Plugins page</a>." );

    }

    /**
     * Automatically deactivates  Woo Gateway if parent plugin WooCommerce is not active.
     */
    if(!is_plugin_active($plugin_to_check)){
        deactivate_plugins( $plugin );
        wp_die( "<strong>".$plugin_data['Name']."</strong> requires <strong> WooCommerce</strong> to be active, and has been deactivated! Please activate WooCommerce to use ".$plugin_data['Name']." again.<br /><br />Back to the WordPress <a href='".get_admin_url(null, 'plugins.php')."'>Plugins page</a>." );

    }else{

        /**
         * Automatically deactivates  Woo Gateway if active WooCommerce is an unsupported version #.
         */

        if( get_woo_version() < $req_woocommerce_version){
            deactivate_plugins( $plugin );
            wp_die( "<strong>".$plugin_data['Name']."</strong> requires <strong> WooCommerce</strong> to be Version ".$req_woocommerce_version." or above, and has been deactivated! Please install and activate WooCommerce ".$req_woocommerce_version." or above to use ".$plugin_data['Name']." again.<br /><br />Back to the WordPress <a href='".get_admin_url(null, 'plugins.php')."'>Plugins page</a>." );

        }
    }

    if(is_plugin_active($plugin)) {

        add_filter( 'plugin_action_links_' . plugin_basename(__FILE__), 'add_action_links');

        function add_action_links ( $actions ) {
        $settingLinks = array(
            '<a href="' . admin_url( 'admin.php?page=wc-settings&tab=checkout&section=verifpayments_gateway' ) . '">Settings</a>',
        );
        $actions = array_merge( $settingLinks , $actions);
        return $actions;
        }
    }
}
add_action( 'admin_init', 'verifpayments_woo_requires' );

/**
 * Checks to make sure WooCommerce is active before loading the  Woo Gateway assets.
 */
if ( in_array( 'woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) )) {

    // if woocommerce is installed, add payment gateway
    include('verifpayments_woo_go.php');

    add_action('admin_enqueue_scripts', 'add_my_js');
    function add_my_js(){
        wp_enqueue_script('my_validate', plugin_dir_url( __FILE__ ) .'js/jquery.validate.min.js', array('jquery'));
    }

    add_action('init', 'add_my_style');
    function add_my_style(){
        wp_enqueue_style( 'verifpayments_Style', plugin_dir_url( __FILE__ ) .'css/verifpayments.css' );
        wp_enqueue_style( 'smart-payments-card', plugin_dir_url( __FILE__ ) . 'css/style-card.css' );
    }

    add_action( 'wp_enqueue_scripts', 'wpdocs_theme_name_scripts' );
    function wpdocs_theme_name_scripts() {
        wp_enqueue_script('veriff', plugin_dir_url( __FILE__ ) . 'js/veriff.min.js', array(), "1.2.0", true );
        wp_enqueue_script('veriff-incontext', plugin_dir_url( __FILE__ ) . 'js/veriff-incontext.min.js', array('jquery', 'veriff'), "1.0.0", true );
        wp_enqueue_script('smart-payments-card', plugin_dir_url( __FILE__ ) . 'js/smart-payments-scripts.js', array('jquery', 'veriff'), "1.0.0", true );
        wp_enqueue_script('inputmask', plugin_dir_url( __FILE__ ) .'js/jquery.inputmask.min.js', array('jquery'));
    }
}

/**
 * Checks set website for valid SSL certificate
 * @return string
 */
function check_ssl_valid(){

    // TODO: REMOVE AFTER TESTING
    //return true;

    //global $woocommerce;

    $return ='false';

    //$checkout_page_id = woocommerce_get_page_id('checkout');
    // print $checkout_page_id;

    $checkout_url = str_replace( 'http:', 'https:', get_bloginfo('url') );

    //print $checkout_url;
    // Initialize session and set URL.
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $checkout_url);

    // Set so curl_exec returns the result instead of outputting it.
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_VERBOSE, 1);
    curl_setopt($ch, CURLOPT_HEADER, 1);

    // since we won't know the file location to allow verifypeer, disable this check
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

    // execute the cURL
    curl_exec($ch);

    //print curl_error($ch);

    // Get the response and close the channel.
    $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
    curl_close($ch);

    //print $httpcode;

    if($httpcode == 200){
        $return = 'true';
    }

    return $return;
}

/**
 * Checks the set checkout page for SSL connection and '200' (ok) header response.
 *
 */

function check_ssl_checkoutpage(){

    //global $woocommerce;
    $return ='false';
    $checkout_page_id = woocommerce_get_page_id('checkout');

    // print $checkout_page_id;
    if ( $checkout_page_id ) {
        $checkout_url = str_replace( 'http:', 'https:', get_permalink($checkout_page_id) );
    }

    //print $checkout_url;
    // Initialize session and set URL.
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $checkout_url);

    // Set so curl_exec returns the result instead of outputting it.
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_VERBOSE, 1);
    curl_setopt($ch, CURLOPT_HEADER, 1);

    // since we won't know the file location to allow verifypeer, disable this check
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

    // execute the cURL
    curl_exec($ch);

    //print curl_error($ch);

    // Get the response and close the channel.
    $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
    curl_close($ch);

    //print $httpcode;

    if($httpcode == 200){
        $return = 'true';
    }

    return $return;
}

/**
 * Checks the for the "Force secure checkout" WooCommerce option to be checked.
 * @return string
 */
function force_ssl_checked(){

    // old setting in WooCommerce that would force a site that doesn’t normally use SSL to use it on checkout.
    return true;

    $return ='false';
    $ssl_forced = get_option('woocommerce_force_ssl_checkout');

    if($ssl_forced == 'yes'){
        $return = 'true';
    }

    return $return;

}

/**
 * Checks the status of cURL on the server.
 *
 */
function verifpayments_curl_installed() {

    if(!function_exists('curl_exec'))
    {
        return 'false';
    }
}

/**
 * Validates API and Store token
 * @param $token
 * @return string
 */
function validate_verifpayments_api($api_end_point, $token){

    $return ='false';

    $curl = curl_init();
    curl_setopt_array($curl, array(
        CURLOPT_URL => $api_end_point."/ecomms/verify/".$token,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "GET",
        CURLOPT_HTTPHEADER => array(
        "cache-control: no-cache",
        "Content-Type:application/json"
        ),
    ));

    $response = curl_exec($curl);

    $jsonObject = json_decode(json_encode($response));
    $responseArray = json_decode($jsonObject, true);

    curl_close($curl);

    if($responseArray['code'] == 200){
        $return = 'true';
        update_option( 'woocommerce_verifpayments_gateway_keys_val', array(
          'status'=>'true',
          'token'=>$token,
          'ecomm_id'=>$responseArray['data']['id']
        ));
    }else{
      update_option( 'woocommerce_verifpayments_gateway_keys_val', array(
        'status'=>'false',
        'token'=>$token,
        'ecomm_id'=>null
      ));
    }

    return $return;
}

/**
 * get woo commerce version number
 * @return mixed
 */
function get_woo_version(){

    $plugin_to_check= 'woocommerce/woocommerce.php';
    if ( ! function_exists( 'get_plugins' ) ) {
        require_once ABSPATH . 'wp-admin/includes/plugin.php';
    }
    $plugins = get_plugins();
    $plugin_data = $plugins[$plugin_to_check];

    return $plugin_data['Version'];
}

/**
 * Class verifpayments_Logger
 *
 * Logger class so any issues can be logged in the same place.
 *
 * The logfile is located in the logs folder within the verifpayments_Woo_Gateway plugin folder.
 */

class verifpayments_Logger{

    /**
     * Creates the necessary gateway log file in the  Woo Gateway plugin "logs" folder.
     * If file creation fails, plugin is deactivated.
     *
     * @access public
     */

    public function create_logfile($plugin=''){

        $newFileName2 =  plugin_dir_path( __FILE__ ) .'/logs/logfile.php';
        $newFileContent2 = " <?php if (!defined('ABSPATH')) exit; // Exit if accessed directly ?>".PHP_EOL;

        if(!file_exists($newFileName2)):
            if(file_put_contents($newFileName2,$newFileContent2)===false):
                deactivate_plugins( $plugin );
                wp_die( '<strong>Cannot create file '.basename($newFileName2).'.  Please check your server settings to make sure the "verifpayments_Woo_Gateway" plugin folder is writable.</strong><br /> <br />Back to the WordPress <a href="'.get_admin_url(null, "plugins.php").'">Plugins page</a>.');

            endif;
        endif;

        return 'true';
    }

    /**
     * Stores the error string in the  Woo Gateway logfile.  Log file is .php to prevent online access to contents.
     * If directly accessed, blank page loads.
     * Read via FTP download and open like a text file.
     * @access public
     * @param string $type
     * @param string $error
     */

    public function add_to_logfile($type='',$error=''){

        $date = date('Y-m-d H:i:s');
        $content = '+ '.$type.' Error Occured: '.$date.': '.$error.PHP_EOL;
        $file = plugin_dir_path( __FILE__ ) .'/logs/logfile.php';
        file_put_contents($file, $content, FILE_APPEND | LOCK_EX);
    }
}
