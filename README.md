[![Commitizen friendly](https://img.shields.io/badge/commitizen-friendly-brightgreen.svg)](http://commitizen.github.io/cz-cli/)
[![Latest Tag](https://img.shields.io/gitlab/v/tag/onseen.yt/verifsystem-mono-repo)](https://gitlab.com/onseen.yt/verifsystem-mono-repo/-/tags)

# Prerequisites
- docker with docker-compose (for wordpress plugin development)
- node 14+
- yarn
- nestjs
- nextjs
- vscode as editor

# Folder structure
Since we're using/utilizing **nx-workspace** below is our code folder structure.

```
root/
├── apps/
│   ├── backend (verifsystem)
│   ├── dashboard (admin portal)
│   ├── payment (vancityscooter)
│   └── veriff (nsbeautysupply)
├── docker/
│   ├── smartpayment (wp plugin)
│   └── ...
├── libs/
│   ├── api (shareable libs between backend, payment and veriff)
│   │   ├── features
│   │   ├── shared
│   │   └── utils
│   ├── dashboard (dashboard libs)
│   │   ├── data-access
│   │   ├── features
│   │   ├── shared
│   │   ├── styles
│   │   └── utils
│   └── shared (shareable libs)
│       ├── data-access
│       └── utils
└── ...
```
## 1. apps folder
The purpose of the apps is to act as an application container which call our business logic in `libs` folder.


## 2. docker folder
Containing all docker related file and wp plugin. During boot up via `docker-compose up -d`, it will expose phpmyadmin at `http://localhost:8081`. Below is our local development mysql credential
```
host: mysql
user: user
password: password
database: verifsystem
````

## 3. libs folder
All business logic code related to apps is hosted here. Meaning this libs is shareable among apps **backend**, **dashboard**, **payment** and **veriff**.


# Development
the first step is to install all nodejs dependencies and docker container. `cd` into root folder and run `yarn install` and `docker-compose up -d` command. This command will install all required docker and nodejs packages for our codebase.

## 1. Backend, Payment & Veriff
To start **backend**, **payment** or ** veriff** development, copy `.env.example` file, paste it in the same directory and rename it to `.env`. This file contain all configuration for backend to run. Below is the sample of `.env` file.
```
PORT=3000
NODE_ENV=development

WPAPI_USER=admin
WPAPI_BASEURL=https://wp.selfiepay.ngrok.io
WPAPI_KEY=RMEX vxUE EjlI hvDF SkTb cQXe

WOO_COMSUMER_KEY=ck_830893816f754c2b745c79545a35082fff456529
WOO_COMSUMER_SECRET=cs_fcf6a570005d37b3e804a42aaf151d79b4bb37ff

MAILGUN_KEY=YXBpOjBiNzEzZjkwM2YxMmM1ZWZkN2JjNzJkMGNiOWFmYmFlLTA2Nzc1MTdmLWVlZTJhZjU4
MAILGUN_URL=https://api.mailgun.net/v3/mg.verifsystem.com/messages

MYSQL_PORT=3306
MYSQL_HOST=0.0.0.0
MYSQL_USER=user
MYSQL_PASS=password
MYSQL_DB=verifsystem

JWT_SECRET_KEY=loremipsumdolor
CORS_ALLOW_ORIGIN=http://localhost:4200,http://localhost:4300
```
run `yarn start:{backend|veriff|payment}` command. This command will boot up backend and expose swagger API documentation at `http://localhost:3000/docs`, `http://localhost:3001/docs` and `http://localhost:3002/docs` respectively.

## 2. Dashboard
To start dashboard(adminp portal) development, run `yarn start:dashboard` command. Then visit `http://localhost:4200`.
