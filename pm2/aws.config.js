const BASE_ENV = {
  NODE_ENV: 'staging',
  MAILGUN_KEY: 'YXBpOjBiNzEzZjkwM2YxMmM1ZWZkN2JjNzJkMGNiOWFmYmFlLTA2Nzc1MTdmLWVlZTJhZjU4',
  MAILGUN_URL: 'https://api.mailgun.net/v3/mg.verifsystem.com/messages',
  MYSQL_PORT: 3306,
  MYSQL_HOST: 'verifsystem-prod.cmmi16rqt9rg.us-east-2.rds.amazonaws.com',
  MYSQL_USER: 'root',
  MYSQL_PASS: 't96buarfkffbbqpj$',
  MYSQL_DB: 'verifSystem',
  REDIS_HOST: '127.0.0.1',
  REDIS_PORT: 6379,
  JWT_SECRET_KEY: 'Gk207iXzRuSOIlxYY04bEGfqJQj70Qql',
  CORS_ALLOW_ORIGIN: 'https://app.verifsystem.com'
};

const BACKEND_APP = {
  name: 'backend',
  namespace: 'service',
  script: './dist/apps/backend/main.js',
  autorestart: true,
  restart_delay: 5000,
  watch: true,
  watch_delay: 5000,
  ignore_watch: ['coverage', '.vscode', 'docker', 'node_modules'],
  exec_mode: 'fork',
  env: {
    ...BASE_ENV,
    PORT: 3000
  },
};

const PAYMENT_APP = {
  name: 'payment',
  namespace: 'service',
  script: './dist/apps/payment/main.js',
  autorestart: true,
  restart_delay: 5000,
  watch: true,
  watch_delay: 5000,
  ignore_watch: ['coverage', '.vscode', 'docker', 'node_modules'],
  exec_mode: 'fork',
  env: {
    ...BASE_ENV,
    PORT: 3001
  },
};

const VERIFF_APP = {
  name: 'veriff',
  namespace: 'service',
  script: './dist/apps/veriff/main.js',
  autorestart: true,
  restart_delay: 5000,
  watch: true,
  watch_delay: 5000,
  ignore_watch: ['coverage', '.vscode', 'docker', 'node_modules'],
  exec_mode: 'fork',
  env: {
    ...BASE_ENV,
    PORT: 3002
  },
};

const extra = {};

module.exports = {
  apps: [BACKEND_APP, PAYMENT_APP, VERIFF_APP],
  extra,
};
