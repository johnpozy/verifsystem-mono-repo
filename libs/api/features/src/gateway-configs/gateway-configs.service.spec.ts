import { Test, TestingModule } from '@nestjs/testing';
import { GatewayConfigsService } from './gateway-configs.service';

describe('GatewayConfigsService', () => {
  let service: GatewayConfigsService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [GatewayConfigsService],
    }).compile();

    service = module.get<GatewayConfigsService>(GatewayConfigsService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
