import { Column, Entity, ManyToOne, OneToMany, PrimaryGeneratedColumn } from 'typeorm';

import { EGatewayConfigStatus, EGatewayConfigType, IGatewayConfig } from '@verifsystem/shared/data-access';

import { CardGatewayConfig } from '../cards/card-gateway-config.entity';
import { Processor } from '../processors/processor.entity';
import { Transaction } from '../transactions/transaction.entity';

@Entity()
export class GatewayConfig implements IGatewayConfig {
  @PrimaryGeneratedColumn('increment')
  id: number;

  @Column({
    type: 'enum',
    default: EGatewayConfigType.STRIPE,
    enum: EGatewayConfigType,
  })
  type: EGatewayConfigType;

  @Column({ type: 'tinytext' })
  name: string;

  @Column({ type: 'tinytext' })
  apiKey: string;

  @Column({
    type: 'bigint',
    default: 0,
  })
  maxDailySessions: number;

  @Column({
    type: 'bigint',
    default: 0,
  })
  maxDailyTxnNumber: number;

  @Column({
    type: 'bigint',
    default: 0,
  })
  maxDailyTxnAmount: number;

  @Column({
    type: 'bigint',
    default: 0,
  })
  maxMonthlyTxnNumber: number;

  @Column({
    type: 'bigint',
    default: 0,
  })
  maxMonthlyTxnAmount: number;

  @Column({
    type: 'timestamp',
    default: () => 'CURRENT_TIMESTAMP',
  })
  createdAt: Date;

  @Column({
    type: 'enum',
    default: EGatewayConfigStatus.ACTIVATED,
    enum: EGatewayConfigStatus,
  })
  status: EGatewayConfigStatus;

  @OneToMany(() => CardGatewayConfig, (cardGatewayConfig) => cardGatewayConfig.card, {
    onDelete: 'CASCADE'
  })
  cardGatewayConfigs: CardGatewayConfig[];

  @ManyToOne(() => Processor, (processor) => processor.gatewayConfigs, {
    onDelete: 'CASCADE'
  })
  processor: Processor;

  @OneToMany(() => Transaction, (transaction) => transaction.gatewayConfig, {
    onDelete: 'CASCADE'
  })
  transactions: Transaction[];
}
