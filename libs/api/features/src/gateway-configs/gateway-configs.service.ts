import { Injectable, Logger } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { InjectRepository } from '@nestjs/typeorm';
import { HttpService } from '@nestjs/axios';
import { Repository } from 'typeorm';
import { Stripe } from 'stripe';
import * as moment from 'moment';
import * as FormData from 'form-data';
import { catchError, firstValueFrom, tap, throwError } from 'rxjs';
import * as WPAPI from 'wpapi';

import { decryptCard } from '@verifsystem/api/utils';

import { UserVeriff } from '../users/user-veriff.entity';
import { GatewayConfig } from './gateway-config.entity';
import { Card } from '../cards/card.entity';
import { CardGatewayConfig } from '../cards/card-gateway-config.entity';
import { Ecomm } from '../ecomms/ecomm.entity';

import { UsersService } from '../users/users.service';

import { GatewayConfigVeriffDecisionDto } from '../gateway-configs/dtos/gateway-config-veriff-decision.dto';
import { GatewayConfigVeriffEventDto } from '../gateway-configs/dtos/gateway-config-veriff-event.dto';
import { GatewayConfigUpdateDto } from '../gateway-configs/dtos/gateway-config-update.dto';
import { CardChargeDto } from '../cards/dtos/card-charge.dto';
import { UserVeriffCreateDto } from '../users/dtos/user-veriff-create.dto';

@Injectable()
export class GatewayConfigsService {
  constructor(
    @InjectRepository(GatewayConfig) private gatewayConfigRepository: Repository<GatewayConfig>,
    @InjectRepository(Card) private cardRepository: Repository<Card>,
    @InjectRepository(CardGatewayConfig) private cardGatewayConfigRepository: Repository<CardGatewayConfig>,
    @InjectRepository(Ecomm) private ecommRepository: Repository<Ecomm>,
    private httpService: HttpService,
    private configService: ConfigService,
    private usersService: UsersService
  ) {}

  async getGatewayConfigById(gatewayConfigId): Promise<GatewayConfig> {
    return await this.gatewayConfigRepository.findOneBy({ id: gatewayConfigId });
  }

  async update(gatewayConfigUpdateDto: GatewayConfigUpdateDto, gatewayConfigId: number): Promise<GatewayConfig> {
    await this.gatewayConfigRepository.update(gatewayConfigId, gatewayConfigUpdateDto);

    const gatewayConfig = await this.gatewayConfigRepository.findOneBy({ id: gatewayConfigId });

    return gatewayConfig;
  }

  async delete(gatewayConfigId: number): Promise<GatewayConfig> {
    const gatewayConfig = await this.gatewayConfigRepository.findOneBy({ id: gatewayConfigId });

    await this.gatewayConfigRepository.delete(gatewayConfigId);

    return gatewayConfig;
  }

  async veriffEvent(gatewayConfigVeriffEventDto: GatewayConfigVeriffEventDto): Promise<UserVeriff> {
    const userVeriffCreateDto = new UserVeriffCreateDto();
    const { externalId, ecommId } = gatewayConfigVeriffEventDto.vendorData;
    const user = await this.usersService.findEcommUser(externalId, +ecommId);

    userVeriffCreateDto.sessionId = gatewayConfigVeriffEventDto.id;
    userVeriffCreateDto.documentType = null;
    userVeriffCreateDto.decision = gatewayConfigVeriffEventDto.action;

    return this.usersService.createUserVeriff(userVeriffCreateDto, user);
  }

  async veriffDecision(processorVeriffDecisionDto: GatewayConfigVeriffDecisionDto): Promise<UserVeriff> {
    const userVeriffCreateDto = new UserVeriffCreateDto();
    const { document, vendorData, decisionTime, code } = processorVeriffDecisionDto.verification;
    const { externalId, ecommId } = vendorData;
    const mailerData = new FormData();
    const ecomm = await this.ecommRepository
      .createQueryBuilder('ecomm')
      .leftJoinAndSelect('ecomm.configs', 'ecommConfigs')
      .where('ecomm.id = :ecommId', { ecommId: +ecommId })
      .getOne();
    const user = await this.usersService.findEcommUser(externalId, +ecommId);

    userVeriffCreateDto.sessionId = processorVeriffDecisionDto.verification.id;
    userVeriffCreateDto.documentType = processorVeriffDecisionDto.verification.document.type;
    userVeriffCreateDto.decision = processorVeriffDecisionDto.verification.status;

    // Update ecomm customer status
    if (ecomm.configs && ecomm.configs.length > 0) {
      let wpUserRole = 'customer';

      switch (code) {
        case 9001:
          wpUserRole = 'approved_customer';
          break;

        case 9102:
          wpUserRole = 'declined_customer';
          break;

        default:
          wpUserRole = 'customer';
          break;
      }
      const wp = new WPAPI({
        endpoint: `${ecomm.configs[0].wordpress.apiBaseUrl}/wp-json`,
        username: ecomm.configs[0].wordpress.apiUser,
        password: ecomm.configs[0].wordpress.apiKey,
      });

      wp.users().id(+externalId).update({
        roles: [wpUserRole]
      })
      .catch(err => Logger.log(err, 'ERROR'))
    }

    mailerData.append('from', 'Verif System <sessions@verifsystem.com>');
    mailerData.append('to', `${user.firstName} ${user.lastName} <${user.email}>`);
    mailerData.append('subject', `Verif System - ${userVeriffCreateDto.decision}`);
    mailerData.append('template', `${userVeriffCreateDto.decision}-veriff`);
    mailerData.append(
      'h:X-Mailgun-Variables',
      JSON.stringify({
        documentType: document.type,
        decisionTime: moment(decisionTime).format('MMM DD, YYYY, @ hh:mm UTC'),
        websiteName: ecomm.title,
      })
    );

    firstValueFrom(
      this.httpService.post(this.configService.get('MAILGUN_URL'), mailerData, {
        headers: {
          Authorization: `Basic ${this.configService.get('MAILGUN_KEY')}`,
          ...mailerData.getHeaders(),
        },
      }).pipe(
        catchError(err => throwError(() => new Error(err?.response?.data?.message))),
        tap(() => Logger.log(`Email send to ${user.email}`, 'INFO'))
      )
    ).catch(err => Logger.log(err, 'ERROR'))

    return this.usersService.createUserVeriff(userVeriffCreateDto, user);
  }

  async stripeCharge(cardChargeDto: CardChargeDto): Promise<any> {
    const { number: cardNumber, cvc: cardCvc } = decryptCard(cardChargeDto.card.cardHash);
    const stripe = new Stripe(cardChargeDto.gateway.apiKey, { apiVersion: '2020-08-27' });
    const customers = await stripe.customers.search({ query: `email:'${cardChargeDto.user.email}'` });
    const card = await this.cardRepository.findOneBy({ id: cardChargeDto.card.id });
    const gatewayConfig = await this.gatewayConfigRepository.findOneBy({ id: cardChargeDto.gateway.id });

    let customer = customers.data[0];

    if (customers.data.length === 0) {
      const token = await stripe.tokens.create({
        card: {
          number: cardNumber,
          cvc: cardCvc.toString(),
          exp_month: cardChargeDto.card.expMonth.toString(),
          exp_year: cardChargeDto.card.expYear.toString(),
          name: cardChargeDto.card.cardHolderName,
          address_city: cardChargeDto.card.billingAddress.city,
          address_country: cardChargeDto.card.billingAddress.country,
          address_line1: cardChargeDto.card.billingAddress.address1,
          address_line2: cardChargeDto.card.billingAddress.address2,
          address_state: cardChargeDto.card.billingAddress.state,
          address_zip: cardChargeDto.card.billingAddress.zipCode,
        },
      });

      customer = await stripe.customers.create({
        email: cardChargeDto.user.email,
        name: cardChargeDto.user.displayName,
        phone: cardChargeDto.user.phoneNumber,
      });

      await stripe.customers.createSource(customer.id, { source: token.id });
      await this.cardGatewayConfigRepository.save({ card, gatewayConfig });
    }

    return stripe.charges.create({
      amount: cardChargeDto.amount,
      currency: cardChargeDto.currency,
      customer: customer.id,
      description: cardChargeDto.description,
    });
  }
}
