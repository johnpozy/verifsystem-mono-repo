import { HttpModule } from '@nestjs/axios';
import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { TypeOrmModule } from '@nestjs/typeorm';

import { CardGatewayConfig } from '../cards/card-gateway-config.entity';
import { Card } from '../cards/card.entity';
import { Ecomm } from '../ecomms/ecomm.entity';
import { UsersModule } from '../users/users.module';
import { GatewayConfig } from './gateway-config.entity';
import { GatewayConfigsService } from './gateway-configs.service';

@Module({
  imports: [TypeOrmModule.forFeature([GatewayConfig, Card, CardGatewayConfig, Ecomm]), HttpModule, ConfigModule, UsersModule],
  providers: [GatewayConfigsService],
  exports: [GatewayConfigsService],
})
export class GatewayConfigsModule {}
