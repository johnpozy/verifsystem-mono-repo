import { ApiProperty } from '@nestjs/swagger';

import { IGatewayConfigVeriffVendordata } from '@verifsystem/shared/data-access';

export class GatewayConfigVeriffVendordataDto implements IGatewayConfigVeriffVendordata {
  @ApiProperty()
  ecommId: string;

  @ApiProperty()
  externalId: string;
}
