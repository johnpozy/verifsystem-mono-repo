import { ApiProperty } from '@nestjs/swagger';

import { IGatewayConfigVeriffDocument } from '@verifsystem/shared/data-access';

export class GatewayConfigVeriffDocumentDto implements IGatewayConfigVeriffDocument {
  @ApiProperty()
  number: number;

  @ApiProperty()
  type: string;

  @ApiProperty()
  country: string;

  @ApiProperty()
  validFrom: Date;

  @ApiProperty()
  validUntil: Date;
}
