import { ApiProperty } from '@nestjs/swagger';

import { IGatewayConfigVeriffPerson } from '@verifsystem/shared/data-access';

export class GatewayConfigVeriffPersonDto implements IGatewayConfigVeriffPerson {
  @ApiProperty()
  firstName: string;

  @ApiProperty()
  lastName: string;

  @ApiProperty()
  citizenship: string;

  @ApiProperty()
  idNumber: number;

  @ApiProperty()
  gender: string;

  @ApiProperty()
  dateOfBirth: Date;

  @ApiProperty()
  yearOfBirth: number;

  @ApiProperty()
  placeOfBirth: string;

  @ApiProperty()
  nationality: string;

  @ApiProperty()
  pepSanctionMatch: any;
}
