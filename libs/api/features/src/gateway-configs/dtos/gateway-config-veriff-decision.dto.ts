import { ApiProperty } from '@nestjs/swagger';

import { IGatewayConfigVeriffDecision, IGatewayConfigVeriffVerification } from '@verifsystem/shared/data-access';

import { GatewayConfigVeriffVerificationDto } from './gateway-config-veriff-verification.dto';

export class GatewayConfigVeriffDecisionDto implements IGatewayConfigVeriffDecision {
  @ApiProperty()
  status: string;

  @ApiProperty({ type: GatewayConfigVeriffVerificationDto })
  verification: IGatewayConfigVeriffVerification;

  @ApiProperty()
  technicalData: object;
}
