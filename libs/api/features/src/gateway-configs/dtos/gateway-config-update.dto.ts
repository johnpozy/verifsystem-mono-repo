import { ApiProperty } from '@nestjs/swagger';

import { IGatewayConfig, EGatewayConfigStatus, EGatewayConfigType } from '@verifsystem/shared/data-access';

export class GatewayConfigUpdateDto implements IGatewayConfig {
  id: number;

  @ApiProperty({ enum: EGatewayConfigType, enumName: 'GatewayConfigType' })
  type: EGatewayConfigType;

  @ApiProperty()
  name: string;

  @ApiProperty()
  apiKey: string;

  @ApiProperty()
  maxDailySessions: number;

  @ApiProperty()
  maxDailyTxnNumber: number;

  @ApiProperty()
  maxDailyTxnAmount: number;

  @ApiProperty()
  maxMonthlyTxnNumber: number;

  @ApiProperty()
  maxMonthlyTxnAmount: number;

  createdAt: Date;

  @ApiProperty({ enum: EGatewayConfigStatus, enumName: 'GatewayConfigStatus' })
  status: EGatewayConfigStatus;
}
