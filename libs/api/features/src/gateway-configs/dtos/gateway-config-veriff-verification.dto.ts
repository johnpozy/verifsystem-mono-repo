import { ApiProperty } from '@nestjs/swagger';

import {
  IGatewayConfigVeriffDocument,
  IGatewayConfigVeriffPerson,
  IGatewayConfigVeriffVendordata,
  IGatewayConfigVeriffVerification,
} from '@verifsystem/shared/data-access';

import { GatewayConfigVeriffDocumentDto } from './gateway-config-veriff-document.dto';
import { GatewayConfigVeriffPersonDto } from './gateway-config-veriff-person.dto';
import { GatewayConfigVeriffVendordataDto } from './gateway-config-veriff-vendordata.dto';

export class GatewayConfigVeriffVerificationDto implements IGatewayConfigVeriffVerification {
  @ApiProperty()
  acceptanceTime: Date;

  @ApiProperty()
  decisionTime: Date;

  @ApiProperty()
  code: number;

  @ApiProperty()
  id: string;

  @ApiProperty({ type: GatewayConfigVeriffVendordataDto })
  vendorData: IGatewayConfigVeriffVendordata;

  @ApiProperty()
  status: string;

  @ApiProperty()
  reason: string;

  @ApiProperty()
  reasonCode: number;

  @ApiProperty({ type: GatewayConfigVeriffPersonDto })
  person: IGatewayConfigVeriffPerson;

  @ApiProperty({ type: GatewayConfigVeriffDocumentDto })
  document: IGatewayConfigVeriffDocument;

  @ApiProperty()
  comments: string[];

  @ApiProperty()
  additionalVerifiedData: object;
}
