import { ApiProperty } from '@nestjs/swagger';

import { IGatewayConfigVeriffVendordata, IGatewayConfigVeriffEvent } from '@verifsystem/shared/data-access';

import { GatewayConfigVeriffVendordataDto } from './gateway-config-veriff-vendordata.dto';

export class GatewayConfigVeriffEventDto implements IGatewayConfigVeriffEvent {
  @ApiProperty()
  id: string;

  @ApiProperty()
  attemptId: string;

  @ApiProperty()
  feature: string;

  @ApiProperty()
  code: number;

  @ApiProperty()
  action: string;

  @ApiProperty({ type: GatewayConfigVeriffVendordataDto })
  vendorData: IGatewayConfigVeriffVendordata;
}
