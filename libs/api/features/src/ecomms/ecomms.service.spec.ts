import { Test, TestingModule } from '@nestjs/testing';
import { EcommsService } from './ecomms.service';

describe('EcommsService', () => {
  let service: EcommsService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [EcommsService],
    }).compile();

    service = module.get<EcommsService>(EcommsService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
