import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { BullModule } from '@nestjs/bull';

import { EcommsService } from './ecomms.service';
import { EcommsProcessor } from './ecomms.processor';

import { Ecomm as EcommEntity } from './ecomm.entity';
import { EcommProcessor as EcommProcessorEntity } from './ecomm-processor.entity';
import { EcommConfig as EcommConfigEntity } from './ecomm-config.entity';
import { GatewayConfig as GatewayConfigEntity } from '../gateway-configs/gateway-config.entity';
import { Processor as ProcessorEntity } from '../processors/processor.entity';
import { UserEcomm as UserEcommEntity } from '../users/user-ecomm.entity';
import { Transaction as TransactionEntity } from '../transactions/transaction.entity';
import { User as UserEntity } from '../users/user.entity';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      EcommEntity,
      EcommProcessorEntity,
      EcommConfigEntity,
      GatewayConfigEntity,
      ProcessorEntity,
      UserEntity,
      UserEcommEntity,
      TransactionEntity,
    ]),
    BullModule.registerQueue({
      name: 'ecomms',
    }, {
      redis: {
        host: process.env.REDIS_HOST || 'localhost',
        port: +process.env.REDIS_PORT || 6379
      }
    }),
  ],
  providers: [EcommsService, EcommsProcessor],
  exports: [EcommsService, EcommsProcessor],
})
export class EcommsModule {}
