import { Column, Entity, JoinColumn, OneToMany, OneToOne, PrimaryGeneratedColumn } from 'typeorm';

import { IEcomm, IEcommContact, EEcommStatus } from '@verifsystem/shared/data-access';

import { PayoutAccount } from '../payouts/payout-account.entity';
import { UserEcomm } from '../users/user-ecomm.entity';
import { EcommProcessor } from './ecomm-processor.entity';
import { Transaction } from '../transactions/transaction.entity';
import { EcommConfig } from './ecomm-config.entity';

@Entity()
export class Ecomm implements IEcomm {
  @PrimaryGeneratedColumn('increment')
  id: number;

  @Column({ type: 'tinytext' })
  title: string;

  @Column({ unique: true })
  token: string;

  @Column({
    type: 'longtext',
    nullable: true,
  })
  description: string;

  @Column({ type: 'tinytext' })
  baseUrl: string;

  @Column({ type: 'json' })
  contact: IEcommContact;

  @Column({
    type: 'timestamp',
    default: () => 'CURRENT_TIMESTAMP',
  })
  createdAt: Date;

  @Column({
    type: 'enum',
    default: EEcommStatus.ACTIVATED,
    enum: EEcommStatus,
  })
  status: EEcommStatus;

  @OneToMany(() => UserEcomm, (userEcomm) => userEcomm.ecomm, {
    onDelete: 'CASCADE'
  })
  users: UserEcomm[];

  @OneToMany(() => PayoutAccount, (payout) => payout.ecomm, {
    onDelete: 'CASCADE'
  })
  payouts: PayoutAccount[];

  @OneToMany(() => Transaction, (transaction) => transaction.ecomm, {
    onDelete: 'CASCADE'
  })
  transactions: Transaction[];

  @OneToMany(() => EcommProcessor, (ecommProcessor) => ecommProcessor.ecomm)
  ecommProcessors!: EcommProcessor[];

  @OneToMany(() => EcommConfig, (ecommConfig) => ecommConfig.ecomm, {
    onDelete: 'CASCADE'
  })
  configs: EcommConfig[];
}
