import { ApiProperty } from '@nestjs/swagger';

import { IEcommConfig, IEcommConfigWoocommerce, IEcommConfigWordpress } from '@verifsystem/shared/data-access';

import { EcommConfigWoocommerceCreateDto } from './ecomm-config-woocommerce-create.dto';
import { EcommConfigWordpressCreateDto } from './ecomm-config-wordpress-create.dto';


export class EcommConfigCreateDto implements IEcommConfig {
  id: number;

  @ApiProperty({ type: EcommConfigWordpressCreateDto })
  wordpress: IEcommConfigWordpress;

  @ApiProperty({ type: EcommConfigWoocommerceCreateDto })
  woocommerce: IEcommConfigWoocommerce;
}
