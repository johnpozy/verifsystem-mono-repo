import { ApiProperty } from '@nestjs/swagger';

import { IEcommContact } from '@verifsystem/shared/data-access';

export class EcommContactCreateDto implements IEcommContact {
  @ApiProperty()
  name: string;

  @ApiProperty()
  phoneNumber: string;
}
