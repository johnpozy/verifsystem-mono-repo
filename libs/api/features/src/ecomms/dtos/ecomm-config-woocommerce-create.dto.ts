import { ApiProperty } from '@nestjs/swagger';

import { IEcommConfigWoocommerce } from '@verifsystem/shared/data-access';

export class EcommConfigWoocommerceCreateDto implements IEcommConfigWoocommerce {
  @ApiProperty()
  consumerKey: string;

  @ApiProperty()
  consumerSecret: string;
}
