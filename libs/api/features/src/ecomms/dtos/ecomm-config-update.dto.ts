import { EcommConfigCreateDto } from './ecomm-config-create.dto';

export class EcommConfigUpdateDto extends EcommConfigCreateDto {}
