import { ApiProperty } from '@nestjs/swagger';

import { IEcommConfigWordpress } from '@verifsystem/shared/data-access';

export class EcommConfigWordpressCreateDto implements IEcommConfigWordpress {
  @ApiProperty()
  apiUser: string;

  @ApiProperty()
  apiBaseUrl: string;

  @ApiProperty()
  apiKey: string;
}
