import { ApiProperty } from '@nestjs/swagger';

import { IEcommContact, IEcommProcessor, IEcomm, EEcommStatus } from '@verifsystem/shared/data-access';

import { EcommContactCreateDto } from './ecomm-contact-create.dto';

export class EcommCreateDto implements IEcomm {
  id: number;

  @ApiProperty()
  title: string;

  token: string;

  @ApiProperty()
  description: string;

  @ApiProperty()
  baseUrl: string;

  @ApiProperty({ type: EcommContactCreateDto })
  contact: IEcommContact;

  createdAt: Date;

  @ApiProperty({ enum: EEcommStatus, enumName: 'EcommStatus' })
  status: EEcommStatus;

  processors: IEcommProcessor[];
}
