import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { InjectQueue } from '@nestjs/bull';
import { Queue } from 'bull';
import { Not, Repository } from 'typeorm';
import { nanoid } from 'nanoid';
import { startOfMonth, endOfMonth } from 'date-fns';

import { EEcommStatus, EProcessorType, EUserRole, EUserEcommStatus, EGatewayConfigStatus, EProcessorStatus } from '@verifsystem/shared/data-access';

import { Ecomm } from './ecomm.entity';
import { Processor } from '../processors/processor.entity';
import { EcommProcessor } from './ecomm-processor.entity';
import { UserEcomm } from '../users/user-ecomm.entity';
import { Transaction } from '../transactions/transaction.entity';
import { GatewayConfig } from '../gateway-configs/gateway-config.entity';
import { EcommCreateDto } from './dtos/ecomm-create.dto';
import { ProcessorCreateDto } from '../processors/dtos/processor-create.dto';
import { EcommConfigCreateDto } from './dtos/ecomm-config-create.dto';
import { EcommConfig } from './ecomm-config.entity';
import { EcommConfigUpdateDto } from './dtos/ecomm-config-update.dto';
import { paginate, Paginated, PaginateQuery } from 'nestjs-paginate';

@Injectable()
export class EcommsService {
  constructor(
    @InjectRepository(Ecomm) private ecommRepository: Repository<Ecomm>,
    @InjectRepository(UserEcomm) private userEcommRepository: Repository<UserEcomm>,
    @InjectRepository(Processor) private processorRepository: Repository<Processor>,
    @InjectRepository(EcommProcessor) private ecommProcessorRepository: Repository<EcommProcessor>,
    @InjectRepository(EcommConfig) private ecommConfigRepository: Repository<EcommConfig>,
    @InjectRepository(GatewayConfig) private gatewayConfigRepository: Repository<GatewayConfig>,
    @InjectRepository(Transaction) private transactionRepository: Repository<Transaction>,
    @InjectQueue('ecomms') private readonly ecommsQueue: Queue,
  ) {}

  async getEcommById(ecommId: number): Promise<Ecomm> {
    const getEcommQuery = this.ecommRepository
      .createQueryBuilder('ecomm')
      .leftJoinAndSelect('ecomm.configs', 'ecommConfigs')
      .leftJoinAndSelect('ecomm.ecommProcessors', 'ecommProcessors')
      .where('ecomm.id = :ecommId', { ecommId });

    return getEcommQuery.getOne();
  }

  async create(ecommCreateDto: EcommCreateDto): Promise<Ecomm> {
    const ecomm = new Ecomm();

    ecomm.baseUrl = ecommCreateDto.baseUrl;
    ecomm.contact = ecommCreateDto.contact;
    ecomm.description = ecommCreateDto.description;
    ecomm.title = ecommCreateDto.title;
    ecomm.token = nanoid(48);

    const result = await this.ecommRepository.save(ecomm);

    return result;
  }

  async createProcessor(processorCreateDto: ProcessorCreateDto, ecommId: number): Promise<Processor> {
    const processorEntity = new Processor();
    const ecommProcessorEntity = new EcommProcessor();
    const ecomm = await this.ecommRepository.findOneBy({ id: ecommId });

    processorEntity.baseUrl = processorCreateDto.baseUrl;
    processorEntity.description = processorCreateDto.description;
    processorEntity.title = processorCreateDto.title;
    processorEntity.type = processorCreateDto.type;

    const processor = await this.processorRepository.save(processorEntity);

    ecommProcessorEntity.ecomm = ecomm;
    ecommProcessorEntity.processor = processor;

    await this.ecommProcessorRepository.save(ecommProcessorEntity);

    return processor;
  }

  async pickProcessor(ecommId: number, processorType: EProcessorType): Promise<GatewayConfig> {
    const currentDate = new Date();
    const gatewayConfigIds = await this.ecommProcessorRepository
      .createQueryBuilder('ecommProcessor')
      .leftJoinAndSelect('ecommProcessor.processor', 'processor')
      .leftJoinAndSelect('processor.gatewayConfigs', 'gatewayConfigs')
      .where('ecommProcessor.ecommId = :ecommId', { ecommId })
      .andWhere('processor.status = :processorStatus', { processorStatus: EProcessorStatus.ACTIVATED })
      .andWhere('processor.type = :processorType', { processorType })
      .select('gatewayConfigs.id as id')
      .groupBy('gatewayConfigs.id')
      .getRawMany();

    const aggTransactions = await this.transactionRepository
      .createQueryBuilder('transaction')
      .where('transaction.createdAt >= :startDate AND transaction.createdAt <= :endDate', {
        startDate: startOfMonth(currentDate),
        endDate: endOfMonth(currentDate),
      })
      .leftJoinAndSelect('transaction.gatewayConfig', 'gatewayConfig')
      .select('*')
      .addSelect('count(*)', 'count')
      .addSelect('SUM(transaction.amount)', 'sum')
      .andWhere('gatewayConfig.id IN(:gatewayConfigIds)', { gatewayConfigIds: gatewayConfigIds.map((g) => g.id) })
      .andWhere('gatewayConfig.status = :status', { status: EGatewayConfigStatus.ACTIVATED })
      .groupBy('gatewayConfig.id')
      .getRawMany();

    if (aggTransactions.length === 0) {
      return await this.gatewayConfigRepository
        .createQueryBuilder('gatewayConfig')
        .leftJoinAndSelect('gatewayConfig.processor', 'processor')
        .where('gatewayConfig.id IN(:gatewayConfigIds)', { gatewayConfigIds: gatewayConfigIds.map((g) => g.id) })
        .andWhere(`processor.status = :status`, { status: EProcessorStatus.ACTIVATED })
        .orderBy('RAND()')
        .getOne();
    }

    const filteredGateways = aggTransactions.filter((t) => +t.sum <= t.maxMonthlyTxnAmount);
    const minGateway = filteredGateways.length > 0 ? filteredGateways.reduce((prev, curr) => (+prev.sum < +curr.sum ? prev : curr)) : null;

    return this.gatewayConfigRepository.findOne({
      relations: {
        processor: true,
      },
      where: {
        id: minGateway?.gatewayConfigId ? minGateway?.gatewayConfigId : 999999999,
      },
    });
  }

  async getEcomms(): Promise<Ecomm[]> {
    return await this.ecommRepository.find();
  }

  async getCustomers(ecommId: number ): Promise<UserEcomm[]> {
    return this.userEcommRepository.find({
      where: {
        ecomm: {
          id: ecommId,
        },
        user: {
          role: EUserRole.CUSTOMER,
        },
        status: Not(EUserEcommStatus.ARCHIVED),
      },
      relations: {
        ecomm: true,
        user: {
          ecomms: true,
          veriffs: true,
        },
      },
    });
  }

  async getProcessors(): Promise<EcommProcessor[]> {
    return this.ecommProcessorRepository.find({
      relations: {
        processor: true,
        ecomm: true,
      },
    });
  }

  async getEcommProcessors(ecommId: number): Promise<EcommProcessor[]> {
    return this.ecommProcessorRepository.find({
      where: {
        ecomm: {
          id: ecommId,
        },
      },
      relations: {
        processor: true,
        ecomm: true,
      },
    });
  }

  async getTransactions(ecommId: number): Promise<Transaction[]> {
    return this.transactionRepository.find({
      where: {
        user: {
          ecomms: {
            ecomm: {
              id: ecommId,
            },
          },
        },
      },
      relations: {
        user: true,
        gatewayConfig: true,
      },
    });
  }

  createConfig = async (body: EcommConfigCreateDto, ecommId: number): Promise<EcommConfig> => {
    let ecommConfigId = null;
    const ecomm = await this.ecommRepository
      .createQueryBuilder('ecomm')
      .leftJoinAndSelect('ecomm.configs', 'ecommConfigs')
      .where('ecomm.id = :ecommId', { ecommId })
      .getOne();

    if (ecomm.configs && ecomm.configs.length > 0) {
      await this.ecommConfigRepository
        .createQueryBuilder('ecommConfig')
        .where('id = :ecommConfigId', { ecommConfigId: ecomm.configs[0].id })
        .update()
        .set({
          ...body,
          ecomm,
        })
        .execute();

        ecommConfigId = ecomm.configs[0].id;
    } else {
      const insertQuery = await this.ecommConfigRepository
        .createQueryBuilder('ecommConfig')
        .insert()
        .values({
          ...body,
          ecomm,
        })
        .execute();

        ecommConfigId = insertQuery.raw.insertId;
    }

    return this.ecommConfigRepository
      .createQueryBuilder('ecommConfig')
      .where('id = :ecommConfigId', { ecommConfigId })
      .getOne();
  }

  updateConfig = async (body: EcommConfigUpdateDto, ecommId: number, ecommConfigId: number): Promise<EcommConfig> => {
    await this.ecommConfigRepository
      .createQueryBuilder('ecommConfig')
      .where('id = :ecommConfigId', { ecommConfigId })
      .update()
      .set(body)
      .execute();

    return this.ecommConfigRepository
      .createQueryBuilder('ecommConfig')
      .where('ecommConfig.id = :ecommConfigId', { ecommConfigId })
      .getOne();
  }

  getConfigs = async (ecommId: number): Promise<EcommConfig[]> => {
    return this.ecommConfigRepository
      .createQueryBuilder('ecommConfig')
      .where('ecommId = :ecommId', { ecommId })
      .getMany();
  }

  verify(token: string): Promise<Ecomm> {
    return this.ecommRepository
      .createQueryBuilder('ecomm')
      .where('ecomm.token = :token', { token })
      .andWhere('ecomm.status = :status', { status: EEcommStatus.ACTIVATED })
      .getOne();
  }

  getExternalEcommUsers = async (ecommId: number): Promise<any> => {
    await this.ecommsQueue.add('get-external-ecomm-users', { ecommId }, {
      removeOnComplete: true
    });
  }

  saveExternalEcommUsers = async (ecommId: number, users: Array<any>): Promise<any> => {
    await this.ecommsQueue.add('save-external-ecomm-users', { ecommId, users }, {
      removeOnComplete: true
    });
  }
}
