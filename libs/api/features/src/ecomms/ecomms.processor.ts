import { OnQueueActive, OnQueueCompleted, OnQueueFailed, OnQueueProgress, Process, Processor } from '@nestjs/bull';
import { InjectRepository } from '@nestjs/typeorm';
import {  WebSocketGateway, WebSocketServer } from '@nestjs/websockets';
import { Server } from 'socket.io';
import { Job } from 'bull';
import { Repository } from 'typeorm';
import * as WPAPI from 'wpapi';

import { EUserRole } from '@verifsystem/shared/data-access';

import { Ecomm } from './ecomm.entity';
import { UserEcomm } from '../users/user-ecomm.entity';
import { User } from '../users/user.entity';
import { paginate } from '@verifsystem/api/utils';

@Processor('ecomms')
@WebSocketGateway({ cors: { origin: '*' }})
export class EcommsProcessor {
  @WebSocketServer() server: Server;

  constructor(
    @InjectRepository(Ecomm) private ecommRepository: Repository<Ecomm>,
    @InjectRepository(UserEcomm) private userEcommRepository: Repository<UserEcomm>,
    @InjectRepository(User) private userRepository: Repository<User>,
  ) {}

  @Process('get-external-ecomm-users')
  async getExternalEcommUsers(job: Job) {
    const ecommId = +job.data.ecommId;
    const wpUsers = [];
    const ecomm = await this.ecommRepository
      .createQueryBuilder('ecomm')
      .leftJoinAndSelect('ecomm.configs', 'ecommConfigs')
      .leftJoinAndSelect('ecomm.users', 'ecommUsers')
      .where('ecomm.id = :ecommId', { ecommId })
      .getOne();

    const wp = new WPAPI({
      endpoint: `${ecomm.baseUrl}/wp-json`,
      username: ecomm.configs[0].wordpress.apiUser,
      password: ecomm.configs[0].wordpress.apiKey,
    });

    const wpUsersQuery = wp.users()
      .context('edit')
      .param('roles', [EUserRole.CUSTOMER, EUserRole.APPROVED_CUSTOMER, EUserRole.DECLINED_CUSTOMER, 'approvedcustomer'])
      .param('per_page', 100);

    // const totalPages = 100;
    const { _paging: { totalPages }} = await wpUsersQuery
      .param('_fields', 'id')
      .get()
      .catch(err => {
        throw new Error(err);
      });

    for (let index = 1; index <= totalPages; index++) {
      const progress = (100 * index) / totalPages;
      const users = await wpUsersQuery
        .param('_fields', 'id, username, name, first_name, last_name, email, nickname, roles, avatar_urls')
        .page(index + 1)
        .get()
        .catch(err => {
          throw new Error(err);
        });

      await job.progress(progress);

      wpUsers.push(...users.map(user => ({
        ...user,
        imported: ecomm.users.some(ecommUser => +ecommUser.externalId === user.id)
      })));
    }

    return wpUsers;
  }

  @OnQueueActive({name: 'get-external-ecomm-users'})
  onGetExternalEcommUserActive(job: Job) {
    this.server.emit('get-external-ecomm-users-active', { job });
  }

  @OnQueueProgress({name: 'get-external-ecomm-users'})
  onGetExternalEcommUserProgress(job: Job, progress: number) {
    this.server.emit('get-external-ecomm-users-progress', { job, progress });
  }

  @OnQueueCompleted({name: 'get-external-ecomm-users'})
  onGetExternalEcommUserCompleted(job: Job, result: any) {
    this.server.emit('get-external-ecomm-users-complete', { job, result });
  }

  @OnQueueFailed({name: 'get-external-ecomm-users'})
  onGetExternalEcommUserFailed(job: Job, error: any) {
    this.server.emit('get-external-ecomm-users-error', { job, error });
  }

  @Process('save-external-ecomm-users')
  async saveExternalEcommUsers(job: Job) {
    const ecommId = +job.data.ecommId;
    const users = job.data.users;
    const ecomm = await this.ecommRepository
      .createQueryBuilder('ecomm')
      .where('id = :ecommId', { ecommId })
      .getOne();

    const ecommUsers = await this.userEcommRepository
      .createQueryBuilder('ecommUser')
      .leftJoinAndSelect('ecommUser.user', 'user')
      .where('ecommUser.externalId IN (:...externalIds)', { externalIds: users.map(user => user.id)})
      .andWhere('ecommUser.ecommId = :ecommId', { ecommId })
      .getMany();

    if (ecommUsers.length === 0) {
      const otherEcommUsers = await this.userRepository
        .createQueryBuilder('user')
        .where('email IN (:...emails)', { emails: users.map(user => user.email)})
        .getMany();

      if (otherEcommUsers.length > 0) {
        let progress = 0;
        const paginateUsers = paginate(otherEcommUsers, 100);

        paginateUsers.forEach(async (pUsers) => {
          const userEcommEntities = pUsers.map(otherEcommUser => {
            const user = users.find(user => user.email === otherEcommUser.email);
            const userEcommEntity = new UserEcomm();

            userEcommEntity.ecomm = ecomm;
            userEcommEntity.externalId = user.id.toString();
            userEcommEntity.user = otherEcommUser;

            return userEcommEntity;
          });

          await this.userEcommRepository
            .createQueryBuilder('userEcomm')
            .insert()
            .values(userEcommEntities)
            .execute();

          progress += 1;

          await job.progress((100 * progress) / paginateUsers.length);
        });
      } else {
        let progress = 0;
        const paginateUsers = paginate(users, 100);

        paginateUsers.forEach(async (pUsers) => {
          const userEcommEntities = pUsers.map(user => {
            const userEcommEntity = new UserEcomm();
            const userEntity = new User();

            userEntity.displayName = user.nickname;
            userEntity.firstName = user.first_name;
            userEntity.lastName = user.last_name;
            userEntity.email = user.email;

            userEcommEntity.ecomm = ecomm;
            userEcommEntity.externalId = user.id.toString();
            userEcommEntity.user = userEntity;

            return userEcommEntity;
          });

          await this.userEcommRepository.save(userEcommEntities);

          progress += 1;

          await job.progress((100 * progress) / paginateUsers.length);
        });
      }
    } else {
      let progress = 0;
      const paginateUsers = paginate(ecommUsers, 100);

      paginateUsers.forEach(async (pUsers) => {
        const newUserEntities = pUsers.map(ecommUser => {
          const user = users.find(user => user.id === +ecommUser.externalId);
          const newUserEntity = new User();

          newUserEntity.id = ecommUser.user.id;
          newUserEntity.displayName = user.nickname;
          newUserEntity.firstName = user.first_name;
          newUserEntity.lastName = user.last_name;
          newUserEntity.email = user.email;

          return newUserEntity;
        });

        await this.userRepository
          .createQueryBuilder('user')
          .insert()
          .values(newUserEntities)
          .orUpdate(['displayName', 'firstName', 'lastName', 'email'])
          .execute();

        progress += 1;

        await job.progress((100 * progress) / paginateUsers.length);
      });
    }

    return true;
  }

  @OnQueueActive({name: 'save-external-ecomm-users'})
  onSaveExternalEcommUserActive(job: Job) {
    this.server.emit('save-external-ecomm-users-active', { job });
  }

  @OnQueueProgress({name: 'save-external-ecomm-users'})
  onSaveExternalEcommUserProgress(job: Job, progress: number) {
    this.server.emit('save-external-ecomm-users-progress', { job, progress });
  }

  @OnQueueCompleted({name: 'save-external-ecomm-users'})
  onSaveExternalEcommUserCompleted(job: Job, result: any) {
    this.server.emit('save-external-ecomm-users-complete', { job, result });
  }

  @OnQueueFailed({name: 'save-external-ecomm-users'})
  onSaveExternalEcommUserFailed(job: Job, error: any) {
    this.server.emit('save-external-ecomm-users-error', { job, error });
  }
}
