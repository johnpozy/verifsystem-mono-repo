import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';

import { EEcommProcessorStatus, IEcommProcessor } from '@verifsystem/shared/data-access';

import { Ecomm } from './ecomm.entity';
import { Processor } from '../processors/processor.entity';

@Entity()
export class EcommProcessor implements IEcommProcessor {
  @PrimaryGeneratedColumn('increment')
  id: number;

  @Column({
    type: 'timestamp',
    default: () => 'CURRENT_TIMESTAMP',
  })
  createdAt: Date;

  @Column({
    type: 'enum',
    default: EEcommProcessorStatus.ACTIVATED,
    enum: EEcommProcessorStatus,
  })
  status: EEcommProcessorStatus;

  @Column()
  processorId: number;

  @Column()
  ecommId: number;

  @ManyToOne(() => Processor, (processor) => processor.ecommProcessors, {
    onDelete: 'CASCADE'
  })
  processor: Processor;

  @ManyToOne(() => Ecomm, (ecomm) => ecomm.ecommProcessors, {
    onDelete: 'CASCADE'
  })
  ecomm: Ecomm;
}
