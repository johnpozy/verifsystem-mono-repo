import { IEcommConfig, IEcommConfigWoocommerce, IEcommConfigWordpress } from '@verifsystem/shared/data-access';
import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { Ecomm } from './ecomm.entity';

@Entity()
export class EcommConfig implements IEcommConfig {
  @PrimaryGeneratedColumn('increment')
  id: number;

  @Column({ type: 'json' })
  wordpress: IEcommConfigWordpress;

  @Column({ type: 'json' })
  woocommerce: IEcommConfigWoocommerce;

  @ManyToOne(() => Ecomm, (ecomm) => ecomm.configs, {
    onDelete: 'CASCADE'
  })
  ecomm: Ecomm;
}
