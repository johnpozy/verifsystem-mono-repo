import { Injectable } from '@nestjs/common';
import { compare } from 'bcrypt';
import { JwtService } from '@nestjs/jwt';

import { APP_PREFIX } from '@verifsystem/shared/utils';

import { User } from '../users/user.entity';
import { UsersService } from '../users/users.service';

@Injectable()
export class AuthService {
  constructor(
    private usersService: UsersService,
    private jwtService: JwtService
  ) {}

  async validateUser(email: string, password: string): Promise<User> {
    const user = await this.usersService.getUserByEmail(email);
    const isPasswordValid = await compare(password, user.password);

    if (isPasswordValid) {
      return user;
    }

    return null;
  }

  async login(req, res) {
    const { user } = req;
    const payload = { email: user.email, sub: user.id };
    const accessToken = this.jwtService.sign(payload);

    res.cookie(`${APP_PREFIX}AccessToken`, accessToken, {
      httpOnly: true,
      secure: true,
      sameSite: 'none',
      path: '/',
      // maxAge: 60000
    });

    return {
      ...user,
      accessToken,
    };
  }

  async logout(req, res) {
    res.clearCookie(`${APP_PREFIX}AccessToken`, {
      path: '/'
    });

    return null;
  }
}
