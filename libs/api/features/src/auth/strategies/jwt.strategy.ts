import { Injectable } from '@nestjs/common';
import { PassportStrategy } from '@nestjs/passport';
import { ExtractJwt, Strategy } from 'passport-jwt';

import { APP_PREFIX } from '@verifsystem/shared/utils';

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
  constructor() {
    super({
      jwtFromRequest: ExtractJwt.fromExtractors([
        JwtStrategy.cookieExtractor,
        ExtractJwt.fromAuthHeaderAsBearerToken()
      ]),
      ignoreExpiration: false,
      secretOrKey: process.env.JWT_SECRET_KEY,
    });
  }

  async validate(payload: any) {
    return {
      userId: payload.sub,
      email: payload.email
    };
  }

  private static cookieExtractor = (req) => {
    if (req && req.cookies) {
      return req.cookies[`${APP_PREFIX}AccessToken`];
    }
    return null;
  };
}
