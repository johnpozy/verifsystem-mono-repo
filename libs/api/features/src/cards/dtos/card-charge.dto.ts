import { ApiProperty } from '@nestjs/swagger';

import { ICard, IProcessor, IUser, IGatewayConfig } from '@verifsystem/shared/data-access';

import { CardCreateDto } from './card-create-dto';
import { UserCreateDto } from '../../users/dtos/user-create.dto';
import { GatewayConfigCreateDto } from '../../gateway-configs/dtos/gateway-config-create.dto';
import { ProcessorCreateDto } from '../../processors/dtos/processor-create.dto';

export class CardChargeDto {
  @ApiProperty()
  currency: string;

  @ApiProperty()
  amount: number;

  @ApiProperty()
  description: string;

  @ApiProperty({ type: CardCreateDto })
  card: ICard;

  @ApiProperty({ type: UserCreateDto })
  user: IUser;

  @ApiProperty({ type: GatewayConfigCreateDto })
  gateway: IGatewayConfig;

  @ApiProperty({ type: ProcessorCreateDto })
  processor: IProcessor;

  @ApiProperty()
  ecommId: number;
}
