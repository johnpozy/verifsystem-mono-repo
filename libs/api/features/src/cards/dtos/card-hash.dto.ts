import { ApiProperty } from '@nestjs/swagger';

import { ICardHash } from '@verifsystem/shared/data-access';

export class CardHashDto implements ICardHash {
  @ApiProperty()
  iv: string;

  @ApiProperty()
  hash: string;
}
