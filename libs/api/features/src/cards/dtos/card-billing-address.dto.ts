import { ApiProperty } from '@nestjs/swagger';

import { ICardBillingAddress } from '@verifsystem/shared/data-access';

export class CardBillingAddressDto implements ICardBillingAddress {
  @ApiProperty()
  address1: string;

  @ApiProperty()
  address2: string;

  @ApiProperty()
  city: string;

  @ApiProperty()
  state: string;

  @ApiProperty()
  country: string;

  @ApiProperty()
  zipCode: string;
}
