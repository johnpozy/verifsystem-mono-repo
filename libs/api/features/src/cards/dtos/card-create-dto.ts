import { ApiProperty } from '@nestjs/swagger';

import { ICard, ECardStatus, ICardBillingAddress, ICardHash } from '@verifsystem/shared/data-access';

import { CardBillingAddressDto } from './card-billing-address.dto';
import { CardHashDto } from './card-hash.dto';

export class CardCreateDto implements ICard {
  id: number;

  @ApiProperty()
  cardHolderName: string;

  @ApiProperty({ type: CardBillingAddressDto })
  billingAddress: ICardBillingAddress;

  @ApiProperty()
  description: string;

  @ApiProperty({ type: CardHashDto })
  cardHash: ICardHash;

  @ApiProperty()
  number: number;

  last4: number;

  @ApiProperty()
  expYear: number;

  @ApiProperty()
  expMonth: number;

  @ApiProperty()
  cvc: number;

  createdAt: Date;

  @ApiProperty({ enum: ECardStatus, enumName: 'CardStatus' })
  status: ECardStatus;
}
