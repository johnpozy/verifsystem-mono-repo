import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

import { encryptCard } from '@verifsystem/api/utils';

import { CardCreateDto } from './dtos/card-create-dto';
import { Card } from './card.entity';
import { User } from '../users/user.entity';

@Injectable()
export class CardsService {
  constructor(
    @InjectRepository(Card) private cardRepository: Repository<Card>
  ) {}

  async findAll(): Promise<Card[]> {
    return await this.cardRepository.find();
  }

  async findById(cardId: number): Promise<Card> {
    return await this.cardRepository.findOne({
      where: {
        id: cardId
      }
    });
  }

  async create(cardCreateDto: CardCreateDto, user?: User): Promise<Card> {
    const cardEntity = new Card();

    cardEntity.billingAddress = cardCreateDto.billingAddress;
    cardEntity.last4 = +cardCreateDto.number.toString().slice(-4);
    cardEntity.cardHash = encryptCard(cardCreateDto.number, cardCreateDto.cvc);
    cardEntity.cardHolderName = cardCreateDto.cardHolderName;
    cardEntity.cvc = cardCreateDto.cvc;
    cardEntity.description = cardCreateDto.description;
    cardEntity.expMonth = cardCreateDto.expMonth;
    cardEntity.expYear = cardCreateDto.expYear;
    cardEntity.status = cardCreateDto.status;
    cardEntity.user = user ? user : null;

    const card = await this.cardRepository.save(cardEntity);

    return card;
  }

  async delete(cardId): Promise<Card> {
    const card = await this.cardRepository.findOneByOrFail({ id: cardId });

    await this.cardRepository.delete({ id: cardId });

    return card;
  }
}
