import { Column, Entity, ManyToOne, OneToMany, PrimaryGeneratedColumn } from 'typeorm';

import { ECardStatus, ICardBillingAddress, ICardHash, ICard } from '@verifsystem/shared/data-access';

import { User } from '../users/user.entity';
import { CardGatewayConfig } from './card-gateway-config.entity';

@Entity()
export class Card implements Omit<ICard, 'number'> {
  @PrimaryGeneratedColumn('increment')
  id: number;

  @Column()
  cardHolderName: string;

  @Column({ type: 'json' })
  billingAddress: ICardBillingAddress;

  @Column({
    nullable: true,
  })
  description: string;

  @Column({ type: 'json' })
  cardHash: ICardHash;

  @Column({ type: 'smallint' })
  last4: number;

  @Column({
    type: 'smallint',
    default: 0,
  })
  expYear: number;

  @Column({
    type: 'smallint',
    default: 0,
  })
  expMonth: number;

  @Column({
    type: 'smallint',
    default: 0,
  })
  cvc: number;

  @Column({
    type: 'timestamp',
    default: () => 'CURRENT_TIMESTAMP',
  })
  createdAt: Date;

  @Column({
    type: 'enum',
    default: ECardStatus.ACTIVATED,
    enum: ECardStatus,
  })
  status: ECardStatus;

  @ManyToOne(() => User, (user) => user.cards, {
    onDelete: 'CASCADE'
  })
  user: User;

  @OneToMany(() => CardGatewayConfig, (cardGatewayConfig) => cardGatewayConfig.card, {
    onDelete: 'CASCADE'
  })
  gatewayConfigs: CardGatewayConfig[];
}
