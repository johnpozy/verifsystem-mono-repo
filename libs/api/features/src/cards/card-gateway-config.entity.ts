import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';

import { ICardGatewayConfig } from '@verifsystem/shared/data-access';

import { Card } from './card.entity';
import { GatewayConfig } from '../gateway-configs/gateway-config.entity';


@Entity()
export class CardGatewayConfig implements ICardGatewayConfig {
  @PrimaryGeneratedColumn('increment')
  id: number;

  @Column({ type: 'tinytext' })
  cardId: string;

  @ManyToOne(() => Card, (card) => card.gatewayConfigs, {
    onDelete: 'CASCADE'
  })
  card: Card;

  @ManyToOne(() => GatewayConfig, (gatewayConfig) => gatewayConfig.cardGatewayConfigs, {
    onDelete: 'CASCADE'
  })
  gatewayConfig: GatewayConfig;
}
