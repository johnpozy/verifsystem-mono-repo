import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { Card } from './card.entity';
import { CardGatewayConfig } from './card-gateway-config.entity';
import { CardsService } from './cards.service';

@Module({
  imports: [TypeOrmModule.forFeature([Card, CardGatewayConfig])],
  providers: [CardsService],
  exports: [CardsService]
})
export class CardsModule {}
