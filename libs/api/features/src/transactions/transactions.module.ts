import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { Ecomm } from '../ecomms/ecomm.entity';
import { Transaction } from './transaction.entity';
import { TransactionsService } from './transactions.service';

@Module({
  imports: [TypeOrmModule.forFeature([Transaction, Ecomm])],
  providers: [TransactionsService],
  exports: [TransactionsService]
})
export class TransactionsModule {}
