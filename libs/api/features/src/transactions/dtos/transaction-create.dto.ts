import { ApiProperty } from '@nestjs/swagger';
import { ETransactionPaymentMethod, ETransactionStatus, ITransaction } from '@verifsystem/shared/data-access';

export class TransactionCreateDto implements ITransaction {
  id: number;

  @ApiProperty()
  amount: number;

  @ApiProperty()
  orderId: number;

  createdAt: Date;

  @ApiProperty({ enum: ETransactionStatus, enumName: 'TransactionStatus' })
  status: ETransactionStatus;

  @ApiProperty({ enum: ETransactionPaymentMethod, enumName: 'TransactionPaymentMethod' })
  paymentMethod: ETransactionPaymentMethod;

  @ApiProperty()
  description: string;

  @ApiProperty()
  userId: number;

  @ApiProperty()
  ecommId: number;

  @ApiProperty()
  gatewayConfigId: number;
}
