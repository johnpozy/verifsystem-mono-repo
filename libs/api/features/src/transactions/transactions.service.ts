import { Injectable, Logger } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { FilterOperator, paginate, Paginated, PaginateQuery } from 'nestjs-paginate';
import WooCommerceRestApi from '@woocommerce/woocommerce-rest-api';

import { Transaction } from './transaction.entity';
import { Ecomm } from '../ecomms/ecomm.entity';
import { TransactionUpdateDto } from './dtos/transaction-update.dto';

@Injectable()
export class TransactionsService {
  constructor(
    @InjectRepository(Transaction) private transactionRepository: Repository<Transaction>,
    @InjectRepository(Ecomm) private ecommRepository: Repository<Ecomm>
  ) {}

  async getAll(): Promise<Array<Transaction>> {
    return await this.transactionRepository.find();
  }

  async update(transactionId: number, transactionUpdateDto: TransactionUpdateDto): Promise<Transaction> {
    const transaction = await this.transactionRepository
      .createQueryBuilder('transaction')
      .where('id = :transactionId', { transactionId })
      .getOne();

    const ecomm = await this.ecommRepository
      .createQueryBuilder('ecomm')
      .leftJoinAndSelect('ecomm.configs', 'ecommConfigs')
      .where('ecomm.id = :ecommId', { ecommId: transaction.ecommId })
      .getOne();

    const woocommerceApi = new WooCommerceRestApi({
      url: ecomm.baseUrl,
      consumerKey: ecomm.configs[0].woocommerce.consumerKey,
      consumerSecret: ecomm.configs[0].woocommerce.consumerSecret,
      version: 'wc/v3'
    });

    // This will trigger /webhooks/woocommerce/order
    // TODO: find a way to disable hook in wp-plugin
    woocommerceApi
      .put(`orders/${transaction.orderId}`, {
        status: transactionUpdateDto.status
      })
      .then((s) => Logger.log(`Order id ${transaction.orderId} status have been updated.`, 'INFO'))
      .catch((error) => Logger.log(error, 'ERROR'));

    await this.transactionRepository.update(transactionId, transactionUpdateDto)

    return this.transactionRepository.findOne({
      where: {
        id: transactionId
      },
      relations: {
        user: true,
        gatewayConfig: true,
        ecomm: true
      }
    });
  }

  async getByUser(userId: number): Promise<Array<Transaction>> {
    return this.transactionRepository.find({
      where: {
        user: {
          id: userId
        }
      },
      relations: {
        user: true,
        gatewayConfig: true
      }
    })
  }

  getByEcomm = async (ecommId: number, query: PaginateQuery): Promise<Paginated<Transaction>> => {
    query.filter = JSON.parse(query.filter as any);

    const queryBuilder = this.transactionRepository.createQueryBuilder('transaction');

    queryBuilder.leftJoinAndSelect('transaction.user', 'user');
    queryBuilder.leftJoinAndSelect('transaction.gatewayConfig', 'gatewayConfig');
    queryBuilder.leftJoinAndSelect('transaction.ecomm', 'ecomm');
    queryBuilder.leftJoinAndSelect('ecomm.configs', 'ecommConfigs');

    if (ecommId) {
      queryBuilder.where('ecomm.id = :ecommId', { ecommId });
    }

    return paginate(query, queryBuilder, {
      sortableColumns: ['createdAt'],
      searchableColumns: [],
      filterableColumns: {
        status: [FilterOperator.IN]
      },
    });
  }
}
