import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';

import { ETransactionStatus, ETransactionPaymentMethod, ITransaction } from '@verifsystem/shared/data-access';

import { GatewayConfig } from '../gateway-configs/gateway-config.entity';
import { User } from '../users/user.entity';
import { Ecomm } from '../ecomms/ecomm.entity';

@Entity()
export class Transaction implements ITransaction {
  @PrimaryGeneratedColumn('increment')
  id: number;

  @Column({
    type: 'timestamp',
    default: () => 'CURRENT_TIMESTAMP',
  })
  createdAt: Date;

  @Column({
    default: 0,
    comment: 'amount in cent'
  })
  amount: number;

  @Column({
    type: 'enum',
    default: ETransactionStatus.Processing,
    enum: ETransactionStatus,
  })
  status: ETransactionStatus;

  @Column({
    type: 'enum',
    default: ETransactionPaymentMethod['Smartpayment Gateway'],
    enum: ETransactionPaymentMethod,
    nullable: true
  })
  paymentMethod: ETransactionPaymentMethod;

  @Column({ default: null })
  description: string;

  @Column()
  userId: number;

  @Column()
  ecommId: number;

  @Column({ default: null })
  orderId: number;

  @ManyToOne(() => GatewayConfig, (gatewayConfig) => gatewayConfig.transactions, {
    onDelete: 'CASCADE'
  })
  gatewayConfig: GatewayConfig;

  @ManyToOne(() => User, (user) => user.transactions, {
    onDelete: 'CASCADE'
  })
  user: User;

  @ManyToOne(() => Ecomm, (ecomm) => ecomm.transactions, {
    onDelete: 'CASCADE'
  })
  ecomm: Ecomm;
}
