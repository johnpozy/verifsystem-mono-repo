import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { Ecomm } from '../ecomms/ecomm.entity';
import { PayoutTransaction } from './payout-transaction.entity';
import { PayoutAccount } from './payout-account.entity';

import { PayoutsService } from './payouts.service';

@Module({
  imports: [TypeOrmModule.forFeature([PayoutAccount, PayoutTransaction, Ecomm])],
  providers: [PayoutsService],
  exports: [PayoutsService],
})
export class PayoutsModule {}
