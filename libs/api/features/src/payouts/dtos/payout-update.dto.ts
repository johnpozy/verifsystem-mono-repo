import { ApiProperty } from '@nestjs/swagger';

import { EPayoutStatus, EPayoutType, IPayoutAccount } from '@verifsystem/shared/data-access';

export class PayoutUpdateDto implements IPayoutAccount {
  id?: number;

  @ApiProperty()
  name: string;

  type: EPayoutType;

  @ApiProperty()
  accountNumber: string;

  @ApiProperty()
  details: any;

  createdAt: Date;

  @ApiProperty({ enum: EPayoutStatus, enumName: 'PayoutStatus' })
  status: EPayoutStatus;
}
