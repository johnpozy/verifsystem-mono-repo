import { ApiProperty } from '@nestjs/swagger';
import { EPayoutTransactionStatus, IPayoutTransaction } from '@verifsystem/shared/data-access';

export class PayoutTransactionCreateDto implements IPayoutTransaction {
  id: number;

  @ApiProperty()
  amount: number;

  @ApiProperty()
  description: string;

  createdAt: Date;

  @ApiProperty({ enum: EPayoutTransactionStatus, enumName: 'PayoutTransactionStatus' })
  status: EPayoutTransactionStatus;
}
