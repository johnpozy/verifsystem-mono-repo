import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';

import { EPayoutTransactionStatus, IPayoutTransaction } from '@verifsystem/shared/data-access';

import { PayoutAccount } from './payout-account.entity';

@Entity()
export class PayoutTransaction implements IPayoutTransaction {
  @PrimaryGeneratedColumn('increment')
  id: number;

  @Column({
    default: 0,
    comment: 'amount in cent'
  })
  amount: number;

  @Column()
  description: string;

  @Column({
    type: 'timestamp',
    default: () => 'CURRENT_TIMESTAMP'
  })
  createdAt: Date;

  @Column({
    type: 'enum',
    default: EPayoutTransactionStatus.Pending,
    enum: EPayoutTransactionStatus
  })
  status: EPayoutTransactionStatus;

  @ManyToOne(() => PayoutAccount, (payout) => payout.payoutTransactions, {
    onDelete: 'CASCADE'
  })
  payout: PayoutAccount;
}
