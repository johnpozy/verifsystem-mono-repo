import { Column, Entity, ManyToOne, OneToMany, PrimaryGeneratedColumn } from 'typeorm';

import { IPayoutAccount, EPayoutStatus, EPayoutType } from '@verifsystem/shared/data-access';

import { Ecomm } from '../ecomms/ecomm.entity';
import { PayoutTransaction } from './payout-transaction.entity';

@Entity()
export class PayoutAccount implements IPayoutAccount {
  @PrimaryGeneratedColumn('increment')
  id: number;

  @Column()
  name: string;

  @Column({
    type: 'enum',
    default: EPayoutType['Bill Payment'],
    enum: EPayoutType
  })
  type: EPayoutType;

  @Column()
  accountNumber: string;

  @Column({ type: 'json' })
  details: any;

  @Column({
    type: 'timestamp',
    default: () => 'CURRENT_TIMESTAMP'
  })
  createdAt: Date;

  @Column({
    type: 'enum',
    default: EPayoutStatus.ACTIVATED,
    enum: EPayoutStatus
  })
  status: EPayoutStatus;

  @ManyToOne(() => Ecomm, (ecomm) => ecomm.payouts, {
    onDelete: 'CASCADE'
  })
  ecomm: Ecomm;

  @OneToMany(() => PayoutTransaction, (payoutTransaction) => payoutTransaction.payout, {
    onDelete: 'CASCADE'
  })
  payoutTransactions: PayoutTransaction[];
}
