import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { PayoutCreateDto } from './dtos/payout-create.dto';

import { Ecomm } from '../ecomms/ecomm.entity';
import { PayoutAccount } from './payout-account.entity';
import { PayoutTransaction } from './payout-transaction.entity';

import { PayoutUpdateDto } from './dtos/payout-update.dto';
import { PayoutTransactionCreateDto } from './dtos/payout-transaction-create.dto';
import { PayoutTransactionUpdateDto } from './dtos/payout-transaction-update.dto';

@Injectable()
export class PayoutsService {
  constructor(
    @InjectRepository(PayoutAccount) private payoutRepository: Repository<PayoutAccount>,
    @InjectRepository(PayoutTransaction) private payoutTransactionRepository: Repository<PayoutTransaction>,
    @InjectRepository(Ecomm) private ecommRepository: Repository<Ecomm>
  ) {}

  async getAll(): Promise<Array<PayoutAccount>> {
    return await this.payoutRepository.find({
      relations: {
        ecomm: true
      }
    });
  }

  async getByEcomm(ecommId): Promise<Array<PayoutAccount>> {
    return await this.payoutRepository.find({
      where: {
        ecomm: {
          id: ecommId,
        },
      },
      relations: {
        ecomm: true
      }
    });
  }

  async create(ecommId, payoutCreateDto: PayoutCreateDto): Promise<PayoutAccount> {
    const ecomm = await this.ecommRepository.findOneBy({ id: ecommId });
    const payoutEntity = new PayoutAccount();

    payoutEntity.accountNumber = payoutCreateDto.accountNumber;
    payoutEntity.details = payoutCreateDto.details;
    payoutEntity.name = payoutCreateDto.name;
    payoutEntity.type = payoutCreateDto.type;
    payoutEntity.ecomm = ecomm;

    const result = await this.payoutRepository.save(payoutEntity);

    return result;
  }

  async update(accountId: number, payoutUpdateDto: PayoutUpdateDto): Promise<PayoutAccount> {
    const payoutEntity = new PayoutAccount();

    payoutEntity.accountNumber = payoutUpdateDto.accountNumber;
    payoutEntity.details = payoutUpdateDto.details;
    payoutEntity.name = payoutUpdateDto.name;
    payoutEntity.type = payoutUpdateDto.type;

    await this.payoutRepository.update(accountId, payoutEntity)

    const result = await this.payoutRepository.findOneBy({ id: accountId });

    return result;
  }

  async deleteAccount(payoutAccountId: number): Promise<PayoutAccount> {
    const payoutAccount = await this.payoutRepository.findOneBy({ id: payoutAccountId });

    await this.payoutRepository.delete(payoutAccountId);

    return payoutAccount;
  }

  async createTransaction(accountId: number, payoutTransactionCreateDto: PayoutTransactionCreateDto): Promise<PayoutTransaction> {
    const payoutAccount = await this.payoutRepository.findOneBy({ id: accountId });
    const payoutTransactionEntity = new PayoutTransaction();

    payoutTransactionEntity.amount = payoutTransactionCreateDto.amount;
    payoutTransactionEntity.description = payoutTransactionCreateDto.description;
    payoutTransactionEntity.payout = payoutAccount;

    const result = this.payoutTransactionRepository.save(payoutTransactionEntity);

    return result;
  }

  async getTransactions(accountId: number, query?: any): Promise<Array<PayoutTransaction>> {
    return this.payoutTransactionRepository.find({
      where: {
        payout: {
          id: accountId
        },
        status: query.status
      },
      relations: {
        payout: {
          ecomm: true
        }
      }
    });
  }

  async getEcommTransactions(ecommId: number, query?: any): Promise<Array<PayoutTransaction>> {
    return this.payoutTransactionRepository.find({
      where: {
        payout: {
          ecomm: {
            id: ecommId
          }
        },
        status: query.status
      },
      relations: {
        payout: {
          ecomm: true
        }
      }
    });
  }

  async updateTransaction(accountId, transactionId, payoutTransactionUpdateDto: PayoutTransactionUpdateDto): Promise<PayoutTransaction> {
    const payoutAccount = await this.payoutRepository.findOneBy({ id: accountId });
    const payoutTransactionEntity = new PayoutTransaction();

    payoutTransactionEntity.amount = payoutTransactionUpdateDto.amount;
    payoutTransactionEntity.description = payoutTransactionUpdateDto.description;
    payoutTransactionEntity.payout = payoutAccount;
    payoutTransactionEntity.status = payoutTransactionUpdateDto.status;

    await this.payoutTransactionRepository.update(transactionId, payoutTransactionEntity);

    const result = this.payoutTransactionRepository.findOneBy({ id: transactionId });

    return result;
  }
}
