import { Test } from '@nestjs/testing';
import { PayoutsService } from './payouts.service';

describe('PayoutsService', () => {
  let service: PayoutsService;

  beforeEach(async () => {
    const module = await Test.createTestingModule({
      providers: [PayoutsService],
    }).compile();

    service = module.get(PayoutsService);
  });

  it('should be defined', () => {
    expect(service).toBeTruthy();
  });
});
