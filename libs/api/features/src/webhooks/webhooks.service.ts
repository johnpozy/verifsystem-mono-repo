import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

import { Ecomm } from '../ecomms/ecomm.entity';
import { Transaction } from '../transactions/transaction.entity';
import { UserEcomm } from '../users/user-ecomm.entity';
import { User } from '../users/user.entity';

import { WebhookWoocommerceCustomerDto } from './dtos/webhook-woocommerce-customer.dto';
import { WebhookWoocommerceOrderCreateDto } from './dtos/webhook-woocommerce-order-create.dto';

@Injectable()
export class WebhooksService {
  constructor(
    @InjectRepository(User) private userRepository: Repository<User>,
    @InjectRepository(UserEcomm) private userEcommRepository: Repository<UserEcomm>,
    @InjectRepository(Ecomm) private ecommRepository: Repository<Ecomm>,
    @InjectRepository(Transaction) private transactionRepository: Repository<Transaction>
  ) {}

  async woocommerceCustomer(webhookWoocommerceCustomerDto: WebhookWoocommerceCustomerDto): Promise<User> {
    const { ecommId, externalId, email } = webhookWoocommerceCustomerDto;
    const ecommUser = await this.userEcommRepository.findOne({
      where: {
        externalId,
        ecomm: {
          id: ecommId
        }
      },
      relations: {
        user: true
      }
    });
    const userEntity = new User();
    const userEcommEntity = new UserEcomm();

    userEntity.email = webhookWoocommerceCustomerDto.email;
    userEntity.displayName = webhookWoocommerceCustomerDto.displayName;
    userEntity.firstName = webhookWoocommerceCustomerDto.firstName;
    userEntity.lastName = webhookWoocommerceCustomerDto.lastName;
    userEntity.phoneNumber = webhookWoocommerceCustomerDto.phoneNumber;
    userEntity.postcode = webhookWoocommerceCustomerDto.postcode;

    if (!ecommUser) {
      const otherEcommUser = await this.userRepository.findOne({
        where: { email }
      });

      if (otherEcommUser) {
        const ecomm = await this.ecommRepository.findOneBy({ id: ecommId });
        const user = await this.userRepository.save({
          id: otherEcommUser.id,
          ...userEntity
        });

        userEcommEntity.ecomm = ecomm;
        userEcommEntity.externalId = externalId;
        userEcommEntity.user = user;

        await this.userEcommRepository.save(userEcommEntity);

        return user;
      } else {
        const ecomm = await this.ecommRepository.findOneBy({ id: ecommId });
        const user = await this.userRepository.save(userEntity);

        userEcommEntity.ecomm = ecomm;
        userEcommEntity.externalId = externalId;
        userEcommEntity.user = user;

        await this.userEcommRepository.save(userEcommEntity);

        return user;
      }
    } else {
      return await this.userRepository.save({
        id: ecommUser.user.id,
        ...userEntity
      });
    }
  }

  async woocommerceUpdateOrder(webhookWoocommerceOrderCreateDto: WebhookWoocommerceOrderCreateDto): Promise<Transaction> {
    const transaction = this.transactionRepository
      .createQueryBuilder('transaction')
      .where('transaction.userId = :userId', { userId: webhookWoocommerceOrderCreateDto.userId })
      .andWhere('transaction.ecommId = :ecommId', { ecommId: webhookWoocommerceOrderCreateDto.ecommId })
      .andWhere('transaction.orderId = :orderId', { orderId: webhookWoocommerceOrderCreateDto.orderId});

    const existingTransaction = await transaction.getOne();

    if (!existingTransaction?.id) {
      await transaction
        .insert()
        .values(webhookWoocommerceOrderCreateDto)
        .execute();

      return transaction.getOne();
    }

    await transaction
      .update()
      .set(webhookWoocommerceOrderCreateDto)
      .execute();

    return transaction.getOne();
  }
}
