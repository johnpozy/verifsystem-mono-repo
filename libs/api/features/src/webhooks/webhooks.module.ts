import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { Ecomm } from '../ecomms/ecomm.entity';
import { Transaction } from '../transactions/transaction.entity';
import { UserEcomm } from '../users/user-ecomm.entity';
import { User } from '../users/user.entity';

import { WebhooksService } from './webhooks.service';

@Module({
  imports: [TypeOrmModule.forFeature([User, Ecomm, UserEcomm, Transaction])],
  providers: [WebhooksService],
  exports: [WebhooksService]
})
export class WebhooksModule {}
