import { ApiProperty } from '@nestjs/swagger';

export class WebhookWoocommerceCustomerPostcodeDto {
  @ApiProperty()
  billing: string;

  @ApiProperty()
  shipping: string;
}
