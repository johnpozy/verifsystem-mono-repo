import { ApiProperty } from '@nestjs/swagger';
import { WebhookWoocommerceCustomerPostcodeDto } from './webhook-woocommerce-customer-postcode.dto';

export class WebhookWoocommerceCustomerDto {
  @ApiProperty()
  displayName: string;

  @ApiProperty()
  firstName: string;

  @ApiProperty()
  lastName: string;

  @ApiProperty()
  email: string;

  @ApiProperty()
  phoneNumber: string;

  @ApiProperty({ type: WebhookWoocommerceCustomerPostcodeDto })
  postcode: any;

  @ApiProperty()
  ecommId: number;

  @ApiProperty()
  externalId: string;
}
