import { ApiProperty } from '@nestjs/swagger';
import { ETransactionPaymentMethod, ETransactionStatus } from '@verifsystem/shared/data-access';

export class WebhookWoocommerceOrderUpdateDto {
  @ApiProperty()
  amount: number;

  @ApiProperty({ enum: ETransactionStatus, enumName: 'TransactionStatus' })
  status: ETransactionStatus;

  @ApiProperty({ enum: ETransactionPaymentMethod, enumName: 'TransactionPaymentMethod' })
  paymentMethod: ETransactionPaymentMethod;

  @ApiProperty()
  description: string;

  @ApiProperty()
  userId: number;

  @ApiProperty()
  ecommId: number;

  @ApiProperty()
  orderId: number;

  @ApiProperty()
  gatewayConfigId: number;
}
