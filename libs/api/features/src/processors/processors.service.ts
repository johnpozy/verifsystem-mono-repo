import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { PaginateQuery, paginate, Paginated } from 'nestjs-paginate';

import { EProcessorType, EProcessorStatus } from '@verifsystem/shared/data-access';

import { Processor } from './processor.entity';
import { GatewayConfig } from '../gateway-configs/gateway-config.entity';

import { ProcessorCreateDto } from '../processors/dtos/processor-create.dto';
import { ProcessorUpdateDto } from '../processors/dtos/processor-update.dto';
import { GatewayConfigCreateDto } from '../gateway-configs/dtos/gateway-config-create.dto';

@Injectable()
export class ProcessorsService {
  constructor(
    @InjectRepository(Processor) private processorRepository: Repository<Processor>,
    @InjectRepository(GatewayConfig) private gatewayConfigRepository: Repository<GatewayConfig>
  ) {}

  create(processorCreateDto: ProcessorCreateDto): Promise<Processor> {
    const processor = new Processor();

    processor.baseUrl = processorCreateDto.baseUrl;
    processor.description = processorCreateDto.description;
    processor.title = processorCreateDto.title;
    processor.type = processorCreateDto.type;

    return this.processorRepository.save(processor);
  }

  async update(processorUpdateDto: ProcessorUpdateDto, processorId: number): Promise<Processor> {
    const processorQuery = this.processorRepository
      .createQueryBuilder('processor')
      .where('processor.id = :processorId', { processorId });

    await processorQuery
      .update()
      .set({
        type: processorUpdateDto.type,
        title: processorUpdateDto.title,
        description: processorUpdateDto.description,
        baseUrl: processorUpdateDto.baseUrl,
        status: processorUpdateDto.status
      })
      .execute();

    return processorQuery.getOne();
  }

  async delete(processorId: number): Promise<Processor> {
    const processor = await this.processorRepository.findOneBy({ id: processorId });

    await this.processorRepository.delete(processorId);

    return processor;
  }

  findAll(): Promise<Processor[]> {
    return this.processorRepository.find();
  }

  findByType(processorType: EProcessorType): Promise<Processor[]> {
    return this.processorRepository.find({
      where: {
        type: processorType,
        status: EProcessorStatus.ACTIVATED,
      },
    });
  }

  async getGatewayConfigs(processorId: number): Promise<GatewayConfig[]> {
    return await this.gatewayConfigRepository.findBy({
      processor: {
        id: processorId,
      },
    });
  }

  async createGatewayConfig(gatewayConfigCreateDto: GatewayConfigCreateDto, processorId: number): Promise<GatewayConfig> {
    const gatewayConfigEntity = new GatewayConfig();
    const processor = await this.processorRepository.findOneBy({ id: processorId });

    gatewayConfigEntity.name = gatewayConfigCreateDto.name;
    gatewayConfigEntity.type = gatewayConfigCreateDto.type;
    gatewayConfigEntity.apiKey = gatewayConfigCreateDto.apiKey;
    gatewayConfigEntity.maxDailySessions = gatewayConfigCreateDto.maxDailySessions;
    gatewayConfigEntity.maxDailyTxnNumber = gatewayConfigCreateDto.maxDailyTxnNumber;
    gatewayConfigEntity.maxDailyTxnAmount = gatewayConfigCreateDto.maxDailyTxnAmount;
    gatewayConfigEntity.maxMonthlyTxnNumber = gatewayConfigCreateDto.maxMonthlyTxnNumber;
    gatewayConfigEntity.maxMonthlyTxnAmount = gatewayConfigCreateDto.maxMonthlyTxnAmount;
    gatewayConfigEntity.processor = processor;

    return this.gatewayConfigRepository.save(gatewayConfigEntity);
  }

  getEcommsProcessors = async (query: PaginateQuery): Promise<Paginated<Processor>> => {
    const queryBuilder = this.processorRepository.createQueryBuilder('processor');

    queryBuilder.leftJoinAndSelect('processor.ecommProcessors', 'ecommProcessors');
    queryBuilder.leftJoinAndSelect('ecommProcessors.ecomm', 'ecomm');

    return paginate(query, queryBuilder, {
      sortableColumns: ['title', 'createdAt'],
      searchableColumns: [],
      filterableColumns: {},
    });
  };

  getEcommProcessors = async (ecommId: number, query: PaginateQuery): Promise<Paginated<Processor>> => {
    const queryBuilder = this.processorRepository.createQueryBuilder('processor');

    queryBuilder.leftJoinAndSelect('processor.ecommProcessors', 'ecommProcessors');
    queryBuilder.leftJoinAndSelect('ecommProcessors.ecomm', 'ecomm');
    queryBuilder.where('ecomm.id = :ecommId', { ecommId })

    return paginate(query, queryBuilder, {
      sortableColumns: ['title', 'createdAt'],
      searchableColumns: [],
      filterableColumns: {},
    });
  };
}
