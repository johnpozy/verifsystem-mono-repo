import { ApiProperty } from '@nestjs/swagger';

import { IProcessor, EProcessorStatus, EProcessorType } from '@verifsystem/shared/data-access';

export class ProcessorCreateDto implements IProcessor {
  id: number;

  @ApiProperty()
  title: string;

  @ApiProperty()
  description: string;

  @ApiProperty()
  baseUrl: string;

  @ApiProperty({ enum: EProcessorType, enumName: 'ProcessorType' })
  type: EProcessorType;

  createdAt: Date;

  @ApiProperty({ enum: EProcessorStatus, enumName: 'ProcessorStatus' })
  status: EProcessorStatus;
}
