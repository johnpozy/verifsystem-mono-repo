import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { GatewayConfig } from '../gateway-configs/gateway-config.entity';
import { Processor } from './processor.entity';
import { ProcessorsService } from './processors.service';

@Module({
  imports: [TypeOrmModule.forFeature([Processor, GatewayConfig])],
  providers: [ProcessorsService],
  exports: [ProcessorsService]
})
export class ProcessorsModule {}
