import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { EcommProcessor } from '../ecomms/ecomm-processor.entity';
import { GatewayConfig } from '../gateway-configs/gateway-config.entity';

import { EProcessorStatus, EProcessorType, IProcessor } from '@verifsystem/shared/data-access';

@Entity()
export class Processor implements IProcessor {
  @PrimaryGeneratedColumn('increment')
  id: number;

  @Column({
    type: 'enum',
    default: EProcessorType.PAYMENT,
    enum: EProcessorType,
  })
  type: EProcessorType;

  @Column({ type: 'tinytext' })
  title: string;

  @Column({ type: 'mediumtext' })
  description: string;

  @Column({ type: 'tinytext' })
  baseUrl: string;

  @Column({
    type: 'timestamp',
    default: () => 'CURRENT_TIMESTAMP',
  })
  createdAt: Date;

  @Column({
    type: 'enum',
    default: EProcessorStatus.ACTIVATED,
    enum: EProcessorStatus,
  })
  status: EProcessorStatus;

  @OneToMany(() => GatewayConfig, (gatewayConfig) => gatewayConfig.processor, {
    onDelete: 'CASCADE'
  })
  gatewayConfigs: GatewayConfig[];

  @OneToMany(() => EcommProcessor, (ecommProcessor) => ecommProcessor.processor)
  ecommProcessors!: EcommProcessor[];

}
