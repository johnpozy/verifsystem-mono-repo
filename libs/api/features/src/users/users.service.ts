import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Not, Repository } from 'typeorm';
import { hash } from 'bcrypt';

import { Ecomm } from '../ecomms/ecomm.entity';
import { User } from './user.entity';
import { UserEcomm } from './user-ecomm.entity';
import { UserVeriff } from './user-veriff.entity';

import { UserCreateDto } from './dtos/user-create.dto';
import { UserUpdateDto } from './dtos/user-update.dto';
import { UserVeriffCreateDto } from './dtos/user-veriff-create.dto';
import { UserVeriffUpdateDto } from './dtos/user-veriff-update.dto';
import { UserEcommUpdateDto } from './dtos/user-ecomm-update.dto';
import { EUserEcommStatus, EUserRole } from '@verifsystem/shared/data-access';
import { paginate, Paginated, PaginateQuery } from 'nestjs-paginate';

@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(User) private userRepository: Repository<User>,
    @InjectRepository(UserEcomm) private userEcommRepository: Repository<UserEcomm>,
    @InjectRepository(UserVeriff) private userVeriffRepository: Repository<UserVeriff>,
    @InjectRepository(Ecomm) private ecommRepository: Repository<Ecomm>
  ) {}

  getUsers(): Promise<Array<User>> {
    return this.userRepository.find();
  }

  async getUserById(userId: number): Promise<User> {
    return await this.userRepository.findOneByOrFail({ id: userId });
  }

  async getUserByEmail(email: string): Promise<User> {
    return await this.userRepository.findOneOrFail({
      where: { email },
      relations: {
        ecomms: {
          ecomm: true
        },
        veriffs: true
      }
    });
  }

  async create(userCreateDto: UserCreateDto): Promise<User> {
    const userEntity = new User();

    userEntity.displayName = userCreateDto.displayName;
    userEntity.email = userCreateDto.email;
    userEntity.firstName = userCreateDto.firstName;
    userEntity.lastName = userCreateDto.lastName;
    userEntity.phoneNumber = userCreateDto.phoneNumber;
    userEntity.role = userCreateDto.role;

    if (userCreateDto.password) {
      userEntity.password = await hash(userCreateDto.password, 10);
    }

    const result = await this.userRepository.save(userEntity);

    return result;
  }

  async update(userUpdateDto: UserUpdateDto, userId: number): Promise<User> {
    const userEntity = new User();

    userEntity.email = userUpdateDto.email;
    userEntity.displayName = userUpdateDto.displayName;
    userEntity.firstName = userUpdateDto.firstName;
    userEntity.lastName = userUpdateDto.lastName;
    userEntity.phoneNumber = userUpdateDto.phoneNumber;
    userEntity.role = userUpdateDto.role;

    if (userUpdateDto.password) {
      userEntity.password = await hash(userUpdateDto.password, 10);
    }

    await this.userRepository.update(userId, userUpdateDto);

    return await this.userRepository.findOneOrFail({
      where: {
        id: userId
      },
      relations: {
        ecomms: true,
        veriffs: true,
        cards: true
      }
    });
  }

  getEcommCustomers = async (query: PaginateQuery, ecommId?: number | string): Promise<Paginated<UserEcomm>> => {
    const queryBuilder = this.userEcommRepository.createQueryBuilder('userEcomm');

    queryBuilder.leftJoinAndSelect('userEcomm.ecomm', 'ecomm');
    queryBuilder.leftJoinAndSelect('userEcomm.user', 'user');
    queryBuilder.leftJoinAndSelect('user.ecomms', 'userEcomms');
    queryBuilder.leftJoinAndSelect('user.veriffs', 'userVeriffs');
    queryBuilder.where('user.role = :userRole', { userRole: EUserRole.CUSTOMER });
    queryBuilder.andWhere('userEcomm.status != :userStatus', { userStatus: EUserEcommStatus.ARCHIVED })

    if (ecommId !== 'all') {
      queryBuilder.andWhere('ecomm.id = :ecommId', { ecommId });
    }

    return paginate(query, queryBuilder, {
      sortableColumns: ['createdAt'],
      searchableColumns: [],
      filterableColumns: {},
    });
  }

  async findEcommUser(externalId: string, ecommId: number): Promise<User> {
    return this.userEcommRepository.findOne({
      where:{
        externalId,
        ecomm: {
          id: ecommId
        }
      },
      relations: {
        user: {
          ecomms: true,
          veriffs: true,
          cards: true
        }
      }
    }).then(ecommUser => ecommUser.user);
  }

  async createEcommUser(userCreateDto: UserCreateDto, externalId: string, ecommId: number): Promise<User> {
    const userEntity = new User();
    const userEcommEntity = new UserEcomm();

    userEntity.email = userCreateDto.email;
    userEntity.displayName = userCreateDto.displayName;
    userEntity.firstName = userCreateDto.firstName;
    userEntity.lastName = userCreateDto.lastName;
    userEntity.phoneNumber = userCreateDto.phoneNumber;
    userEntity.role = userCreateDto.role;

    const ecomm = await this.ecommRepository.findOneBy({ id: ecommId });
    const user = await this.userRepository.save(userEntity);

    userEcommEntity.ecomm = ecomm;
    userEcommEntity.externalId = externalId;
    userEcommEntity.user = user;

    await this.userEcommRepository.save(userEcommEntity);

    return user;
  }

  async updateEcommUser(userEcommUpdateDto: UserEcommUpdateDto, userId: number, ecommId: number): Promise<UserEcomm> {
    const userEcomm = await this.userEcommRepository.findOne({
      where: {
        user: {
          id: userId
        },
        ecomm: {
          id: ecommId
        }
      },
      relations: {
        user: true,
        ecomm: true
      }
    });
    const userEcommEntity = new UserEcomm();

    userEcommEntity.status = userEcommUpdateDto.status;

    await this.userEcommRepository.update(userEcomm.id, userEcommEntity);

    return userEcomm;
  }

  async assignEcommUser(userId: number, ecommId: number) {
    const ecomm = await this.ecommRepository.findOneBy({ id: ecommId });
    const user = await this.userRepository.findOneBy({ id: userId });
    const userEcommEntity = new UserEcomm();

    userEcommEntity.ecomm = ecomm;
    userEcommEntity.user = user;

    return this.userEcommRepository.save(userEcommEntity);
  }

  async getUserVeriffs(userId: number): Promise<UserVeriff[]> {
    return this.userVeriffRepository.findBy({
      user: {
        id: userId,
      },
    });
  }

  async getUserVeriff(userId: number, veriffId: number): Promise<UserVeriff> {
    return this.userVeriffRepository.findOneBy({
      id: veriffId,
      user: {
        id: userId,
      },
    });
  }

  async createUserVeriff(userVeriffCreateDto: UserVeriffCreateDto, user: User): Promise<UserVeriff> {
    const userVeriffEntity = new UserVeriff();

    userVeriffEntity.user = user;
    userVeriffEntity.decision = userVeriffCreateDto.decision;
    userVeriffEntity.documentType = userVeriffCreateDto.documentType;
    userVeriffEntity.sessionId = userVeriffCreateDto.sessionId;

    const userVeriff = await this.userVeriffRepository.save(userVeriffEntity);

    return userVeriff;
  }

  async patchUserVeriff(userVeriffUpdateDto: UserVeriffUpdateDto, veriffId: number): Promise<User> {
    await this.userVeriffRepository.update(veriffId, userVeriffUpdateDto);

    return await this.userRepository.findOne({
      where: {
        id: userVeriffUpdateDto.userId
      },
      relations: {
        ecomms: true,
        veriffs: true
      }
    });
  }
}
