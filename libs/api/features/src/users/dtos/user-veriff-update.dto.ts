import { ApiProperty } from '@nestjs/swagger';

import { IUserVeriff } from '@verifsystem/shared/data-access';

export class UserVeriffUpdateDto implements IUserVeriff {
  id: number;

  @ApiProperty()
  sessionId: string;

  @ApiProperty()
  documentType: string;

  @ApiProperty()
  decision: string;

  datetime: Date;

  userId?: number;
}
