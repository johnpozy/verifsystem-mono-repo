import { ApiProperty } from '@nestjs/swagger';

import { IUser, EUserRole, EUserStatus } from '@verifsystem/shared/data-access';

export class UserCreateDto implements IUser {
  id: number;

  @ApiProperty({
    enum: EUserRole,
    enumName: 'UserRole',
  })
  role: EUserRole;

  @ApiProperty()
  displayName: string;

  @ApiProperty()
  firstName: string;

  @ApiProperty()
  lastName: string;

  @ApiProperty()
  password: string;

  @ApiProperty()
  email: string;

  @ApiProperty()
  phoneNumber: string;

  @ApiProperty()
  postcode: any;

  lastLogin: Date;

  createdAt: Date;

  @ApiProperty({
    enum: EUserStatus,
    enumName: 'UserStatus',
  })
  status: EUserStatus;
}
