import { ApiProperty } from '@nestjs/swagger';

import { EUserEcommStatus, IEcomm, IUser, IUserEcomm } from '@verifsystem/shared/data-access';

export class UserEcommCreateDto implements IUserEcomm {
  id: number;

  @ApiProperty({
    enum: EUserEcommStatus,
    enumName: 'UserEcommStatus',
  })
  status: EUserEcommStatus;

  ecomm?: IEcomm;

  externalId: string;

  createdAt: Date;

  user?: IUser;
}
