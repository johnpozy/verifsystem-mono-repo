import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';

import { IUserVeriff } from '@verifsystem/shared/data-access';

import { User } from './user.entity';

@Entity()
export class UserVeriff implements IUserVeriff {
  @PrimaryGeneratedColumn('increment')
  id: number;

  @Column()
  sessionId: string;

  @Column({
    type: 'tinytext',
    nullable: true
  })
  documentType: string;

  @Column({ type: 'tinytext' })
  decision: string;

  @Column({
    type: 'timestamp',
    default: () => 'CURRENT_TIMESTAMP'
  })
  datetime: Date;

  @ManyToOne(() => User, (user) => user.veriffs, {
    onDelete: 'CASCADE'
  })
  user: User;
}
