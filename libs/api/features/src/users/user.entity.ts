import { BeforeInsert, Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { hash } from 'bcrypt';

import { Card } from '../cards/card.entity';

import { IUser, EUserRole, EUserStatus, IUserEcomm } from '@verifsystem/shared/data-access';

import { UserEcomm } from './user-ecomm.entity';
import { UserVeriff } from './user-veriff.entity';
import { Transaction } from '../transactions/transaction.entity';

@Entity()
export class User implements IUser {
  @PrimaryGeneratedColumn('increment')
  id: number;

  @Column({
    type: 'enum',
    default: EUserRole.CUSTOMER,
    enum: EUserRole,
  })
  role: EUserRole;

  @Column({
    type: 'tinytext',
    nullable: true
  })
  displayName: string;

  @Column({
    type: 'tinytext',
    nullable: true,
  })
  firstName: string;

  @Column({
    type: 'tinytext',
    nullable: true,
  })
  lastName: string;

  @Column({
    type: 'longtext',
    nullable: true
  })
  password: string;

  @Column({
    nullable: false,
    unique: true
  })
  email: string;

  @Column({
    type: 'tinytext',
    nullable: true,
  })
  phoneNumber: string;

  // TODO: to discuss on this later
  @Column({
    type: 'json',
    default: null
  })
  postcode: any;

  @Column({
    type: 'timestamp',
    nullable: true,
  })
  lastLogin: Date;

  @Column({
    type: 'timestamp',
    default: () => 'CURRENT_TIMESTAMP',
  })
  createdAt: Date;

  @Column({
    type: 'enum',
    default: EUserStatus.ACTIVATED,
    enum: EUserStatus,
  })
  status: EUserStatus;

  @OneToMany(() => UserEcomm, (userEcomm) => userEcomm.user, {
    onDelete: 'CASCADE'
  })
  ecomms: IUserEcomm[];

  @OneToMany(() => Card, (card) => card.user, {
    onDelete: 'CASCADE'
  })
  cards: Card[];

  @OneToMany(() => UserVeriff, (veriff) => veriff.user, {
    onDelete: 'CASCADE'
  })
  veriffs: UserVeriff[];

  @OneToMany(() => Transaction, (transaction) => transaction.user, {
    onDelete: 'CASCADE'
  })
  transactions: Transaction[];

  @BeforeInsert() async hashPassword() {
    if (!this.password) {
      this.password = await hash('password', 10);
    }
  }
}
