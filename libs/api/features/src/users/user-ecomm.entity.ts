import { Column, Entity, ManyToOne, PrimaryGeneratedColumn, Unique } from 'typeorm';

import { EUserEcommStatus, IUserEcomm } from '@verifsystem/shared/data-access';

import { Ecomm } from '../ecomms/ecomm.entity';
import { User } from './user.entity';

@Entity()
@Unique('unique_ecom_user', ['ecomm', 'user', 'externalId'])
export class UserEcomm implements IUserEcomm {
  @PrimaryGeneratedColumn('increment')
  id: number;

  @Column({ nullable: true })
  externalId: string;

  @Column({
    type: 'timestamp',
    default: () => 'CURRENT_TIMESTAMP',
  })
  createdAt: Date;

  @Column({
    type: 'enum',
    default: EUserEcommStatus.ACTIVATED,
    enum: EUserEcommStatus,
  })
  status: EUserEcommStatus;

  @ManyToOne(() => Ecomm, (ecomm) => ecomm.users, {
    cascade: true,
    onDelete: 'CASCADE'
  })
  ecomm: Ecomm;

  @ManyToOne(() => User, (user) => user.ecomms, {
    cascade: true,
    onDelete: 'CASCADE'
  })
  user: User;
}
