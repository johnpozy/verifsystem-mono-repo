import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { Ecomm } from '../ecomms/ecomm.entity';
import { User } from './user.entity';
import { UserEcomm } from './user-ecomm.entity';
import { UserVeriff } from './user-veriff.entity';
import { UsersService } from './users.service';

@Module({
  imports: [TypeOrmModule.forFeature([User, Ecomm, UserEcomm, UserVeriff])],
  providers: [UsersService],
  exports: [UsersService]
})
export class UsersModule {}
