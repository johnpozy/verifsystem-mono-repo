import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { Transaction } from '../transactions/transaction.entity';
import { ReportsService } from './reports.service';

@Module({
  imports: [TypeOrmModule.forFeature([Transaction])],
  providers: [ReportsService],
  exports: [ReportsService]
})
export class ReportsModule {}
