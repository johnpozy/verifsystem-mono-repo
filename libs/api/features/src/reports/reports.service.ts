import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Between, In, LessThan, MoreThan, Not, Repository } from 'typeorm';
import { formatISO, eachDayOfInterval, format } from 'date-fns';

import { ETransactionPaymentMethod, ETransactionStatus } from '@verifsystem/shared/data-access';

import { Transaction } from '../transactions/transaction.entity';

@Injectable()
export class ReportsService {
  constructor(@InjectRepository(Transaction) private transactionRepository: Repository<Transaction>) {}

  async countOrders(query) {
    let betweenDateQuery = {};
    const { startDate, endDate } = query;

    if (startDate && endDate) {
      betweenDateQuery = {
        createdAt: Between(formatISO(new Date(startDate), { format: 'extended' }), formatISO(new Date(endDate), { format: 'extended' })),
      };
    } else {
      if (startDate) {
        betweenDateQuery = {
          createdAt: MoreThan(formatISO(new Date(startDate), { format: 'extended' })),
        };
      }

      if (endDate) {
        betweenDateQuery = {
          createdAt: LessThan(formatISO(new Date(endDate), { format: 'extended' })),
        };
      }
    }

    const total = await this.transactionRepository.countBy({ ...betweenDateQuery });
    const creditcard = await this.transactionRepository.countBy({
      paymentMethod: ETransactionPaymentMethod['Smartpayment Gateway'],
      ...betweenDateQuery,
    });
    const failed = await this.transactionRepository.countBy({
      status: ETransactionStatus.Failed,
      ...betweenDateQuery,
    });
    const disputed = await this.transactionRepository.countBy({
      status: ETransactionStatus.Dispute,
      ...betweenDateQuery,
    });
    const successful = await this.transactionRepository.countBy({
      status: Not(In([ETransactionStatus.Failed, ETransactionStatus.Dispute])),
      ...betweenDateQuery,
    });

    return {
      total: +total,
      creditcard: +creditcard,
      failed: +failed,
      disputed: +disputed,
      successful: +successful,
    };
  }

  async calculateSales(query) {
    const { startDate, endDate } = query;

    const grossQuery = this.transactionRepository.createQueryBuilder('transaction').select('SUM(transaction.amount)', 'sumAmount');

    const creditCardQuery = this.transactionRepository
      .createQueryBuilder('transaction')
      .select('SUM(transaction.amount)', 'sumAmount')
      .where('transaction.paymentMethod = :paymentMethod', { paymentMethod: ETransactionPaymentMethod['Smartpayment Gateway'] });

    const refundedQuery = this.transactionRepository
      .createQueryBuilder('transaction')
      .select('SUM(transaction.amount)', 'sumAmount')
      .where('transaction.status = :status', { status: ETransactionStatus.Refunded });

    const chargebackQuery = this.transactionRepository
      .createQueryBuilder('transaction')
      .select('SUM(transaction.amount)', 'sumAmount')
      .where('transaction.status = :status', { status: ETransactionStatus['On Hold'] });

    if (startDate) {
      grossQuery.andWhere('transaction.createdAt >= :startDate', {
        startDate: formatISO(new Date(startDate), { format: 'extended' }),
      });

      creditCardQuery.andWhere('transaction.createdAt >= :startDate', {
        startDate: formatISO(new Date(startDate), { format: 'extended' }),
      });

      refundedQuery.andWhere('transaction.createdAt >= :startDate', {
        startDate: formatISO(new Date(startDate), { format: 'extended' }),
      });

      chargebackQuery.andWhere('transaction.createdAt >= :startDate', {
        startDate: formatISO(new Date(startDate), { format: 'extended' }),
      });
    }

    if (endDate) {
      grossQuery.andWhere('transaction.createdAt <= :endDate', {
        endDate: formatISO(new Date(endDate), { format: 'extended' }),
      });

      creditCardQuery.andWhere('transaction.createdAt <= :endDate', {
        endDate: formatISO(new Date(endDate), { format: 'extended' }),
      });

      refundedQuery.andWhere('transaction.createdAt <= :endDate', {
        endDate: formatISO(new Date(endDate), { format: 'extended' }),
      });

      chargebackQuery.andWhere('transaction.createdAt <= :endDate', {
        endDate: formatISO(new Date(endDate), { format: 'extended' }),
      });
    }

    const { sumAmount: gross } = await grossQuery.getRawOne();
    const { sumAmount: creditcard } = await creditCardQuery.getRawOne();
    const { sumAmount: refunded } = await refundedQuery.getRawOne();
    const { sumAmount: chargeback } = await chargebackQuery.getRawOne();

    return {
      gross: +gross,
      creditcard: +creditcard,
      refunded: +refunded,
      chargeback: +chargeback,
    };
  }

  async export(query) {
    let betweenDateQuery = {};
    const { startDate, endDate } = query;

    if (startDate && endDate) {
      betweenDateQuery = {
        createdAt: Between(formatISO(new Date(startDate), { format: 'extended' }), formatISO(new Date(endDate), { format: 'extended' })),
      };
    } else {
      if (startDate) {
        betweenDateQuery = {
          createdAt: MoreThan(formatISO(new Date(startDate), { format: 'extended' })),
        };
      }

      if (endDate) {
        betweenDateQuery = {
          createdAt: LessThan(formatISO(new Date(endDate), { format: 'extended' })),
        };
      }
    }

    return this.transactionRepository.find({
      where: betweenDateQuery,
      relations: {
        ecomm: true,
        gatewayConfig: true,
      },
    });
  }

  async chart(query) {
    let betweenDateQuery = {};
    const { startDate, endDate } = query;

    if (startDate && endDate) {
      betweenDateQuery = {
        createdAt: Between(formatISO(new Date(startDate), { format: 'extended' }), formatISO(new Date(endDate), { format: 'extended' })),
      };
    } else {
      if (startDate) {
        betweenDateQuery = {
          createdAt: MoreThan(formatISO(new Date(startDate), { format: 'extended' })),
        };
      }

      if (endDate) {
        betweenDateQuery = {
          createdAt: LessThan(formatISO(new Date(endDate), { format: 'extended' })),
        };
      }
    }

    const transactions = await this.transactionRepository.find({
      where: betweenDateQuery,
    });

    return eachDayOfInterval({
      start: new Date(startDate),
      end: new Date(endDate),
    }).map((date) => {
      const formattedDate = format(date, 'yyyy-MM-dd');

      return {
        date: format(date, 'yyyy-MM-dd'),
        grossSale: this.sumAmount(transactions.filter((transaction) => format(transaction.createdAt, 'yyyy-MM-dd') === formattedDate)),
        creditcardSale: this.sumAmount(
          transactions.filter((transaction) => {
            return (
              format(transaction.createdAt, 'yyyy-MM-dd') === formattedDate &&
              transaction.paymentMethod === ETransactionPaymentMethod['Smartpayment Gateway']
            );
          })
        ),
        grossOrder: transactions.filter((transaction) => format(transaction.createdAt, 'yyyy-MM-dd') === formattedDate).length,
        creditcardOrder: transactions.filter((transaction) => {
          return (
            format(transaction.createdAt, 'yyyy-MM-dd') === formattedDate &&
            transaction.paymentMethod === ETransactionPaymentMethod['Smartpayment Gateway']
          );
        }).length,
      };
    });
  }

  sumAmount(transactions: Transaction[]) {
    if (transactions.length > 0) {
      return transactions.map((d) => d.amount).reduce((prev, next) => prev + next) / 100;
    }

    return 0;
  }
}
