import * as crypto from 'crypto';

const ALGORITHM = 'aes-256-ctr';
const SECRET = 'lorem-ipsum-dolor';

export const encryptCard = (number, cvc) => {
  const iv = crypto.randomBytes(16);
  const secretKey = crypto.createHash('md5').update(SECRET).digest('hex');
  const cipher = crypto.createCipheriv(ALGORITHM, Buffer.from(secretKey), iv);
  const encrypted = cipher.update(JSON.stringify({ number, cvc }));

  return { iv: iv.toString('hex'), hash: encrypted.toString('hex') };
};

export const decryptCard = (cardHash): { number: string; cvc: number } => {
  const iv = Buffer.from(cardHash.iv, 'hex');
  const encryptedText = Buffer.from(cardHash.hash, 'hex');
  const secretKey = crypto.createHash('md5').update(SECRET).digest('hex');
  const decipher = crypto.createDecipheriv(ALGORITHM, Buffer.from(secretKey), iv);
  let decrypted = decipher.update(encryptedText);
  decrypted = Buffer.concat([decrypted, decipher.final()]);

  const result = JSON.parse(decrypted.toString());
  result.number = result.number.replace(/\s/g, '');
  result.cvc = +result.cvc;

  return result;
};

export const paginate = (arr, size): Array<any> => {
  return arr.reduce((acc, val, i) => {
    const idx = Math.floor(i / size);
    const page = acc[idx] || (acc[idx] = []);
    page.push(val);

    return acc;
  }, []);
};
