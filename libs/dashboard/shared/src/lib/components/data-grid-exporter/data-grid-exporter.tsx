import { useState } from 'react';
import {
  Button,
  ListItemIcon,
  Menu,
  MenuItem,
  Typography,
} from '@mui/material';
import {
  DownloadOutline,
  FileDelimitedOutline,
  FileExcelOutline,
  RedoVariant,
} from 'mdi-material-ui';

import './data-grid-exporter.module.scss';

/* eslint-disable-next-line */
export interface DataGridExporterProps {}

export function DataGridExporter(props: DataGridExporterProps) {
  const [anchorEl, setAnchorEl] = useState(null);
  const open = Boolean(anchorEl);

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  return (
    <>
      <Button
        onClick={handleClick}
        startIcon={<RedoVariant sx={{ color: 'text.secondary' }} />}
        color="inherit"
      >
        Export To
      </Button>
      <Menu
        anchorEl={anchorEl}
        open={open}
        onClose={handleClose}
        anchorOrigin={{
          vertical: 'bottom',
          horizontal: 'right',
        }}
        transformOrigin={{
          vertical: 'top',
          horizontal: 'right',
        }}
        sx={{
          '& .MuiPaper-root': {
            minWidth: 140,
          },
        }}
      >
        <MenuItem>
          <ListItemIcon>
            <FileDelimitedOutline fontSize="small" />
          </ListItemIcon>
          <Typography variant="inherit" noWrap>
            CSV
          </Typography>
        </MenuItem>
        <MenuItem>
          <ListItemIcon>
            <FileExcelOutline fontSize="small" />
          </ListItemIcon>
          <Typography variant="inherit" noWrap>
            Excel
          </Typography>
        </MenuItem>
      </Menu>
    </>
  );
}

export default DataGridExporter;
