import { render } from '@testing-library/react';

import DataGridExporter from './data-grid-exporter';

describe('DataGridExporter', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<DataGridExporter />);
    expect(baseElement).toBeTruthy();
  });
});
