import { Box, Divider } from '@mui/material';

/* eslint-disable-next-line */
export interface PageHeaderProps {
  children: JSX.Element;
}

export const PageHeader = (props: PageHeaderProps) => {
  return (
    <>
      <Box
        p={3}
        sx={{ display: 'flex', backgroundColor: 'white', minHeight: '82px' }}
        alignItems="center"
      >
        {props.children}
      </Box>
      <Divider />
    </>
  );
};

export default PageHeader;
