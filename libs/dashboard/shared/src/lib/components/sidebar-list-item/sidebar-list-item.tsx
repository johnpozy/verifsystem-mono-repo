import { useState } from 'react';
import {
  Avatar,
  Collapse,
  List,
  ListItemButton,
  ListItemIcon,
  ListItemText,
} from '@mui/material';
import { MenuDown, MenuUp } from 'mdi-material-ui';

import { useRouter } from 'next/router';
import { pink } from '@mui/material/colors';

interface SidebarListItemProps {
  icon?: JSX.Element;
  text: string;
  children?: any;
  disabled?: boolean;
  offsetLeft?: number;
  path?: string;
  active?: boolean;
}

/**
 * SidebarListItem
 *
 * @param props
 * @returns JSX.Element
 */
export const SidebarListItem = (props: SidebarListItemProps) => {
  const { icon, text, children, offsetLeft, path, active, disabled } = props;
  const [open, setOpen] = useState(false);
  const router = useRouter();

  const handleClick = () => {
    if (path) {
      router.push(path);
    } else {
      setOpen(!open);
    }
  };

  const renderArrow = () => {
    if (children) {
      return open ? <MenuUp /> : <MenuDown />;
    }

    return null;
  };

  return (
    <>
      <ListItemButton
        onClick={handleClick}
        disabled={disabled}
        sx={{
          borderRadius: 1,
          pl: offsetLeft ? offsetLeft : null,
          bgcolor: active ? 'rgba(236, 64, 122, 0.2)' : null,
        }}
      >
        {icon && (
          <ListItemIcon>
            <Avatar
              variant="rounded"
              sx={{
                bgcolor: active ? pink[400] : 'primary.contrastText',
                color: active ? 'primary.contrastText' : 'primary.main',
                width: 32,
                height: 32,
              }}
            >
              {icon}
            </Avatar>
          </ListItemIcon>
        )}
        <ListItemText
          primary={text}
          sx={{ color: active ? pink[50] : 'primary.contrastText' }}
        />
        {renderArrow()}
      </ListItemButton>
      {children && (
        <Collapse in={open} timeout="auto" unmountOnExit>
          <List component="div" disablePadding>
            {children}
          </List>
        </Collapse>
      )}
    </>
  );
};

export default SidebarListItem;
