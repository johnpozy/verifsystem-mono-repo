import { render } from '@testing-library/react';

import DataGridFooter from './data-grid-footer';

describe('DataGridFooter', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<DataGridFooter />);
    expect(baseElement).toBeTruthy();
  });
});
