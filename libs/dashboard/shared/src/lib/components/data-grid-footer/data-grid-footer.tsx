import {
  Grid,
  Pagination,
  PaginationItem,
  Divider,
  Typography,
} from '@mui/material';
import {
  gridPageCountSelector,
  gridPageSelector,
  useGridApiContext,
  useGridSelector,
  gridPageSizeSelector,
  gridRowCountSelector,
} from '@mui/x-data-grid';
import { ArrowLeft, ArrowRight } from 'mdi-material-ui';

export const DataGridFooter = () => {
  const apiRef = useGridApiContext();
  const page = useGridSelector(apiRef, gridPageSelector);
  const pageCount = useGridSelector(apiRef, gridPageCountSelector);
  const pageSize = useGridSelector(apiRef, gridPageSizeSelector);
  const rowCount = useGridSelector(apiRef, gridRowCountSelector);

  return (
    <>
      <Divider />
      <Grid p={2} container columnSpacing={3} alignItems="center">
        <Grid item xs="auto">
          <Typography>Total rows {rowCount}</Typography>
        </Grid>
        <Grid item xs />
        {/* <Grid item xs={1.2}>
          <FormControl fullWidth size="small">
            <InputLabel>Row per page</InputLabel>
            <Select label="Row per page">
              <MenuItem value={25}>25</MenuItem>
              <MenuItem value={50}>50</MenuItem>
              <MenuItem value={50}>100</MenuItem>
            </Select>
          </FormControl>
        </Grid> */}
        <Grid item xs="auto">
          <Pagination
            color="primary"
            shape="rounded"
            page={page + 1}
            count={pageCount}
            renderItem={(item) => (
              <PaginationItem
                components={{ previous: ArrowLeft, next: ArrowRight }}
                {...item}
              />
            )}
            onChange={(event: React.ChangeEvent<unknown>, value: number) =>
              apiRef.current.setPage(value - 1)
            }
          />
        </Grid>
      </Grid>
    </>
  );
};

export default DataGridFooter;
