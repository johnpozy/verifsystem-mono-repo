import React, { useEffect, useState } from 'react';
import { useRouter } from 'next/router';
import {
  AppBar,
  Avatar,
  Badge,
  Divider,
  IconButton,
  LinearProgress,
  List,
  ListItem,
  ListItemAvatar,
  ListItemIcon,
  ListItemText,
  Menu,
  MenuItem,
  Toolbar,
  Tooltip,
  Typography,
} from '@mui/material';
import { Box } from '@mui/system';
import { Power, ArrowLeft, Email, HandCoinOutline } from 'mdi-material-ui';

import styles from './header.module.scss';

export interface HeaderProps {
  drawerWidth: number;
  title: string | number;
}

/**
 * Header
 *
 * @param {HeaderProps} props
 * @returns JSX.Element
 */
export const Header = (props: HeaderProps) => {
  const { drawerWidth, title } = props;
  const [anchorProfileEl, setProfileAnchorEl] = useState(null);
  const [anchorNotificationEl, setNotificationAnchorEl] = useState(null);
  const [showProgressBar, setShowProgressBar] = useState(false);
  const openProfile = Boolean(anchorProfileEl);
  const openNotification = Boolean(anchorNotificationEl);
  const router = useRouter();

  const handleProfileClick = (event) => {
    setProfileAnchorEl(event.currentTarget);
  };

  const handleProfileClose = () => {
    setProfileAnchorEl(null);
  };

  const handleNotificationClick = (event) => {
    setNotificationAnchorEl(event.currentTarget);
  };

  const handleNotificationClose = () => {
    setNotificationAnchorEl(null);
  };

  const goBack = () => {
    router.back();
  };

  useEffect(() => {
    const handleStart = () => {
      setShowProgressBar(true);
    }

    const handleStop = () => {
      setShowProgressBar(false);
    }

    router.events.on('routeChangeStart', handleStart)
    router.events.on('routeChangeComplete', handleStop)
    router.events.on('routeChangeError', handleStop)

    return () => {
      router.events.off('routeChangeStart', handleStart)
      router.events.off('routeChangeComplete', handleStop)
      router.events.off('routeChangeError', handleStop)
    }
  }, [router]);

  return (
    <AppBar
      className={styles.appBar}
      elevation={0}
      position="fixed"
      color="inherit"
      sx={{
        width: { sm: `calc(100% - ${drawerWidth}px)` },
        ml: { sm: `${drawerWidth}px` },
      }}
    >
      <Toolbar id="app-bar-toolbar">
        <IconButton
          color="inherit"
          aria-label="open drawer"
          edge="start"
          sx={{ mr: 2 }}
          onClick={goBack}
        >
          <ArrowLeft color="primary" />
        </IconButton>
        <Typography variant="h6" noWrap component="div" sx={{ flexGrow: 1 }}>
          {title}
        </Typography>
        <Typography variant="h5" noWrap component="div" mr={3}>$544.12</Typography>
        <Divider orientation='vertical' flexItem />
        <Box mx={2}>
          <Tooltip title="Notifications">
            <IconButton onClick={handleNotificationClick}>
              <Badge badgeContent={2} color="error">
                <Email />
              </Badge>
            </IconButton>
          </Tooltip>
          <Menu
            anchorEl={anchorNotificationEl}
            open={openNotification}
            onClose={handleNotificationClose}
            onClick={handleNotificationClose}
            PaperProps={{
              elevation: 0,
              sx: {
                overflow: 'visible',
                filter: 'drop-shadow(0px 2px 6px rgba(0, 0, 0, 0.2))',
                mt: 1.5,
                '&:before': {
                  content: '""',
                  display: 'block',
                  position: 'absolute',
                  top: 0,
                  right: 20,
                  width: 10,
                  height: 10,
                  bgcolor: 'background.paper',
                  transform: 'translateY(-50%) rotate(45deg)',
                  zIndex: 0,
                },
              },
            }}
            transformOrigin={{ horizontal: 'right', vertical: 'top' }}
            anchorOrigin={{ horizontal: 'right', vertical: 'bottom' }}
          >
            <List sx={{ width: '100%', maxWidth: 360, bgcolor: 'background.paper' }}>
              <ListItem>
                <ListItemAvatar>
                  <Avatar>
                    <HandCoinOutline />
                  </Avatar>
                </ListItemAvatar>
                <ListItemText primary="Pending Payout" secondary={
                  <React.Fragment>
                    <Typography
                      sx={{ display: 'inline' }}
                      component="span"
                      variant="body2"
                      color="text.primary"
                    >
                      budcargo.net
                    </Typography>
                    {" — You have pending payout"}
                  </React.Fragment>
                } />
              </ListItem>
              <ListItem>
                <ListItemAvatar>
                  <Avatar>
                    <HandCoinOutline />
                  </Avatar>
                </ListItemAvatar>
                <ListItemText primary="Pending Payout" secondary={
                  <React.Fragment>
                    <Typography
                      sx={{ display: 'inline' }}
                      component="span"
                      variant="body2"
                      color="text.primary"
                    >
                      wbud.co
                    </Typography>
                    {" — You have pending payout"}
                  </React.Fragment>
                } />
              </ListItem>
            </List>
          </Menu>
        </Box>
        <Box sx={{ flexGrow: 0 }}>
          <Tooltip title="Account settings">
            <IconButton
              onClick={handleProfileClick}
              size="small"
              sx={{ ml: 2 }}
              aria-controls={openProfile ? 'account-menu' : undefined}
              aria-haspopup="true"
              aria-expanded={openProfile ? 'true' : undefined}
            >
              <Avatar
                alt="Travis Howard"
                variant="rounded"
              />
            </IconButton>
          </Tooltip>
          <Menu
            anchorEl={anchorProfileEl}
            open={openProfile}
            onClose={handleProfileClose}
            onClick={handleProfileClose}
            PaperProps={{
              elevation: 0,
              sx: {
                overflow: 'visible',
                filter: 'drop-shadow(0px 2px 6px rgba(0, 0, 0, 0.2))',
                mt: 1.5,
                '&:before': {
                  content: '""',
                  display: 'block',
                  position: 'absolute',
                  top: 0,
                  right: 20,
                  width: 10,
                  height: 10,
                  bgcolor: 'background.paper',
                  transform: 'translateY(-50%) rotate(45deg)',
                  zIndex: 0,
                },
              },
            }}
            transformOrigin={{ horizontal: 'right', vertical: 'top' }}
            anchorOrigin={{ horizontal: 'right', vertical: 'bottom' }}
          >
            <MenuItem onClick={() => router.push('/auth/logout')}>
              <ListItemIcon>
                <Power color="error" />
              </ListItemIcon>
              Logout
            </MenuItem>
          </Menu>
        </Box>
      </Toolbar>

      { showProgressBar && <LinearProgress sx={{ height: '2px' }} /> }
    </AppBar>
  );
};

export default Header;
