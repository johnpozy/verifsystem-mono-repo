import { useEffect, useState } from 'react';
import { useRouter } from 'next/router';
import Image from 'next/image';
import {
  Drawer,
  List,
  Typography,
  Avatar,
  Box,
  Menu,
  MenuItem,
  ListItem,
  ListItemAvatar,
  ListItemText,
  IconButton,
  Divider,
  ListItemIcon,
  Snackbar,
  Alert,
} from '@mui/material';
import {
  Home,
  SwapHorizontalBold,
  ChartBoxOutline,
  AccountMultipleOutline,
  Earth,
  MenuUp,
  Plus,
  AtomVariant,
  RotateRight,
  WalletOutline,
  TuneVertical,
} from 'mdi-material-ui';
import { blue, pink } from '@mui/material/colors';

import {
  useAppDispatch,
  useAppSelector,
  selectEcomm,
  stateSelectedEcomm,
  useGetEcommsQuery,
  stateLoggedUser,
} from '@verifsystem/dashboard/data-access';
import { DialogEcommCreate } from '@verifsystem/dashboard/shared';
import { EUserRole, IEcomm } from '@verifsystem/shared/data-access';

import { SidebarListItem } from '../sidebar-list-item/sidebar-list-item';
import styles from './sidebar.module.scss';
import { accessControl } from '@verifsystem/shared/utils';

export interface SidebarProps {
  width: number;
  window?: () => Window;
}

/**
 * Sidebar
 *
 * @param {SidebarProps} props
 * @returns JSX.Element
 */
export const Sidebar = (props: SidebarProps) => {
  const dispatch = useAppDispatch();
  const { data: ecomms = [] } = useGetEcommsQuery();
  const selectedEcomm = useAppSelector(stateSelectedEcomm);
  const loggedUser = useAppSelector(stateLoggedUser);
  const { width } = props;
  const [anchorEl, setAnchorEl] = useState(null);
  const open = Boolean(anchorEl);
  const [dialogOpen, setDialogOpen] = useState(false);
  const [snackbarOpen, setSnackbarOpen] = useState(false);
  const router = useRouter();

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = (ecomm?: IEcomm) => {
    setAnchorEl(null);
    dispatch(selectEcomm(ecomm));
  };

  const handleDialogOpen = () => {
    setDialogOpen(true);
    setAnchorEl(null);
  };

  const handleDialogClose = () => {
    setDialogOpen(false);
  };

  const handleSnackbarClose = (event?: React.SyntheticEvent | Event, reason?: string) => {
    if (reason === 'clickaway') {
      return;
    }

    setSnackbarOpen(false);
  };

  const renderSiteSelectorMenu = () => {
    return (
      <Menu
        anchorEl={anchorEl}
        open={open}
        onClose={() => handleClose(selectedEcomm)}
        anchorOrigin={{
          vertical: 'top',
          horizontal: 'right',
        }}
        transformOrigin={{
          vertical: 'bottom',
          horizontal: 'right',
        }}
        PaperProps={{
          style: {
            maxHeight: 240,
            width: width - 32,
          },
          sx: {
            '& .MuiAvatar-root': {
              width: 32,
              height: 32,
              ml: -0.5,
              mr: 1,
            },
          },
        }}
      >
        {loggedUser?.role === EUserRole.PARTNER
          ? loggedUser?.ecomms.map((userEcomms) => (
              <MenuItem key={userEcomms.ecomm.id} onClick={() => handleClose(userEcomms.ecomm)}>
                <Avatar variant="rounded">
                  <Earth />
                </Avatar>
                {userEcomms.ecomm.title}
              </MenuItem>
            ))
          : [
              <MenuItem onClick={() => handleClose(null)}>All Ecomm</MenuItem>,
              <Divider />,
              ecomms &&
                ecomms.length > 0 &&
                ecomms.map((ecomm) => (
                  <MenuItem key={ecomm.id} onClick={() => handleClose(ecomm)}>
                    <Avatar variant="rounded">
                      <Earth />
                    </Avatar>
                    {ecomm.title}
                  </MenuItem>
                )),
            ]}
        <Divider />
        <MenuItem onClick={handleDialogOpen}>
          <ListItemIcon>
            <Plus />
          </ListItemIcon>
          Add New
        </MenuItem>
      </Menu>
    );
  };

  const renderSiteSelector = () => {
    return (
      <>
        <List>
          <ListItem
            secondaryAction={
              <IconButton edge="end" color="inherit" onClick={handleClick}>
                <MenuUp />
              </IconButton>
            }
          >
            <ListItemAvatar>
              <Avatar sx={{ bgcolor: pink[400], width: 46, height: 46 }} variant="rounded">
                <Earth />
              </Avatar>
            </ListItemAvatar>
            <ListItemText primary={!selectedEcomm ? 'All Ecomm' : selectedEcomm?.title} />
          </ListItem>
        </List>
        {renderSiteSelectorMenu()}
      </>
    );
  };

  const drawer = (
    <>
      <Box className={styles.logo} px={3} py={3}>
        <Avatar sx={{ width: 48, height: 48, color: 'primary.main' }} variant="rounded" className={styles.logoFigure}>
          <Image loader={({ src }) => src} src="/img/logo.svg" placeholder="empty" objectFit="cover" quality={100} width={38} height={38} />
        </Avatar>
        <Box>
          <Typography variant="h5" noWrap component="div" className={styles.logoText} sx={{ color: 'primary.contrastText' }}>
            VerifSystem.
          </Typography>
        </Box>
      </Box>

      <Box px={2} mb={3}>
        <Typography variant="overline" color="primary.contrastText" pl={2}>
          Main Menu
        </Typography>
        <List>
          {accessControl.can(loggedUser?.role).readAny('dashboard').granted && (
            <SidebarListItem icon={<Home />} text="Home" path="/dashboard" active={router.pathname === '/dashboard'} />
          )}
          {accessControl.can(loggedUser?.role).readAny('payouts').granted && (
            <SidebarListItem icon={<WalletOutline />} text="Payouts" path="/payout" active={router.pathname === '/payout'} />
          )}
          {accessControl.can(loggedUser?.role).readAny('transactions').granted && (
            <SidebarListItem icon={<SwapHorizontalBold />} text="Transactions" path="/transaction" active={router.pathname === '/transaction'} />
          )}
          {accessControl.can(loggedUser?.role).readAny('reports').granted && (
            <SidebarListItem icon={<ChartBoxOutline />} text="Reports" path="/report" active={router.pathname === '/report'} />
          )}
          {accessControl.can(loggedUser?.role).readAny('customers').granted && (
            <SidebarListItem icon={<AccountMultipleOutline />} text="Customers" path="/customer" active={router.pathname === '/customer'} />
          )}
          {accessControl.can(loggedUser?.role).readAny('processors').granted && (
            <SidebarListItem icon={<AtomVariant />} text="Processors" path="/processor" active={router.pathname === '/processor'} />
          )}
          {accessControl.can(loggedUser?.role).readAny('disputes').granted && (
            <SidebarListItem icon={<RotateRight />} text="Disputes" path="/dispute" active={router.pathname === '/dispute'} />
          )}
        </List>
      </Box>

      <Box px={2}>
        <Typography variant="overline" color={blue[100]} pl={2}>
          Other Menu
        </Typography>
        <List>
          <SidebarListItem icon={<TuneVertical />} text="Settings">
            <SidebarListItem text="Import Users" path="/setting/user/import" active={router.pathname === '/setting/user/import'} offsetLeft={9} />
          </SidebarListItem>
        </List>
      </Box>

      <Box px={2} className={styles.siteSelector}>
        {renderSiteSelector()}
      </Box>
    </>
  );

  useEffect(() => {
    if (loggedUser?.role === EUserRole.PARTNER) {
      if (loggedUser?.ecomms.length > 0) {
        dispatch(selectEcomm(loggedUser?.ecomms[0].ecomm));
      }
    } else {
      dispatch(selectEcomm(null));
    }
  }, []);

  return (
    <>
      <Drawer
        variant="permanent"
        sx={{
          display: { xs: 'none', sm: 'block' },
          '& .MuiDrawer-paper': {
            bgcolor: 'primary.main',
            color: '#fff',
            width: width,
            overflowX: 'visible',
            boxShadow: '10px 0 10px -10px rgba(0, 0, 0, 0.2)',
            borderWidth: 0,
            backgroundImage: 'url(img/sidebar-bg.png)',
            backgroundPosition: 'center bottom',
            backgroundBlendMode: 'multiply',
            backgroundSize: '700px',
            backgroundRepeat: 'no-repeat',
          },
        }}
        open
      >
        {drawer}
      </Drawer>

      <DialogEcommCreate open={dialogOpen} onClose={handleDialogClose} />

      <Snackbar
        open={snackbarOpen}
        autoHideDuration={5000}
        anchorOrigin={{
          vertical: 'top',
          horizontal: 'center',
        }}
        onClose={handleSnackbarClose}
      >
        <Alert onClose={handleSnackbarClose} severity="success" sx={{ width: '100%' }} elevation={2}>
          Ecomm created successfully
        </Alert>
      </Snackbar>
    </>
  );
};

export default Sidebar;
