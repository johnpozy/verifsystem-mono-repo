import { Button, Dialog, DialogActions, DialogTitle, Divider, Grid, InputAdornment, TextField, Typography } from '@mui/material';
import { Box } from '@mui/system';
import { LoadingButton } from '@mui/lab';
import { useForm, Controller } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import * as yup from 'yup';

import { useCreateEcommMutation } from '@verifsystem/dashboard/data-access';

/* eslint-disable-next-line */
export interface DialogEcommCreateProps {
  open: boolean;
  onClose: () => void;
}

export function DialogEcommCreate(props: DialogEcommCreateProps) {
  const [createEcomm, { isLoading }] = useCreateEcommMutation();
  const { onClose, open } = props;

  const validationScheme = yup
    .object({
      title: yup.string().required(),
      baseUrl: yup.string().required(),
      contact: yup.object().shape({
        name: yup.string().required(),
        phoneNumber: yup.string().required(),
      }),
    })
    .required();

  const {
    control,
    handleSubmit,
    reset,
    formState: { errors },
  } = useForm({
    defaultValues: {
      title: '',
      baseUrl: '',
      description: '',
      contact: {
        name: '',
        phoneNumber: '',
      },
    },
    resolver: yupResolver(validationScheme),
  });

  const handleClose = () => {
    onClose();
    reset();
  };

  const handleFormSubmit = async (body) => {
    const result = await createEcomm({ body });

    if (result['data']) {
      handleClose();
    }
  };

  return (
    <Dialog onClose={handleClose} open={open} maxWidth="sm" fullWidth>
      <DialogTitle>Create Ecomm</DialogTitle>
      <Divider />
      <form onSubmit={handleSubmit(handleFormSubmit)}>
        <Box sx={{ padding: 3 }}>
          <Grid container spacing={3}>
            <Grid item xs={12}>
              <Controller
                name="title"
                control={control}
                render={({ field }) => (
                  <TextField
                    {...field}
                    label="Title"
                    size="small"
                    fullWidth
                    error={!!errors?.title}
                    helperText={errors.title?.message}
                    InputLabelProps={{
                      shrink: true,
                    }}
                  />
                )}
              />
            </Grid>
            <Grid item xs={12}>
              <Controller
                name="baseUrl"
                control={control}
                render={({ field }) => (
                  <TextField
                    {...field}
                    label="Base URL"
                    size="small"
                    fullWidth
                    error={!!errors?.baseUrl}
                    helperText={errors.baseUrl?.message}
                    InputProps={{
                      startAdornment: <InputAdornment position="start">https://</InputAdornment>,
                    }}
                    InputLabelProps={{
                      shrink: true,
                    }}
                  />
                )}
              />
            </Grid>
            <Grid item xs={12}>
              <Controller
                name="description"
                control={control}
                render={({ field }) => (
                  <TextField
                    {...field}
                    label="Description"
                    multiline
                    rows={4}
                    size="small"
                    fullWidth
                    error={!!errors?.description}
                    helperText={errors.description?.message}
                    InputLabelProps={{
                      shrink: true,
                    }}
                  />
                )}
              />
            </Grid>
            <Grid item xs={12}>
              <Typography color="text.secondary" display="block" variant="overline">
                Contact Detail
              </Typography>
            </Grid>
            <Grid item xs={6}>
              <Controller
                name="contact.name"
                control={control}
                render={({ field }) => (
                  <TextField
                    {...field}
                    label="Name"
                    size="small"
                    fullWidth
                    error={!!errors?.contact?.name}
                    helperText={errors.contact?.name?.message}
                    InputLabelProps={{
                      shrink: true,
                    }}
                  />
                )}
              />
            </Grid>
            <Grid item xs={6}>
              <Controller
                name="contact.phoneNumber"
                control={control}
                render={({ field }) => (
                  <TextField
                    {...field}
                    label="Phone No."
                    size="small"
                    fullWidth
                    error={!!errors?.contact?.phoneNumber}
                    helperText={errors.contact?.phoneNumber?.message}
                    InputLabelProps={{
                      shrink: true,
                    }}
                  />
                )}
              />
            </Grid>
          </Grid>
        </Box>
        <DialogActions>
          <Button onClick={handleClose}>Close</Button>
          <LoadingButton variant="contained" type="submit" loading={isLoading}>
            Save
          </LoadingButton>
        </DialogActions>
      </form>
    </Dialog>
  );
}

export default DialogEcommCreate;
