import { render } from '@testing-library/react';

import DialogEcommCreate from './dialog-ecomm-create';

describe('DialogEcommCreate', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<DialogEcommCreate />);
    expect(baseElement).toBeTruthy();
  });
});
