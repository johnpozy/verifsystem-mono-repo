import { useState } from 'react';
import {
  Box,
  IconButton,
  FormControl,
  Badge,
  InputAdornment,
  InputLabel,
  Menu,
  MenuItem,
  OutlinedInput,
  Select,
} from '@mui/material';
import { FilterVariant, Magnify } from 'mdi-material-ui';

import './data-grid-filter.module.scss';

/* eslint-disable-next-line */
export interface DataGridFilterProps {}

export function DataGridFilter(props: DataGridFilterProps) {
  const [anchorEl, setAnchorEl] = useState(null);
  const open = Boolean(anchorEl);

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  return (
    <>
      <IconButton onClick={handleClick} color="inherit">
        <Badge badgeContent={2} color="info">
          <FilterVariant sx={{ color: 'text.secondary' }} />
        </Badge>
      </IconButton>

      <Menu
        anchorEl={anchorEl}
        open={open}
        onClose={handleClose}
        anchorOrigin={{
          vertical: 'bottom',
          horizontal: 'right',
        }}
        transformOrigin={{
          vertical: 'top',
          horizontal: 'right',
        }}
        sx={{
          '& .MuiPaper-root': {
            width: 280,
          },
        }}
      >
        <Box px={2} py={1}>
          <FormControl
            variant="outlined"
            size="small"
            fullWidth
            sx={{ marginBottom: 2 }}
          >
            <InputLabel>Search keywords</InputLabel>
            <OutlinedInput
              type="text"
              endAdornment={
                <InputAdornment position="end">
                  <Magnify />
                </InputAdornment>
              }
              label="Search keywords"
            />
          </FormControl>
          <FormControl fullWidth size="small">
            <InputLabel>Status</InputLabel>
            <Select label="Status">
              <MenuItem value={10}>Success</MenuItem>
              <MenuItem value={20}>Failed</MenuItem>
            </Select>
          </FormControl>
        </Box>
      </Menu>
    </>
  );
}

export default DataGridFilter;
