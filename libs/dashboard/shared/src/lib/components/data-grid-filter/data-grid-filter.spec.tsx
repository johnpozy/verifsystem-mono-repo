import { render } from '@testing-library/react';

import DataGridFilter from './data-grid-filter';

describe('DataGridFilter', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<DataGridFilter />);
    expect(baseElement).toBeTruthy();
  });
});
