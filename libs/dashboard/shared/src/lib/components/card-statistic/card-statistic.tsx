import { Card, CardContent, Typography } from '@mui/material';
import { green, red } from '@mui/material/colors';
import { Box } from '@mui/system';
import { MenuUp, MenuDown, Minus } from 'mdi-material-ui';

import styles from './card-statistic.module.scss';

export interface CardStatisticProps {
  title: string;
  icon?: JSX.Element;
  content?: string | number;
  secondaryContent?: string | number;
  captionContent?: string | number;
  color?: object;
  state?: 'down' | 'up' | 'minus';
  action?: JSX.Element;
  highlight?: object;
}

/**
 * CardStatistic
 *
 * @param {CardStatisticProps} props
 * @returns JSX.Element
 */
export const CardStatistic = (props: CardStatisticProps) => {
  const {
    title,
    icon,
    content,
    secondaryContent,
    captionContent,
    state = 'up',
    action,
    highlight,
  } = props;

  const stateIcon = () => {
    if (state === 'up') {
      return <MenuUp color="success" />;
    }

    if (state === 'down') {
      return <MenuDown color="error" />;
    }

    if (state === 'minus') {
      return <Minus color="error" />;
    }

    return <MenuUp color="success" />;
  };

  return (
    <Card
      sx={{
        position: 'relative',
        backgroundColor: highlight ? highlight[50] : '',
        boxShadow: highlight ? `0 0 10px -3px ${highlight[100]}` : 'none',
        border: `1px solid ${
          highlight ? highlight[100] : 'rgba(0, 0, 0, 0.08)'
        }`,
      }}
    >
      {highlight && <div className={styles.statHighlightIcon}>{icon}</div>}
      <CardContent className={styles.statContent}>
        <div className={styles.statHeader}>
          {icon && <Box className={styles.statHeaderIcon}>{icon}</Box>}
          <Typography variant="body1" component="div">
            {title}
          </Typography>
          {action && <div className={styles.statHeaderAction}>{action}</div>}
        </div>
        <Box className={styles.statContentFigure}>
          {content && (
            <Typography variant="h4" component="div" mr={1}>
              {content}
            </Typography>
          )}
          {captionContent && (
            <Box className={styles.statContentFigureCaption}>
              {stateIcon()}
              <Typography
                variant="body1"
                component="div"
                color={{ color: state === 'up' ? green[600] : red[600] }}
              >
                {captionContent}
              </Typography>
            </Box>
          )}
        </Box>
        {secondaryContent && (
          <Typography
            variant="overline"
            component="div"
            color="text.secondary"
            mt={1}
          >
            {secondaryContent}
          </Typography>
        )}
      </CardContent>
    </Card>
  );
};

export default CardStatistic;
