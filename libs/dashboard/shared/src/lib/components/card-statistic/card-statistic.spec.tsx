import { render } from '@testing-library/react';

import CardStatistic from './card-statistic';

describe('CardStatistic', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<CardStatistic />);
    expect(baseElement).toBeTruthy();
  });
});
