import Head from 'next/head';
import { Box } from '@mui/system';
import { Toolbar } from '@mui/material';

import { useAppSelector, stateSessionExpired } from '@verifsystem/dashboard/data-access';

import { Sidebar } from '../sidebar/sidebar';
import { Header } from '../header/header';

import './layout.module.scss';
import { useRouter } from 'next/router';

interface LayoutProps {
  htmlTitle?: string | number;
  drawerWidth: number;
  children: JSX.Element;
}

export const Layout = (props: LayoutProps) => {
  const router = useRouter();
  const sessionExpired = useAppSelector(stateSessionExpired);
  const { children, drawerWidth, htmlTitle } = props;

  if (sessionExpired) {
    router.push('/auth/login');
  }

  return (
    <>
      <Head>
        <title>Verifsystem {htmlTitle && `- ${htmlTitle}`}</title>
      </Head>
      <Header drawerWidth={drawerWidth} title={htmlTitle} />
      <Box sx={{ display: 'flex' }}>
        <Box
          component="nav"
          sx={{ width: { sm: drawerWidth }, flexShrink: { sm: 0 } }}
        >
          <Sidebar width={drawerWidth} />
        </Box>
        <Box
          component="main"
          sx={{ flexGrow: 1, width: { sm: `calc(100% - ${drawerWidth}px)` } }}
        >
          <Toolbar />
          {children}
        </Box>
      </Box>
    </>
  );
};

export default Layout;
