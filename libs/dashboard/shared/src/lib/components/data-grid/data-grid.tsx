import { LinearProgress, styled } from '@mui/material';
import { grey } from '@mui/material/colors';
import { DataGrid as MUIDataGrid, DataGridProps } from '@mui/x-data-grid';

import { DataGridFooter } from '../data-grid-footer/data-grid-footer';

const DataGridStyles = styled(MUIDataGrid)(({ theme }) => ({
  border: 0,
  WebkitFontSmoothing: 'auto',
  letterSpacing: 'normal',
  '& .MuiDataGrid-columnsContainer': {
    backgroundColor: theme.palette.mode === 'light' ? '#fafafa' : '#1d1d1d',
  },
  '& .MuiDataGrid-iconSeparator': {
    display: 'none',
  },
  '& .MuiDataGrid-columnHeaders': {
    borderColor: 'rgba(0, 0, 0, 0.08)',
    backgroundColor: 'rgba(0, 0, 0, 0.02)',
  },
  '& .MuiDataGrid-columnHeader': {
    textTransform: 'uppercase',
    fontSize: '11px',
    color: grey[600],
  },
  '& .MuiDataGrid-columnHeaderTitle': {
    fontWeight: 700,
  },
  '& .MuiDataGrid-columnHeaderTitleContainer': {
    padding: 0,
  },
  '& .MuiDataGrid-cell': {
    color:
      theme.palette.mode === 'light'
        ? 'rgba(0,0,0,.85)'
        : 'rgba(255,255,255,0.65)',
    borderColor: 'rgba(0, 0, 0, 0.08)',
  },
  '& .MuiDataGrid-footerContainer': {
    padding: '0.7rem',
  },
  '& .MuiDataGrid-row:nth-of-type(even)': {
    backgroundColor: 'rgba(0, 0, 0, 0.008)',
  },
  '& .MuiDataGrid-row:hover': {
    backgroundColor: 'rgba(0, 0, 0, 0.008)',
  },
}));

export const DataGrid = (props: DataGridProps) => {
  return (
    <DataGridStyles
      autoHeight
      headerHeight={44}
      pageSize={25}
      components={{
        Footer: DataGridFooter,
        LoadingOverlay: () => {
          return <LinearProgress sx={{height: '2px'}} />
        },
      }}
      {...props}
    />
  );
};

export default DataGrid;
