import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react';
import { IGatewayConfig } from '@verifsystem/shared/data-access';

const { NEXT_PUBLIC_BACKEND_ENDPOINT } = process.env;

export const gatewayConfigApi = createApi({
  tagTypes: ['GatewayConfigs'],
  reducerPath: 'gatewayConfigApi',
  baseQuery: fetchBaseQuery({ baseUrl: NEXT_PUBLIC_BACKEND_ENDPOINT }),
  endpoints: (builder) => ({
    getGatewayConfigs: builder.query<Array<IGatewayConfig>, { processorId: number }>({
      query: (payload) => ({
        url: `/processors/${payload.processorId}/gateway-configs`,
        method: 'GET'
      }),
      transformResponse: (response: any) => {
        return response.data;
      },
      providesTags: (result) => {
        if (result) {
          return [
            ...result.map(({ id }) => ({ type: 'GatewayConfigs', id } as const)),
            { type: 'GatewayConfigs', id: 'LIST' },
          ];
        }

        return [{ type: 'GatewayConfigs', id: 'LIST' }];
      }
    }),
    createGatewayConfig: builder.mutation<IGatewayConfig, { processorId: number, body }>({
      query: (payload) => ({
        url: `processors/${payload.processorId}/gateway-configs`,
        method: 'POST',
        body: payload.body
      }),
      transformResponse: (response: any) => {
        return response.data;
      },
      invalidatesTags: [{ type: 'GatewayConfigs', id: 'LIST' }],
    }),
    updateGatewayConfig: builder.mutation<IGatewayConfig, { gatewayConfigId: number, body }>({
      query: (payload) => ({
        url: `gateway-configs/${payload.gatewayConfigId}`,
        method: 'PATCH',
        body: payload.body
      }),
      transformResponse: (response: any) => {
        return response.data;
      },
      invalidatesTags: (result, error, { gatewayConfigId }) => [{ type: 'GatewayConfigs', gatewayConfigId }],
    }),
    deleteGatewayConfig: builder.mutation<IGatewayConfig, { gatewayConfigId: number }>({
      query: (payload) => ({
        url: `gateway-configs/${payload.gatewayConfigId}`,
        method: 'DELETE'
      }),
      transformResponse: (response: any) => {
        return response.data;
      },
      invalidatesTags: [{ type: 'GatewayConfigs', id: 'LIST' }],
    }),
  }),
})

export const {
  useGetGatewayConfigsQuery,
  useCreateGatewayConfigMutation,
  useUpdateGatewayConfigMutation,
  useDeleteGatewayConfigMutation
} = gatewayConfigApi
