import { createAsyncThunk } from '@reduxjs/toolkit';
import axios from 'axios';

const { NEXT_PUBLIC_BACKEND_ENDPOINT } = process.env;

export const getGatewayConfigs = createAsyncThunk(
  'gatewayConfig/getGatewayConfigs',
  async (payload: { processorId: number }) => {
    const { processorId } = payload;
    const response = await axios.get(`${NEXT_PUBLIC_BACKEND_ENDPOINT}/processors/${processorId}/gateway-configs`);

    return response.data;
  }
);

export const createGatewayConfig = createAsyncThunk(
  'gatewayConfig/createGatewayConfig',
  async (payload: { processorId: number; body: any }) => {
    const { processorId, body } = payload;
    const response = await axios.post(`${NEXT_PUBLIC_BACKEND_ENDPOINT}/processors/${processorId}/gateway-configs`, body);

    return response.data;
  }
);

export const updateGatewayConfig = createAsyncThunk(
  'gatewayConfig/updateGatewayConfig',
  async (payload: { gatewayConfigId: number; body: any }) => {
    const { gatewayConfigId, body } = payload;
    const response = await axios.patch(`${NEXT_PUBLIC_BACKEND_ENDPOINT}/gateway-configs/${gatewayConfigId}`, body);

    return response.data;
  }
);

export const deleteGatewayConfig = createAsyncThunk(
  'gatewayConfig/deleteGatewayConfig',
  async (payload: { gatewayConfigId: number }) => {
    const { gatewayConfigId } = payload;
    const response = await axios.delete(`${NEXT_PUBLIC_BACKEND_ENDPOINT}/gateway-configs/${gatewayConfigId}`);

    return response.data;
  }
);
