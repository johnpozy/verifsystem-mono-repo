import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { IGatewayConfig } from '@verifsystem/shared/data-access';

import { AppState } from '../../store';

import { getGatewayConfigs, createGatewayConfig, updateGatewayConfig, deleteGatewayConfig } from './thunk';

export interface GatewayConfigState {
  gatewayConfigs: Array<IGatewayConfig>;
  selectedGatewayConfig: IGatewayConfig;
  pending: boolean;
  error: boolean;
}

const initialState: GatewayConfigState = {
  gatewayConfigs: [],
  selectedGatewayConfig: null,
  pending: false,
  error: false,
};

export const gatewayConfigSlice = createSlice({
  name: 'gatewayConfig',
  initialState,
  reducers: {
    selectGatewayConfig: (state, action: PayloadAction<any>) => {
      state.selectedGatewayConfig = action.payload;
    },
  },
  extraReducers: (builder) => {
    builder
      .addCase(getGatewayConfigs.pending, (state) => {
        state.pending = true;
      })
      .addCase(getGatewayConfigs.fulfilled, (state, { payload }) => {
        state.pending = false;
        state.gatewayConfigs = payload.data;
      })
      .addCase(getGatewayConfigs.rejected, (state) => {
        state.pending = false;
        state.error = true;
      });

    builder
      .addCase(createGatewayConfig.pending, (state) => {
        state.pending = true;
      })
      .addCase(createGatewayConfig.fulfilled, (state, { payload }) => {
        state.pending = false;
        state.gatewayConfigs.push(payload.data);
      })
      .addCase(createGatewayConfig.rejected, (state) => {
        state.pending = false;
        state.error = true;
      });

    builder
      .addCase(updateGatewayConfig.pending, (state) => {
        state.pending = true;
      })
      .addCase(updateGatewayConfig.fulfilled, (state, { payload }) => {
        state.pending = false;
        state.gatewayConfigs.forEach((gatewayConfig, index) => {
          if (gatewayConfig.id === payload.data.id) {
            state.gatewayConfigs[index] = payload.data;
          }
        });
      })
      .addCase(updateGatewayConfig.rejected, (state) => {
        state.pending = false;
        state.error = true;
      });

    builder
      .addCase(deleteGatewayConfig.pending, (state) => {
        state.pending = true;
      })
      .addCase(deleteGatewayConfig.fulfilled, (state, { payload }) => {
        state.pending = false;
        state.gatewayConfigs.forEach((gatewayConfig, index) => {
          if (gatewayConfig.id === payload.data.id) {
            state.gatewayConfigs.splice(index, 1);
          }
        });
      })
      .addCase(deleteGatewayConfig.rejected, (state) => {
        state.pending = false;
        state.error = true;
      });
  },
});

export const { selectGatewayConfig } = gatewayConfigSlice.actions;

export const stateGatewayConfigs = (state: AppState) => state.gatewayConfig.gatewayConfigs;

export const stateSelectedGatewayConfig = (state: AppState) => state.gatewayConfig.selectedGatewayConfig;
