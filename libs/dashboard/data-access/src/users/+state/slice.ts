import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { PURGE } from 'redux-persist';

import { IUser } from '@verifsystem/shared/data-access';

import { AppState } from '../../store';

export interface UserState {
  loggedUser: IUser;
  selectedUser: IUser;
}

const initialState: UserState = {
  loggedUser: null,
  selectedUser: null
};

export const userSlice = createSlice({
  name: 'user',
  initialState,
  reducers: {
    setLoggedUser: (state, action: PayloadAction<any>) => {
      state.loggedUser = action.payload;
    },
    setSelectedUser: (state, action: PayloadAction<any>) => {
      state.selectedUser = action.payload;
    }
  },
  extraReducers: (builder) => {
    builder.addCase(PURGE, (state) => {
      return initialState;
    });
  }
});

export const { setLoggedUser, setSelectedUser } = userSlice.actions;

export const stateLoggedUser = (state: AppState) => state.user.loggedUser;

export const stateSelectedUsers = (state: AppState) => state.user.selectedUser;
