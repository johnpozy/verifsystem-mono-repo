import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react';
import { IUser, IUsers } from '@verifsystem/shared/data-access';

const { NEXT_PUBLIC_BACKEND_ENDPOINT } = process.env;

export const userApi = createApi({
  tagTypes: ['Users'],
  reducerPath: 'userApi',
  baseQuery: fetchBaseQuery({ baseUrl: NEXT_PUBLIC_BACKEND_ENDPOINT }),
  endpoints: (builder) => ({
    getEcommUsers: builder.query<IUsers, { page?: number; limit?: number; sortBy?: string; ecommId?: number; }>({
      query: (payload: any) => {
        const params = new URLSearchParams(payload);
        const ecommId = payload?.ecommId ? payload?.ecommId : 'all';

        return {
          url: ecommId ? `users/ecomms/${ecommId}/customers?${params.toString()}` : `users/ecomms/customers?${params.toString()}`,
          method: 'GET'
        }
      },
      transformResponse: (response: any) => {
        return response;
      },
      providesTags: (result) => {
        if (result) {
          return [
            ...result.data.map(({ id }) => ({ type: 'Users', id } as const)),
            { type: 'Users', id: 'LIST' },
          ];
        }

        return [{ type: 'Users', id: 'LIST' }];
      }
    }),
    updateUser: builder.mutation<IUser, { userId: number, body }>({
      query: (payload) => ({
        url: `users/${payload.userId}`,
        method: 'PATCH',
        body: payload.body
      }),
      transformResponse: (response: any) => {
        return response.data;
      },
      invalidatesTags: (result, error, { userId }) => [{ type: 'Users', userId }],
    }),
    updateUserVeriff: builder.mutation<IUser, { userId: number, veriffId: number, body }>({
      query: (payload) => ({
        url: `users/${payload.userId}/veriffs/${payload.veriffId}`,
        method: 'PATCH',
        body: payload.body
      }),
      transformResponse: (response: any) => {
        return response.data;
      },
      invalidatesTags: (result, error, { userId }) => [{ type: 'Users', userId }],
    }),
    updateUserEcomm: builder.mutation<IUser, { userId: number, ecommId: number, body: any }>({
      query: (payload) => ({
        url: `users/${payload.userId}/ecomms/${payload.ecommId}`,
        method: 'PATCH',
        body: payload.body
      }),
      transformResponse: (response: any) => {
        return response.data;
      },
      invalidatesTags: (result, error, { userId }) => [{ type: 'Users', userId }],
    }),
  }),
})

export const {
  useGetEcommUsersQuery,
  useUpdateUserMutation,
  useUpdateUserVeriffMutation,
  useUpdateUserEcommMutation
} = userApi
