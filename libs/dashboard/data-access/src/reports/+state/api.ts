import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react';

const { NEXT_PUBLIC_BACKEND_ENDPOINT } = process.env;

export const reportApi = createApi({
  tagTypes: ['Report'],
  reducerPath: 'reportApi',
  baseQuery: fetchBaseQuery({
    baseUrl: NEXT_PUBLIC_BACKEND_ENDPOINT,
    credentials: 'include',
  }),
  endpoints: (builder) => ({
    getReportOrders: builder.query<any, { startDate?: string; endDate?: string }>({
      query: (payload) => {
        const params = new URLSearchParams(payload);

        return {
          url: `reports/orders`,
          method: 'GET',
          params: params,
        };
      },
      transformResponse: (response: any) => {
        return response.data;
      },
      providesTags: (result) => {
        return [{ type: 'Report', id: 'LIST' }];
      },
    }),
    getReportSales: builder.query<any, { startDate?: string; endDate?: string }>({
      query: (payload) => {
        const params = new URLSearchParams(payload);

        return {
          url: `reports/sales`,
          method: 'GET',
          params: params,
        };
      },
      transformResponse: (response: any) => {
        return response.data;
      },
      providesTags: (result) => {
        return [{ type: 'Report', id: 'LIST' }];
      },
    }),
    exportReport: builder.query<Array<any>, { startDate?: string; endDate?: string }>({
      query: (payload) => {
        const params = new URLSearchParams(payload);

        return {
          url: `reports/export`,
          method: 'GET',
          params: params,
        };
      },
      transformResponse: (response: any) => {
        return response.data;
      },
      providesTags: (result) => {
        return [{ type: 'Report', id: 'LIST' }];
      },
    }),
    chartReport: builder.query<Array<any>, { startDate?: string; endDate?: string }>({
      query: (payload) => {
        const params = new URLSearchParams(payload);

        return {
          url: `reports/chart`,
          method: 'GET',
          params: params,
        };
      },
      transformResponse: (response: any) => {
        return response.data;
      },
      providesTags: (result) => {
        return [{ type: 'Report', id: 'LIST' }];
      },
    }),
  }),
});

export const { useGetReportOrdersQuery, useExportReportQuery, useGetReportSalesQuery, useChartReportQuery } = reportApi;
