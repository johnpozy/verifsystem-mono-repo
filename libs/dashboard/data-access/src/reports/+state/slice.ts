import { createSlice } from '@reduxjs/toolkit';

// eslint-disable-next-line @typescript-eslint/no-empty-interface
export interface ReportState {}

const initialState: ReportState = {};

export const reportSlice = createSlice({
  name: 'report',
  initialState,
  reducers: {}
});
