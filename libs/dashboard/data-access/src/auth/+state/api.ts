import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react';
import { IUser } from '@verifsystem/shared/data-access';

const { NEXT_PUBLIC_BACKEND_ENDPOINT } = process.env;

export const authApi = createApi({
  tagTypes: ['Auth'],
  reducerPath: 'authApi',
  baseQuery: fetchBaseQuery({
    baseUrl: NEXT_PUBLIC_BACKEND_ENDPOINT,
    credentials: 'include',
  }),
  endpoints: (builder) => ({
    login: builder.mutation<IUser, { body }>({
      query: (payload) => ({
        url: `auth/login`,
        method: 'POST',
        body: payload.body
      }),
      transformResponse: (response: any) => {
        return response.data;
      },
    }),
    logout: builder.mutation<IUser, null>({
      query: () => ({
        url: `auth/logout`,
        method: 'POST'
      }),
      transformResponse: (response: any) => {
        return response.data;
      }
    })
  }),
})

export const { useLoginMutation, useLogoutMutation } = authApi
