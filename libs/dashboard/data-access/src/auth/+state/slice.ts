import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { AppState } from '../../store';

// eslint-disable-next-line @typescript-eslint/no-empty-interface
export interface AuthState {
  sessionExpired: boolean;
}

const initialState: AuthState = {
  sessionExpired: true
};

export const authSlice = createSlice({
  name: 'auth',
  initialState,
  reducers: {
    setSessionExpired: (state, action: PayloadAction<any>) => {
      state.sessionExpired = action.payload;
    }
  },
});

export const { setSessionExpired } = authSlice.actions;

export const stateSessionExpired = (state: AppState) => state.auth.sessionExpired;
