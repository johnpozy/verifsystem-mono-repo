import { useEffect, useState } from 'react';
import { TypedUseSelectorHook, useDispatch, useSelector } from 'react-redux';
import io from 'socket.io-client';

import type { AppDispatch, AppState } from './store';

export const useAppDispatch = () => useDispatch<AppDispatch>();

export const useAppSelector: TypedUseSelectorHook<AppState> = useSelector;

export const useSocket = (url) => {
  const [socket, setSocket] = useState(null);

  useEffect(() => {
    const socketIo = io(url);

    setSocket(socketIo);

    const cleanup = () => {
      socketIo.disconnect();
    }

    return cleanup;

  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return socket;
};
