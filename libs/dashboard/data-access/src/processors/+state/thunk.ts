import { createAsyncThunk } from '@reduxjs/toolkit';
import axios from 'axios';

const { NEXT_PUBLIC_BACKEND_ENDPOINT } = process.env;

export const getProcessors = createAsyncThunk('processor/getProcessors', async (payload: { ecommId: number }) => {
  const response = await axios.get(`${NEXT_PUBLIC_BACKEND_ENDPOINT}/ecomms/${payload.ecommId}/processors`);

  return response.data;
});

export const createProcessor = createAsyncThunk('processor/createProcessor', async (payload: { ecommId: number; body: any }) => {
  const response = await axios.post(`${NEXT_PUBLIC_BACKEND_ENDPOINT}/ecomms/${payload.ecommId}/processors`, payload.body);

  return response.data;
});

export const updateProcessor = createAsyncThunk('processor/updateProcessor', async (payload: { processorId: number; body: any }) => {
  const response = await axios.patch(`${NEXT_PUBLIC_BACKEND_ENDPOINT}/processors/${payload.processorId}`, payload.body);

  return response.data;
});

export const deleteProcessor = createAsyncThunk('processor/deleteProcessor', async (payload: { processorId: number }) => {
  const { processorId } = payload;
  const response = await axios.delete(`${NEXT_PUBLIC_BACKEND_ENDPOINT}/processors/${processorId}`);

  return response.data;
});
