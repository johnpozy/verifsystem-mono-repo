import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react';
import { IProcessor, IProcessors } from '@verifsystem/shared/data-access';

const { NEXT_PUBLIC_BACKEND_ENDPOINT } = process.env;

export const processorApi = createApi({
  tagTypes: ['Processors'],
  reducerPath: 'processorApi',
  baseQuery: fetchBaseQuery({ baseUrl: NEXT_PUBLIC_BACKEND_ENDPOINT }),
  endpoints: (builder) => ({
    getProcessors: builder.query<Array<IProcessor>, void>({
      query: () => ({
        url: 'processors',
        method: 'GET'
      }),
      transformResponse: (response: any) => {
        return response.data;
      },
      providesTags: (result) => {
        if (result) {
          return [
            ...result.map(({ id }) => ({ type: 'Processors', id } as const)),
            { type: 'Processors', id: 'LIST' },
          ];
        }

        return [{ type: 'Processors', id: 'LIST' }];
      }
    }),
    getEcommProcessors: builder.query<IProcessors, { page?: number; limit?: number; sortBy?: string; ecommId?: number; }>({
      query: (payload: any) => {
        const params = new URLSearchParams(payload);
        const id = payload?.ecommId ? payload?.ecommId : 'all'

        return {
          url: `processors/ecomms/${id}?${params.toString()}`,
          method: 'GET'
        }
      },
      transformResponse: (response: any) => {
        return response;
      },
      providesTags: (result) => {
        if (result) {
          return [
            ...result.data.map(({ id }) => ({ type: 'Processors', id } as const)),
            { type: 'Processors', id: 'LIST' },
          ];
        }

        return [{ type: 'Processors', id: 'LIST' }];
      }
    }),
    createProcessor: builder.mutation<IProcessor, { ecommId: number, body }>({
      query: (payload) => ({
        url: `ecomms/${payload.ecommId}/processors`,
        method: 'POST',
        body: payload.body
      }),
      transformResponse: (response: any) => {
        return response.data;
      },
      invalidatesTags: [{ type: 'Processors', id: 'LIST' }],
    }),
    updateProcessor: builder.mutation<IProcessor, { processorId: number, body }>({
      query: (payload) => ({
        url: `processors/${payload.processorId}`,
        method: 'PATCH',
        body: payload.body
      }),
      transformResponse: (response: any) => {
        return response.data;
      },
      invalidatesTags: (result, error, { processorId }) => [{ type: 'Processors', processorId }],
    }),
    deleteProcessor: builder.mutation<IProcessor, { processorId: number }>({
      query: (payload) => ({
        url: `processors/${payload.processorId}`,
        method: 'DELETE'
      }),
      transformResponse: (response: any) => {
        return response.data;
      },
      invalidatesTags: [{ type: 'Processors', id: 'LIST' }],
    })
  }),
})

export const {
  useGetProcessorsQuery,
  useGetEcommProcessorsQuery,
  useCreateProcessorMutation,
  useUpdateProcessorMutation,
  useDeleteProcessorMutation
} = processorApi
