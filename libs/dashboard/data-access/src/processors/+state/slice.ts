import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { IProcessor } from '@verifsystem/shared/data-access';

import { AppState } from '../../store';

import { getProcessors, createProcessor, updateProcessor, deleteProcessor } from './thunk';

export interface ProcessorState {
  processors: Array<IProcessor>;
  selectedProcessor: any;
  pending: boolean;
  error: boolean;
}

const initialState: ProcessorState = {
  processors: [],
  selectedProcessor: null,
  pending: false,
  error: false,
};

export const processorSlice = createSlice({
  name: 'processor',
  initialState,
  reducers: {
    selectProcessor: (state, action: PayloadAction<any>) => {
      state.selectedProcessor = action.payload;
    },
  },
  extraReducers: (builder) => {
    builder
      .addCase(getProcessors.pending, (state) => {
        state.pending = true;
      })
      .addCase(getProcessors.fulfilled, (state, { payload }) => {
        state.pending = false;
        state.processors = payload.data;
      })
      .addCase(getProcessors.rejected, (state) => {
        state.pending = false;
        state.error = true;
      });

    builder
      .addCase(createProcessor.pending, (state) => {
        state.pending = true;
      })
      .addCase(createProcessor.fulfilled, (state, { payload }) => {
        state.pending = false;
        state.processors.push(payload.data);
      })
      .addCase(createProcessor.rejected, (state) => {
        state.pending = false;
        state.error = true;
      });

    builder
      .addCase(updateProcessor.pending, (state) => {
        state.pending = true;
      })
      .addCase(updateProcessor.fulfilled, (state, { payload }) => {
        state.pending = false;
        state.processors.forEach((processor, index) => {
          if (processor.id === payload.data.id) {
            state.processors[index] = payload.data;
          }
        });
      })
      .addCase(updateProcessor.rejected, (state) => {
        state.pending = false;
        state.error = true;
      });

    builder
      .addCase(deleteProcessor.pending, (state) => {
        state.pending = true;
      })
      .addCase(deleteProcessor.fulfilled, (state, { payload }) => {
        state.pending = false;
        state.processors.forEach((processor, index) => {
          if (processor.id === payload.data.id) {
            state.processors.splice(index, 1);
          }
        });
      })
      .addCase(deleteProcessor.rejected, (state) => {
        state.pending = false;
        state.error = true;
      });
  },
});

export const { selectProcessor } = processorSlice.actions;

export const stateProcessors = (state: AppState) => state.processor.processors;

export const stateSelectedProcessor = (state: AppState) => state.processor.selectedProcessor;
