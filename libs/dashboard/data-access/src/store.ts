import { configureStore, ThunkAction, Action } from '@reduxjs/toolkit';
import { setupListeners } from '@reduxjs/toolkit/query';
import { createWrapper } from 'next-redux-wrapper';
import { persistReducer, persistStore } from 'redux-persist';
import { defaultMiddleware, persistReducerConfig, rootReducers } from './reducers';

const persistedReducer = persistReducer(
  persistReducerConfig,
  rootReducers
);

export function makeStore() {
  return configureStore({
    reducer: persistedReducer,
    middleware: defaultMiddleware,
  });
}

export const store = makeStore();

export const persistor = persistStore(store);

export type AppState = ReturnType<typeof store.getState>;

export type AppDispatch = typeof store.dispatch;

export type AppThunk<ReturnType = void> = ThunkAction<ReturnType, AppState, unknown, Action<string>>;

export const wrapper = createWrapper(makeStore, { debug: true });

setupListeners(store.dispatch);
