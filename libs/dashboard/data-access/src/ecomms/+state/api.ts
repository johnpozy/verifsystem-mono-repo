import { createApi } from '@reduxjs/toolkit/query/react';
import { IEcomm } from '@verifsystem/shared/data-access';

import { baseQueryWithReauth } from '../../interceptors';

export const ecommApi = createApi({
  tagTypes: ['Ecomms', 'EcommProcessors'],
  reducerPath: 'ecommApi',
  baseQuery: baseQueryWithReauth,
  endpoints: (builder) => ({
    getEcomms: builder.query<Array<IEcomm>, void>({
      query: () => ({
        url: `ecomms`,
        method: 'GET'
      }),
      transformResponse: (response: any) => {
        return response.data;
      },
      providesTags: (result) => {
        if (result) {
          return [
            ...result.map(({ id }) => ({ type: 'Ecomms', id } as const)),
            { type: 'Ecomms', id: 'LIST' },
          ];
        }

        return [{ type: 'Ecomms', id: 'LIST' }];
      }
    }),
    createEcomm: builder.mutation<IEcomm, { body }>({
      query: (payload) => ({
        url: `ecomms`,
        method: 'POST',
        body: payload.body
      }),
      transformResponse: (response: any) => {
        return response.data;
      },
      invalidatesTags: [{ type: 'Ecomms', id: 'LIST' }],
    }),
    getEcommUsersImport: builder.query<Array<any>, { ecommId: number }>({
      query: (payload) => ({
        url: `ecomms/${payload.ecommId}/users/import`,
        method: 'GET'
      }),
      transformResponse: (response: any) => {
        return response.data;
      }
    }),
    saveExternalEcommUsers: builder.mutation<any, { ecommId: number, body: any }>({
      query: (payload) => ({
        url: `ecomms/${payload.ecommId}/users/import`,
        method: 'POST',
        body: payload.body
      }),
      transformResponse: (response: any) => {
        return response.data;
      }
    }),
  }),
})

export const {
  useGetEcommsQuery,
  useCreateEcommMutation,
  useGetEcommUsersImportQuery,
  useSaveExternalEcommUsersMutation
} = ecommApi
