import { createSlice, PayloadAction } from '@reduxjs/toolkit';

import { IEcomm } from '@verifsystem/shared/data-access';

import { AppState } from '../../store';

export interface EcommState {
  selectedEcomm: IEcomm;
}

const initialState: EcommState = {
  selectedEcomm: null
};

export const ecommSlice = createSlice({
  name: 'ecomm',
  initialState,
  reducers: {
    selectEcomm: (state, action: PayloadAction<any>) => {
      state.selectedEcomm = action.payload;
    },
  }
});

export const { selectEcomm } = ecommSlice.actions;

export const stateSelectedEcomm = (state: AppState) => state.ecomm.selectedEcomm;
