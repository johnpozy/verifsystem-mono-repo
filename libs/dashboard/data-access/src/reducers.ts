import { combineReducers } from '@reduxjs/toolkit';
import { FLUSH, REHYDRATE, PAUSE, PERSIST, PURGE, REGISTER } from 'redux-persist';

import { APP_PREFIX } from '@verifsystem/shared/utils';

import { ecommSlice } from './ecomms/+state/slice';
import { gatewayConfigSlice } from './gateway-configs/+state/slice';
import { processorSlice } from './processors/+state/slice';
import { userSlice } from './users/+state/slice';
import { payoutSlice } from './payouts/+state/slice';
import { disputeSlice } from './disputes/+state/slice';
import { transactionSlice } from './transactions/+state/slice';

import { transactionApi } from './transactions/+state/api';
import { ecommApi } from './ecomms/+state/api';
import { userApi } from './users/+state/api';
import { gatewayConfigApi } from './gateway-configs/+state/api';
import { payoutApi } from './payouts/+state/api';
import { processorApi } from './processors/+state/api';
import { authApi } from './auth/+state/api';

import { authSlice } from './auth/+state/slice';
import { reportSlice } from './reports/+state/slice';
import { reportApi } from './reports/+state/api';
import { settingApi } from './settings/+state/api';
import { settingSlice } from './settings/+state/slice';

import storage from './storage';

export const persistReducerConfig = {
  key: APP_PREFIX,
  version: 1,
  storage,
  whitelist: ['user', 'auth'],
  blacklist: [
    authApi.reducerPath,
    userApi.reducerPath,
    ecommApi.reducerPath,
    transactionApi.reducerPath,
    processorApi.reducerPath,
    gatewayConfigApi.reducerPath,
    payoutApi.reducerPath,
    settingApi.reducerPath,
  ]
};

export const rootReducers = combineReducers({
  auth: authSlice.reducer,
  ecomm: ecommSlice.reducer,
  gatewayConfig: gatewayConfigSlice.reducer,
  processor: processorSlice.reducer,
  user: userSlice.reducer,
  payout: payoutSlice.reducer,
  dispute: disputeSlice.reducer,
  transaction: transactionSlice.reducer,
  report: reportSlice.reducer,
  setting: settingSlice.reducer,

  [authApi.reducerPath]: authApi.reducer,
  [userApi.reducerPath]: userApi.reducer,
  [ecommApi.reducerPath]: ecommApi.reducer,
  [transactionApi.reducerPath]: transactionApi.reducer,
  [processorApi.reducerPath]: processorApi.reducer,
  [gatewayConfigApi.reducerPath]: gatewayConfigApi.reducer,
  [payoutApi.reducerPath]: payoutApi.reducer,
  [reportApi.reducerPath]: reportApi.reducer,
  [settingApi.reducerPath]: settingApi.reducer,
});

export const defaultMiddleware = (getDefaultMiddleware) => [
  ...getDefaultMiddleware({
    serializableCheck: {
      ignoredActions: [FLUSH, REHYDRATE, PAUSE, PERSIST, PURGE, REGISTER],
    },
  }),
  authApi.middleware,
  ecommApi.middleware,
  transactionApi.middleware,
  userApi.middleware,
  processorApi.middleware,
  gatewayConfigApi.middleware,
  payoutApi.middleware,
  reportApi.middleware,
  settingApi.middleware,
];
