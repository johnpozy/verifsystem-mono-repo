import { createApi } from '@reduxjs/toolkit/query/react';

import { baseQueryWithReauth } from '../../interceptors';

export const settingApi = createApi({
  tagTypes: ['Settings'],
  reducerPath: 'settingApi',
  baseQuery: baseQueryWithReauth,
  endpoints: (builder) => ({}),
})
