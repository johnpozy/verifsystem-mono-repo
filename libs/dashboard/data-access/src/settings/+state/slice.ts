import { createSlice, PayloadAction } from '@reduxjs/toolkit';

import { IUser } from '@verifsystem/shared/data-access';

import { AppState } from '../../store';

export interface SettingState {
  selectedUsersImport: Array<IUser>;
}

const initialState: SettingState = {
  selectedUsersImport: []
};

export const settingSlice = createSlice({
  name: 'setting',
  initialState,
  reducers: {
    selectUsersImport: (state, action: PayloadAction<any>) => {
      state.selectedUsersImport = action.payload;
    },
  }
});

export const { selectUsersImport } = settingSlice.actions;

export const stateSelectedUsersImport = (state: AppState) => state.setting.selectedUsersImport;
