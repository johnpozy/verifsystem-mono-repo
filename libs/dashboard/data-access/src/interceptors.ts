import { fetchBaseQuery } from '@reduxjs/toolkit/query';
import type { BaseQueryFn, FetchArgs, FetchBaseQueryError } from '@reduxjs/toolkit/query';

import { persistor } from './store';
import { setSessionExpired } from './auth/+state/slice';

const { NEXT_PUBLIC_BACKEND_ENDPOINT } = process.env;

const baseQuery = fetchBaseQuery({
  baseUrl: NEXT_PUBLIC_BACKEND_ENDPOINT,
  credentials: 'include'
});

export const baseQueryWithReauth: BaseQueryFn<string | FetchArgs, unknown, FetchBaseQueryError> = async (args, api, extraOptions) => {
  const result = await baseQuery(args, api, extraOptions);

  if (result.error && result.error.status === 401) {
    persistor.purge();
    api.dispatch(setSessionExpired(true));
  }

  return result;
};
