import { createAsyncThunk } from '@reduxjs/toolkit';
import axios from 'axios';

const { NEXT_PUBLIC_BACKEND_ENDPOINT } = process.env;

export const getEcommDisputes = createAsyncThunk('dispute/getEcommDisputes', async (payload: { ecommId: number, query?: any }) => {
  const { ecommId, query } = payload;
  const response = await axios.get(`${NEXT_PUBLIC_BACKEND_ENDPOINT}/transactions/ecomms/${ecommId}`, {
    params: query
  });

  return response.data;
});
