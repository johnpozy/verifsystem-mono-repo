import { createSlice, PayloadAction } from '@reduxjs/toolkit';

import { AppState } from '../../store';
import { getEcommDisputes } from './thunk';

import { ITransaction } from '@verifsystem/shared/data-access';


export interface DisputeState {
  disputes: Array<ITransaction>;
  selectedDispute: ITransaction;
  pending: boolean;
  error: boolean;
}

const initialState: DisputeState = {
  selectedDispute: null,
  disputes: [],
  pending: false,
  error: false,
};

export const disputeSlice = createSlice({
  name: 'dispute',
  initialState,
  reducers: {
    selectDispute: (state, action: PayloadAction<any>) => {
      state.selectedDispute = action.payload;
    },
  },
  extraReducers: (builder) => {
    builder
      .addCase(getEcommDisputes.pending, (state) => {
        state.pending = true;
      })
      .addCase(getEcommDisputes.fulfilled, (state, { payload }) => {
        state.pending = false;
        state.disputes = payload.data;
      })
      .addCase(getEcommDisputes.rejected, (state) => {
        state.pending = false;
        state.error = true;
      });
  },
});

export const { selectDispute } = disputeSlice.actions;

export const stateDisputes = (state: AppState) => state.dispute.disputes;
