import { createSlice, PayloadAction } from '@reduxjs/toolkit';

import { AppState } from '../../store';

import { ITransaction } from '@verifsystem/shared/data-access';

export interface TransactionState {
  selectedTransaction: ITransaction;
}

const initialState: TransactionState = {
  selectedTransaction: null
};

export const transactionSlice = createSlice({
  name: 'transaction',
  initialState,
  reducers: {
    selectTransaction: (state, action: PayloadAction<any>) => {
      state.selectedTransaction = action.payload;
    },
  }
});

export const { selectTransaction } = transactionSlice.actions;

export const stateSelectedTransaction = (state: AppState) => state.transaction.selectedTransaction;
