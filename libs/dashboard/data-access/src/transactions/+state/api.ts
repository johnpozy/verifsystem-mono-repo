import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react';
import { ITransaction, ITransactions } from '@verifsystem/shared/data-access';

const { NEXT_PUBLIC_BACKEND_ENDPOINT } = process.env;

export const transactionApi = createApi({
  tagTypes: ['Transactions'],
  reducerPath: 'transactionApi',
  baseQuery: fetchBaseQuery({ baseUrl: NEXT_PUBLIC_BACKEND_ENDPOINT }),
  endpoints: (builder) => ({
    getUserTransactions: builder.query<Array<ITransaction>, { userId: number, params?: any }>({
      query: (payload) => ({
        url: `transactions/users/${payload.userId}`,
        method: 'GET',
        params: payload.params
      }),
      transformResponse: (response: any) => {
        return response.data;
      },
      providesTags: (result) => {
        if (result) {
          return [
            ...result.map(({ id }) => ({ type: 'Transactions', id } as const)),
            { type: 'Transactions', id: 'LIST' },
          ];
        }

        return [{ type: 'Transactions', id: 'LIST' }];
      }
    }),
    getEcommTransactions: builder.query<ITransactions, { page?: number; limit?: number; sortBy?: string; filter: any; ecommId?: number; }>({
      query: (payload) => {
        const { page, limit, sortBy, filter } = payload;
        const params = new URLSearchParams({
          page,
          limit,
          sortBy,
          filter: JSON.stringify(filter)
        } as any);
        const id = payload?.ecommId ? payload?.ecommId : 'all';

        return {
          url: `transactions/ecomms/${id}?${params.toString()}`,
          method: 'GET'
        }
      },
      transformResponse: (response: any) => {
        return response;
      },
      providesTags: (result) => {
        if (result) {
          return [
            ...result.data.map(({ id }) => ({ type: 'Transactions', id } as const)),
            { type: 'Transactions', id: 'LIST' },
          ];
        }

        return [{ type: 'Transactions', id: 'LIST' }];
      }
    }),
    updateTransaction: builder.mutation<Array<ITransaction>, { transactionId: number, body }>({
      query: (payload) => ({
        url: `transactions/${payload.transactionId}`,
        method: 'PATCH',
        body: payload.body
      }),
      transformResponse: (response: any) => {
        return response.data;
      },
      invalidatesTags: (result, error, { transactionId }) => [{ type: 'Transactions', transactionId }],
    }),
  }),
})

export const {
  useGetEcommTransactionsQuery,
  useGetUserTransactionsQuery,
  useUpdateTransactionMutation
} = transactionApi
