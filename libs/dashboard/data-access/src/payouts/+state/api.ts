import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react';
import { IPayoutAccount, IPayoutTransaction } from '@verifsystem/shared/data-access';

const { NEXT_PUBLIC_BACKEND_ENDPOINT } = process.env;

export const payoutApi = createApi({
  tagTypes: ['PayoutAccounts', 'PayoutTransactions'],
  reducerPath: 'payoutApi',
  baseQuery: fetchBaseQuery({ baseUrl: NEXT_PUBLIC_BACKEND_ENDPOINT }),
  endpoints: (builder) => ({
    getPayoutAccounts: builder.query<Array<IPayoutAccount>, { ecommId?: number }>({
      query: (payload) => ({
        url: `${payload.ecommId ? 'payouts/accounts/ecomms/'+payload.ecommId : 'payouts/accounts/ecomms'}`,
        method: 'GET'
      }),
      transformResponse: (response: any) => {
        return response.data;
      },
      providesTags: (result) => {
        if (result) {
          return [
            ...result.map(({ id }) => ({ type: 'PayoutAccounts', id } as const)),
            { type: 'PayoutAccounts', id: 'LIST' },
          ];
        }

        return [{ type: 'PayoutAccounts', id: 'LIST' }];
      }
    }),
    createPayoutAccount: builder.mutation<IPayoutAccount, { ecommId: number, body }>({
      query: (payload) => ({
        url: `payouts/accounts/ecomms/${payload.ecommId}`,
        method: 'POST',
        body: payload.body
      }),
      transformResponse: (response: any) => {
        return response.data;
      },
      invalidatesTags: [{ type: 'PayoutAccounts', id: 'LIST' }],
    }),
    updatePayoutAccount: builder.mutation<IPayoutAccount, { accountId: number, body }>({
      query: (payload) => ({
        url: `payouts/accounts/${payload.accountId}`,
        method: 'PATCH',
        body: payload.body
      }),
      transformResponse: (response: any) => {
        return response.data;
      },
      invalidatesTags: (result, error, { accountId }) => [{ type: 'PayoutAccounts', accountId }],
    }),
    deletePayoutAccount: builder.mutation<IPayoutAccount, { accountId: number }>({
      query: (payload) => ({
        url: `payouts/accounts/${payload.accountId}`,
        method: 'DELETE'
      }),
      transformResponse: (response: any) => {
        return response.data;
      },
      invalidatesTags: [{ type: 'PayoutAccounts', id: 'LIST' }],
    }),
    getPayoutTransactions: builder.query<Array<IPayoutTransaction>, { accountId: number, params?: any }>({
      query: (payload) => ({
        url: payload.accountId ? `payouts/accounts/${payload.accountId}/transactions` : 'payouts/transactions',
        method: 'GET',
        params: payload.params
      }),
      transformResponse: (response: any) => {
        return response.data;
      },
      providesTags: (result) => {
        if (result) {
          return [
            ...result.map(({ id }) => ({ type: 'PayoutTransactions', id } as const)),
            { type: 'PayoutTransactions', id: 'LIST' },
          ];
        }

        return [{ type: 'PayoutTransactions', id: 'LIST' }];
      }
    }),
    createPayoutTransaction: builder.mutation<IPayoutTransaction, { accountId: number, body }>({
      query: (payload) => ({
        url: `payouts/accounts/${payload.accountId}/transactions`,
        method: 'POST',
        body: payload.body
      }),
      transformResponse: (response: any) => {
        return response.data;
      },
      invalidatesTags: [{ type: 'PayoutTransactions', id: 'LIST' }],
    }),
    updatePayoutTransaction: builder.mutation<IPayoutAccount, { accountId: number, transactionId: number, body }>({
      query: (payload) => ({
        url: `payouts/accounts/${payload.accountId}/transactions/${payload.transactionId}`,
        method: 'PATCH',
        body: payload.body
      }),
      transformResponse: (response: any) => {
        return response.data;
      },
      invalidatesTags: (result, error, { transactionId }) => [{ type: 'PayoutTransactions', transactionId }],
    }),
    getEcommPayoutTransactions: builder.query<Array<IPayoutTransaction>, { ecommId: number, params?: any }>({
      query: (payload) => ({
        url: payload.ecommId ? `payouts/transactions/ecomms/${payload.ecommId}` : 'payouts/transactions/ecomms',
        method: 'GET',
        params: payload.params
      }),
      transformResponse: (response: any) => {
        return response.data;
      },
      providesTags: (result) => {
        if (result) {
          return [
            ...result.map(({ id }) => ({ type: 'PayoutTransactions', id } as const)),
            { type: 'PayoutTransactions', id: 'LIST' },
          ];
        }

        return [{ type: 'PayoutTransactions', id: 'LIST' }];
      }
    }),
  }),
})

export const {
  useGetPayoutAccountsQuery,
  useCreatePayoutAccountMutation,
  useUpdatePayoutAccountMutation,
  useDeletePayoutAccountMutation,
  useGetPayoutTransactionsQuery,
  useCreatePayoutTransactionMutation,
  useUpdatePayoutTransactionMutation,
  useGetEcommPayoutTransactionsQuery
} = payoutApi
