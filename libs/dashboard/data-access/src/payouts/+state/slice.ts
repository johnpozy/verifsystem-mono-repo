import { createSlice, PayloadAction } from '@reduxjs/toolkit';

import { AppState } from '../../store';
import { IPayoutAccount, IPayoutTransaction } from '@verifsystem/shared/data-access';

import { getPayoutAccounts, createPayoutAccount, updatePayoutAccount, deletePayoutAccount, getPayoutTransactions, createPayoutTransaction, updatePayoutTransaction } from './thunk';

export interface PayoutState {
  accounts: Array<IPayoutAccount>;
  transactions: Array<IPayoutTransaction>;
  selectedPayout: IPayoutAccount;
  pending: boolean;
  error: boolean;
}

const initialState: PayoutState = {
  accounts: [],
  selectedPayout: null,
  transactions: [],
  pending: false,
  error: false,
};

export const payoutSlice = createSlice({
  name: 'payout',
  initialState,
  reducers: {
    selectPayout: (state, action: PayloadAction<any>) => {
      state.selectedPayout = action.payload;
    },
  },
  extraReducers: (builder) => {
    builder
      .addCase(getPayoutAccounts.pending, (state) => {
        state.pending = true;
      })
      .addCase(getPayoutAccounts.fulfilled, (state, { payload }) => {
        state.pending = false;
        state.accounts = payload.data;
      })
      .addCase(getPayoutAccounts.rejected, (state) => {
        state.pending = false;
        state.error = true;
      });

    builder
      .addCase(createPayoutAccount.pending, (state) => {
        state.pending = true;
      })
      .addCase(createPayoutAccount.fulfilled, (state, { payload }) => {
        state.accounts.push(payload.data);
      })
      .addCase(createPayoutAccount.rejected, (state) => {
        state.error = true;
      });

    builder
      .addCase(updatePayoutAccount.pending, (state) => {
        state.pending = true;
      })
      .addCase(updatePayoutAccount.fulfilled, (state, { payload }) => {
        state.accounts.forEach((payout, index) => {
          if (payout.id === payload.data.id) {
            state.accounts[index] = payload.data;
          }
        });
      })
      .addCase(updatePayoutAccount.rejected, (state) => {
        state.error = true;
      });

    builder
      .addCase(createPayoutTransaction.pending, (state) => {
        state.pending = true;
      })
      .addCase(createPayoutTransaction.fulfilled, (state, { payload }) => {
        state.transactions.push(payload.data);
      })
      .addCase(createPayoutTransaction.rejected, (state) => {
        state.error = true;
      });

    builder
      .addCase(getPayoutTransactions.pending, (state) => {
        state.pending = true;
      })
      .addCase(getPayoutTransactions.fulfilled, (state, { payload }) => {
        state.pending = false;
        state.transactions = payload.data;
      })
      .addCase(getPayoutTransactions.rejected, (state) => {
        state.pending = false;
        state.error = true;
      });

    builder
      .addCase(updatePayoutTransaction.pending, (state) => {
        state.pending = true;
      })
      .addCase(updatePayoutTransaction.fulfilled, (state, { payload }) => {
        state.transactions.forEach((transaction, index) => {
          if (transaction.id === payload.data.id) {
            state.transactions[index] = payload.data;
          }
        });
      })
      .addCase(updatePayoutTransaction.rejected, (state) => {
        state.error = true;
      });

    builder
      .addCase(deletePayoutAccount.pending, (state) => {
        state.pending = true;
      })
      .addCase(deletePayoutAccount.fulfilled, (state, { payload }) => {
        state.pending = false;
        state.accounts.forEach((payout, index) => {
          if (payout.id === payload.data.id) {
            state.accounts.splice(index, 1);
          }
        });
      })
      .addCase(deletePayoutAccount.rejected, (state) => {
        state.pending = false;
        state.error = true;
      });
  },
});

export const { selectPayout } = payoutSlice.actions;

export const statePayoutAccounts = (state: AppState) => state.payout.accounts;
export const statePayoutTransactions = (state: AppState) => state.payout.transactions;
