import { createAsyncThunk } from '@reduxjs/toolkit';
import axios from 'axios';

const { NEXT_PUBLIC_BACKEND_ENDPOINT } = process.env;

export const getPayoutAccounts = createAsyncThunk('payout/getPayoutAccounts', async (payload: { ecommId: number }) => {
  const { ecommId } = payload;
  const response = await axios.get(`${NEXT_PUBLIC_BACKEND_ENDPOINT}/payouts/accounts/ecomms/${ecommId}`);

  return response.data;
});

export const createPayoutAccount = createAsyncThunk('payout/createPayoutAccount', async (payload: { ecommId: number; body: any }) => {
  const { body, ecommId } = payload;
  const response = await axios.post(`${NEXT_PUBLIC_BACKEND_ENDPOINT}/payouts/accounts/ecomms/${ecommId}`, body);

  return response.data;
});

export const updatePayoutAccount = createAsyncThunk('payout/updatePayout', async (payload: { accountId: number; body: any }) => {
  const { body, accountId } = payload;
  const response = await axios.patch(`${NEXT_PUBLIC_BACKEND_ENDPOINT}/payouts/accounts/${accountId}`, body);

  return response.data;
});

export const deletePayoutAccount = createAsyncThunk('payout/deletePayoutAccount', async (payload: { accountId: number }) => {
  const { accountId } = payload;
  const response = await axios.delete(`${NEXT_PUBLIC_BACKEND_ENDPOINT}/payouts/accounts/${accountId}`);

  return response.data;
});

export const getPayoutTransactions = createAsyncThunk('payout/getPayoutTransactions', async (payload: { accountId: number }) => {
  const { accountId } = payload;
  const response = await axios.get(`${NEXT_PUBLIC_BACKEND_ENDPOINT}/payouts/accounts/${accountId}/transactions`);

  return response.data;
});

export const createPayoutTransaction = createAsyncThunk('payout/createPayoutTransaction', async (payload: { accountId: number; body: any }) => {
  const { body, accountId } = payload;
  const response = await axios.post(`${NEXT_PUBLIC_BACKEND_ENDPOINT}/payouts/accounts/${accountId}/transactions`, body);

  return response.data;
});

export const updatePayoutTransaction = createAsyncThunk(
  'payout/updatePayoutTransaction',
  async (payload: { accountId: number; transactionId: number; body: any }) => {
    const { body, accountId, transactionId } = payload;
    const response = await axios.patch(`${NEXT_PUBLIC_BACKEND_ENDPOINT}/payouts/accounts/${accountId}/transactions/${transactionId}`, body);

    return response.data;
  }
);
