import { render } from '@testing-library/react';

import CardPendingPayout from './card-pending-payout';

describe('CardPendingPayout', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<CardPendingPayout />);
    expect(baseElement).toBeTruthy();
  });
});
