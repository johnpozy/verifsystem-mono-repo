import { Card, CardHeader, Divider } from '@mui/material';

import { TablePendingPayout } from '../table-pending-payout/table-pending-payout';

/* eslint-disable-next-line */
export interface CardPendingPayoutProps {}

export function CardPendingPayout(props: CardPendingPayoutProps) {
  return (
    <Card>
      <CardHeader titleTypographyProps={{ variant: 'h6' }} title="Pending Payouts" />
      <Divider />

      <TablePendingPayout />
    </Card>
  );
}

export default CardPendingPayout;
