import { Chip, Typography } from '@mui/material';
import { GridActionsCellItem, GridRowsProp } from '@mui/x-data-grid';
import { DotsHorizontal } from 'mdi-material-ui';
import moment from 'moment';

import { DataGrid } from '@verifsystem/dashboard/shared';

/* eslint-disable-next-line */
export interface TableRecentTransactionProps {}

/**
 * TableRecentTransaction
 *
 * @param {TableRecentTransactionProps} props
 * @returns JSX.Element
 */
export function TableRecentTransaction(props: TableRecentTransactionProps) {
  const columns: any = [
    {
      field: 'amountDeposit',
      headerName: 'Amount Deposit',
      width: 200,
      renderCell: (params) => <Typography variant="body1">{params.row.amountDeposit}</Typography>,
    },
    {
      field: 'status',
      headerName: 'Status',
      width: 150,
      renderCell: (params) => <Chip label={params.row.status} color="success" size="small" />,
    },
    {
      field: 'amountRequested',
      headerName: 'Amount Requested',
      width: 200,
      renderCell: (params) => <Typography variant="body1">{params.row.amountRequested}</Typography>,
    },
    {
      field: 'details',
      headerName: 'Details',
      flex: 1,
      renderCell: (params) => <Typography variant="body1">{params.row.details}</Typography>,
    },
    {
      field: 'date',
      headerName: 'Date',
      flex: 1,
      renderCell: (params) => <Typography variant="body1">{moment(params.row.date).format('ll')}</Typography>,
    },
    {
      field: 'actions',
      type: 'actions',
      flex: 1,
      align: 'right',
      getActions: (params) => [<GridActionsCellItem icon={<DotsHorizontal />} label="Deposit now" onClick={() => null} />],
    },
  ];

  const rows: GridRowsProp = [
    {
      id: 1,
      amountDeposit: '$10.55',
      status: 'SUCCESS',
      amountRequested: '$2.11',
      details: 'Test data',
      date: new Date(),
    },
  ];

  return <DataGrid rows={rows} columns={columns} />;
}

export default TableRecentTransaction;
