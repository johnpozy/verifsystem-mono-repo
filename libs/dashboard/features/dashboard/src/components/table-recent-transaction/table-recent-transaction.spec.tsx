import { render } from '@testing-library/react';

import TableRecentTransaction from './table-recent-transaction';

describe('TableRecentTransaction', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<TableRecentTransaction />);
    expect(baseElement).toBeTruthy();
  });
});
