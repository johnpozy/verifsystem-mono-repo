import { Card, CardHeader, Divider, IconButton } from '@mui/material';
import { DotsVertical } from 'mdi-material-ui';

import TableRecentTransaction from '../table-recent-transaction/table-recent-transaction';

/* eslint-disable-next-line */
export interface CardRecentTransactionProps {}

export function CardRecentTransaction(props: CardRecentTransactionProps) {
  return (
    <Card>
      <CardHeader
        action={
          <IconButton aria-label="settings">
            <DotsVertical />
          </IconButton>
        }
        titleTypographyProps={{ variant: 'h6' }}
        title="Recent Transactions"
      />
      <Divider />

      <TableRecentTransaction />
    </Card>
  );
}

export default CardRecentTransaction;
