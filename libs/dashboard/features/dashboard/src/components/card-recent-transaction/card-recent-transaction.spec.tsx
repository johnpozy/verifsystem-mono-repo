import { render } from '@testing-library/react';

import CardRecentTransaction from './card-recent-transaction';

describe('CardRecentTransaction', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<CardRecentTransaction />);
    expect(baseElement).toBeTruthy();
  });
});
