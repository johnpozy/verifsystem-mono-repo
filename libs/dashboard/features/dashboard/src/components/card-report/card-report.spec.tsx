import { render } from '@testing-library/react';

import CardReport from './card-report';

describe('CardReport', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<CardReport />);
    expect(baseElement).toBeTruthy();
  });
});
