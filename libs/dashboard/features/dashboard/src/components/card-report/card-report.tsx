import { useState } from 'react';
import {
  Button,
  Card,
  CardContent,
  CardHeader,
  Divider,
  Grid,
  Menu,
  MenuItem,
  Typography,
} from '@mui/material';
import { Box } from '@mui/system';
import { Calendar } from 'mdi-material-ui';
import { CardStatistic } from '@verifsystem/dashboard/shared';

import ChartReportOverview from '../chart-report-overview/chart-report-overview';
import ChartReportSummary from '../chart-report-summary/chart-report-summary';

import styles from './card-report.module.scss';

/* eslint-disable-next-line */
export interface CardReportProps {}

/**
 * CardReport
 *
 * @param {CardReportProps} props
 * @returns JSX.Element
 */
export function CardReport(props: CardReportProps) {
  const [anchorEl, setAnchorEl] = useState(null);
  const [selectedDateRangeText, setSelectedDateRangeText] =
    useState('Current Month');
  const open = Boolean(anchorEl);

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const handleSelect = (dateRangeText: string) => {
    setSelectedDateRangeText(dateRangeText);
  };

  const renderDatePicker = () => {
    return (
      <>
        <Button onClick={handleClick} endIcon={<Calendar />} color="inherit">
          {selectedDateRangeText}
        </Button>
        <Menu
          anchorEl={anchorEl}
          open={open}
          onClose={handleClose}
          anchorOrigin={{
            vertical: 'bottom',
            horizontal: 'right',
          }}
          transformOrigin={{
            vertical: 'top',
            horizontal: 'right',
          }}
        >
          <MenuItem onClick={() => handleSelect('Daily')}>Daily</MenuItem>
          <MenuItem onClick={() => handleSelect('Weekly')}>Weekly</MenuItem>
          <MenuItem onClick={() => handleSelect('Monthly')}>Monthly</MenuItem>
          <Divider />
          <MenuItem onClick={() => handleSelect('Custom Range')}>
            Custom Range
          </MenuItem>
        </Menu>
      </>
    );
  };

  return (
    <Box>
      <Card>
        <CardHeader
          action={renderDatePicker()}
          titleTypographyProps={{ variant: 'h6' }}
          title="Last 30 days Report"
        />
        <CardContent>
          <Grid container spacing={3}>
            <Grid item xs={8}>
              <Grid container rowSpacing={3} columnSpacing={3}>
                <Grid item xs={4}>
                  <CardStatistic
                    title="Number of Transaction"
                    captionContent="115"
                    state="down"
                  />
                </Grid>
                <Grid item xs={4}>
                  <CardStatistic
                    title="Gross Sales"
                    captionContent="$15.01"
                    state="up"
                  />
                </Grid>
                <Grid item xs={4}>
                  <CardStatistic
                    title="Total Profit"
                    captionContent="$321.59"
                    state="up"
                  />
                </Grid>
              </Grid>
            </Grid>
            <Grid item xs={4}></Grid>
            <Grid item xs={8}>
              <Typography variant="h6">Sales Figures</Typography>
              <Box>
                <ChartReportOverview />
              </Box>
            </Grid>
            <Grid item xs={4} sx={{ display: 'flex', flexDirection: 'column' }}>
              <Typography variant="h6">Sales Report</Typography>
              <Box flex={1}>
                <ChartReportSummary />
              </Box>
            </Grid>
          </Grid>
        </CardContent>
      </Card>
    </Box>
  );
}

export default CardReport;
