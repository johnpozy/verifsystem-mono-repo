import { blue, green, grey, pink } from '@mui/material/colors';
import { LineChart, Line, XAxis, YAxis, CartesianGrid, Tooltip, Legend, ResponsiveContainer } from 'recharts';

import styles from './chart-report-overview.module.scss';

/* eslint-disable-next-line */
export interface ChartReportOverviewProps {}

export function ChartReportOverview(props: ChartReportOverviewProps) {
  const data = [
    {
      name: 'Jan',
      '%': 4000,
      $: 2400,
      w: 400,
    },
    {
      name: 'Feb',
      '%': 4000,
      $: 2400,
      w: 600,
    },
    {
      name: 'Mar',
      '%': 2000,
      $: 7433,
      w: 600,
    },
    {
      name: 'Apr',
      '%': 2000,
      $: 7433,
      w: 600,
    },
    {
      name: 'May',
      '%': 2800,
      $: 4800,
      w: 300,
    },
    {
      name: 'Jun',
      '%': 2800,
      $: 4800,
      w: 300,
    },
    {
      name: 'Jul',
      '%': 3800,
      $: 4300,
      w: 700,
    },
    {
      name: 'Aug',
      '%': 3800,
      $: 4300,
      w: 700,
    },
    {
      name: 'Sep',
      '%': 4600,
      $: 4800,
      w: 500,
    },
    {
      name: 'Oct',
      '%': 4600,
      $: 4800,
      w: 500,
    },
    {
      name: 'Nov',
      '%': 3600,
      $: 5800,
      w: 200,
    },
    {
      name: 'Dec',
      '%': 3600,
      $: 5800,
      w: 200,
    },
  ];

  const formatTooltipValue = (value) => {
    return `$${value}`;
  };

  return (
    <div style={{ height: 360, width: '100%' }}>
      <div style={{ display: 'flex', height: '100%' }}>
        <div style={{ flexGrow: 1 }}>
          <ResponsiveContainer className={styles.chart}>
            <LineChart
              data={data}
              margin={{
                top: 0,
                right: 20,
                left: -20,
                bottom: 0,
              }}
            >
              <CartesianGrid vertical={false} strokeDasharray="4 4" stroke={grey[300]} />
              <XAxis strokeWidth={0} dataKey="name" />
              <YAxis strokeWidth={0} />
              <Tooltip formatter={formatTooltipValue} />
              <Legend verticalAlign="top" height={36} iconType="circle" iconSize={12} />
              <Line name="Sales" type="linear" dataKey="%" stroke={blue[600]} strokeWidth={2} dot={{ r: 0 }} activeDot={{ r: 6 }} />
              <Line name="Balance" type="linear" dataKey="$" stroke={pink[400]} strokeWidth={2} dot={{ r: 0 }} activeDot={{ r: 6 }} />
              <Line
                name="Withdrawalable Balance"
                type="linear"
                dataKey="w"
                stroke={green[400]}
                strokeWidth={2}
                dot={{ r: 0 }}
                activeDot={{ r: 6 }}
              />
            </LineChart>
          </ResponsiveContainer>
        </div>
      </div>
    </div>
  );
}

export default ChartReportOverview;
