import { render } from '@testing-library/react';

import CardPendingDispute from './card-pending-dispute';

describe('CardPendingDispute', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<CardPendingDispute />);
    expect(baseElement).toBeTruthy();
  });
});
