import { Card, CardHeader, Divider } from '@mui/material';

import { TablePendingDispute } from '../table-pending-dispute/table-pending-dispute';

/* eslint-disable-next-line */
export interface CardPendingDisputeProps {}

export function CardPendingDispute(props: CardPendingDisputeProps) {
  return (
    <Card>
      <CardHeader titleTypographyProps={{ variant: 'h6' }} title="Pending Disputes & Chargebacks" />
      <Divider />

      <TablePendingDispute />
    </Card>
  );
}

export default CardPendingDispute;
