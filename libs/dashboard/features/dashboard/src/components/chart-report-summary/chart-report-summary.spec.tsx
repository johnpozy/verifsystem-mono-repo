import { render } from '@testing-library/react';

import ChartReportSummary from './chart-report-summary';

describe('ChartReportSummary', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<ChartReportSummary />);
    expect(baseElement).toBeTruthy();
  });
});
