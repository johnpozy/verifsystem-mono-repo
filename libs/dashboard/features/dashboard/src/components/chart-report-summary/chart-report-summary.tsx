import { blue, grey, pink } from '@mui/material/colors';
import { BarChart, Bar, XAxis, YAxis, CartesianGrid, ResponsiveContainer } from 'recharts';

import styles from './chart-report-summary.module.scss';

/* eslint-disable-next-line */
export interface ChartReportSummaryProps {}

/**
 * ChartReportSummary
 *
 * @param {ChartReportSummaryProps} props
 * @returns JSX.Element
 */
export function ChartReportSummary(props: ChartReportSummaryProps) {
  const data = [
    {
      name: 'Jan',
      '%': 4000,
      $: 2400,
    },
    {
      name: 'Feb',
      '%': 4000,
      $: 2400,
    },
    {
      name: 'Mar',
      '%': 2000,
      $: 7433,
    },
  ];

  return (
    <ResponsiveContainer className={styles.chart}>
      <BarChart
        data={data}
        margin={{
          top: 35,
          right: 0,
          left: -20,
          bottom: 0,
        }}
        height={400}
        barSize={30}
        barGap={15}
      >
        <CartesianGrid vertical={false} strokeDasharray="3 3" stroke={grey[300]} />
        <XAxis strokeWidth={0} dataKey="name" />
        <YAxis strokeWidth={0} />
        <Bar dataKey="%" fill={blue[600]} radius={3} />
        <Bar dataKey="$" fill={pink[400]} radius={3} />
      </BarChart>
    </ResponsiveContainer>
  );
}

export default ChartReportSummary;
