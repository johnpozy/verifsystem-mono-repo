import { render } from '@testing-library/react';

import TableOrderDeclined from './table-order-declined';

describe('TableOrderDeclined', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<TableOrderDeclined />);
    expect(baseElement).toBeTruthy();
  });
});
