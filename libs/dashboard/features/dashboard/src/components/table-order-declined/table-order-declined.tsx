import { Chip, IconButton, Tooltip, Typography } from '@mui/material';
import { GridColDef, GridRowsProp } from '@mui/x-data-grid';
import { Eye } from 'mdi-material-ui';

import { DataGrid } from '@verifsystem/dashboard/shared';

/* eslint-disable-next-line */
export interface TableOrderDeclinedProps {}

/**
 * TableOrderDeclined
 *
 * @param {TableOrderDeclinedProps} props
 * @returns JSX.Element
 */
export function TableOrderDeclined(props: TableOrderDeclinedProps) {
  const columns: GridColDef[] = [
    {
      field: 'order',
      headerName: 'Order',
      width: 250,
      renderCell: (params) => <Typography color="primary">{params.row.order}</Typography>,
    },
    {
      field: 'date',
      headerName: 'Date',
      width: 150,
      renderCell: (params) => <Typography>{params.row.date}</Typography>,
    },
    {
      field: 'status',
      headerName: 'Status',
      width: 150,
      renderCell: (params) => <Chip label={params.row.status} color="error" size="small" />,
    },
    {
      field: 'total',
      headerName: 'Total',
      renderCell: (params) => <Typography>{params.row.total}</Typography>,
    },
    {
      field: 'paymentMethod',
      headerName: 'Payment Method',
      flex: 1,
      renderCell: (params) => <Typography>{params.row.paymentMethod}</Typography>,
    },
    {
      field: 'shipmentTracking',
      headerName: 'Shipment Tracking',
      flex: 1,
      renderCell: (params) => <Typography>{params.row.shipmentTracking ? params.row.shipmentTracking : '-'}</Typography>,
    },
    {
      field: 'affiliateReferal',
      headerName: 'Affiliate Referal',
      flex: 1,
      renderCell: (params) => <Typography>{params.row.shipmentTracking ? params.row.shipmentTracking : '-'}</Typography>,
    },
    {
      field: 'actions',
      type: 'actions',
      width: 80,
      align: 'center',
      renderCell: (params) => {
        return (
          <Tooltip title="View order">
            <IconButton>
              <Eye />
            </IconButton>
          </Tooltip>
        );
      },
    },
  ];

  const rows: GridRowsProp = [
    {
      id: 1,
      order: '#71970 Annaka Mekish',
      date: '2 hour ago',
      status: 'DECLINED',
      total: '$15.12',
      paymentMethod: 'Interac eTransfer Payment',
      shipmentTracking: null,
      affiliateReferal: null,
    },
    {
      id: 2,
      order: '#71971 Romain Tchierry',
      date: '10 hour ago',
      status: 'DECLINED',
      total: '$15.12',
      paymentMethod: 'Interac eTransfer Payment',
      shipmentTracking: null,
      affiliateReferal: null,
    },
    {
      id: 3,
      order: '#22154 John Doe',
      date: '1 day ago',
      status: 'DECLINED',
      total: '$114.33',
      paymentMethod: 'Interac eTransfer Payment',
      shipmentTracking: null,
      affiliateReferal: null,
    },
  ];

  return <DataGrid rows={rows} columns={columns} checkboxSelection />;
}

export default TableOrderDeclined;
