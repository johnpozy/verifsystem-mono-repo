import { render } from '@testing-library/react';

import CardPerformingClient from './card-performing-client';

describe('CardPerformingClient', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<CardPerformingClient />);
    expect(baseElement).toBeTruthy();
  });
});
