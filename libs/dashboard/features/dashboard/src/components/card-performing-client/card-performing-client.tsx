import { Card, CardHeader, IconButton } from '@mui/material';
import { DotsVertical } from 'mdi-material-ui';

import TablePerformingClient from '../table-performing-client/table-performing-client';

/* eslint-disable-next-line */
export interface CardPerformingClientProps {}

/**
 * CardPerformingClient
 *
 * @param {CardPerformingClientProps} props
 * @returns JSX.Element
 */
export function CardPerformingClient(props: CardPerformingClientProps) {
  return (
    <Card>
      <CardHeader
        action={
          <IconButton aria-label="settings">
            <DotsVertical />
          </IconButton>
        }
        titleTypographyProps={{ variant: 'h6' }}
        title="Top Performing Clients"
      />
      <TablePerformingClient />
    </Card>
  );
}

export default CardPerformingClient;
