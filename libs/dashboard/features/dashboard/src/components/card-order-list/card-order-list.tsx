import {
  Box,
  Button,
  Card,
  CardHeader,
  Divider,
  FormControl,
  Grid,
  IconButton,
  InputAdornment,
  InputLabel,
  OutlinedInput,
} from '@mui/material';
import { DataGridExporter } from '@verifsystem/dashboard/shared';
import { ChevronDown, Magnify } from 'mdi-material-ui';
import TableOrderList from '../table-order-list/table-order-list';

/* eslint-disable-next-line */
export interface CardOrderListProps {}

/**
 * CardOrderList
 *
 * @param {CardOrderListProps} props
 * @returns JSX.Element
 */
export function CardOrderList(props: CardOrderListProps) {
  return (
    <Card>
      <CardHeader
        titleTypographyProps={{ variant: 'h6' }}
        action={
          <Grid container columnSpacing={2} alignItems="center">
            <Grid item xs="auto">
              <DataGridExporter />
            </Grid>
          </Grid>
        }
        title="Orders"
      />
      <Divider />
      <Box px={2} py={2}>
        <Grid container columnSpacing={2} alignItems="center">
          <Grid item xs>
            <Button variant="contained" color="inherit" disableElevation endIcon={<ChevronDown />}>
              Bulk Action
            </Button>
          </Grid>
          <Grid item xs />
          <Grid item xs={3}>
            <FormControl variant="outlined" size="small" fullWidth sx={{ bgcolor: 'white', margin: 0 }}>
              <InputLabel>Search orders</InputLabel>
              <OutlinedInput
                type="text"
                endAdornment={
                  <InputAdornment position="end">
                    <IconButton edge="end">
                      <Magnify />
                    </IconButton>
                  </InputAdornment>
                }
                label="Search orders"
              />
            </FormControl>
          </Grid>
        </Grid>
      </Box>
      <Divider />

      <TableOrderList />
    </Card>
  );
}

export default CardOrderList;
