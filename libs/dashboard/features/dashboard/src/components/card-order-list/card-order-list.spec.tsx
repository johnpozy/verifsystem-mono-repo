import { render } from '@testing-library/react';

import CardOrderList from './card-order-list';

describe('CardOrderList', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<CardOrderList />);
    expect(baseElement).toBeTruthy();
  });
});
