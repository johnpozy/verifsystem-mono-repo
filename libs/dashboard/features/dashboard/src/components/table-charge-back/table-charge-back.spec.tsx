import { render } from '@testing-library/react';

import TableChargeBack from './table-charge-back';

describe('TableChargeBack', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<TableChargeBack />);
    expect(baseElement).toBeTruthy();
  });
});
