import { Avatar, Chip, Typography } from '@mui/material';
import { Box } from '@mui/system';
import { GridColDef, GridRowsProp } from '@mui/x-data-grid';

import { DataGrid } from '@verifsystem/dashboard/shared';

/* eslint-disable-next-line */
export interface TableChargeBackProps {}

/**
 * TableChargeBack
 *
 * @param {TableChargeBackProps} props
 * @returns JSX.Element
 */
export function TableChargeBack(props: TableChargeBackProps) {
  const columns: GridColDef[] = [
    {
      field: 'col1',
      headerName: 'Client',
      width: 200,
      renderCell: (params) => <Typography>{params.row.col1}</Typography>,
    },
    {
      field: 'col2',
      headerName: 'Customer',
      width: 300,
      renderCell: (params) => {
        return (
          <Box sx={{ display: 'flex', alignItems: 'center' }}>
            <Avatar alt={params.row.col2} sx={{ marginRight: '.5rem', width: 32, height: 32 }} src={params.row.avatar} />
            <Typography>{params.row.col2}</Typography>
          </Box>
        );
      },
    },
    {
      field: 'col3',
      headerName: 'Total Amount',
      width: 300,
      renderCell: (params) => <Typography>{params.row.col3}</Typography>,
    },
    {
      field: 'col4',
      headerName: 'Reason?',
      flex: 1,
      renderCell: (params) => <Typography>{params.row.col4}</Typography>,
    },
    {
      field: 'col5',
      headerName: 'Status',
      width: 120,
      renderCell: (params) => <Chip label={params.row.col5} color="success" size="small" />,
    },
  ];

  const rows: GridRowsProp = [
    {
      id: 1,
      col1: 'Client 1',
      col2: 'Nathan Smith',
      col3: '$512.44',
      col4: "Didn't approve",
      col5: 'SUCCESS',
      avatar: 'img/avatars/avatar-3.jpg',
    },
    {
      id: 2,
      col1: 'Client 2',
      col2: 'Sandra Block',
      col3: '$1032.88',
      col4: 'Unknow reason',
      col5: 'SUCCESS',
      avatar: 'img/avatars/avatar-2.jpg',
    },
  ];

  return <DataGrid rows={rows} columns={columns} checkboxSelection />;
}

export default TableChargeBack;
