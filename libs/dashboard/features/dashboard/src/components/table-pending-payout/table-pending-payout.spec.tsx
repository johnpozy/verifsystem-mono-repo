import { render } from '@testing-library/react';

import TablePendingPayout from './table-pending-payout';

describe('TablePendingPayout', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<TablePendingPayout />);
    expect(baseElement).toBeTruthy();
  });
});
