import { Chip, Typography } from '@mui/material';
import { GridColDef } from '@mui/x-data-grid';
import moment from 'moment';

import { DataGrid } from '@verifsystem/dashboard/shared';
import { stateSelectedEcomm, useAppSelector, useGetEcommPayoutTransactionsQuery } from '@verifsystem/dashboard/data-access';
import { EPayoutTransactionStatus } from '@verifsystem/shared/data-access';

/* eslint-disable-next-line */
export interface TablePendingPayoutProps {}

/**
 * TablePendingPayout
 *
 * @param {TablePendingPayoutProps} props
 * @returns JSX.Element
 */
export function TablePendingPayout(props: TablePendingPayoutProps) {
  const selectedEcomm = useAppSelector(stateSelectedEcomm);
  const { data: transactions = [], isLoading } = useGetEcommPayoutTransactionsQuery({
    ecommId: selectedEcomm ? selectedEcomm.id : null,
    params: { status: EPayoutTransactionStatus.Pending },
  });

  const columns: GridColDef[] = [
    {
      field: 'payout.ecomm',
      headerName: 'Ecomm',
      flex: 1,
      hideable: false,
      valueGetter: ({ row }) => row?.payout?.ecomm?.title,
      renderCell: ({ row }) => <Typography>{row?.payout?.ecomm?.title}</Typography>,
    },
    {
      field: 'amount',
      headerName: 'Amount',
      width: 250,
      renderCell: ({ row }) => <Typography>${row?.amount / 100}</Typography>,
    },
    {
      field: 'status',
      headerName: 'Status',
      width: 150,
      renderCell: ({ row }) => (
        <Chip
          label={row?.status.toUpperCase()}
          color={row?.status === EPayoutTransactionStatus.Approved ? 'success' : 'warning'}
          size="small"
        />
      ),
    },
    {
      field: 'description',
      headerName: 'Description',
      flex: 1,
      renderCell: ({ row }) => <Typography>{row?.description}</Typography>,
    },
    {
      field: 'createdAt',
      headerName: 'Date / Time',
      width: 250,
      renderCell: ({ row }) => <Typography>{moment(row?.createdAt).format('ll LT')}</Typography>,
    },
  ];

  return <DataGrid rows={transactions} columns={columns} checkboxSelection loading={isLoading} />;
}

export default TablePendingPayout;
