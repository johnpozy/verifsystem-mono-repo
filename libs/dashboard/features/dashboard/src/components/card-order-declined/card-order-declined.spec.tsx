import { render } from '@testing-library/react';

import CardOrderDeclined from './card-order-declined';

describe('CardOrderDeclined', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<CardOrderDeclined />);
    expect(baseElement).toBeTruthy();
  });
});
