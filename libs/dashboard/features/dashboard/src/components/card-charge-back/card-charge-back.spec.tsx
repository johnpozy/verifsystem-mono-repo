import { render } from '@testing-library/react';

import CardChargeBack from './card-charge-back';

describe('CardChargeBack', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<CardChargeBack />);
    expect(baseElement).toBeTruthy();
  });
});
