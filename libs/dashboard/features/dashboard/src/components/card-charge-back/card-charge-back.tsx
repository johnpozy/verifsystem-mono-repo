import {
  Box,
  Button,
  Card,
  CardHeader,
  Divider,
  FormControl,
  Grid,
  IconButton,
  InputAdornment,
  InputLabel,
  OutlinedInput,
} from '@mui/material';
import { DataGridExporter } from '@verifsystem/dashboard/shared';
import { ChevronDown, Magnify } from 'mdi-material-ui';

import TableChargeBack from '../table-charge-back/table-charge-back';

/* eslint-disable-next-line */
export interface CardChargeBackProps {}

/**
 * CardChargeBack
 *
 * @param {CardChargeBackProps} props
 * @returns JSX.Element
 */
export function CardChargeBack(props: CardChargeBackProps) {
  return (
    <Card>
      <CardHeader
        titleTypographyProps={{ variant: 'h6' }}
        action={
          <Grid container columnSpacing={2} alignItems="center">
            <Grid item xs="auto">
              <DataGridExporter />
            </Grid>
          </Grid>
        }
        title="Chargebacks"
      />
      <Divider />
      <Box px={2} py={2}>
        <Grid container columnSpacing={2} alignItems="center">
          <Grid item xs>
            <Button variant="contained" color="inherit" disableElevation endIcon={<ChevronDown />}>
              Bulk Action
            </Button>
          </Grid>
          <Grid item xs />
          <Grid item xs={3}>
            <FormControl variant="outlined" size="small" fullWidth sx={{ bgcolor: 'white', margin: 0 }}>
              <InputLabel>Search customers</InputLabel>
              <OutlinedInput
                type="text"
                endAdornment={
                  <InputAdornment position="end">
                    <IconButton edge="end">
                      <Magnify />
                    </IconButton>
                  </InputAdornment>
                }
                label="Search customers"
              />
            </FormControl>
          </Grid>
        </Grid>
      </Box>
      <Divider />

      <TableChargeBack />
    </Card>
  );
}

export default CardChargeBack;
