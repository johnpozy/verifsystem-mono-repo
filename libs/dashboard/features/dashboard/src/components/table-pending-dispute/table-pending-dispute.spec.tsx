import { render } from '@testing-library/react';

import TablePendingDispute from './table-pending-dispute';

describe('TablePendingDispute', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<TablePendingDispute />);
    expect(baseElement).toBeTruthy();
  });
});
