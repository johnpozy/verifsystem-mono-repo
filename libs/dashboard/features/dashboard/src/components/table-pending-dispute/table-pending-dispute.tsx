import { useCallback, useState } from 'react';
import { Avatar, Chip, Tooltip, Typography } from '@mui/material';
import { GridColDef, GridSortModel } from '@mui/x-data-grid';
import { Box } from '@mui/system';

import { DataGrid } from '@verifsystem/dashboard/shared';
import { useAppSelector, stateSelectedEcomm, useGetEcommTransactionsQuery } from '@verifsystem/dashboard/data-access';
import { ETransactionStatus } from '@verifsystem/shared/data-access';

/* eslint-disable-next-line */
export interface TablePendingDisputeProps {}

/**
 * TableDisputeList
 *
 * @param {TablePendingDisputeProps} props
 * @returns JSX.Element
 */
export function TablePendingDispute(props: TablePendingDisputeProps) {
  const selectedEcomm = useAppSelector(stateSelectedEcomm);
  const [page, setPage] = useState(0);
  const [pageSize, setPageSize] = useState(10);
  const [sortBy, setSortBy] = useState(null);
  const { data: transactions , isLoading } = useGetEcommTransactionsQuery({
    page: page + 1,
    limit: pageSize,
    sortBy,
    filter: {
      status: `$in:${ETransactionStatus.Dispute}`
    },
    ecommId: selectedEcomm?.id
  });

  const handleSortModelChange = useCallback((sortModel: GridSortModel) => {
    if (sortModel && sortModel.length > 0) {
      setSortBy(`${sortModel[0].field}:${sortModel[0].sort}`);
    }
  }, []);

  const columns: GridColDef[] = [
    {
      field: 'ecomm',
      headerName: 'Ecomm',
      width: 250,
      hideable: false,
      valueGetter: ({ row }) => row?.ecomm?.title,
      renderCell: ({ row }) => <Typography>{row?.ecomm?.title}</Typography>,
    },
    {
      field: 'user',
      headerName: 'Customer',
      width: 250,
      valueGetter: ({ row }) => row?.user?.displayName,
      renderCell: ({ row }) => {
        return (
          <Box sx={{ display: 'flex', alignItems: 'center' }}>
            <Avatar alt={row.user.displayName} sx={{ marginRight: '.5rem', width: 36, height: 36 }} src={row.avatar} variant="rounded" />
            <Typography>{row.user.displayName}</Typography>
          </Box>
        );
      },
    },
    {
      field: 'total',
      headerName: 'Amount',
      flex: 1,
      renderCell: (params) => <Typography>${params.row.amount / 100}</Typography>,
    },
    {
      field: 'status',
      headerName: 'Status',
      flex: 1,
      renderCell: (params) => {
        return (
          <Tooltip title="Status">
            <Chip label={params.row.status.toUpperCase()} color={params.row.status === 'processing' ? 'success' : 'warning'} size="small" />
          </Tooltip>
        );
      },
    }
  ];

  return <DataGrid
    rows={transactions?.data || []}
    rowCount={transactions?.meta?.totalItems || 0}
    page={page}
    pageSize={pageSize}
    paginationMode="server"
    onPageChange={(newPage) => setPage(newPage)}
    onPageSizeChange={(newPageSize) => setPageSize(newPageSize)}
    sortingMode="server"
    onSortModelChange={handleSortModelChange}
    columns={columns}
    checkboxSelection
    loading={isLoading}
  />
}

export default TablePendingDispute;
