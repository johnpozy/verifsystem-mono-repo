import { render } from '@testing-library/react';

import TablePerformingClient from './table-performing-client';

describe('TablePerformingClient', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<TablePerformingClient />);
    expect(baseElement).toBeTruthy();
  });
});
