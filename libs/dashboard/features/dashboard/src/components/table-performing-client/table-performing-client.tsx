import { Avatar, Typography } from '@mui/material';
import { Box } from '@mui/system';
import { DataGrid, GridColDef, GridRowsProp } from '@mui/x-data-grid';

import styles from './table-performing-client.module.scss';

/* eslint-disable-next-line */
export interface TablePerformingClientProps {}

/**
 * TablePerformingClient
 *
 * @param {TablePerformingClientProps} props
 * @returns JSX.Element
 */
export function TablePerformingClient(props: TablePerformingClientProps) {
  const columns: GridColDef[] = [
    {
      field: 'col1',
      headerName: 'Customer',
      width: 200,
      renderCell: (params) => {
        return (
          <Box sx={{ display: 'flex', alignItems: 'center' }}>
            <Avatar alt={params.row.col1} sx={{ marginRight: '.5rem', width: 32, height: 32 }} src={params.row.avatar} />
            <Typography variant="body2">{params.row.col1}</Typography>
          </Box>
        );
      },
    },
    {
      field: 'col2',
      headerName: 'Total Amount',
      width: 150,
      renderCell: (params) => <Typography variant="body1">{params.row.col2}</Typography>,
    },
  ];

  const rows: GridRowsProp = [
    {
      id: 1,
      col1: 'John Abraham',
      col2: '$512.44',
      avatar: 'img/avatars/avatar-5.jpg',
    },
    {
      id: 2,
      col1: 'Katy Smile',
      col2: '$1032.88',
      avatar: 'img/avatars/avatar-6.jpg',
    },
  ];

  return (
    <div style={{ height: 320, width: '100%' }}>
      <div style={{ display: 'flex', height: '100%' }}>
        <div style={{ flexGrow: 1 }}>
          <DataGrid rows={rows} columns={columns} className={styles.dataGrid} hideFooter />
        </div>
      </div>
    </div>
  );
}

export default TablePerformingClient;
