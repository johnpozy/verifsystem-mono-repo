import { useRouter } from 'next/router';
import { Box, Button, Grid } from '@mui/material';
import { ArrowULeftTop, Dolly, MinusCircleOutline, SwapHorizontalBold, WalletPlusOutline } from 'mdi-material-ui';
import { green } from '@mui/material/colors';

import { CardStatistic } from '@verifsystem/dashboard/shared';

import { CardReport } from '../../components/card-report/card-report';
import { CardPendingPayout } from '../../components/card-pending-payout/card-pending-payout';
import { CardPendingDispute } from '../../components/card-pending-dispute/card-pending-dispute';

/* eslint-disable-next-line */
export interface DashboardViewProps {}

/**
 * DashboardView
 *
 * @param {DashboardViewProps} props
 * @returns JSX.Element
 */
export const DashboardView = (props: DashboardViewProps) => {
  const router = useRouter();

  return (
    <Box p={3}>
      <Grid container rowSpacing={3} columnSpacing={3} mb={3}>
        <Grid item xs={3}>
          <CardStatistic
            title="Balance"
            icon={<WalletPlusOutline fontSize="large" color="success" />}
            content="$492.90"
            captionContent="$51.22"
            secondaryContent="Current Balance"
            action={
              <Button color="success" endIcon={<SwapHorizontalBold />} onClick={() => router.push('/withdrawal')}>
                Withdraw
              </Button>
            }
            highlight={green}
            state="minus"
          />
        </Grid>
        <Grid item xs={3}>
          <CardStatistic
            title="Total Orders"
            icon={<Dolly fontSize="large" color="primary" />}
            content="1,240"
            captionContent="$1221.33"
            secondaryContent="This month"
            action={
              <Button color="primary" onClick={() => router.push('/dashboard/overview')}>
                View All
              </Button>
            }
          />
        </Grid>
        <Grid item xs={3}>
          <CardStatistic
            title="Total Chargebacks"
            icon={<ArrowULeftTop fontSize="large" color="warning" />}
            content="2"
            captionContent="$51.22"
            secondaryContent="This month"
            state="down"
            action={
              <Button color="primary" onClick={() => router.push('/dashboard/overview')}>
                View All
              </Button>
            }
          />
        </Grid>
        <Grid item xs={3}>
          <CardStatistic
            title="Declined Orders"
            icon={<MinusCircleOutline fontSize="large" color="error" />}
            content="511"
            captionContent="31"
            secondaryContent="This month"
            state="down"
            action={
              <Button color="primary" onClick={() => router.push('/dashboard/overview')}>
                View All
              </Button>
            }
          />
        </Grid>
      </Grid>

      <CardPendingPayout />
      <Box mb={3} />

      <CardPendingDispute />
      <Box mb={3} />

      <CardReport />
    </Box>
  );
};

export default DashboardView;
