import { useState } from 'react';
import { Box, Breadcrumbs, Grid, Link, Tab, Typography } from '@mui/material';
import { TabContext, TabList, TabPanel } from '@mui/lab';

import { PageHeader } from '@verifsystem/dashboard/shared';

import CardChargeBack from '../../components/card-charge-back/card-charge-back';
import CardOrderList from '../../components/card-order-list/card-order-list';
import CardOrderDeclined from '../../components/card-order-declined/card-order-declined';

/* eslint-disable-next-line */
export interface DashboardOverviewViewProps {}

export function DashboardOverviewView(props: DashboardOverviewViewProps) {
  const [value, setValue] = useState('1');

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  return (
    <>
      <PageHeader>
        <Grid container columnSpacing={3} alignItems="center">
          <Grid item xs>
            <Breadcrumbs>
              <Link underline="hover" color="inherit" href="/dashboard">
                Home
              </Link>
              <Link underline="hover" color="inherit" href="/dashboard">
                Dashboard
              </Link>
              <Typography color="text.primary">Overview</Typography>
            </Breadcrumbs>
          </Grid>
        </Grid>
      </PageHeader>

      <TabContext value={value}>
        <Box
          sx={{
            paddingX: 1,
            borderBottom: 1,
            borderColor: 'divider',
            backgroundColor: 'white',
          }}
        >
          <TabList onChange={handleChange}>
            <Tab label="Chargeback" value="1" />
            <Tab label="Order" value="2" />
            <Tab label="Declined Order" value="3" />
          </TabList>
        </Box>
        <TabPanel value="1">
          <CardChargeBack />
        </TabPanel>
        <TabPanel value="2">
          <CardOrderList />
        </TabPanel>
        <TabPanel value="3">
          <CardOrderDeclined />
        </TabPanel>
      </TabContext>
    </>
  );
}

export default DashboardOverviewView;
