import { render } from '@testing-library/react';

import { DialogTransactionApprove } from './dialog-transaction-approve';

describe('DialogTransactionForm', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<DialogTransactionApprove />);
    expect(baseElement).toBeTruthy();
  });
});
