import { useRouter } from 'next/router';
import { Button, Dialog, DialogActions, DialogTitle, Divider, Typography } from '@mui/material';
import { LoadingButton } from '@mui/lab';
import { Box } from '@mui/system';
import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import * as yup from 'yup';

import { useUpdatePayoutTransactionMutation } from '@verifsystem/dashboard/data-access';
import { IPayoutTransaction, EPayoutTransactionStatus } from '@verifsystem/shared/data-access';

/* eslint-disable-next-line */
export interface DialogTransactionApproveProps {
  open: boolean;
  transaction: IPayoutTransaction;
  onClose: () => void;
}

export function DialogTransactionApprove(props: DialogTransactionApproveProps) {
  const router = useRouter();
  const [updatePayoutTransaction, { isLoading }] = useUpdatePayoutTransactionMutation();
  const { transaction, onClose, open } = props;

  const validationScheme = yup
    .object({
      status: yup.string().required(),
    })
    .required();

  const { handleSubmit } = useForm({
    defaultValues: {
      status: EPayoutTransactionStatus.Approved,
    },
    resolver: yupResolver(validationScheme),
  });

  const handleClose = () => {
    onClose();
  };

  const handleFormSubmit = async (body) => {
    const accountId = +router.query.accountId;
    const transactionId = transaction.id;
    const result = await updatePayoutTransaction({ accountId, transactionId, body });

    if (result['data']) {
      handleClose();
    }
  };

  return (
    <Dialog onClose={handleClose} open={open} maxWidth="xs" fullWidth>
      <DialogTitle>Approve Transaction</DialogTitle>
      <Divider />
      <form onSubmit={handleSubmit(handleFormSubmit)}>
        <Box sx={{ padding: 3 }}>
          <Typography>Approve this transaction?</Typography>
        </Box>
        <DialogActions>
          <Button onClick={handleClose}>Close</Button>
          <LoadingButton variant="contained" type="submit" loading={isLoading}>
            Approve
          </LoadingButton>
        </DialogActions>
      </form>
    </Dialog>
  );
}

export default DialogTransactionApprove;
