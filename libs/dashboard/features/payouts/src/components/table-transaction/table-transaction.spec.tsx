import { render } from '@testing-library/react';

import TableTransaction from './table-transaction';

describe('TableAccount', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<TableTransaction />);
    expect(baseElement).toBeTruthy();
  });
});
