import { useState } from 'react';
import { useRouter } from 'next/router';
import { Chip, IconButton, ListItemIcon, ListItemText, Menu, MenuItem, Tooltip, Typography } from '@mui/material';
import { GridColDef } from '@mui/x-data-grid';
import { DotsHorizontal, ShareOutline } from 'mdi-material-ui';
import moment from 'moment';

import { DataGrid } from '@verifsystem/dashboard/shared';
import { useGetPayoutTransactionsQuery } from '@verifsystem/dashboard/data-access';
import { EPayoutTransactionStatus, IPayoutTransaction } from '@verifsystem/shared/data-access';

import { DialogTransactionApprove } from '../dialog-transaction-approve/dialog-transaction-approve';

/* eslint-disable-next-line */
export interface TableTransactionProps {}

/**
 * TableTransaction
 *
 * @param {TableTransactionProps} props
 * @returns JSX.Element
 */
export function TableTransaction(props: TableTransactionProps) {
  const router = useRouter();
  const { data: transactions = [], isLoading } = useGetPayoutTransactionsQuery({ accountId: +router?.query?.accountId }, { skip: !router?.query?.accountId });
  const [actionMenuAnchorEl, setActionMenuAnchorEl] = useState<null | HTMLElement>(null);
  const [dialogTransactionApproveOpen, setDialogTransactionApproveOpen] = useState(false);
  const [selectedTransaction, setSelectedTransaction] = useState<IPayoutTransaction>(null);
  const actionMenuOpen = Boolean(actionMenuAnchorEl);

  const handleDialogTransactionApproveOpen = () => {
    setDialogTransactionApproveOpen(true);
    handleActionMenuClose();
  };

  const handleDialogTransactionApproveClose = () => {
    setDialogTransactionApproveOpen(false);
  };

  const handleActionMenuClose = () => {
    setActionMenuAnchorEl(null);
  };

  const handleActionMenuClick = (event: React.MouseEvent<HTMLButtonElement>, transaction) => {
    setSelectedTransaction(transaction);
    setActionMenuAnchorEl(event.currentTarget);
  };

  const columns: GridColDef[] = [
    {
      field: 'amount',
      headerName: 'Amount',
      width: 250,
      renderCell: ({ row }) => <Typography>${row?.amount / 100}</Typography>,
    },
    {
      field: 'status',
      headerName: 'Status',
      width: 150,
      renderCell: ({ row }) => (
        <Chip
          label={row?.status.toUpperCase()}
          color={row?.status === EPayoutTransactionStatus.Approved ? 'success' : 'warning'}
          size="small"
        />
      ),
    },
    {
      field: 'description',
      headerName: 'Description',
      flex: 1,
      renderCell: ({ row }) => <Typography>{row?.description}</Typography>,
    },
    {
      field: 'createdAt',
      headerName: 'Date / Time',
      width: 250,
      renderCell: ({ row }) => <Typography>{moment(row?.createdAt).format('ll LT')}</Typography>,
    },
    {
      field: 'actions',
      type: 'actions',
      flex: 1,
      align: 'right',
      renderCell: (params) => {
        return (
          <>
            <Tooltip title="Action menu">
              <IconButton onClick={(event) => handleActionMenuClick(event, params.row)}>
                <DotsHorizontal />
              </IconButton>
            </Tooltip>
            <Menu
              anchorEl={actionMenuAnchorEl}
              open={actionMenuOpen}
              onClose={handleActionMenuClose}
              anchorOrigin={{
                vertical: 'bottom',
                horizontal: 'right',
              }}
              transformOrigin={{
                vertical: 'top',
                horizontal: 'right',
              }}
              PaperProps={{
                style: {
                  minWidth: 180,
                  boxShadow:
                    'rgb(255, 255, 255) 0px 0px 0px 0px, rgba(0, 0, 0, 0.02) 0px 0px 0px 1px, rgba(0, 0, 0, 0.01) 0px 4px 6px -2px',
                },
              }}
            >
              <MenuItem onClick={handleDialogTransactionApproveOpen}>
                <ListItemIcon>
                  <ShareOutline />
                </ListItemIcon>
                <ListItemText>Approve</ListItemText>
              </MenuItem>
            </Menu>
          </>
        );
      },
    },
  ];

  return (
    <>
      <DataGrid rows={transactions} columns={columns} checkboxSelection loading={isLoading} />
      <DialogTransactionApprove
        open={dialogTransactionApproveOpen}
        onClose={handleDialogTransactionApproveClose}
        transaction={selectedTransaction}
      />
    </>
  );
}

export default TableTransaction;
