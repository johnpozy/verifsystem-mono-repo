import { useEffect, useState } from 'react';
import { useRouter } from 'next/router';
import { Avatar, Chip, Divider, IconButton, ListItemIcon, ListItemText, Menu, MenuItem, Tooltip, Typography } from '@mui/material';
import { Box } from '@mui/system';
import { GridColDef, GridColumnVisibilityModel } from '@mui/x-data-grid';
import { ArchiveOutline, DotsHorizontal, History, Pencil, ShareOutline, TrashCanOutline } from 'mdi-material-ui';
import moment from 'moment';

import { DataGrid } from '@verifsystem/dashboard/shared';
import { useAppSelector, stateSelectedEcomm, useGetPayoutAccountsQuery } from '@verifsystem/dashboard/data-access';
import { IPayoutAccount } from '@verifsystem/shared/data-access';

import { DialogPayoutForm } from '../dialog-payout-form/dialog-payout-form';
import { DialogTransactionForm } from '../dialog-transaction-form/dialog-transaction-form';
import { DialogAccountDelete } from '../dialog-account-delete/dialog-account-delete';

/* eslint-disable-next-line */
export interface TableAccountProps {}

/**
 * TableAccount
 *
 * @param {TableAccountProps} props
 * @returns JSX.Element
 */
export function TableAccount(props: TableAccountProps) {
  const router = useRouter();
  const selectedEcomm = useAppSelector(stateSelectedEcomm);
  const { data: payouts = [], isLoading } = useGetPayoutAccountsQuery({ ecommId: selectedEcomm ? selectedEcomm.id : null });
  const [actionMenuAnchorEl, setActionMenuAnchorEl] = useState<null | HTMLElement>(null);
  const [dialogAccountFormOpen, setDialogAccountFormOpen] = useState(false);
  const [dialogAccountDeleteOpen, setDialogAccountDeleteOpen] = useState(false);
  const [dialogTransactionFormOpen, setDialogTransactionFormOpen] = useState(false);
  const [selectedPayout, setSelectedPayout] = useState<IPayoutAccount>(null);
  const [columnVisibilityModel, setColumnVisibilityModel] = useState<GridColumnVisibilityModel>({ ecomm: false });
  const actionMenuOpen = Boolean(actionMenuAnchorEl);

  const handleDialogAccountFormOpen = () => {
    setDialogAccountFormOpen(true);
    handleActionMenuClose();
  };

  const handleDialogAccountDeleteOpen = () => {
    setDialogAccountDeleteOpen(true);
    handleActionMenuClose();
  };

  const handleDialogTransactionFormOpen = () => {
    setDialogTransactionFormOpen(true);
    handleActionMenuClose();
  };

  const handleDialogAccountFormClose = () => {
    setDialogAccountFormOpen(false);
  };

  const handleDialogAccountDeleteClose = () => {
    setDialogAccountDeleteOpen(false);
  };

  const handleDialogTransactionFormClose = () => {
    setDialogTransactionFormOpen(false);
  };

  const handleActionMenuClose = () => {
    setActionMenuAnchorEl(null);
  };

  const handleActionMenuClick = (event: React.MouseEvent<HTMLButtonElement>, payout) => {
    setSelectedPayout(payout);
    setActionMenuAnchorEl(event.currentTarget);
  };

  const gotToHistory = () => {
    router.push(`payout/${selectedPayout.id}/history`);
    handleActionMenuClose();
  };

  const columns: GridColDef[] = [
    {
      field: 'name',
      headerName: 'Account Name',
      width: 200,
      renderCell: (params) => {
        return (
          <Box sx={{ display: 'flex', alignItems: 'center' }}>
            <Avatar alt={params.row.name} sx={{ marginRight: '.5rem', width: 32, height: 32 }} variant="rounded">
              {params.row.name.toUpperCase().charAt(0)}
            </Avatar>
            <Typography>{params.row.name}</Typography>
          </Box>
        );
      },
    },
    {
      field: 'ecomm',
      headerName: 'Ecomm',
      flex: 1,
      hideable: false,
      valueGetter: ({ row }) => row?.ecomm?.title,
      renderCell: ({ row }) => <Typography>{row?.ecomm?.title}</Typography>,
    },
    {
      field: 'type',
      headerName: 'Account Type',
      flex: 1,
      renderCell: (params) => <Typography>{params.row.type}</Typography>,
    },
    {
      field: 'lastActive',
      headerName: 'Account Number',
      width: 250,
      renderCell: (params) => <Typography>{moment(params.row.lastActive).format('ll')}</Typography>,
    },
    {
      field: 'status',
      headerName: 'Status',
      width: 150,
      renderCell: (params) => <Chip label={params.row.status.toUpperCase()} color="primary" size="small" />,
    },
    {
      field: 'actions',
      type: 'actions',
      flex: 1,
      align: 'right',
      renderCell: (params) => {
        return (
          <>
            <Tooltip title="Action menu">
              <IconButton onClick={(event) => handleActionMenuClick(event, params.row)}>
                <DotsHorizontal />
              </IconButton>
            </Tooltip>
            <Menu
              anchorEl={actionMenuAnchorEl}
              open={actionMenuOpen}
              onClose={handleActionMenuClose}
              anchorOrigin={{
                vertical: 'bottom',
                horizontal: 'right',
              }}
              transformOrigin={{
                vertical: 'top',
                horizontal: 'right',
              }}
              PaperProps={{
                style: {
                  minWidth: 180,
                  boxShadow:
                    'rgb(255, 255, 255) 0px 0px 0px 0px, rgba(0, 0, 0, 0.02) 0px 0px 0px 1px, rgba(0, 0, 0, 0.01) 0px 4px 6px -2px',
                },
              }}
            >
              <MenuItem onClick={handleDialogTransactionFormOpen}>
                <ListItemIcon>
                  <ShareOutline />
                </ListItemIcon>
                <ListItemText>Make Transaction</ListItemText>
              </MenuItem>
              <Divider />
              <MenuItem onClick={gotToHistory}>
                <ListItemIcon>
                  <History />
                </ListItemIcon>
                <ListItemText>View History</ListItemText>
              </MenuItem>
              <MenuItem onClick={handleDialogAccountFormOpen}>
                <ListItemIcon>
                  <Pencil />
                </ListItemIcon>
                <ListItemText>Edit Account</ListItemText>
              </MenuItem>
              <Divider />
              <MenuItem disabled>
                <ListItemIcon>
                  <ArchiveOutline />
                </ListItemIcon>
                <ListItemText>Archive Account</ListItemText>
              </MenuItem>
              <MenuItem onClick={handleDialogAccountDeleteOpen}>
                <ListItemIcon>
                  <TrashCanOutline color="error" />
                </ListItemIcon>
                <ListItemText>Delete</ListItemText>
              </MenuItem>
            </Menu>
          </>
        );
      },
    },
  ];

  useEffect(() => {
    setColumnVisibilityModel({
      ecomm: !selectedEcomm
    });
  }, [selectedEcomm]);

  return (
    <>
      <DataGrid
        rows={payouts}
        columns={columns}
        checkboxSelection
        loading={isLoading}
        columnVisibilityModel={columnVisibilityModel}
        onColumnVisibilityModelChange={(newModel) => setColumnVisibilityModel(newModel)}
      />
      <DialogPayoutForm open={dialogAccountFormOpen} payout={selectedPayout} onClose={handleDialogAccountFormClose} />
      <DialogTransactionForm open={dialogTransactionFormOpen} payout={selectedPayout} onClose={handleDialogTransactionFormClose} />
      <DialogAccountDelete open={dialogAccountDeleteOpen} payout={selectedPayout} onClose={handleDialogAccountDeleteClose} />
    </>
  );
}

export default TableAccount;
