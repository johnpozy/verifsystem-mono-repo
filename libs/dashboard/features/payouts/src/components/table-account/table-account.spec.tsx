import { render } from '@testing-library/react';

import TableAccount from './table-account';

describe('TableAccount', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<TableAccount />);
    expect(baseElement).toBeTruthy();
  });
});
