import { Button, Dialog, DialogActions, DialogTitle, Divider, Typography } from '@mui/material';
import { LoadingButton } from '@mui/lab';
import { Box } from '@mui/system';
import { useForm } from 'react-hook-form';

import { useDeletePayoutAccountMutation } from '@verifsystem/dashboard/data-access';
import { IPayoutAccount } from '@verifsystem/shared/data-access';

/* eslint-disable-next-line */
export interface DialogAccountDeleteProps {
  open: boolean;
  payout: IPayoutAccount;
  onClose: () => void;
}

export function DialogAccountDelete(props: DialogAccountDeleteProps) {
  const [deletePayoutAccount, { isLoading }] = useDeletePayoutAccountMutation();
  const { payout, onClose, open } = props;
  const { handleSubmit } = useForm();

  const handleClose = () => {
    onClose();
  };

  const handleFormSubmit = async (body) => {
    const accountId = payout.id;
    const result = await deletePayoutAccount({ accountId });

    if (result['data']) {
      handleClose();
    }
  };

  return (
    <Dialog onClose={handleClose} open={open} maxWidth="xs" fullWidth>
      <DialogTitle>Delete Payout Account</DialogTitle>
      <Divider />
      <form onSubmit={handleSubmit(handleFormSubmit)}>
        <Box sx={{ padding: 3 }}>
          <Typography mb={1}>Delete this payout account?</Typography>
          <Typography variant="body2" color="gray">
            Note: All transaction under this payout account will be deleted too.
          </Typography>
        </Box>
        <DialogActions>
          <Button onClick={handleClose}>Close</Button>
          <LoadingButton variant="contained" type="submit" color="error" loading={isLoading}>
            Delete
          </LoadingButton>
        </DialogActions>
      </form>
    </Dialog>
  );
}

export default DialogAccountDelete;
