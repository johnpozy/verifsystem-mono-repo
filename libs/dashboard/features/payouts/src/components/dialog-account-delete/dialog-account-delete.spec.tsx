import { render } from '@testing-library/react';

import DialogAccountDelete from './dialog-account-delete';

describe('DialogAccountDelete', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<DialogAccountDelete />);
    expect(baseElement).toBeTruthy();
  });
});
