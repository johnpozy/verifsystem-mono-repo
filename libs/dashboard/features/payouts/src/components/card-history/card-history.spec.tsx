import { render } from '@testing-library/react';

import CardHistory from './card-history';

describe('CardHistory', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<CardHistory />);
    expect(baseElement).toBeTruthy();
  });
});
