import { Card, CardHeader, Divider } from '@mui/material';

import { TableTransaction } from '../table-transaction/table-transaction';

/* eslint-disable-next-line */
export interface CardHistoryProps {}

export function CardHistory(props: CardHistoryProps) {
  return (
    <Card>
      <CardHeader titleTypographyProps={{ variant: 'h6' }} title="Transaction History" />
      <Divider />
      <TableTransaction />
    </Card>
  );
}

export default CardHistory;
