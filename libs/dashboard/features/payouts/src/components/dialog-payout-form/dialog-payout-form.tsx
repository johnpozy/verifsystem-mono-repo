import { useEffect, useState } from 'react';
import {
  Button,
  Dialog,
  DialogActions,
  DialogTitle,
  Divider,
  FormControl,
  FormHelperText,
  Grid,
  InputLabel,
  MenuItem,
  Select,
  TextField,
  Typography,
} from '@mui/material';
import { Box } from '@mui/system';
import { useForm, Controller } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import * as yup from 'yup';

import { useAppSelector, stateSelectedEcomm, useCreatePayoutAccountMutation, useUpdatePayoutAccountMutation, useGetEcommsQuery } from '@verifsystem/dashboard/data-access';
import { EPayoutType, IPayoutAccount } from '@verifsystem/shared/data-access';
import { LoadingButton } from '@mui/lab';

/* eslint-disable-next-line */
export interface DialogPayoutFormProps {
  open: boolean;
  payout?: IPayoutAccount;
  onClose: () => void;
}

export function DialogPayoutForm(props: DialogPayoutFormProps) {
  const [createPayoutAccount, { isLoading: isCreating }] = useCreatePayoutAccountMutation();
  const [updatePayoutAccount, { isLoading: isUpdating }] = useUpdatePayoutAccountMutation();
  const selectedEcomm = useAppSelector(stateSelectedEcomm);
  const { data: ecomms = [] } = useGetEcommsQuery();
  const [ecommId, setEcommId] = useState<string>('');
  const { payout, onClose, open } = props;

  const validationScheme = yup
    .object({
      name: yup.string().required(),
      type: yup.string().required(),
      accountNumber: yup.string().required(),
      details: yup.object().shape({
        contactEmail: yup.string().email(),
      }),
    })
    .required();

  const {
    control,
    handleSubmit,
    reset,
    setValue,
    formState: { errors },
  } = useForm({
    defaultValues: {
      name: '',
      type: '',
      accountNumber: '',
      details: {
        contactName: '',
        contactPhone: '',
        contactEmail: '',
      },
    },
    resolver: yupResolver(validationScheme),
  });

  const handleClose = () => {
    onClose();
    reset();
  };

  const handleFormSubmit = async (body) => {
    let result;

    if (payout) {
      const accountId = payout.id;
      result = await updatePayoutAccount({ accountId, body });
    } else {
      result = await createPayoutAccount({ ecommId: selectedEcomm ? selectedEcomm.id : +ecommId, body });
    }

    if (result['data']) {
      handleClose();
    }
  };

  useEffect(() => {
    if (payout) {
      setEcommId(payout?.ecomm?.id.toString());

      reset({
        accountNumber: payout.accountNumber,
        details: payout.details,
        name: payout.name,
        type: payout.type
      });
    }
  }, [reset, payout]);

  return (
    <Dialog onClose={handleClose} open={open} maxWidth="sm" fullWidth>
      <DialogTitle>{payout?.id ? 'Update' : 'Add'} Payout Account</DialogTitle>
      <Divider />
      <form onSubmit={handleSubmit(handleFormSubmit)}>
        <Box sx={{ padding: 3 }}>
          <Grid container spacing={3}>
            <Grid item xs={12}>
              <Controller
                name="name"
                control={control}
                render={({ field }) => (
                  <TextField
                    {...field}
                    label="Account Name"
                    size="small"
                    fullWidth
                    error={!!errors?.name}
                    helperText={errors.name?.message}
                    InputLabelProps={{
                      shrink: true,
                    }}
                  />
                )}
              />
            </Grid>
            <Grid item xs={12}>
              <Controller
                name="accountNumber"
                control={control}
                render={({ field }) => (
                  <TextField
                    {...field}
                    label="Account Number"
                    size="small"
                    fullWidth
                    error={!!errors?.accountNumber}
                    helperText={errors.accountNumber?.message}
                    InputLabelProps={{
                      shrink: true,
                    }}
                  />
                )}
              />
            </Grid>
            <Grid item xs={6}>
              <Controller
                name="type"
                control={control}
                render={({ field }) => (
                  <FormControl size="small" fullWidth error={!!errors?.type}>
                    <InputLabel shrink>Type</InputLabel>
                    <Select {...field} label="Type" notched onChange={({ target }) => setValue('type', target.value)}>
                      {Object.keys(EPayoutType).map((payoutType) => {
                        return <MenuItem key={EPayoutType[payoutType]} value={EPayoutType[payoutType]}>{payoutType}</MenuItem>;
                      })}
                    </Select>
                    <FormHelperText>{errors.type?.message}</FormHelperText>
                  </FormControl>
                )}
              />
            </Grid>
            {!selectedEcomm && (
              <Grid item xs={6}>
                <FormControl size="small" fullWidth disabled={!!payout}>
                  <InputLabel shrink>Ecomm</InputLabel>
                  <Select value={ecommId} label="Ecomm" notched onChange={({ target }) => setEcommId(target.value as string)}>
                    {ecomms.map((ecomm) => {
                      return <MenuItem key={ecomm?.id} value={ecomm?.id}>{ecomm?.title}</MenuItem>;
                    })}
                  </Select>
                </FormControl>
              </Grid>
            )}
            <Grid item xs={12}>
              <Typography color="text.secondary" display="block" variant="overline">
                Contact Detail
              </Typography>
            </Grid>
            <Grid item xs={12}>
              <Controller
                name="details.contactName"
                control={control}
                render={({ field }) => (
                  <TextField
                    {...field}
                    label="Name"
                    size="small"
                    fullWidth
                    error={!!errors?.details?.contactName}
                    helperText={errors?.details?.contactName?.message}
                    InputLabelProps={{
                      shrink: true,
                    }}
                  />
                )}
              />
            </Grid>
            <Grid item xs={6}>
              <Controller
                name="details.contactEmail"
                control={control}
                render={({ field }) => (
                  <TextField
                    {...field}
                    type="email"
                    label="Email"
                    size="small"
                    fullWidth
                    error={!!errors?.details?.contactEmail}
                    helperText={errors?.details?.contactEmail?.message}
                    InputLabelProps={{
                      shrink: true,
                    }}
                  />
                )}
              />
            </Grid>
            <Grid item xs={6}>
              <Controller
                name="details.contactPhone"
                control={control}
                render={({ field }) => (
                  <TextField
                    {...field}
                    label="Phone"
                    size="small"
                    fullWidth
                    error={!!errors?.details?.contactPhone}
                    helperText={errors?.details?.contactPhone?.message}
                    InputLabelProps={{
                      shrink: true,
                    }}
                  />
                )}
              />
            </Grid>
          </Grid>
        </Box>
        <DialogActions>
          <Button onClick={handleClose}>Close</Button>
          <LoadingButton variant="contained" type="submit" loading={isCreating || isUpdating}>
            Save
          </LoadingButton>
        </DialogActions>
      </form>
    </Dialog>
  );
}

export default DialogPayoutForm;
