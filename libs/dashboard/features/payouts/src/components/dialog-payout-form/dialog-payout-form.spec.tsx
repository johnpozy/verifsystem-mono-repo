import { render } from '@testing-library/react';

import DialogPayoutForm from './dialog-payout-form';

describe('DialogPayoutForm', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<DialogPayoutForm />);
    expect(baseElement).toBeTruthy();
  });
});
