import { Card, CardHeader, Divider } from '@mui/material';

import { TableAccount } from '../table-account/table-account';

/* eslint-disable-next-line */
export interface CardPayoutListProps {}

export function CardPayoutList(props: CardPayoutListProps) {
  return (
    <Card>
      <CardHeader titleTypographyProps={{ variant: 'h6' }} title="Account List" />
      <Divider />

      <TableAccount />
    </Card>
  );
}

export default CardPayoutList;
