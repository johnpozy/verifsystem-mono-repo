import { render } from '@testing-library/react';

import CardPayoutList from './card-payout-list';

describe('CardPayoutList', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<CardPayoutList />);
    expect(baseElement).toBeTruthy();
  });
});
