import { render } from '@testing-library/react';

import DialogTransactionForm from './dialog-transaction-form';

describe('DialogTransactionForm', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<DialogTransactionForm />);
    expect(baseElement).toBeTruthy();
  });
});
