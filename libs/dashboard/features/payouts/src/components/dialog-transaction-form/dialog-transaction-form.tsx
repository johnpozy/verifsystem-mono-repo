import { useRouter } from 'next/router';
import { Button, Dialog, DialogActions, DialogTitle, Divider, Grid, TextField, InputAdornment } from '@mui/material';
import { LoadingButton } from '@mui/lab';
import { Box } from '@mui/system';
import { useForm, Controller } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import * as yup from 'yup';

import { useCreatePayoutTransactionMutation } from '@verifsystem/dashboard/data-access';
import { IPayoutAccount } from '@verifsystem/shared/data-access';

/* eslint-disable-next-line */
export interface DialogTransactionFormProps {
  open: boolean;
  payout?: IPayoutAccount;
  onClose: () => void;
}

export function DialogTransactionForm(props: DialogTransactionFormProps) {
  const router = useRouter();
  const [createPayoutTransaction, { isLoading }] = useCreatePayoutTransactionMutation();
  const { payout, onClose, open } = props;

  const validationScheme = yup
    .object({
      amount: yup.number().required(),
      description: yup.string().required(),
    })
    .required();

  const {
    control,
    handleSubmit,
    reset,
    formState: { errors },
  } = useForm({
    defaultValues: {
      amount: 0,
      description: '',
    },
    resolver: yupResolver(validationScheme),
  });

  const handleClose = () => {
    onClose();
    reset();
  };

  const handleFormSubmit = async (payload) => {
    const accountId = payout.id;
    const body = {...payload, amount: payload.amount * 100};
    const result = await createPayoutTransaction({ accountId, body });

    if (result['data']) {
      handleClose();

      setTimeout(() => {
        router.push(`payout/${payout.id}/history`);
      }, 100);
    }
  };

  return (
    <Dialog onClose={handleClose} open={open} maxWidth="sm" fullWidth>
      <DialogTitle>Make Transaction</DialogTitle>
      <Divider />
      <form onSubmit={handleSubmit(handleFormSubmit)}>
        <Box sx={{ padding: 3 }}>
          <Grid container spacing={3}>
            <Grid item xs={12}>
              <Controller
                name="amount"
                control={control}
                render={({ field }) => (
                  <TextField
                    {...field}
                    type="number"
                    label="Amount"
                    size="small"
                    fullWidth
                    error={!!errors?.amount}
                    helperText={errors.amount?.message}
                    InputProps={{
                      startAdornment: <InputAdornment position="start">$</InputAdornment>,
                    }}
                    InputLabelProps={{
                      shrink: true,
                    }}
                  />
                )}
              />
            </Grid>
            <Grid item xs={12}>
              <Controller
                name="description"
                control={control}
                render={({ field }) => (
                  <TextField
                    {...field}
                    label="Description"
                    multiline
                    rows={4}
                    size="small"
                    fullWidth
                    error={!!errors?.description}
                    helperText={errors.description?.message}
                    InputLabelProps={{
                      shrink: true,
                    }}
                  />
                )}
              />
            </Grid>
          </Grid>
        </Box>
        <DialogActions>
          <Button onClick={handleClose}>Close</Button>
          <LoadingButton variant="contained" type="submit" loading={isLoading}>
            Save
          </LoadingButton>
        </DialogActions>
      </form>
    </Dialog>
  );
}

export default DialogTransactionForm;
