import { useState } from 'react';
import { Breadcrumbs, Button, Grid, Link, Typography } from '@mui/material';
import { Box } from '@mui/system';

import { PageHeader } from '@verifsystem/dashboard/shared';
import { Plus } from 'mdi-material-ui';

import { CardPayoutList } from '../../components/card-payout-list/card-payout-list';
import { DialogPayoutForm } from '../../components/dialog-payout-form/dialog-payout-form';

/* eslint-disable-next-line */
export interface PayoutProps {}

export function Payout(props: PayoutProps) {
  const [dialogOpen, setDialogOpen] = useState(false);

  const handleDialogOpen = () => {
    setDialogOpen(true);
  };

  const handleDialogClose = () => {
    setDialogOpen(false);
  };

  return (
    <>
      <Box>
        <PageHeader>
          <Grid container columnSpacing={3} alignItems="center">
            <Grid item xs>
              <Breadcrumbs>
                <Link underline="hover" color="inherit" href="/dashboard">
                  Home
                </Link>
                <Typography color="text.primary">Payout</Typography>
              </Breadcrumbs>
            </Grid>
            <Grid item xs="auto">
                <Button
                  endIcon={<Plus />}
                  variant="contained"
                  fullWidth
                  disableElevation
                  onClick={handleDialogOpen}
                >
                  Add Account
                </Button>
              </Grid>
          </Grid>
        </PageHeader>

        <Box p={3}>
          <CardPayoutList />
        </Box>
      </Box>

      <DialogPayoutForm open={dialogOpen} onClose={handleDialogClose} />
    </>
  );
}

export default Payout;
