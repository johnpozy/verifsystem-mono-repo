import { Breadcrumbs, Button, Grid, Link, Typography } from '@mui/material';
import { Box } from '@mui/system';

import { PageHeader } from '@verifsystem/dashboard/shared';
import { Plus } from 'mdi-material-ui';

import { CardHistory } from '../../components/card-history/card-history';

/* eslint-disable-next-line */
export interface HistoryProps {}

export function History(props: HistoryProps) {
  return (
    <Box>
      <PageHeader>
        <Grid container columnSpacing={3} alignItems="center">
          <Grid item xs>
            <Breadcrumbs>
              <Link underline="hover" color="inherit" href="/dashboard">
                Home
              </Link>
              <Link underline="hover" color="inherit" href="/payout">
                Payout
              </Link>
              <Typography color="text.primary">Transaction History</Typography>
            </Breadcrumbs>
          </Grid>
        </Grid>
      </PageHeader>

      <Box p={3}>
        <CardHistory />
      </Box>
    </Box>
  );
}

export default History;
