import { useEffect } from 'react';
import { useRouter } from 'next/router';
import { Card, CardContent, Grid, Typography } from '@mui/material';
import { grey } from '@mui/material/colors';

import { persistor, setSessionExpired, useAppDispatch } from '@verifsystem/dashboard/data-access';

import styles from './index.module.scss';

/* eslint-disable-next-line */
export interface LogoutProps {}

export function Logout(props: LogoutProps) {
  const router = useRouter();
  const dispatch = useAppDispatch();

  useEffect(() => {
    persistor.purge();
    dispatch(setSessionExpired(true));

    setTimeout(() => {
      router.push('/auth/login');
    }, 1000);
  }, [router, dispatch]);

  return (
    <div className={styles.container}>
      <Grid container spacing={2} justifyContent="center">
        <Grid item xs={3}>
          <Card variant="outlined">
            <CardContent>
              <Typography variant="body2" align='center' component="div" color={grey[700]}>
                Logging out...
              </Typography>
            </CardContent>
          </Card>
        </Grid>
      </Grid>
    </div>
  );
}

export default Logout;
