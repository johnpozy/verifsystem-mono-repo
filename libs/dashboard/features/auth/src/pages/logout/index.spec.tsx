import { render } from '@testing-library/react';

import Logout from './index';

describe('Logout', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<Logout />);
    expect(baseElement).toBeTruthy();
  });
});
