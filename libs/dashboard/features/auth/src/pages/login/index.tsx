import Image from 'next/image';
import { useRouter } from 'next/router';
import { Controller, useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import { Alert, Avatar, Card, CardContent, Checkbox, FormControlLabel, Grid, InputAdornment, TextField, Typography } from '@mui/material';
import { Box } from '@mui/system';
import { grey } from '@mui/material/colors';
import { LoadingButton } from '@mui/lab';
import { Email, Key } from 'mdi-material-ui';
import * as yup from 'yup';

import { setLoggedUser, setSessionExpired, useAppDispatch, useLoginMutation } from '@verifsystem/dashboard/data-access';

import styles from './index.module.scss';
import { accessControl } from '@verifsystem/shared/utils';

/* eslint-disable-next-line */
export interface LoginProps {}

export function Login(props: LoginProps) {
  const router = useRouter();
  const dispatch = useAppDispatch();
  const [login, { isLoading, isError }] = useLoginMutation();
  const validationScheme = yup
    .object({
      email: yup.string().email().required(),
      password: yup.string().min(5).required(),
    })
    .required();

  const {
    control,
    handleSubmit,
    formState: { errors },
  } = useForm({
    defaultValues: {
      email: '',
      password: '',
    },
    resolver: yupResolver(validationScheme),
  });

  const handleFormSubmit = async (body) => {
    const user = await login({ body });

    if (user['data']) {
      dispatch(setLoggedUser(user['data']));
      dispatch(setSessionExpired(false));

      if (accessControl.can(user['data']?.role).readAny('dashboard').granted) {
        router.push('/dashboard');
      } else {
        router.push('/processor');
      }
    }
  };

  return (
    <form className={styles.container} onSubmit={handleSubmit(handleFormSubmit)}>
      <Grid container spacing={2} justifyContent="center">
        <Grid item xs={3}>
          <Box className={styles.logo} px={3} py={3}>
            <Avatar sx={{ width: 68, height: 68, color: 'primary.main' }} variant="rounded" className={styles.logoFigure}>
              <Image
                loader={({ src }) => src}
                src="/img/logo.svg"
                placeholder="empty"
                objectFit="cover"
                quality={100}
                width={48}
                height={48}
              />
            </Avatar>
          </Box>

          <Card variant="outlined">
            <CardContent>
              <Typography variant="h5" component="div" mt={4} sx={{ fontWeight: 700 }}>
                Welcome back!
              </Typography>
              <Typography variant="body2" mb={4} component="div" color={grey[700]}>
                Enter your credential.
              </Typography>

              <Grid container spacing={3}>
                {isError && (
                  <Grid item xs={12}>
                    <Alert severity="error">Please ensure your login credential is correct!</Alert>
                  </Grid>
                )}

                <Grid item xs={12}>
                  <Controller
                    name="email"
                    control={control}
                    render={({ field }) => (
                      <TextField
                        {...field}
                        label="Email"
                        size="small"
                        fullWidth
                        error={!!errors?.email}
                        helperText={errors.email?.message}
                        InputLabelProps={{
                          shrink: true,
                        }}
                        InputProps={{
                          startAdornment: (
                            <InputAdornment position="start">
                              <Email />
                            </InputAdornment>
                          ),
                        }}
                      />
                    )}
                  />
                </Grid>
                <Grid item xs={12}>
                  <Controller
                    name="password"
                    control={control}
                    render={({ field }) => (
                      <TextField
                        {...field}
                        type="password"
                        label="Password"
                        size="small"
                        fullWidth
                        error={!!errors?.password}
                        helperText={errors.password?.message}
                        InputLabelProps={{
                          shrink: true,
                        }}
                        InputProps={{
                          startAdornment: (
                            <InputAdornment position="start">
                              <Key />
                            </InputAdornment>
                          ),
                        }}
                      />
                    )}
                  />
                </Grid>
                <Grid item xs={12}>
                  <FormControlLabel control={<Checkbox />} label="Remember my login" />
                </Grid>
                <Grid item xs={12}>
                  <LoadingButton variant="contained" fullWidth type="submit" loading={isLoading}>
                    Login
                  </LoadingButton>
                </Grid>
              </Grid>
            </CardContent>
          </Card>
        </Grid>
      </Grid>
    </form>
  );
}

export default Login;
