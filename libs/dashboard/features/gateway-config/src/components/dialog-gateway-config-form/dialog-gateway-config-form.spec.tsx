import { render } from '@testing-library/react';

import DialogGatewayConfigForm from './dialog-gateway-config-form';

describe('DialogGatewayConfigForm', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<DialogGatewayConfigForm />);
    expect(baseElement).toBeTruthy();
  });
});
