import { useEffect } from 'react';
import {
  Button,
  Dialog,
  DialogActions,
  DialogTitle,
  Divider,
  FormControl,
  FormHelperText,
  Grid,
  InputAdornment,
  InputLabel,
  MenuItem,
  Select,
  TextField,
  Typography,
} from '@mui/material';
import { Box } from '@mui/system';
import { useForm, Controller } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import * as yup from 'yup';

import { useUpdateGatewayConfigMutation, useCreateGatewayConfigMutation } from '@verifsystem/dashboard/data-access';
import { IGatewayConfig } from '@verifsystem/shared/data-access';
import { LoadingButton } from '@mui/lab';

/* eslint-disable-next-line */
export interface DialogGatewayConfigFormProps {
  open: boolean;
  gatewayConfig?: IGatewayConfig;
  processorId: number;
  onClose: () => void;
}

export function DialogGatewayConfigForm(props: DialogGatewayConfigFormProps) {
  const [createGatewayConfig, { isLoading: isCreating }] = useCreateGatewayConfigMutation();
  const [updateGatewayConfig, { isLoading: isUpdating }] = useUpdateGatewayConfigMutation();
  const { gatewayConfig, onClose, open } = props;

  const validationScheme = yup
    .object({
      name: yup.string().required(),
      apiKey: yup.string().required(),
      type: yup.string().required(),
    })
    .required();

  const {
    control,
    handleSubmit,
    reset,
    setValue,
    formState: { errors },
  } = useForm({
    defaultValues: {
      name: '',
      apiKey: '',
      type: '',
      maxDailySessions: 0,
      maxDailyTxnNumber: 0,
      maxDailyTxnAmount: 0,
      maxMonthlyTxnNumber: 0,
      maxMonthlyTxnAmount: 0,
    },
    resolver: yupResolver(validationScheme),
  });

  const handleClose = () => {
    onClose();
    reset();
  };

  const handleFormSubmit = async (body) => {
    let result;

    if (gatewayConfig) {
      const gatewayConfigId = gatewayConfig.id;
      result = await updateGatewayConfig({ gatewayConfigId, body });
    } else {
      const processorId = props.processorId;
      result = await createGatewayConfig({ processorId, body });
    }

    if (result['data']) {
      handleClose();
    }
  };

  useEffect(() => {
    if (gatewayConfig) {
      reset(gatewayConfig);
    }
  }, [reset, gatewayConfig]);

  return (
    <Dialog onClose={handleClose} open={open} maxWidth="sm" fullWidth>
      <DialogTitle>{gatewayConfig?.id ? 'Update' : 'Add'} Gateway Config</DialogTitle>
      <Divider />
      <form onSubmit={handleSubmit(handleFormSubmit)}>
        <Box sx={{ padding: 3 }}>
          <Grid container spacing={3}>
            <Grid item xs={12}>
              <Controller
                name="name"
                control={control}
                render={({ field }) => (
                  <TextField
                    {...field}
                    label="Name"
                    size="small"
                    fullWidth
                    error={!!errors?.name}
                    helperText={errors.name?.message}
                    InputLabelProps={{
                      shrink: true,
                    }}
                  />
                )}
              />
            </Grid>
            <Grid item xs={12}>
              <Controller
                name="apiKey"
                control={control}
                render={({ field }) => (
                  <TextField
                    {...field}
                    label="API Key"
                    size="small"
                    fullWidth
                    error={!!errors?.apiKey}
                    helperText={errors.apiKey?.message}
                    InputLabelProps={{
                      shrink: true,
                    }}
                  />
                )}
              />
            </Grid>
            <Grid item xs={6}>
              <Controller
                name="type"
                control={control}
                render={({ field }) => (
                  <FormControl size="small" fullWidth error={!!errors?.type}>
                    <InputLabel shrink>Type</InputLabel>
                    <Select {...field} label="Type" notched onChange={({ target }) => setValue('type', target.value)}>
                      <MenuItem value="veriff">Veriff</MenuItem>
                      <MenuItem value="stripe">Stripe</MenuItem>
                      <MenuItem value="bambora" disabled>
                        Bambora
                      </MenuItem>
                      <MenuItem value="elavon" disabled>
                        Elavon
                      </MenuItem>
                    </Select>
                    <FormHelperText>{errors.type?.message}</FormHelperText>
                  </FormControl>
                )}
              />
            </Grid>
            <Grid item xs={12}>
              <Typography color="text.secondary" display="block" variant="overline">
                Session
              </Typography>
            </Grid>
            <Grid item xs={6}>
              <Controller
                name="maxDailySessions"
                control={control}
                render={({ field }) => (
                  <TextField
                    {...field}
                    type="number"
                    label="Max Daily Session"
                    size="small"
                    fullWidth
                    error={!!errors?.maxDailySessions}
                    helperText={errors.maxDailySessions?.message}
                    InputLabelProps={{
                      shrink: true,
                    }}
                  />
                )}
              />
            </Grid>
            <Grid item xs={12}>
              <Typography color="text.secondary" display="block" variant="overline">
                Transaction
              </Typography>
            </Grid>
            <Grid item xs={6}>
              <Controller
                name="maxDailyTxnNumber"
                control={control}
                render={({ field }) => (
                  <TextField
                    {...field}
                    type="number"
                    label="Max Daily Transaction Number"
                    size="small"
                    fullWidth
                    error={!!errors?.maxDailyTxnNumber}
                    helperText={errors.maxDailyTxnNumber?.message}
                    InputLabelProps={{
                      shrink: true,
                    }}
                  />
                )}
              />
            </Grid>
            <Grid item xs={6}>
              <Controller
                name="maxDailyTxnAmount"
                control={control}
                render={({ field }) => (
                  <TextField
                    {...field}
                    type="number"
                    label="Max Daily Transaction Amount"
                    size="small"
                    fullWidth
                    error={!!errors?.maxDailyTxnAmount}
                    helperText={errors.maxDailyTxnAmount?.message}
                    InputProps={{
                      startAdornment: <InputAdornment position="start">$</InputAdornment>,
                    }}
                    InputLabelProps={{
                      shrink: true,
                    }}
                  />
                )}
              />
            </Grid>
            <Grid item xs={6}>
              <Controller
                name="maxMonthlyTxnNumber"
                control={control}
                render={({ field }) => (
                  <TextField
                    {...field}
                    type="number"
                    label="Max Monthly Transaction Number"
                    size="small"
                    fullWidth
                    error={!!errors?.maxMonthlyTxnNumber}
                    helperText={errors.maxMonthlyTxnNumber?.message}
                    InputLabelProps={{
                      shrink: true,
                    }}
                  />
                )}
              />
            </Grid>
            <Grid item xs={6}>
              <Controller
                name="maxMonthlyTxnAmount"
                control={control}
                render={({ field }) => (
                  <TextField
                    {...field}
                    type="number"
                    label="Max Monthly Transaction Amount"
                    size="small"
                    fullWidth
                    error={!!errors?.maxMonthlyTxnAmount}
                    helperText={errors.maxMonthlyTxnAmount?.message}
                    InputProps={{
                      startAdornment: <InputAdornment position="start">$</InputAdornment>,
                    }}
                    InputLabelProps={{
                      shrink: true,
                    }}
                  />
                )}
              />
            </Grid>
          </Grid>
        </Box>
        <DialogActions>
          <Button onClick={handleClose}>Close</Button>
          <LoadingButton variant="contained" type="submit" loading={isCreating || isUpdating}>
            Save
          </LoadingButton>
        </DialogActions>
      </form>
    </Dialog>
  );
}

export default DialogGatewayConfigForm;
