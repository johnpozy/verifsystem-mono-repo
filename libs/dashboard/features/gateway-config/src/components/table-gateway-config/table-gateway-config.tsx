import { useState } from 'react';
import { useRouter } from 'next/router';
import { Avatar, Chip, Divider, IconButton, ListItemIcon, ListItemText, Menu, MenuItem, Tooltip, Typography } from '@mui/material';
import { Box } from '@mui/system';
import { GridColDef } from '@mui/x-data-grid';
import { DotsHorizontal, Pencil, TrashCanOutline } from 'mdi-material-ui';
import moment from 'moment';

import { DataGrid } from '@verifsystem/dashboard/shared';
import { useGetGatewayConfigsQuery } from '@verifsystem/dashboard/data-access';

import { DialogGatewayConfigForm } from '../dialog-gateway-config-form/dialog-gateway-config-form';
import { DialogGatewayConfigDelete } from '../dialog-gateway-config-delete/dialog-gateway-config-delete';

/* eslint-disable-next-line */
export interface TableGatewayConfigProps {}

/**
 * TableGatewayConfig
 *
 * @param {TableGatewayConfigProps} props
 * @returns JSX.Element
 */
export function TableGatewayConfig(props: TableGatewayConfigProps) {
  const router = useRouter();
  const { data: gatewayConfigs = [], isLoading } = useGetGatewayConfigsQuery({ processorId: +router?.query?.processorId });
  const [selectedGatewayConfig, setSelectedGatewayConfig] = useState(null);
  const [actionMenuAnchorEl, setActionMenuAnchorEl] = useState<null | HTMLElement>(null);
  const [dialogGatewayConfigFormOpen, setDialogGatewayConfigFormOpen] = useState(false);
  const [dialogGatewayConfigDeleteOpen, setDialogGatewayConfigDeleteOpen] = useState(false);
  const actionMenuOpen = Boolean(actionMenuAnchorEl);
  const { processorId } = router.query;

  const handleDialogGatewayConfigFormOpen = () => {
    setDialogGatewayConfigFormOpen(true);
    handleActionMenuClose();
  };

  const handleDialogGatewayConfigDeleteOpen = () => {
    setDialogGatewayConfigDeleteOpen(true);
    handleActionMenuClose();
  };

  const handleDialogGatewayConfigFormClose = () => {
    setDialogGatewayConfigFormOpen(false);
  };

  const handleDialogGatewayConfigDeleteClose = () => {
    setDialogGatewayConfigDeleteOpen(false);
  };

  const handleActionMenuClose = () => {
    setActionMenuAnchorEl(null);
  };

  const handleActionMenuClick = (event: React.MouseEvent<HTMLButtonElement>, gatewayConfig) => {
    setSelectedGatewayConfig(gatewayConfig);
    setActionMenuAnchorEl(event.currentTarget);
  };

  const columns: GridColDef[] = [
    {
      field: 'name',
      headerName: 'Name',
      width: 350,
      renderCell: (params) => {
        return (
          <Box sx={{ display: 'flex', alignItems: 'center' }}>
            <Avatar alt={params.row.name} sx={{ marginRight: '.5rem', width: 32, height: 32 }} variant="rounded">
              {params.row.name.charAt(0)}
            </Avatar>
            <Typography variant="body2">{params.row.name}</Typography>
          </Box>
        );
      },
    },
    {
      field: 'createdAt',
      headerName: 'Date Added',
      width: 250,
      renderCell: (params) => <Typography variant="body1">{moment(params.row.createdAt).format('ll')}</Typography>,
    },
    {
      field: 'type',
      headerName: 'Type',
      flex: 1,
      renderCell: (params) => <Typography variant="subtitle2">{params.row.type.toUpperCase()}</Typography>,
    },
    {
      field: 'status',
      headerName: 'Status',
      flex: 1,
      renderCell: (params) => (
        <Chip label={params.row.status.toUpperCase()} color={params.row.status === 'activated' ? 'success' : 'default'} size="small" />
      ),
    },
    {
      field: 'actions',
      type: 'actions',
      flex: 1,
      align: 'right',
      renderCell: (params) => {
        return (
          <>
            <Tooltip title="Action menu">
              <IconButton onClick={(event) => handleActionMenuClick(event, params.row)}>
                <DotsHorizontal />
              </IconButton>
            </Tooltip>
            <Menu
              anchorEl={actionMenuAnchorEl}
              open={actionMenuOpen}
              onClose={handleActionMenuClose}
              anchorOrigin={{
                vertical: 'bottom',
                horizontal: 'right',
              }}
              transformOrigin={{
                vertical: 'top',
                horizontal: 'right',
              }}
              PaperProps={{
                style: {
                  minWidth: 180,
                  boxShadow:
                    'rgb(255, 255, 255) 0px 0px 0px 0px, rgba(0, 0, 0, 0.02) 0px 0px 0px 1px, rgba(0, 0, 0, 0.01) 0px 4px 6px -2px',
                },
              }}
            >
              <MenuItem onClick={handleDialogGatewayConfigFormOpen}>
                <ListItemIcon>
                  <Pencil />
                </ListItemIcon>
                <ListItemText>Edit</ListItemText>
              </MenuItem>
              <Divider />
              <MenuItem onClick={handleDialogGatewayConfigDeleteOpen}>
                <ListItemIcon>
                  <TrashCanOutline color="error" />
                </ListItemIcon>
                <ListItemText>Delete</ListItemText>
              </MenuItem>
            </Menu>
          </>
        );
      },
    },
  ];

  return (
    <>
      <DataGrid rows={gatewayConfigs} columns={columns} checkboxSelection loading={isLoading} />
      <DialogGatewayConfigForm
        open={dialogGatewayConfigFormOpen}
        onClose={handleDialogGatewayConfigFormClose}
        processorId={+processorId}
        gatewayConfig={selectedGatewayConfig}
      />
      <DialogGatewayConfigDelete
        open={dialogGatewayConfigDeleteOpen}
        onClose={handleDialogGatewayConfigDeleteClose}
        gatewayConfig={selectedGatewayConfig}
      />
    </>
  );
}

export default TableGatewayConfig;
