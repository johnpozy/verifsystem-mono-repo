import { render } from '@testing-library/react';

import TableGatewayConfig from './table-gateway-config';

describe('TableGatewayConfig', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<TableGatewayConfig />);
    expect(baseElement).toBeTruthy();
  });
});
