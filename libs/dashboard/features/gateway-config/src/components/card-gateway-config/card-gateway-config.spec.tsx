import { render } from '@testing-library/react';

import CardGatewayConfig from './card-gateway-config';

describe('CardGatewayConfig', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<CardGatewayConfig />);
    expect(baseElement).toBeTruthy();
  });
});
