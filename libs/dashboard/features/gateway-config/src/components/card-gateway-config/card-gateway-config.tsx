import { Card, CardHeader, Divider } from '@mui/material';

import { TableGatewayConfig } from '../table-gateway-config/table-gateway-config';

/* eslint-disable-next-line */
export interface CardGatewayConfigProps {}

/**
 * CardGatewayConfig
 *
 * @param {CardGatewayConfigProps} props
 * @returns JSX.Element
 */
export const CardGatewayConfig = (props: CardGatewayConfigProps) => {
  return (
    <Card>
      <CardHeader titleTypographyProps={{ variant: 'h6' }} title="Gateway Config List" />
      <Divider />
      <TableGatewayConfig />
    </Card>
  );
}

export default CardGatewayConfig;
