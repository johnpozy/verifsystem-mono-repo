import { Button, Dialog, DialogActions, DialogTitle, Divider, Typography } from '@mui/material';
import { LoadingButton } from '@mui/lab';
import { Box } from '@mui/system';
import { useForm } from 'react-hook-form';

import { useDeleteGatewayConfigMutation } from '@verifsystem/dashboard/data-access';
import { IGatewayConfig } from '@verifsystem/shared/data-access';

/* eslint-disable-next-line */
export interface DialogGatewayConfigDeleteProps {
  open: boolean;
  gatewayConfig: IGatewayConfig;
  onClose: () => void;
}

export function DialogGatewayConfigDelete(props: DialogGatewayConfigDeleteProps) {
  const [deleteGatewayConfig, { isLoading }] = useDeleteGatewayConfigMutation();
  const { gatewayConfig, onClose, open } = props;
  const { handleSubmit } = useForm();

  const handleClose = () => {
    onClose();
  };

  const handleFormSubmit = async (body) => {
    const gatewayConfigId = gatewayConfig.id;
    const result = await deleteGatewayConfig({ gatewayConfigId });

    if (result['data']) {
      handleClose();
    }
  };

  return (
    <Dialog onClose={handleClose} open={open} maxWidth="xs" fullWidth>
      <DialogTitle>Delete Gateway Config</DialogTitle>
      <Divider />
      <form onSubmit={handleSubmit(handleFormSubmit)}>
        <Box sx={{ padding: 3 }}>
          <Typography>Delete this gateway config?</Typography>
        </Box>
        <DialogActions>
          <Button onClick={handleClose}>Close</Button>
          <LoadingButton variant="contained" type="submit" color="error" loading={isLoading}>
            Delete
          </LoadingButton>
        </DialogActions>
      </form>
    </Dialog>
  );
}

export default DialogGatewayConfigDelete;
