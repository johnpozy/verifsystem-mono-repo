import { render } from '@testing-library/react';

import DialogGatewayConfigDelete from './dialog-gateway-config-delete';

describe('DialogGatewayConfigDelete', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<DialogGatewayConfigDelete />);
    expect(baseElement).toBeTruthy();
  });
});
