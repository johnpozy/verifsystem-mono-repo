import { useState } from 'react';
import { NextPage } from 'next';
import { Box, Breadcrumbs, Button, Grid, Link, Typography } from '@mui/material';
import { Plus } from 'mdi-material-ui';
import { useRouter } from 'next/router';

import { PageHeader } from '@verifsystem/dashboard/shared';
import { useAppSelector, stateSelectedProcessor } from '@verifsystem/dashboard/data-access';

import CardGatewayConfig from '../../components/card-gateway-config/card-gateway-config';
import DialogGatewayConfigForm from '../../components/dialog-gateway-config-form/dialog-gateway-config-form';

/* eslint-disable-next-line */
export interface GatewayConfigProps {}

export const GatewayConfig: NextPage = (props: GatewayConfigProps) => {
  const router = useRouter();
  const selectedProcessor = useAppSelector(stateSelectedProcessor);
  const [dialogOpen, setDialogOpen] = useState(false);
  const { processorId } = router.query;

  const handleDialogOpen = () => {
    setDialogOpen(true);
  };

  const handleDialogClose = () => {
    setDialogOpen(false);
  };

  return (
    <>
      <Box>
        <PageHeader>
          <Grid container columnSpacing={3} alignItems="center">
            <Grid item xs>
              <Breadcrumbs>
                <Link underline="hover" color="inherit" href="/dashboard">
                  Home
                </Link>
                <Link underline="hover" color="inherit" href="/processor">
                  {selectedProcessor?.title ? selectedProcessor?.title : 'Processor'}
                </Link>
                <Typography color="text.primary">Gateway Config</Typography>
              </Breadcrumbs>
            </Grid>
            <Grid item xs="auto">
              <Button
                endIcon={<Plus />}
                variant="contained"
                fullWidth
                disableElevation
                onClick={handleDialogOpen}
              >
                Add Gateway
              </Button>
            </Grid>
          </Grid>
        </PageHeader>

        <Box p={3}>
          <CardGatewayConfig />
        </Box>
      </Box>

      <DialogGatewayConfigForm open={dialogOpen} onClose={handleDialogClose} processorId={+processorId} />
    </>
  );
}

export default GatewayConfig;
