import { Card, CardContent, Grid, Typography } from '@mui/material';
import { blue } from '@mui/material/colors';
import { Label } from 'mdi-material-ui';

/* eslint-disable-next-line */
export interface CardReportStatisticProps {
  color?: string;
  title: string | number;
  description: string | number;
}

export function CardReportStatistic(props: CardReportStatisticProps) {
  const { color, title, description } = props;

  return (
    <Card variant='outlined'>
      <CardContent>
        <Grid container spacing={2}>
          <Grid item xs={2}>
            <Label sx={{ color: color ? color : blue[500], marginTop: 0.2 }} fontSize="medium" />
          </Grid>
          <Grid item xs>
            <Typography variant="h5" color="text.primary" sx={{ lineHeight: 1.4 }}>{title}</Typography>
            <Typography component="span" variant="body2" color="text.secondary">{description}</Typography>
          </Grid>
        </Grid>
      </CardContent>
    </Card>
  );
}

export default CardReportStatistic;
