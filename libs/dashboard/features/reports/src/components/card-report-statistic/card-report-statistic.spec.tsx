import { render } from '@testing-library/react';

import CardReportStatistic from './card-report-statistic';

describe('CardReportStatistic', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<CardReportStatistic />);
    expect(baseElement).toBeTruthy();
  });
});
