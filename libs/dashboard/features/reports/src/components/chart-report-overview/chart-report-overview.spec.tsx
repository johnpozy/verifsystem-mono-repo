import { render } from '@testing-library/react';

import ChartReportOverview from './chart-report-overview';

describe('ChartReportOverview', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<ChartReportOverview />);
    expect(baseElement).toBeTruthy();
  });
});
