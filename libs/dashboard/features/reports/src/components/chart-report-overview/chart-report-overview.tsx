import { cyan, green, grey, indigo, yellow } from '@mui/material/colors';
import { Line, XAxis, YAxis, CartesianGrid, ResponsiveContainer, Bar, ComposedChart, Tooltip, Legend } from 'recharts';

import styles from './chart-report-overview.module.scss';

/* eslint-disable-next-line */
export interface ChartReportOverviewProps {
  data: any[];
}

export function ChartReportOverview(props: ChartReportOverviewProps) {
  const { data } = props;

  return (
    <div style={{ height: 460, width: '100%' }}>
      <div style={{ display: 'flex', height: '100%' }}>
        <div style={{ flexGrow: 1 }}>
          <ResponsiveContainer className={styles.chart}>
            <ComposedChart
              data={data}
              margin={{
                top: 10,
                right: 20,
                left: -20,
                bottom: 0,
              }}
            >
              <CartesianGrid strokeDasharray="4 4" stroke={grey[300]} />

              <XAxis strokeWidth={0} dataKey="date" />
              <YAxis yAxisId="left" strokeWidth={0} domain={[0, 300]} tickCount={8} scale="linear" />
              <YAxis yAxisId="right" orientation="right" strokeWidth={0} />

              <Tooltip
                formatter={(value, name, props) => {
                  if (props.dataKey === 'grossOrder' || props.dataKey === 'creditcardOrder') {
                    return value;
                  } else {
                    return `$${value}`;
                  }
                }}
              />
              <Legend />

              <Bar name="Total Order" dataKey="grossOrder" stackId="a" fill={indigo[500]} yAxisId="right" barSize={40} />
              <Bar name="Credit Card Order" dataKey="creditcardOrder" stackId="a" fill={cyan[500]} yAxisId="left" barSize={40} />

              <Line
                yAxisId="left"
                name="Gross Sale"
                type="linear"
                dataKey="grossSale"
                stroke={yellow[600]}
                strokeWidth={2}
                dot={{ r: 4 }}
              />
              <Line
                yAxisId="left"
                name="Credit Card Sale"
                type="linear"
                dataKey="creditcardSale"
                stroke={green[600]}
                strokeWidth={2}
                dot={{ r: 4 }}
              />
            </ComposedChart>
          </ResponsiveContainer>
        </div>
      </div>
    </div>
  );
}

export default ChartReportOverview;
