import { render } from '@testing-library/react';

import CardReportOverview from './card-report-overview';

describe('CardReportOverview', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<CardReportOverview />);
    expect(baseElement).toBeTruthy();
  });
});
