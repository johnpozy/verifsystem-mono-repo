import { Card, CardContent, CardHeader, Divider } from '@mui/material';
import { Box } from '@mui/system';

import ChartReportOverview from '../chart-report-overview/chart-report-overview';

/* eslint-disable-next-line */
export interface CardReportOverviewProps {
  data: any[];
}

/**
 * CardReport
 *
 * @param {CardReportOverviewProps} props
 * @returns JSX.Element
 */
export function CardReportOverview(props: CardReportOverviewProps) {
  const { data } = props;

  return (
    <Box>
      <Card>
        <CardHeader titleTypographyProps={{ variant: 'h6' }} title="Report Overview" />
        <Divider />
        <CardContent>
          <ChartReportOverview data={data} />
        </CardContent>
      </Card>
    </Box>
  );
}

export default CardReportOverview;
