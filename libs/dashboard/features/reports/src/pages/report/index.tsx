import { useEffect, useState } from 'react';
import {
  Badge,
  Breadcrumbs,
  Button,
  Chip,
  Divider,
  Grid,
  IconButton,
  Link,
  ListItemIcon,
  Menu,
  MenuItem,
  Tooltip,
  Typography,
} from '@mui/material';
import { DownloadBoxOutline, FilterVariant, RadioboxMarked, RadioboxBlank, Calendar } from 'mdi-material-ui';
import { cyan, deepPurple, green, indigo, red, yellow } from '@mui/material/colors';
import { Box } from '@mui/system';
import saveAs from 'file-saver';
import * as XLSX from 'xlsx';
import { subDays, subMonths, subYears, startOfMonth, endOfMonth, startOfYear, endOfYear } from 'date-fns';

import { PageHeader } from '@verifsystem/dashboard/shared';
import {
  useChartReportQuery,
  useExportReportQuery,
  useGetReportOrdersQuery,
  useGetReportSalesQuery,
} from '@verifsystem/dashboard/data-access';

import CardReportOverview from '../../components/card-report-overview/card-report-overview';
import CardReportStatistic from '../../components/card-report-statistic/card-report-statistic';

/* eslint-disable-next-line */
export interface ReportProps {}

export function Report(props: ReportProps) {
  const [anchorEl, setAnchorEl] = useState(null);
  const [selectedDateRangeText, setSelectedDateRangeText] = useState('Last 7 days');
  const [exportData, setExportData] = useState(true);
  const [dateFilter, setDateFilter] = useState({});
  const { data: reports = [] } = useExportReportQuery({}, { skip: exportData });
  const { data: reportOrder = [] } = useGetReportOrdersQuery(dateFilter);
  const { data: reportSale = [] } = useGetReportSalesQuery(dateFilter);
  const { data: chartData = [] } = useChartReportQuery(dateFilter);
  const open = Boolean(anchorEl);

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const handleSelect = (dateRangeText: string) => {
    setSelectedDateRangeText(dateRangeText);
  };

  const handleClearFilter = () => {
    setSelectedDateRangeText(null);
  };

  const renderFilterMenu = () => {
    return (
      <Grid container columnSpacing={1} alignItems="center">
        <Grid item xs="auto">
          {selectedDateRangeText && <Chip label={selectedDateRangeText} onDelete={handleClearFilter} />}
        </Grid>
        <Grid item xs>
          <Tooltip title="Filter report">
            <IconButton onClick={handleClick} color="inherit">
              <Badge variant="dot" invisible={!selectedDateRangeText} color="info">
                <FilterVariant />
              </Badge>
            </IconButton>
          </Tooltip>
          <Menu
            anchorEl={anchorEl}
            open={open}
            onClose={handleClose}
            anchorOrigin={{
              vertical: 'bottom',
              horizontal: 'right',
            }}
            transformOrigin={{
              vertical: 'top',
              horizontal: 'right',
            }}
          >
            <MenuItem onClick={() => handleSelect('Last 7 days')}>
              <ListItemIcon>
                {selectedDateRangeText === 'Last 7 days' ? (
                  <RadioboxMarked fontSize="small" color="primary" />
                ) : (
                  <RadioboxBlank fontSize="small" />
                )}
              </ListItemIcon>
              <Typography variant="inherit" noWrap>
                Last 7 days
              </Typography>
            </MenuItem>
            <MenuItem onClick={() => handleSelect('This month')}>
              <ListItemIcon>
                {selectedDateRangeText === 'This month' ? (
                  <RadioboxMarked fontSize="small" color="primary" />
                ) : (
                  <RadioboxBlank fontSize="small" />
                )}
              </ListItemIcon>
              <Typography variant="inherit" noWrap>
                This month
              </Typography>
            </MenuItem>
            <MenuItem onClick={() => handleSelect('Last month')}>
              <ListItemIcon>
                {selectedDateRangeText === 'Last month' ? (
                  <RadioboxMarked fontSize="small" color="primary" />
                ) : (
                  <RadioboxBlank fontSize="small" />
                )}
              </ListItemIcon>
              <Typography variant="inherit" noWrap>
                Last month
              </Typography>
            </MenuItem>
            <MenuItem onClick={() => handleSelect('Year to date')}>
              <ListItemIcon>
                {selectedDateRangeText === 'Year to date' ? (
                  <RadioboxMarked fontSize="small" color="primary" />
                ) : (
                  <RadioboxBlank fontSize="small" />
                )}
              </ListItemIcon>
              <Typography variant="inherit" noWrap>
                Year to date
              </Typography>
            </MenuItem>
            <MenuItem onClick={() => handleSelect('Year')}>
              <ListItemIcon>
                {selectedDateRangeText === 'Year' ? (
                  <RadioboxMarked fontSize="small" color="primary" />
                ) : (
                  <RadioboxBlank fontSize="small" />
                )}
              </ListItemIcon>
              <Typography variant="inherit" noWrap>
                Year
              </Typography>
            </MenuItem>
            <Divider />
            <MenuItem>
              <ListItemIcon>
                <Calendar fontSize="small" />
              </ListItemIcon>
              <Typography variant="inherit" noWrap>
                Custom
              </Typography>
            </MenuItem>
          </Menu>
        </Grid>
      </Grid>
    );
  };

  const downloadCsv = () => {
    setExportData((prev) => !prev);
  };

  useEffect(() => {
    const currentDate = new Date();

    switch (selectedDateRangeText) {
      case 'Last 7 days':
        setDateFilter({
          endDate: currentDate.toISOString(),
          startDate: subDays(currentDate, 7).toISOString(),
        });
        break;

      case 'This month':
        setDateFilter({
          endDate: endOfMonth(currentDate).toISOString(),
          startDate: startOfMonth(currentDate).toISOString(),
        });
        break;

      case 'Last month':
        setDateFilter({
          endDate: endOfMonth(subMonths(currentDate, 1)).toISOString(),
          startDate: startOfMonth(subMonths(currentDate, 1)).toISOString(),
        });
        break;

      case 'Year to date':
        setDateFilter({
          endDate: currentDate.toISOString(),
          startDate: endOfMonth(subYears(currentDate, 1)).toISOString(),
        });
        break;

      case 'Year':
        setDateFilter({
          endDate: endOfYear(currentDate).toISOString(),
          startDate: startOfYear(currentDate).toISOString(),
        });
        break;

      default:
        setDateFilter({});
        break;
    }
  }, [selectedDateRangeText]);

  useEffect(() => {
    if (reports.length > 0) {
      const worksheet = XLSX.utils.json_to_sheet(reports);
      const workbook = { Sheets: { data: worksheet }, SheetNames: ['data'] };
      const buffer = XLSX.write(workbook, { bookType: 'csv', type: 'array' });
      const blob: Blob = new Blob([buffer], { type: 'text/csv;charset=utf-8' });

      saveAs(blob, 'reports.csv');
    }
  }, [reports, exportData]);

  return (
    <Box>
      <PageHeader>
        <Grid container columnSpacing={1} alignItems="center">
          <Grid item xs>
            <Breadcrumbs>
              <Link underline="hover" color="inherit" href="/dashboard">
                Home
              </Link>
              <Typography color="text.primary">Report</Typography>
            </Breadcrumbs>
          </Grid>
          <Grid item xs="auto">
            <Button startIcon={<DownloadBoxOutline />} variant="contained" disableElevation onClick={downloadCsv}>
              Download
            </Button>
          </Grid>
        </Grid>
      </PageHeader>

      <Box p={3}>
        <Grid container spacing={2} mb={3}>
          <Grid item xs />
          <Grid item xs="auto">
            {renderFilterMenu()}
          </Grid>
        </Grid>

        <Grid container spacing={1} mb={3}>
          <Grid item xs={3}>
            <CardReportStatistic title={`$${reportSale.gross / 100}`} description="Gross Sales" color={yellow[600]}></CardReportStatistic>
          </Grid>
          <Grid item xs={3}>
            <CardReportStatistic
              title={`$${reportSale.creditcard / 100}`}
              description="Credit Card Sales"
              color={green[600]}
            ></CardReportStatistic>
          </Grid>
          <Grid item xs={3}>
            <CardReportStatistic title={`$${reportSale.chargeback / 100}`} description="Chargeback" color={red[600]}></CardReportStatistic>
          </Grid>
          <Grid item xs={3}>
            <CardReportStatistic title={`$${reportSale.refunded / 100}`} description="Refunded"></CardReportStatistic>
          </Grid>
          <Grid item xs={3}>
            <CardReportStatistic title={reportOrder.total} description="Total Orders Placed" color={indigo[500]}></CardReportStatistic>
          </Grid>
          <Grid item xs={3}>
            <CardReportStatistic
              title={reportOrder.creditcard}
              description="Orders Placed With Creditcard"
              color={cyan[500]}
            ></CardReportStatistic>
          </Grid>
          <Grid item xs={3}>
            <CardReportStatistic
              title={reportOrder.successful}
              description="Sucessful Payments"
              color={deepPurple[500]}
            ></CardReportStatistic>
          </Grid>
          <Grid item xs={3}>
            <CardReportStatistic title={reportOrder.failed} description="Failed Payments" color={deepPurple[500]}></CardReportStatistic>
          </Grid>
          <Grid item xs={3}>
            <CardReportStatistic
              title={reportOrder.disputed}
              description="Disputed Transaction"
              color={deepPurple[500]}
            ></CardReportStatistic>
          </Grid>
        </Grid>

        <CardReportOverview data={chartData} />
      </Box>
    </Box>
  );
}

export default Report;
