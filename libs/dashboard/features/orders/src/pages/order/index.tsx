import { Box, Breadcrumbs, Grid, Link, Typography } from '@mui/material';
import { PageHeader } from '@verifsystem/dashboard/shared';

import CardOrderList from '../../components/card-order-list/card-order-list';


/* eslint-disable-next-line */
export interface OrderProps {}

export function Order(props: OrderProps) {
  return (
    <Box>
      <PageHeader>
        <Grid container columnSpacing={3} alignItems="center">
          <Grid item xs>
            <Breadcrumbs>
              <Link underline="hover" color="inherit" href="/dashboard">
                Home
              </Link>
              <Typography color="text.primary">Transactions</Typography>
            </Breadcrumbs>
          </Grid>
        </Grid>
      </PageHeader>

      <Box p={3}>
        <CardOrderList />
      </Box>
    </Box>
  );
}

export default Order;
