import { Card, CardHeader, Divider } from '@mui/material';

import TableOrderList from '../table-order-list/table-order-list';

/* eslint-disable-next-line */
export interface CardOrderListProps {}

/**
 * CardOrderList
 *
 * @param {CardOrderListProps} props
 * @returns JSX.Element
 */
export function CardOrderList(props: CardOrderListProps) {
  return (
    <Card>
      <CardHeader titleTypographyProps={{ variant: 'h6' }} title="Recent Transactions" />
      <Divider />

      <TableOrderList />
    </Card>
  );
}

export default CardOrderList;
