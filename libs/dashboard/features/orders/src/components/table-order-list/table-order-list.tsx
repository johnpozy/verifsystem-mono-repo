import { useCallback, useEffect, useState } from 'react';
import {
  Avatar,
  Chip,
  Divider,
  FormControl,
  Grid,
  IconButton,
  InputLabel,
  ListItemIcon,
  ListItemText,
  Menu,
  MenuItem,
  Select,
  Tooltip,
  Typography,
} from '@mui/material';
import { GridColDef, GridColumnVisibilityModel, GridSortModel } from '@mui/x-data-grid';
import { Box } from '@mui/system';
import { DotsHorizontal, RotateRight } from 'mdi-material-ui';
import moment from 'moment';
import { useForm, Controller } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import * as yup from 'yup';

import { DataGrid } from '@verifsystem/dashboard/shared';
import { useAppSelector, stateSelectedEcomm, useGetEcommTransactionsQuery, stateLoggedUser } from '@verifsystem/dashboard/data-access';

import { DialogOrderDispute } from './../dialog-order-dispute/dialog-order-dispute';
import { ETransactionPaymentMethod, ETransactionStatus, EUserRole } from '@verifsystem/shared/data-access';

/* eslint-disable-next-line */
export interface TableOrderListProps {}

/**
 * TableOrderList
 *
 * @param {TableOrderListProps} props
 * @returns JSX.Element
 */
export function TableOrderList(props: TableOrderListProps) {
  const validationScheme = yup
    .object({
      status: yup.array().of(yup.string()),
    })
    .required();

  const {
    control,
    watch,
    setValue
  } = useForm({
    defaultValues: {
      status: [],
    },
    resolver: yupResolver(validationScheme),
  });

  const loggedUser = useAppSelector(stateLoggedUser);
  const selectedEcomm = useAppSelector(stateSelectedEcomm);
  const statusField = watch('status');
  const [page, setPage] = useState(0);
  const [pageSize, setPageSize] = useState(10);
  const [sortBy, setSortBy] = useState(null);
  const { data: transactions, isFetching } = useGetEcommTransactionsQuery({
    page: page + 1,
    limit: pageSize,
    sortBy,
    filter: statusField.length > 0 ? { status: `$in:${statusField}` } : {},
    ecommId: selectedEcomm?.id,
  });
  const [dialogOrderDisputeOpen, setDialogOrderDisputeOpen] = useState(false);
  const [selectedTransaction, setSelectedTransaction] = useState(null);
  const [actionMenuAnchorEl, setActionMenuAnchorEl] = useState<null | HTMLElement>(null);
  const [columnVisibilityModel, setColumnVisibilityModel] = useState<GridColumnVisibilityModel>({
    ecomm: false,
    actions: false
  });
  const actionMenuOpen = Boolean(actionMenuAnchorEl);

  const handleDialogOrderDisputeOpen = () => {
    setDialogOrderDisputeOpen(true);
    handleActionMenuClose();
  };

  const handleDialogOrderDisputeClose = () => {
    setDialogOrderDisputeOpen(false);
  };

  const handleActionMenuClose = () => {
    setActionMenuAnchorEl(null);
  };

  const handleActionMenuClick = (event: React.MouseEvent<HTMLButtonElement>, transaction) => {
    setSelectedTransaction(transaction);
    setActionMenuAnchorEl(event.currentTarget);
  };

  const handleSortModelChange = useCallback((sortModel: GridSortModel) => {
    if (sortModel && sortModel.length > 0) {
      setSortBy(`${sortModel[0].field}:${sortModel[0].sort}`);
    }
  }, []);

  const handleStatusFieldChange = ({ target }) => {
    setValue('status', target?.value);
  };

  const columns: GridColDef[] = [
    {
      field: 'createdAt',
      headerName: 'Date',
      flex: 1,
      renderCell: (params) => <Typography>{moment(params.row.createdAt).format('ll')}</Typography>,
    },
    {
      field: 'user',
      headerName: 'Customer',
      flex: 1,
      valueGetter: ({ row }) => row?.displayName,
      renderCell: ({ row }) => {
        return (
          <Box sx={{ display: 'flex', alignItems: 'center' }}>
            <Avatar alt={row?.user?.displayName} sx={{ marginRight: '.5rem', width: 36, height: 36 }} src={row?.avatar} variant="rounded" />
            <Typography>{row?.user?.displayName}</Typography>
          </Box>
        );
      },
    },
    {
      field: 'amount',
      headerName: 'Total',
      width: 100,
      renderCell: ({ row }) => <Typography>${row?.amount / 100}</Typography>,
    },
    {
      field: 'ecomm',
      headerName: 'Ecomm',
      flex: 1,
      hideable: false,
      valueGetter: ({ row }) => row?.ecomm?.title,
      renderCell: ({ row }) => <Typography>{row?.ecomm?.title}</Typography>,
    },
    {
      field: 'status',
      headerName: 'Status',
      width: 150,
      renderCell: ({ row }) => {
        return (
          <Tooltip title="Status">
            <Chip label={row.status.toUpperCase()} color={row.status === 'processing' ? 'success' : 'warning'} size="small" />
          </Tooltip>
        );
      },
    },
    {
      field: 'paymentMethod',
      headerName: 'Payment Method',
      width: 180,
      renderCell: ({ row }) => {
        let prettyPaymentMethod;

        switch (row?.paymentMethod) {
          case ETransactionPaymentMethod['Smartpayment Gateway']:
            prettyPaymentMethod = 'Credit Card';
            break;

          case ETransactionPaymentMethod['Cash On Delivery']:
            prettyPaymentMethod = 'Cash On Delivery';
            break;

          case ETransactionPaymentMethod.Cheque:
            prettyPaymentMethod = 'Cheque';
            break;

          case ETransactionPaymentMethod['Direct Bank Transfer']:
            prettyPaymentMethod = 'Direct Bank Transfer';
            break;

          default:
            prettyPaymentMethod = '-';
            break;
        }

        return prettyPaymentMethod;
      },
    },
    {
      field: 'actions',
      type: 'actions',
      flex: 1,
      align: 'right',
      hideable: false,
      renderCell: ({ row }) => {
        return (
          <>
            <Tooltip title="Action menu">
              <IconButton onClick={(event) => handleActionMenuClick(event, row)}>
                <DotsHorizontal />
              </IconButton>
            </Tooltip>
            <Menu
              anchorEl={actionMenuAnchorEl}
              open={actionMenuOpen}
              onClose={handleActionMenuClose}
              anchorOrigin={{
                vertical: 'bottom',
                horizontal: 'right',
              }}
              transformOrigin={{
                vertical: 'top',
                horizontal: 'right',
              }}
              PaperProps={{
                style: {
                  minWidth: 180,
                  boxShadow:
                    'rgb(255, 255, 255) 0px 0px 0px 0px, rgba(0, 0, 0, 0.02) 0px 0px 0px 1px, rgba(0, 0, 0, 0.01) 0px 4px 6px -2px',
                },
              }}
            >
              <MenuItem onClick={handleDialogOrderDisputeOpen}>
                <ListItemIcon>
                  <RotateRight />
                </ListItemIcon>
                <ListItemText>Mark as dispute</ListItemText>
              </MenuItem>
            </Menu>
          </>
        );
      },
    },
  ];

  useEffect(() => {
    setColumnVisibilityModel({
      ecomm: !selectedEcomm,
      actions: loggedUser.role === EUserRole.PARTNER
    });
  }, [selectedEcomm, loggedUser]);

  return (
    <>
      <form>
        <Box sx={{ padding: 2 }}>
          <Grid container spacing={3}>
            <Grid item xs={3}>
              <Controller
                name="status"
                control={control}
                render={({ field }) => (
                  <FormControl size="small" fullWidth>
                    <InputLabel>Status</InputLabel>
                    <Select multiple {...field} label="Status" onChange={handleStatusFieldChange}>
                      {Object.keys(ETransactionStatus).map((transactionStatus) => {
                        return (
                          <MenuItem key={ETransactionStatus[transactionStatus]} value={ETransactionStatus[transactionStatus]}>
                            {transactionStatus}
                          </MenuItem>
                        );
                      })}
                    </Select>
                  </FormControl>
                )}
              />
            </Grid>
          </Grid>
        </Box>
      </form>

      <Divider />

      <DataGrid
        rows={transactions?.data || []}
        rowCount={transactions?.meta?.totalItems || 0}
        page={page}
        pageSize={pageSize}
        paginationMode="server"
        onPageChange={(newPage) => setPage(newPage)}
        onPageSizeChange={(newPageSize) => setPageSize(newPageSize)}
        sortingMode="server"
        onSortModelChange={handleSortModelChange}
        columns={columns}
        checkboxSelection
        loading={isFetching}
        columnVisibilityModel={columnVisibilityModel}
        onColumnVisibilityModelChange={(newModel) => setColumnVisibilityModel(newModel)}
      />
      <DialogOrderDispute transaction={selectedTransaction} open={dialogOrderDisputeOpen} onClose={handleDialogOrderDisputeClose} />
    </>
  );
}

export default TableOrderList;
