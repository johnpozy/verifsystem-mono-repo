import { render } from '@testing-library/react';

import TableOrderList from './table-order-list';

describe('TableOrderList', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<TableOrderList />);
    expect(baseElement).toBeTruthy();
  });
});
