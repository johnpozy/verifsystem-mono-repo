import { Button, Dialog, DialogActions, DialogTitle, Divider, Typography } from '@mui/material';
import { Box } from '@mui/system';
import { LoadingButton } from '@mui/lab';
import { useForm } from 'react-hook-form';

import { useUpdateTransactionMutation } from '@verifsystem/dashboard/data-access';
import { ETransactionStatus, ITransaction } from '@verifsystem/shared/data-access';

/* eslint-disable-next-line */
export interface DialogOrderDisputeProps {
  open: boolean;
  transaction: ITransaction;
  onClose: () => void;
}

export function DialogOrderDispute(props: DialogOrderDisputeProps) {
  const [updateOrder, { isLoading }] = useUpdateTransactionMutation();
  const { transaction, onClose, open } = props;
  const { handleSubmit } = useForm({
    defaultValues: {
      status: ETransactionStatus.Dispute,
    },
  });

  const handleClose = () => {
    onClose();
  };

  const handleFormSubmit = async (body) => {
    const transactionId = transaction.id;
    const result = await updateOrder({ transactionId, body });

    if (result['data']) {
      handleClose();
    }
  };

  return (
    <Dialog onClose={handleClose} open={open} maxWidth="xs" fullWidth>
      <DialogTitle>Dispute Order</DialogTitle>
      <Divider />
      <form onSubmit={handleSubmit(handleFormSubmit)}>
        <Box sx={{ padding: 3 }}>
          <Typography>Dispute this order?</Typography>
        </Box>
        <DialogActions>
          <Button onClick={handleClose}>Close</Button>
          <LoadingButton variant="contained" type="submit" loading={isLoading}>
            Dispute
          </LoadingButton>
        </DialogActions>
      </form>
    </Dialog>
  );
}

export default DialogOrderDispute;
