import { render } from '@testing-library/react';

import DialogOrderDispute from './dialog-order-dispute';

describe('DialogOrderDispute', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<DialogOrderDispute />);
    expect(baseElement).toBeTruthy();
  });
});
