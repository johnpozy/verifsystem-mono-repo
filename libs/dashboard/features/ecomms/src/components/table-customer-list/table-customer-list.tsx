import { Avatar, IconButton, Tooltip, Typography } from '@mui/material';
import { Box } from '@mui/system';
import { GridColDef, GridRowsProp } from '@mui/x-data-grid';
import { AccountCircleOutline } from 'mdi-material-ui';
import moment from 'moment';

import { DataGrid } from '@verifsystem/dashboard/shared';

/* eslint-disable-next-line */
export interface TableCustomerListProps {}

/**
 * TableCustomerList
 *
 * @param {TableCustomerListProps} props
 * @returns JSX.Element
 */
export function TableCustomerList(props: TableCustomerListProps) {
  const columns: GridColDef[] = [
    {
      field: 'customer',
      headerName: 'Customer',
      width: 250,
      renderCell: (params) => {
        return (
          <Box sx={{ display: 'flex', alignItems: 'center' }}>
            <Avatar alt={params.row.col1} sx={{ marginRight: '.5rem', width: 36, height: 36 }} src={params.row.avatar} variant="rounded" />
            <Typography>{params.row.customer}</Typography>
          </Box>
        );
      },
    },
    {
      field: 'address',
      headerName: 'Address',
      flex: 1,
      renderCell: (params) => <Typography>{params.row.address}</Typography>,
    },
    {
      field: 'lastActive',
      headerName: 'Last Active',
      width: 250,
      renderCell: (params) => <Typography>{moment(params.row.lastActive).format('ll')}</Typography>,
    },
    {
      field: 'orders',
      headerName: 'Orders',
      width: 150,
      renderCell: (params) => <Typography>{params.row.orders}</Typography>,
    },
    {
      field: 'total',
      headerName: 'Total Spend',
      width: 150,
      renderCell: (params) => <Typography>{params.row.total}</Typography>,
    },
    {
      field: 'aov',
      headerName: 'AOV',
      width: 100,
      renderCell: (params) => <Typography>{params.row.aov}</Typography>,
    },
    {
      field: 'actions',
      type: 'actions',
      flex: 1,
      align: 'right',
      renderCell: (params) => {
        return (
          <Tooltip title="View Selfie ID">
            <IconButton onClick={() => null}>
              <AccountCircleOutline />
            </IconButton>
          </Tooltip>
        );
      },
    },
  ];

  const rows: GridRowsProp = [
    {
      id: 1,
      customer: 'Annaka Mekish',
      avatar: 'img/avatars/avatar-5.jpg',
      address: '4058 49th Avenue, Nunavut',
      lastActive: new Date(),
      orders: 10,
      total: '$1511.12',
      aov: '$121.54',
    },
    {
      id: 2,
      customer: 'John Williams',
      avatar: 'img/avatars/avatar-3.jpg',
      address: '2835 Nelson Street, Nobel',
      lastActive: new Date(),
      orders: 5,
      total: '$2521.50',
      aov: '$221.44',
    },
    {
      id: 3,
      customer: 'Paul Lewis',
      avatar: 'img/avatars/avatar-4.jpg',
      address: '4231 Woodvale Drive, West Lorne',
      lastActive: new Date(),
      orders: 7,
      total: '$2521.50',
      aov: '$237.31',
    },
    {
      id: 4,
      customer: 'Tory Lenning',
      avatar: 'img/avatars/avatar-2.jpg',
      address: '4231 Woodvale Drive, West Lorne',
      lastActive: new Date(),
      orders: 19,
      total: '$111.50',
      aov: '$31.04',
    },
  ];

  return (
    <DataGrid rows={rows} columns={columns} checkboxSelection />
  );
}

export default TableCustomerList;
