import { render } from '@testing-library/react';

import TableCustomerList from './table-customer-list';

describe('TableCustomerList', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<TableCustomerList />);
    expect(baseElement).toBeTruthy();
  });
});
