import { render } from '@testing-library/react';

import CardCustomerList from './card-customer-list';

describe('CardCustomerList', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<CardCustomerList />);
    expect(baseElement).toBeTruthy();
  });
});
