import {
  Box,
  Button,
  Card,
  CardHeader,
  Divider,
  FormControl,
  Grid,
  IconButton,
  InputAdornment,
  InputLabel,
  OutlinedInput,
} from '@mui/material';
import { ChevronDown, Magnify } from 'mdi-material-ui';

import { DataGridExporter } from '@verifsystem/dashboard/shared';

import TableCustomerList from '../table-customer-list/table-customer-list';

/* eslint-disable-next-line */
export interface CardCustomerListProps {}

export function CardCustomerList(props: CardCustomerListProps) {
  return (
    <Card>
      <CardHeader
        titleTypographyProps={{ variant: 'h6' }}
        action={
          <Grid container columnSpacing={2} alignItems="center">
            <Grid item xs="auto">
              <DataGridExporter />
            </Grid>
          </Grid>
        }
        title="Customer List"
      />
      <Divider />
      <Box py={2} px={2}>
        <Grid container rowSpacing={3} columnSpacing={3}>
          <Grid item xs>
            <Button variant="contained" color="inherit" disableElevation endIcon={<ChevronDown />}>
              Bulk Action
            </Button>
          </Grid>
          <Grid item xs={3}>
            <FormControl variant="outlined" size="small" fullWidth sx={{ bgcolor: 'white', margin: 0 }}>
              <InputLabel>Search customers</InputLabel>
              <OutlinedInput
                type="text"
                endAdornment={
                  <InputAdornment position="end">
                    <IconButton edge="end">
                      <Magnify />
                    </IconButton>
                  </InputAdornment>
                }
                label="Search customers"
              />
            </FormControl>
          </Grid>
        </Grid>
      </Box>
      <Divider />

      <TableCustomerList />
    </Card>
  );
}

export default CardCustomerList;
