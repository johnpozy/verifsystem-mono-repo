import { Breadcrumbs, Grid, Link, Typography } from '@mui/material';
import { Box } from '@mui/system';

import { PageHeader } from '@verifsystem/dashboard/shared';

import { CardCustomerList } from '../../components/card-customer-list/card-customer-list';

/* eslint-disable-next-line */
export interface CustomerProps {}

export function Customer(props: CustomerProps) {
  return (
    <Box>
      <PageHeader>
        <Grid container columnSpacing={3} alignItems="center">
          <Grid item xs>
            <Breadcrumbs>
              <Link underline="hover" color="inherit" href="/dashboard">
                Home
              </Link>
              <Typography color="text.primary">Customer</Typography>
            </Breadcrumbs>
          </Grid>
        </Grid>
      </PageHeader>

      <Box p={3}>
        <CardCustomerList />
      </Box>
    </Box>
  );
}

export default Customer;
