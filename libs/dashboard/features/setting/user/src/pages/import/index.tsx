import { Breadcrumbs, Grid, Link, Typography } from '@mui/material';
import { Box } from '@mui/system';

import { PageHeader } from '@verifsystem/dashboard/shared';
import CardUserList from '../../components/card-user-list/card-user-list';

/* eslint-disable-next-line */
export interface UserImportProps {}

export function UserImport(props: UserImportProps) {
  return (
    <Box>
      <PageHeader>
        <Grid container columnSpacing={3} alignItems="center">
          <Grid item xs>
            <Breadcrumbs>
              <Link underline="hover" color="inherit" href="/dashboard">
                Home
              </Link>
              <Link underline="hover" color="inherit">
                Setting
              </Link>
              <Typography color="text.primary">Import User</Typography>
            </Breadcrumbs>
          </Grid>
        </Grid>
      </PageHeader>

      <Box p={3}>
        <CardUserList />
      </Box>
    </Box>
  );
}

export default UserImport;
