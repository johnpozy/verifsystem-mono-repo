import { useEffect, useState } from 'react';
import { Avatar, Typography } from '@mui/material';
import { ProgressCheck } from 'mdi-material-ui';
import { Box } from '@mui/system';
import { GridColDef, GridColumnVisibilityModel } from '@mui/x-data-grid';

import { DataGrid } from '@verifsystem/dashboard/shared';
import { useAppSelector, stateSelectedEcomm } from '@verifsystem/dashboard/data-access';

/* eslint-disable-next-line */
export interface TableUserListProps {
  onRowSelected?: (selections) => void;
  onError?: (error) => void;
  selected: Array<any>;
  data: Array<any>;
}

/**
 * TableUserList
 *
 * @param {TableUserListProps} props
 * @returns JSX.Element
 */
export function TableUserList(props: TableUserListProps) {
  const { onRowSelected, selected, data } = props;
  const selectedEcomm = useAppSelector(stateSelectedEcomm);
  const [columnVisibilityModel, setColumnVisibilityModel] = useState<GridColumnVisibilityModel>({ ecomm: false });
  const [selectedUsers, setSelectedUsers] = useState(selected);
  // const {
  //   data: ecommUsers = [],
  //   isError,
  //   isFetching,
  // } = useGetEcommUsersImportQuery(
  //   {
  //     ecommId: selectedEcomm?.id,
  //   },
  //   {
  //     skip: !fetch,
  //   }
  // );
  // const [showToastr, setShowToastr] = useState(isError);

  const disableRowSelection = (params) => {
    return !params?.row?.imported && !params?.row?.roles?.includes('administrator');
  };

  const handleRowSelections = (ids) => {
    onRowSelected(ids.map((id) => data.find((user) => user.id === id)));
  };

  // const handleErrorToastrClose = (event?: React.SyntheticEvent | Event, reason?: string) => {
  //   if (reason === 'clickaway') {
  //     return;
  //   }

  //   setShowToastr(false);
  // };

  const columns: GridColDef[] = [
    {
      field: 'nickname',
      headerName: 'Display Name',
      flex: 1,
      valueGetter: ({ row }) => row?.nickname,
      renderCell: ({ row }) => {
        return (
          <Box sx={{ display: 'flex', alignItems: 'center' }}>
            <Avatar
              alt={row?.nickname}
              sx={{ marginRight: '.5rem', width: 36, height: 36 }}
              src={row?.avatar_urls['24']}
              variant="rounded"
            />
            <Typography>{row?.nickname}</Typography>
          </Box>
        );
      },
    },
    {
      field: 'first_name',
      headerName: 'First Name',
      flex: 1,
      valueGetter: ({ row }) => row?.first_name,
      renderCell: ({ row }) => <Typography>{row?.first_name}</Typography>,
    },
    {
      field: 'last_name',
      headerName: 'Last Name',
      flex: 1,
      valueGetter: ({ row }) => row?.last_name,
      renderCell: ({ row }) => <Typography>{row?.last_name}</Typography>,
    },
    {
      field: 'email',
      headerName: 'Email',
      flex: 1,
      valueGetter: ({ row }) => row?.email,
      renderCell: ({ row }) => <Typography>{row?.email}</Typography>,
    },
    {
      field: 'imported',
      headerName: 'Imported',
      align: 'center',
      headerAlign: 'center',
      sortable: false,
      filterable: false,
      hideable: false,
      disableColumnMenu: true,
      width: 180,
      valueGetter: ({ row }) => row?.imported,
      renderCell: ({ row }) => <ProgressCheck color={row?.imported ? 'success' : 'disabled'} />,
    },
  ];

  useEffect(() => {
    setColumnVisibilityModel({ ecomm: !selectedEcomm });

    if (selected.length > 0) {
      setSelectedUsers(selected.map((s) => s.id));
    } else {
      setSelectedUsers([]);
    }
  }, [selectedEcomm, selected]);

  return (
    <DataGrid
      rows={data}
      columns={columns}
      columnVisibilityModel={columnVisibilityModel}
      isRowSelectable={disableRowSelection}
      checkboxSelection
      onSelectionModelChange={handleRowSelections}
      selectionModel={selectedUsers}
      onColumnVisibilityModelChange={(newModel) => setColumnVisibilityModel(newModel)}
    />
  );
}

export default TableUserList;
