import { render } from '@testing-library/react';

import TableUserList from './table-user-list';

describe('TableUserList', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<TableUserList />);
    expect(baseElement).toBeTruthy();
  });
});
