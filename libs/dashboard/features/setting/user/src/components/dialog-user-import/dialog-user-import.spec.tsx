import { render } from '@testing-library/react';

import DialogUserImport from './dialog-user-import';

describe('DialogUserImport', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<DialogUserImport />);
    expect(baseElement).toBeTruthy();
  });
});
