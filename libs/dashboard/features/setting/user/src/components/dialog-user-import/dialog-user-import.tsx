import { Button, Dialog, DialogActions, DialogTitle, Divider, Typography } from '@mui/material';
import { Box } from '@mui/system';
import { LoadingButton } from '@mui/lab';
import { useForm } from 'react-hook-form';

import { stateSelectedEcomm, useAppSelector, useSaveExternalEcommUsersMutation } from '@verifsystem/dashboard/data-access';

/* eslint-disable-next-line */
export interface DialogUserImportProps {
  open: boolean;
  users: Array<any>;
  onClose: () => void;
}

export function DialogUserImport(props: DialogUserImportProps) {
  const [saveEcommExternalUsers, { isLoading }] = useSaveExternalEcommUsersMutation();
  const selectedEcomm = useAppSelector(stateSelectedEcomm);
  const { users, onClose, open } = props;
  const { handleSubmit } = useForm();

  const handleClose = () => {
    onClose();
  };

  const handleFormSubmit = async () => {
    await saveEcommExternalUsers({
      ecommId: selectedEcomm?.id,
      body: users
    });

    handleClose();
  };

  return (
    <Dialog onClose={handleClose} open={open} maxWidth="xs" fullWidth>
      <DialogTitle>Import Users</DialogTitle>
      <Divider />
      <form onSubmit={handleSubmit(handleFormSubmit)}>
        <Box sx={{ padding: 3 }}>
          <Typography mb={1}>Are you sure to import {users.length} user(s)?</Typography>
          <Typography variant='body2' color='gray'>Note: This process will take some time, depending on how large the data is.</Typography>
        </Box>
        <DialogActions>
          <Button onClick={handleClose}>Close</Button>
          <LoadingButton variant="contained" type="submit" loading={isLoading}>
            Import
          </LoadingButton>
        </DialogActions>
      </form>
    </Dialog>
  );
}

export default DialogUserImport;
