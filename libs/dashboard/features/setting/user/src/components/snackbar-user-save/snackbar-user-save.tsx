import styles from './snackbar-user-save.module.scss';

/* eslint-disable-next-line */
export interface SnackbarUserSaveProps {}

export function SnackbarUserSave(props: SnackbarUserSaveProps) {
  return (
    <div className={styles['container']}>
      <h1>Welcome to SnackbarUserSave!</h1>
    </div>
  );
}

export default SnackbarUserSave;
