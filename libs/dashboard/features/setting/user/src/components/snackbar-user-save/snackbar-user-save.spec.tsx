import { render } from '@testing-library/react';

import SnackbarUserSave from './snackbar-user-save';

describe('SnackbarUserSave', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<SnackbarUserSave />);
    expect(baseElement).toBeTruthy();
  });
});
