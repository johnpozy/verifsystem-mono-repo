import { useEffect, useState } from 'react';
import { Box, Button, Card, CardHeader, Divider, Grid } from '@mui/material';
import { Refresh } from 'mdi-material-ui';

import { stateSelectedEcomm, useAppSelector, useGetEcommUsersImportQuery, useSocket } from '@verifsystem/dashboard/data-access';

import { TableUserList } from '../table-user-list/table-user-list';
import { DialogUserImport } from '../dialog-user-import/dialog-user-import';
import { SnackbarUserEvent } from '../snackbar-user-event/snackbar-user-event';

const { NEXT_PUBLIC_BACKEND_ENDPOINT } = process.env;

/* eslint-disable-next-line */
export interface CardUserList {}

/**
 * CardUserList
 *
 * @param {CardUserList} props
 * @returns JSX.Element
 */
export function CardUserList(props: CardUserList) {
  const socket = useSocket(NEXT_PUBLIC_BACKEND_ENDPOINT);
  const selectedEcomm = useAppSelector(stateSelectedEcomm);
  const [fetchExternalEcommUsers, setFetchExternalEcommUsers] = useState(false);
  const [selectedExternalEcommUsers, setSelectedExternalEcommUsers] = useState([]);
  const [dialogUserImportOpen, setDialogUserImportOpen] = useState(false);
  const [ecommUsers, setEcommUsers] = useState([]);

  const handleRowSelected = (selections) => {
    setSelectedExternalEcommUsers(selections);
  };

  const handleDialogUserImportOpen = () => {
    setDialogUserImportOpen(true);
  };

  const handleDialogUserImportClose = () => {
    setDialogUserImportOpen(false);
  };

  const handleFetchExternalEcommUsers = () => {
    setFetchExternalEcommUsers(true);
  };

  useGetEcommUsersImportQuery(
    {
      ecommId: selectedEcomm?.id,
    },
    {
      skip: !fetchExternalEcommUsers,
    }
  );

  useEffect(() => {
    if (socket) {
      socket.on('get-external-ecomm-users-complete', (data) => {
        setEcommUsers(data?.result);
      });
      socket.on('save-external-ecomm-users-progress', (data) => {
        if (data?.progress === 100) {
          setEcommUsers(prev => {
            return prev.map(p => {
              p.imported = true;

              return p;
            });
          });
        }
      });
    }
  }, [socket]);

  return (
    <>
      <Card>
        <CardHeader titleTypographyProps={{ variant: 'h6' }} title="User List" />
        <Divider />
        <Box p={2}>
          <Grid container columnSpacing={2} alignItems="center">
            <Grid item xs="auto">
              <Button
                variant="contained"
                disableElevation
                disabled={selectedExternalEcommUsers.length === 0}
                onClick={handleDialogUserImportOpen}
              >
                Import Selected ({selectedExternalEcommUsers.length})
              </Button>
            </Grid>
            <Grid item xs="auto">
              <Button variant="text" startIcon={<Refresh />} onClick={handleFetchExternalEcommUsers} disabled={!selectedEcomm?.title}>
                Fetch User
              </Button>
            </Grid>
          </Grid>
        </Box>
        <Divider />
        <TableUserList onRowSelected={handleRowSelected} selected={selectedExternalEcommUsers} data={ecommUsers} />
      </Card>

      <DialogUserImport
        open={dialogUserImportOpen}
        users={selectedExternalEcommUsers}
        onClose={handleDialogUserImportClose}
      />

      <SnackbarUserEvent socket={socket} eventPrefix='get-external-ecomm-users' />
      <SnackbarUserEvent socket={socket} eventPrefix='save-external-ecomm-users' />
    </>
  );
}

export default CardUserList;
