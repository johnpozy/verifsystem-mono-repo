import { render } from '@testing-library/react';

import CardUserList from './card-user-list';

describe('CardUserList', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<CardUserList />);
    expect(baseElement).toBeTruthy();
  });
});
