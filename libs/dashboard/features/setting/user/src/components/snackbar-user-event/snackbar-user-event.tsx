import { Alert, Snackbar, Typography, Grid, LinearProgress } from '@mui/material';

import { useEffect, useState } from 'react';

/* eslint-disable-next-line */
export interface SnackbarUserGetProps {
  socket: any;
  eventPrefix: string;
}

export function SnackbarUserEvent(props: SnackbarUserGetProps) {
  const { socket, eventPrefix } = props;
  const [active, setActive] = useState(false);
  const [progress, setProgress] = useState(0);
  const [error, setError] = useState(null);

  const handleClose = (event?: React.SyntheticEvent | Event, reason?: string) => {
    if (reason === 'clickaway') {
      return;
    }

    setActive(false);
    setError(null);
  };

  const messageTemplate = {
    'get-external-ecomm-users': {
      error: 'There is an error while connecting to ecomm. Please make sure all configuration are correct and wordpress permalink is not set to plain.',
      progress: 'Pulling users data from ecomm. This will take some time, depending on how large the user data is.'
    },
    'save-external-ecomm-users': {
      error: 'There is an error while connecting to ecomm. Please make sure all configuration are correct and wordpress permalink is not set to plain.',
      progress: 'Saving users data to database. This will take some time, depending on how many the user data is.'
    }
  }

  useEffect(() => {
    if (socket) {
      socket.on(`${eventPrefix}-active`, (data) => {
        setActive(true);
        setProgress(0);
        setError(null);
      });
      socket.on(`${eventPrefix}-progress`, (data) => {
        setActive(true);
        setProgress(data?.progress);
        setError(null);
      });
      socket.on(`${eventPrefix}-error`, (data) => {
        setActive(false);
        setProgress(0);
        setError(data?.error);
      });
    }
  }, [socket, eventPrefix]);

  return (
    <>
      <Snackbar
        open={error}
        autoHideDuration={60000}
        onClose={handleClose}
        anchorOrigin={{
          vertical: 'top',
          horizontal: 'right',
        }}
      >
        <Alert onClose={handleClose} severity="error" sx={{ width: '380px' }}>
          {messageTemplate[eventPrefix]?.error}
        </Alert>
      </Snackbar>

      <Snackbar
        open={active}
        onClose={handleClose}
        anchorOrigin={{
          vertical: 'top',
          horizontal: 'right',
        }}
      >
        <Alert onClose={handleClose} severity="info" sx={{ width: '380px' }}>
          <Typography variant="body2" mb={1}>
            {messageTemplate[eventPrefix]?.progress}
          </Typography>

          <Grid container spacing={0} alignItems="center">
            <Grid item xs>
              <LinearProgress
                variant="determinate"
                value={progress}
                sx={{ height: '3px', marginRight: '5px', borderRadius: '3px', marginTop: '1px' }}
              />
            </Grid>
            <Grid item xs={1.5}>
              {progress.toFixed(2)}%
            </Grid>
          </Grid>
        </Alert>
      </Snackbar>
    </>
  );
}

export default SnackbarUserEvent;
