import { render } from '@testing-library/react';

import SnackbarUserGet from './snackbar-user-get';

describe('SnackbarUserGet', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<SnackbarUserGet />);
    expect(baseElement).toBeTruthy();
  });
});
