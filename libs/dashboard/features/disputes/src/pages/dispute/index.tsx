import { NextPage } from 'next';
import { Box, Breadcrumbs, Grid, Link, Typography } from '@mui/material';

import { PageHeader } from '@verifsystem/dashboard/shared';

import { CardDisputeList } from '../../components/card-dispute-list/card-dispute-list';

/* eslint-disable-next-line */
export interface DisputeProps {}

export const Dispute: NextPage = (props: DisputeProps) => {
  return (
    <Box>
      <PageHeader>
        <Grid container columnSpacing={3} alignItems="center">
          <Grid item xs>
            <Breadcrumbs>
              <Link underline="hover" color="inherit" href="/dashboard">
                Home
              </Link>
              <Typography color="text.primary">Dispute</Typography>
            </Breadcrumbs>
          </Grid>
        </Grid>
      </PageHeader>

      <Box p={3}>
        <CardDisputeList />
      </Box>
    </Box>
  );
}

export default Dispute;
