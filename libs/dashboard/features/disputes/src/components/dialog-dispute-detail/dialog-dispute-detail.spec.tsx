import { render } from '@testing-library/react';

import DialogDisputeDetail from './dialog-dispute-detail';

describe('DialogDisputeDetail', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<DialogDisputeDetail />);
    expect(baseElement).toBeTruthy();
  });
});
