import { Box, Button, Chip, Dialog, DialogActions, DialogTitle, Divider, Grid, Typography } from '@mui/material';
import moment from 'moment';

import { ITransaction } from '@verifsystem/shared/data-access';

/* eslint-disable-next-line */
export interface DialogDisputeDetailProps {
  open: boolean;
  dispute: ITransaction;
  onClose: () => void;
}

export function DialogDisputeDetail(props: DialogDisputeDetailProps) {
  const { dispute, onClose, open } = props;

  const handleClose = () => {
    onClose();
  };

  return (
    <Dialog onClose={handleClose} open={open} maxWidth="sm" fullWidth>
      <DialogTitle>Dispute Detail</DialogTitle>
      <Divider />
      <Box p={3}>
        <Grid item xs={12} mb={1}>
          <Typography color="text.secondary" display="block" variant="overline">
            Dispute Detail
          </Typography>
        </Grid>
        <Grid container spacing={3} mb={1}>
          <Grid item xs={3}>
            <Typography>Status</Typography>
          </Grid>
          <Grid item>
            <Chip label={dispute?.status.toUpperCase()} color={dispute?.status === 'processing' ? 'success' : 'warning'} size="small" />
          </Grid>
        </Grid>
        <Grid container spacing={3} mb={1}>
          <Grid item xs={3}>
            <Typography>Website</Typography>
          </Grid>
          <Grid item>{dispute?.ecomm?.title}</Grid>
        </Grid>
        <Grid container spacing={3} mb={1}>
          <Grid item xs={3}>
            <Typography>Customer</Typography>
          </Grid>
          <Grid item>{dispute?.user?.firstName} {dispute?.user?.lastName}</Grid>
        </Grid>
        <Grid container spacing={3} mb={3}>
          <Grid item xs={3}>
            <Typography>Transaction Date</Typography>
          </Grid>
          <Grid item>{moment(dispute?.createdAt).format('ll')}</Grid>
        </Grid>

        <Grid item xs={12} mb={1}>
          <Typography color="text.secondary" display="block" variant="overline">
            Charge Breakdown
          </Typography>
        </Grid>
        <Grid container spacing={3} mb={1}>
          <Grid item xs={3}>
            <Typography>Amount</Typography>
          </Grid>
          <Grid item>${dispute?.amount / 100}</Grid>
        </Grid>
        <Grid container spacing={3} mb={1}>
          <Grid item xs={3}>
            <Typography>7% Fee</Typography>
          </Grid>
          <Grid item>${((dispute?.amount / 100) * 0.07).toPrecision(2)} (fees is between 7% ~ 10% based on client)</Grid>
        </Grid>
        <Grid container spacing={3} mb={1}>
          <Grid item xs={3}>
            <Typography>Dispute Fee</Typography>
          </Grid>
          <Grid item>$2</Grid>
        </Grid>
        <Grid container spacing={3} mb={3}>
          <Grid item xs={3}>
            <Typography>Net</Typography>
          </Grid>
          <Grid item>${((dispute?.amount / 100) - ((dispute?.amount / 100) * 0.07) - 2).toPrecision(2)}</Grid>
        </Grid>

        <Grid item xs={12} mb={1}>
          <Typography color="text.secondary" display="block" variant="overline">
            Payment Detail
          </Typography>
        </Grid>
        <Grid container spacing={3}>
          <Grid item xs={3}>
            <Typography>Gateway</Typography>
          </Grid>
          <Grid item>{dispute?.gatewayConfig?.name}</Grid>
        </Grid>
      </Box>
      <DialogActions>
        <Button onClick={handleClose}>Close</Button>
      </DialogActions>
    </Dialog>
  );
}

export default DialogDisputeDetail;
