import { Card, CardHeader, Divider } from '@mui/material';

import { TableDisputeList } from '../table-dispute-list/table-dispute-list';

/* eslint-disable-next-line */
export interface CardDisputeListProps {}

/**
 * CardOrderList
 *
 * @param {CardDisputeListProps} props
 * @returns JSX.Element
 */
export function CardDisputeList(props: CardDisputeListProps) {
  return (
    <Card>
      <CardHeader titleTypographyProps={{ variant: 'h6' }} title="Dispute List" />
      <Divider />

      <TableDisputeList />
    </Card>
  );
}

export default CardDisputeList;
