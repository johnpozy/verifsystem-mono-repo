import { render } from '@testing-library/react';

import CardDisputeList from './card-dispute-list';

describe('CardDisputeList', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<CardDisputeList />);
    expect(baseElement).toBeTruthy();
  });
});
