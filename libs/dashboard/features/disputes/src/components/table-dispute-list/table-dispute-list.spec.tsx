import { render } from '@testing-library/react';

import TableDisputeList from './table-dispute-list';

describe('TableDisputeList', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<TableDisputeList />);
    expect(baseElement).toBeTruthy();
  });
});
