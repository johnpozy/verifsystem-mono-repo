import { useCallback, useState } from 'react';
import { Avatar, Chip, Divider, IconButton, ListItemIcon, ListItemText, Menu, MenuItem, Tooltip, Typography } from '@mui/material';
import { GridColDef, GridColumnVisibilityModel, GridSortModel } from '@mui/x-data-grid';
import { Box } from '@mui/system';
import { AccountCancelOutline, DotsHorizontal, EyeOutline, RotateRight } from 'mdi-material-ui';
import moment from 'moment';

import { DataGrid } from '@verifsystem/dashboard/shared';
import { useAppSelector, stateSelectedEcomm, useGetEcommTransactionsQuery } from '@verifsystem/dashboard/data-access';
import { ETransactionStatus } from '@verifsystem/shared/data-access';

import { DialogRefundPayment } from '../dialog-refund-payment/dialog-refund-payment';
import DialogBlockUser from '../dialog-block-user/dialog-block-user';
import DialogDisputeDetail from '../dialog-dispute-detail/dialog-dispute-detail';

/* eslint-disable-next-line */
export interface TableDisputeListProps {}

/**
 * TableDisputeList
 *
 * @param {TableDisputeListProps} props
 * @returns JSX.Element
 */
export function TableDisputeList(props: TableDisputeListProps) {
  const selectedEcomm = useAppSelector(stateSelectedEcomm);
  const [page, setPage] = useState(0);
  const [pageSize, setPageSize] = useState(10);
  const [sortBy, setSortBy] = useState(null);
  const [dialogRefundPaymentOpen, setDialogRefundPaymentOpen] = useState(false);
  const [dialogBlockUserOpen, setDialogBlockUserOpen] = useState(false);
  const [dialogDetailOpen, setDialogDetailOpen] = useState(false);
  const [selectedDispute, setSelectedDispute] = useState(null);
  const [actionMenuAnchorEl, setActionMenuAnchorEl] = useState<null | HTMLElement>(null);
  const [columnVisibilityModel, setColumnVisibilityModel] = useState<GridColumnVisibilityModel>({ ecomm: false });
  const actionMenuOpen = Boolean(actionMenuAnchorEl);
  const { data: transactions , isLoading } = useGetEcommTransactionsQuery({
    page: page + 1,
    limit: pageSize,
    sortBy,
    filter: {
      status: `$in:${ETransactionStatus.Dispute}`
    },
    ecommId: selectedEcomm?.id
  });

  const handleDialogRefundPaymentOpen = () => {
    setDialogRefundPaymentOpen(true);
    handleActionMenuClose();
  };

  const handleDialogRefundPaymentClose = () => {
    setDialogRefundPaymentOpen(false);
  };

  const handleDialogBlockUserOpen = () => {
    setDialogBlockUserOpen(true);
    handleActionMenuClose();
  };

  const handleDialogBlockUserClose = () => {
    setDialogBlockUserOpen(false);
  };

  const handleDialogDetailOpen = () => {
    setDialogDetailOpen(true);
    handleActionMenuClose();
  };

  const handleDialogDetailClose = () => {
    setDialogDetailOpen(false);
  };

  const handleActionMenuClose = () => {
    setActionMenuAnchorEl(null);
  };

  const handleActionMenuClick = (event: React.MouseEvent<HTMLButtonElement>, dispute) => {
    setSelectedDispute(dispute);
    setActionMenuAnchorEl(event.currentTarget);
  };

  const handleSortModelChange = useCallback((sortModel: GridSortModel) => {
    if (sortModel && sortModel.length > 0) {
      setSortBy(`${sortModel[0].field}:${sortModel[0].sort}`);
    }
  }, []);

  const columns: GridColDef[] = [
    {
      field: 'total',
      headerName: 'Amount',
      width: 150,
      renderCell: (params) => <Typography>${params.row.amount / 100}</Typography>,
    },
    {
      field: 'status',
      headerName: 'Status',
      width: 150,
      renderCell: (params) => {
        return (
          <Tooltip title="Status">
            <Chip label={params.row.status.toUpperCase()} color={params.row.status === 'processing' ? 'success' : 'warning'} size="small" />
          </Tooltip>
        );
      },
    },
    {
      field: 'user',
      headerName: 'Customer',
      flex: 1,
      valueGetter: ({ row }) => row?.user?.displayName,
      renderCell: ({ row }) => {
        return (
          <Box sx={{ display: 'flex', alignItems: 'center' }}>
            <Avatar alt={row.user.displayName} sx={{ marginRight: '.5rem', width: 36, height: 36 }} src={row.avatar} variant="rounded" />
            <Typography>{row.user.displayName}</Typography>
          </Box>
        );
      },
    },
    {
      field: 'ecomm',
      headerName: 'Ecomm',
      flex: 1,
      hideable: false,
      valueGetter: ({ row }) => row?.ecomm?.title,
      renderCell: ({ row }) => <Typography>{row?.ecomm?.title}</Typography>,
    },
    {
      field: 'gatewayConfig',
      headerName: 'Payment Method',
      flex: 1,
      valueGetter: ({ row }) => row?.gatewayConfig?.name,
      renderCell: ({ row }) => <Typography>{row?.gatewayConfig?.name}</Typography>,
    },
    {
      field: 'createdAt',
      headerName: 'Date',
      flex: 1,
      renderCell: (params) => <Typography>{moment(params.row.createdAt).format('ll')}</Typography>,
    },
    {
      field: 'actions',
      type: 'actions',
      flex: 1,
      align: 'right',
      renderCell: (params) => {
        return (
          <>
            <Tooltip title="Action menu">
              <IconButton onClick={(event) => handleActionMenuClick(event, params.row)}>
                <DotsHorizontal />
              </IconButton>
            </Tooltip>
            <Menu
              anchorEl={actionMenuAnchorEl}
              open={actionMenuOpen}
              onClose={handleActionMenuClose}
              anchorOrigin={{
                vertical: 'bottom',
                horizontal: 'right',
              }}
              transformOrigin={{
                vertical: 'top',
                horizontal: 'right',
              }}
              PaperProps={{
                style: {
                  minWidth: 180,
                  boxShadow:
                    'rgb(255, 255, 255) 0px 0px 0px 0px, rgba(0, 0, 0, 0.02) 0px 0px 0px 1px, rgba(0, 0, 0, 0.01) 0px 4px 6px -2px',
                },
              }}
            >
              <MenuItem onClick={handleDialogDetailOpen}>
                <ListItemIcon>
                  <EyeOutline />
                </ListItemIcon>
                <ListItemText>View detail</ListItemText>
              </MenuItem>
              <MenuItem onClick={handleDialogRefundPaymentOpen}>
                <ListItemIcon>
                  <RotateRight />
                </ListItemIcon>
                <ListItemText>Refund payment</ListItemText>
              </MenuItem>
              <Divider />
              <MenuItem onClick={handleDialogBlockUserOpen}>
                <ListItemIcon>
                  <AccountCancelOutline color="error" />
                </ListItemIcon>
                <ListItemText>Add to blocklist</ListItemText>
              </MenuItem>
            </Menu>
          </>
        );
      },
    },
  ];

  return (
    <>
      <DataGrid
        rows={transactions?.data || []}
        rowCount={transactions?.meta?.totalItems || 0}
        page={page}
        pageSize={pageSize}
        paginationMode="server"
        onPageChange={(newPage) => setPage(newPage)}
        onPageSizeChange={(newPageSize) => setPageSize(newPageSize)}
        sortingMode="server"
        onSortModelChange={handleSortModelChange}
        columns={columns}
        checkboxSelection
        loading={isLoading}
        columnVisibilityModel={columnVisibilityModel}
        onColumnVisibilityModelChange={(newModel) => setColumnVisibilityModel(newModel)}
      />
      <DialogRefundPayment dispute={selectedDispute} open={dialogRefundPaymentOpen} onClose={handleDialogRefundPaymentClose} />
      <DialogBlockUser dispute={selectedDispute} open={dialogBlockUserOpen} onClose={handleDialogBlockUserClose} />
      <DialogDisputeDetail dispute={selectedDispute} open={dialogDetailOpen} onClose={handleDialogDetailClose} />
    </>
  );
}

export default TableDisputeList;
