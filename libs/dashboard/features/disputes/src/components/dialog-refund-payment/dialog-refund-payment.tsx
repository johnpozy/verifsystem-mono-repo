import { useEffect } from 'react';
import { Button, Dialog, DialogActions, DialogTitle, Divider, Grid, TextField } from '@mui/material';
import { Box } from '@mui/system';
import { LoadingButton } from '@mui/lab';
import { useForm, Controller } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import * as yup from 'yup';

import { useUpdateTransactionMutation } from '@verifsystem/dashboard/data-access';
import { ETransactionStatus, ITransaction } from '@verifsystem/shared/data-access';

/* eslint-disable-next-line */
export interface DialogRefundPaymentProps {
  open: boolean;
  dispute: ITransaction;
  onClose: () => void;
}

export function DialogRefundPayment(props: DialogRefundPaymentProps) {
  const { dispute, onClose, open } = props;
  const [updateTransaction, { isLoading }] = useUpdateTransactionMutation();

  const validationScheme = yup
    .object({
      ecomm: yup.string().required(),
      amount: yup.number().required(),
      reason: yup.string().required()
    })
    .required();

  const {
    control,
    handleSubmit,
    reset,
    formState: { errors },
  } = useForm({
    defaultValues: {
      ecomm: '',
      amount: 0,
      reason: '',
      status: ''
    },
    resolver: yupResolver(validationScheme),
  });

  const handleClose = () => {
    onClose();
  };

  const handleFormSubmit = async (payload) => {
    const transactionId = dispute.id;
    const body = {
      description: payload.reason,
      status: payload.status
    };
    const result = await updateTransaction({ transactionId, body });

    if (result['data']) {
      handleClose();
    }
  };

  useEffect(() => {
    if (dispute) {
      reset({
        ecomm: dispute.ecomm.baseUrl,
        amount: dispute.amount / 100,
        reason: dispute.description,
        status: ETransactionStatus.Refunded
      });
    }
  }, [reset, dispute]);

  return (
    <Dialog onClose={handleClose} open={open} maxWidth="xs" fullWidth>
      <DialogTitle>Refund Payment</DialogTitle>
      <Divider />
      <form onSubmit={handleSubmit(handleFormSubmit)}>
        <Box sx={{ padding: 3 }}>
          <Grid container spacing={3}>
            <Grid item xs={12}>
              <Controller
                name="ecomm"
                control={control}
                render={({ field }) => (
                  <TextField
                    {...field}
                    label="Ecomm URL"
                    size="small"
                    fullWidth
                    error={!!errors?.ecomm}
                    helperText={errors.ecomm?.message}
                    InputLabelProps={{
                      shrink: true,
                    }}
                  />
                )}
              />
            </Grid>
            <Grid item xs={12}>
              <Controller
                name="amount"
                control={control}
                render={({ field }) => (
                  <TextField
                    {...field}
                    type="number"
                    label="Amount"
                    size="small"
                    fullWidth
                    error={!!errors?.amount}
                    helperText={errors.amount?.message}
                    InputLabelProps={{
                      shrink: true,
                    }}
                  />
                )}
              />
            </Grid>
            <Grid item xs={12}>
              <Controller
                name="reason"
                control={control}
                render={({ field }) => (
                  <TextField
                    {...field}
                    label="Reason"
                    multiline
                    rows={4}
                    size="small"
                    fullWidth
                    error={!!errors?.reason}
                    helperText={errors.reason?.message}
                    InputLabelProps={{
                      shrink: true,
                    }}
                  />
                )}
              />
            </Grid>
          </Grid>
        </Box>
        <DialogActions>
          <Button onClick={handleClose}>Close</Button>
          <LoadingButton variant="contained" type="submit" loading={isLoading}>
            Refund
          </LoadingButton>
        </DialogActions>
      </form>
    </Dialog>
  );
}

export default DialogRefundPayment;
