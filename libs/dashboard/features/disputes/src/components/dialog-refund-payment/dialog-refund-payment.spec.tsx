import { render } from '@testing-library/react';

import DialogRefundPayment from './dialog-refund-payment';

describe('DialogRefundPayment', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<DialogRefundPayment />);
    expect(baseElement).toBeTruthy();
  });
});
