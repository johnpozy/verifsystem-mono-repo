import { render } from '@testing-library/react';

import DialogBlockUser from './dialog-block-user';

describe('DialogBlockUser', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<DialogBlockUser />);
    expect(baseElement).toBeTruthy();
  });
});
