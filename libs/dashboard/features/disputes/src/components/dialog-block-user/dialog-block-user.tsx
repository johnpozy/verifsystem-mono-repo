import { useEffect } from 'react';
import { Button, Dialog, DialogActions, DialogTitle, Divider, Grid, TextField, Typography } from '@mui/material';
import { LoadingButton } from '@mui/lab';
import { Box } from '@mui/system';
import { useForm, Controller } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import * as yup from 'yup';

import { EUserEcommStatus, ITransaction } from '@verifsystem/shared/data-access';
import { useUpdateUserEcommMutation } from '@verifsystem/dashboard/data-access';

/* eslint-disable-next-line */
export interface DialogBlockUserProps {
  open: boolean;
  dispute?: ITransaction;
  onClose: () => void;
}

export function DialogBlockUser(props: DialogBlockUserProps) {
  const [updateUserEcomm, { isLoading }] = useUpdateUserEcommMutation();
  const { dispute, onClose, open } = props;

  const validationScheme = yup
    .object({
      displayName: yup.string().required(),
      firstName: yup.string().required(),
      lastName: yup.string().required(),
      email: yup.string().email().required(),
      phoneNumber: yup.string().required(),
      postcode: yup.object().shape({
        billing: yup.string().nullable(),
        shipping: yup.string().nullable(),
      }),
    })
    .required();

  const {
    control,
    handleSubmit,
    reset,
    formState: { errors },
  } = useForm({
    defaultValues: {
      displayName: '',
      firstName: '',
      lastName: '',
      email: '',
      phoneNumber: '',
      postcode: {
        billing: '',
        shipping: '',
      },
      status: ''
    },
    resolver: yupResolver(validationScheme),
  });

  const handleClose = () => {
    onClose();
    reset();
  };

  const handleFormSubmit = async (payload) => {
    const userId = dispute.user.id;
    const ecommId = dispute.ecomm.id;
    const body = { status: payload.status };
    const result = await updateUserEcomm({ userId, ecommId, body });

    if (result['data']) {
      handleClose();
    }
  };

  useEffect(() => {
    if (dispute) {
      reset({
        displayName: dispute.user.displayName,
        email: dispute.user.email,
        firstName: dispute.user.firstName,
        lastName: dispute.user.lastName,
        phoneNumber: dispute.user.phoneNumber,
        postcode: dispute.user.postcode,
        status: EUserEcommStatus.BLOCKED
      });
    }
  }, [reset, dispute]);

  return (
    <Dialog onClose={handleClose} open={open} maxWidth="sm" fullWidth>
      <DialogTitle>Block Customer</DialogTitle>
      <Divider />
      <form onSubmit={handleSubmit(handleFormSubmit)}>
        <Box sx={{ padding: 3 }}>
          <Grid container spacing={3}>
            <Grid item xs={6}>
              <Controller
                name="firstName"
                control={control}
                render={({ field }) => (
                  <TextField
                    {...field}
                    label="First name"
                    size="small"
                    fullWidth
                    error={!!errors?.firstName}
                    helperText={errors.firstName?.message}
                    InputLabelProps={{
                      shrink: true,
                    }}
                  />
                )}
              />
            </Grid>
            <Grid item xs={6}>
              <Controller
                name="lastName"
                control={control}
                render={({ field }) => (
                  <TextField
                    {...field}
                    label="Last name"
                    size="small"
                    fullWidth
                    error={!!errors?.lastName}
                    helperText={errors.lastName?.message}
                    InputLabelProps={{
                      shrink: true,
                    }}
                  />
                )}
              />
            </Grid>
            <Grid item xs={6}>
              <Controller
                name="displayName"
                control={control}
                render={({ field }) => (
                  <TextField
                    {...field}
                    label="Display name"
                    size="small"
                    fullWidth
                    error={!!errors?.displayName}
                    helperText={errors.displayName?.message}
                    InputLabelProps={{
                      shrink: true,
                    }}
                  />
                )}
              />
            </Grid>
            <Grid item xs={6}>
              <Controller
                name="email"
                control={control}
                render={({ field }) => (
                  <TextField
                    {...field}
                    type="email"
                    label="Email"
                    size="small"
                    fullWidth
                    error={!!errors?.email}
                    helperText={errors.email?.message}
                    InputLabelProps={{
                      shrink: true,
                    }}
                  />
                )}
              />
            </Grid>
            <Grid item xs={6}>
              <Controller
                name="phoneNumber"
                control={control}
                render={({ field }) => (
                  <TextField
                    {...field}
                    label="Phone number"
                    size="small"
                    fullWidth
                    error={!!errors?.phoneNumber}
                    helperText={errors.phoneNumber?.message}
                    InputLabelProps={{
                      shrink: true,
                    }}
                  />
                )}
              />
            </Grid>
            <Grid item xs={12}>
              <Typography color="text.secondary" display="block" variant="overline">
                Postcode
              </Typography>
            </Grid>
            <Grid item xs={6}>
              <Controller
                name="postcode.billing"
                control={control}
                render={({ field }) => (
                  <TextField
                    {...field}
                    label="Billing"
                    size="small"
                    fullWidth
                    error={!!errors?.postcode?.billing}
                    helperText={errors.postcode?.billing?.message}
                    InputLabelProps={{
                      shrink: true,
                    }}
                  />
                )}
              />
            </Grid>
            <Grid item xs={6}>
              <Controller
                name="postcode.shipping"
                control={control}
                render={({ field }) => (
                  <TextField
                    {...field}
                    label="Shipping"
                    size="small"
                    fullWidth
                    error={!!errors?.postcode?.shipping}
                    helperText={errors.postcode?.shipping?.message}
                    InputLabelProps={{
                      shrink: true,
                    }}
                  />
                )}
              />
            </Grid>
          </Grid>
        </Box>
        <DialogActions>
          <Button onClick={handleClose}>Close</Button>
          <LoadingButton variant="contained" type="submit" color='error' loading={isLoading}>
            Block
          </LoadingButton>
        </DialogActions>
      </form>
    </Dialog>
  );
}

export default DialogBlockUser;
