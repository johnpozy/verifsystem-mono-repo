import { render } from '@testing-library/react';

import Processor from './processor';

describe('Processor', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<Processor />);
    expect(baseElement).toBeTruthy();
  });
});
