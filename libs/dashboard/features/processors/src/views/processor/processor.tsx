import { useState } from 'react';
import { Breadcrumbs, Button, Grid, Link, Typography } from '@mui/material';
import { Box } from '@mui/system';
import { Plus } from 'mdi-material-ui';

import { PageHeader } from '@verifsystem/dashboard/shared';

import { CardProcessor } from '../../components/card-processor/card-processor';
import { DialogProcessorForm } from '../../components/dialog-processor-form/dialog-processor-form';

/* eslint-disable-next-line */
export interface ProcessorProps {}

export function Processor(props: ProcessorProps) {
  const [dialogOpen, setDialogOpen] = useState(false);

  const handleDialogOpen = () => {
    setDialogOpen(true);
  };

  const handleDialogClose = () => {
    setDialogOpen(false);
  };

  return (
    <>
      <Box>
        <PageHeader>
          <Grid container columnSpacing={3} alignItems="center">
            <Grid item xs>
              <Breadcrumbs>
                <Link underline="hover" color="inherit" href="/dashboard">
                  Home
                </Link>
                <Typography color="text.primary">Processor</Typography>
              </Breadcrumbs>
            </Grid>
            <Grid item xs="auto">
              <Button
                endIcon={<Plus />}
                variant="contained"
                fullWidth
                disableElevation
                onClick={handleDialogOpen}
              >
                Add Processor
              </Button>
            </Grid>
          </Grid>
        </PageHeader>

        <Box p={3}>
          <CardProcessor />
        </Box>
      </Box>

      <DialogProcessorForm open={dialogOpen} onClose={handleDialogClose} />
    </>
  );
}

export default Processor;
