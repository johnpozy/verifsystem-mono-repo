import { render } from '@testing-library/react';

import DialogProcessorForm from './dialog-processor-form';

describe('DialogProcessorForm', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<DialogProcessorForm />);
    expect(baseElement).toBeTruthy();
  });
});
