import { useEffect, useState } from 'react';
import {
  Button,
  Dialog,
  DialogActions,
  DialogTitle,
  Divider,
  FormControl,
  FormHelperText,
  Grid,
  InputLabel,
  MenuItem,
  Select,
  TextField,
} from '@mui/material';
import { Box } from '@mui/system';
import { LoadingButton } from '@mui/lab';
import { useForm, Controller } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import * as yup from 'yup';

import {
  useAppSelector,
  stateSelectedEcomm,
  useCreateProcessorMutation,
  useUpdateProcessorMutation,
  useGetEcommsQuery,
} from '@verifsystem/dashboard/data-access';
import { IProcessor } from '@verifsystem/shared/data-access';

/* eslint-disable-next-line */
export interface DialogProcessorFormProps {
  open: boolean;
  processor?: IProcessor;
  onClose: () => void;
}

export function DialogProcessorForm(props: DialogProcessorFormProps) {
  const [createProcessor, { isLoading: isCreating }] = useCreateProcessorMutation();
  const [updateProcessor, { isLoading: isUpdating }] = useUpdateProcessorMutation();
  const { data: ecomms = [] } = useGetEcommsQuery();
  const selectedEcomm = useAppSelector(stateSelectedEcomm);
  const [ecommId, setEcommId] = useState<string>('');
  const { processor, onClose, open } = props;

  const validationScheme = yup
    .object({
      title: yup.string().required(),
      baseUrl: yup.string().required(),
      type: yup.string().required(),
    })
    .required();

  const {
    control,
    handleSubmit,
    reset,
    setValue,
    formState: { errors },
  } = useForm({
    defaultValues: {
      title: '',
      baseUrl: '',
      description: '',
      type: '',
    },
    resolver: yupResolver(validationScheme),
  });

  const handleClose = () => {
    onClose();
    reset();
  };

  const handleFormSubmit = async (body) => {
    let result;

    if (processor) {
      result = await updateProcessor({ processorId: processor.id, body });
    } else {
      result = await createProcessor({ ecommId: selectedEcomm ? selectedEcomm.id : +ecommId, body });
    }

    if (result['data']) {
      handleClose();
    }
  };

  useEffect(() => {
    if (processor) {
      setEcommId(selectedEcomm?.id?.toString());
      reset(processor);
    }
  }, [reset, processor, selectedEcomm]);

  return (
    <Dialog onClose={handleClose} open={open} maxWidth="sm" fullWidth>
      <DialogTitle>{processor ? 'Update' : 'Add'} Processor</DialogTitle>
      <Divider />
      <form onSubmit={handleSubmit(handleFormSubmit)}>
        <Box sx={{ padding: 3 }}>
          <Grid container spacing={3}>
            <Grid item xs={12}>
              <Controller
                name="title"
                control={control}
                render={({ field }) => (
                  <TextField
                    {...field}
                    label="Title"
                    size="small"
                    fullWidth
                    error={!!errors?.title}
                    helperText={errors.title?.message}
                    InputLabelProps={{
                      shrink: true,
                    }}
                  />
                )}
              />
            </Grid>
            <Grid item xs={12}>
              <Controller
                name="baseUrl"
                control={control}
                render={({ field }) => (
                  <TextField
                    {...field}
                    label="Base URL"
                    size="small"
                    fullWidth
                    error={!!errors?.baseUrl}
                    helperText={errors.baseUrl?.message}
                    InputLabelProps={{
                      shrink: true,
                    }}
                  />
                )}
              />
            </Grid>
            <Grid item xs={12}>
              <Controller
                name="description"
                control={control}
                render={({ field }) => (
                  <TextField
                    {...field}
                    label="Description"
                    multiline
                    rows={4}
                    size="small"
                    fullWidth
                    error={!!errors?.description}
                    helperText={errors.description?.message}
                    InputLabelProps={{
                      shrink: true,
                    }}
                  />
                )}
              />
            </Grid>
            <Grid item xs={6}>
              <Controller
                name="type"
                control={control}
                render={({ field }) => (
                  <FormControl size="small" fullWidth error={!!errors?.type}>
                    <InputLabel shrink>Type</InputLabel>
                    <Select {...field} label="Type" notched onChange={({ target }) => setValue('type', target.value)}>
                      <MenuItem value="verification">Verification</MenuItem>
                      <MenuItem value="payment">Payment</MenuItem>
                    </Select>
                    <FormHelperText>{errors.type?.message}</FormHelperText>
                  </FormControl>
                )}
              />
            </Grid>
            {!selectedEcomm && (
              <Grid item xs={6}>
                <FormControl size="small" fullWidth disabled={!!processor}>
                  <InputLabel shrink>Ecomm</InputLabel>
                  <Select value={ecommId} label="Ecomm" notched onChange={({ target }) => setEcommId(target.value as string)}>
                    {ecomms.map((ecomm) => {
                      return <MenuItem key={ecomm?.id} value={ecomm?.id}>{ecomm?.title}</MenuItem>;
                    })}
                  </Select>
                </FormControl>
              </Grid>
            )}
          </Grid>
        </Box>
        <DialogActions>
          <Button onClick={handleClose}>Close</Button>
          <LoadingButton variant="contained" type="submit" loading={isCreating || isUpdating}>
            Save
          </LoadingButton>
        </DialogActions>
      </form>
    </Dialog>
  );
}

export default DialogProcessorForm;
