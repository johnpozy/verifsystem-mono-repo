import { render } from '@testing-library/react';

import DialogProcessorDelete from './dialog-processor-delete';

describe('DialogProcessorDelete', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<DialogProcessorDelete />);
    expect(baseElement).toBeTruthy();
  });
});
