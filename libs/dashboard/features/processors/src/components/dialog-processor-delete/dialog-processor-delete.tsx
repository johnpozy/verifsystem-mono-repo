import { Button, Dialog, DialogActions, DialogTitle, Divider, Typography } from '@mui/material';
import { LoadingButton } from '@mui/lab';
import { Box } from '@mui/system';
import { useForm } from 'react-hook-form';

import { useDeleteProcessorMutation } from '@verifsystem/dashboard/data-access';
import { IProcessor } from '@verifsystem/shared/data-access';

/* eslint-disable-next-line */
export interface DialogProcessorDeleteProps {
  open: boolean;
  processor: IProcessor;
  onClose: () => void;
}

export function DialogProcessorDelete(props: DialogProcessorDeleteProps) {
  const [deleteProcessor, { isLoading }] = useDeleteProcessorMutation();
  const { processor, onClose, open } = props;
  const { handleSubmit } = useForm();

  const handleClose = () => {
    onClose();
  };

  const handleFormSubmit = async () => {
    const processorId = processor.id;
    const result = await deleteProcessor({ processorId });

    if (result['data']) {
      handleClose();
    }
  };

  return (
    <Dialog onClose={handleClose} open={open} maxWidth="xs" fullWidth>
      <DialogTitle>Delete Processor</DialogTitle>
      <Divider />
      <form onSubmit={handleSubmit(handleFormSubmit)}>
        <Box sx={{ padding: 3 }}>
          <Typography mb={1}>Delete this processor?</Typography>
          <Typography variant='body2' color='gray'>Note: All gateway config under this processor will be deleted to.</Typography>
        </Box>
        <DialogActions>
          <Button onClick={handleClose}>Close</Button>
          <LoadingButton variant="contained" type="submit" color='error' loading={isLoading}>
            Delete
          </LoadingButton>
        </DialogActions>
      </form>
    </Dialog>
  );
}

export default DialogProcessorDelete;
