import { Card, CardHeader, Divider } from '@mui/material';

import TableProcessor from '../table-processor/table-processor';

/* eslint-disable-next-line */
export interface CardProcessorProps {}

/**
 * CardProcessor
 *
 * @param {CardProcessorProps} props
 * @returns JSX.Element
 */
export function CardProcessor(props: CardProcessorProps) {
  return (
    <Card>
      <CardHeader titleTypographyProps={{ variant: 'h6' }} title="Processor List" />
      <Divider />
      <TableProcessor />
    </Card>
  );
}

export default CardProcessor;
