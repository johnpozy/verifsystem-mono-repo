import { render } from '@testing-library/react';

import CardProcessor from './card-processor';

describe('CardProcessor', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<CardProcessor />);
    expect(baseElement).toBeTruthy();
  });
});
