import { useCallback, useState } from 'react';
import Link from 'next/link';
import { Avatar, Chip, Divider, IconButton, ListItemIcon, ListItemText, Menu, MenuItem, Tooltip, Typography } from '@mui/material';
import { Box } from '@mui/system';
import { GridColDef, GridSortModel } from '@mui/x-data-grid';
import { ApplicationCogOutline, DotsHorizontal, Pencil, TrashCanOutline } from 'mdi-material-ui';
import moment from 'moment';

import { IProcessor } from '@verifsystem/shared/data-access';
import { DataGrid } from '@verifsystem/dashboard/shared';
import { stateSelectedEcomm, useAppSelector, useGetEcommProcessorsQuery, } from '@verifsystem/dashboard/data-access';

import { DialogProcessorForm } from '../dialog-processor-form/dialog-processor-form';
import { DialogProcessorDelete } from '../dialog-processor-delete/dialog-processor-delete';

/* eslint-disable-next-line */
export interface TableProcessorProps {}

/**
 * TableProcessor
 *
 * @param {TableProcessorProps} props
 * @returns JSX.Element
 */
export function TableProcessor(props: TableProcessorProps) {
  const selectedEcomm = useAppSelector(stateSelectedEcomm);
  const [page, setPage] = useState(0);
  const [pageSize, setPageSize] = useState(10);
  const [sortBy, setSortBy] = useState(null);
  const { data: processors, isLoading } = useGetEcommProcessorsQuery({
    page: page + 1,
    limit: pageSize,
    sortBy,
    ecommId: selectedEcomm?.id
  });
  const [selectedProcessor, setSelectedProcessor] = useState<IProcessor>(null);
  const [actionMenuAnchorEl, setActionMenuAnchorEl] = useState<null | HTMLElement>(null);
  const [dialogProcessorFormOpen, setDialogProcessorFormOpen] = useState(false);
  const [dialogProcessorDeleteOpen, setDialogProcessorDeleteOpen] = useState(false);
  const actionMenuOpen = Boolean(actionMenuAnchorEl);

  const handleDialogProcessorFormOpen = () => {
    setDialogProcessorFormOpen(true);
    handleActionMenuClose();
  };

  const handleDialogProcessorDeleteOpen = () => {
    setDialogProcessorDeleteOpen(true);
    handleActionMenuClose();
  };

  const handleDialogProcessorFormClose = () => {
    setDialogProcessorFormOpen(false);
  };

  const handleDialogProcessorDeleteClose = () => {
    setDialogProcessorDeleteOpen(false);
  };

  const handleActionMenuClose = () => {
    setActionMenuAnchorEl(null);
  };

  const handleActionMenuClick = (event: React.MouseEvent<HTMLButtonElement>, ecommProcessor) => {
    setSelectedProcessor(ecommProcessor);
    setActionMenuAnchorEl(event.currentTarget);
  };

  const handleSortModelChange = useCallback((sortModel: GridSortModel) => {
    if (sortModel && sortModel.length > 0) {
      setSortBy(`${sortModel[0].field}:${sortModel[0].sort}`);
    }
  }, []);

  const columns: GridColDef[] = [
    {
      field: 'title',
      headerName: 'Title',
      width: 350,
      sortable: false,
      valueGetter: ({ row }) => row?.title,
      renderCell: ({ row }) => {
        return (
          <Box sx={{ display: 'flex', alignItems: 'center' }}>
            <Avatar alt={row?.title} sx={{ marginRight: '.5rem', width: 32, height: 32 }} variant="rounded">
              {row?.title.charAt(0)}
            </Avatar>
            <Typography variant="body2">{row?.title}</Typography>
          </Box>
        );
      },
    },
    {
      field: 'createdAt',
      headerName: 'Date Added',
      width: 250,
      sortable: false,
      renderCell: (params) => <Typography variant="body1">{moment(params.row.createdAt).format('ll')}</Typography>,
    },
    {
      field: 'type',
      headerName: 'Type',
      flex: 1,
      sortable: false,
      valueGetter: ({ row }) => row?.type,
      renderCell: ({ row }) => <Typography variant="subtitle2">{row?.type.toUpperCase()}</Typography>,
    },
    {
      field: 'status',
      headerName: 'Status',
      flex: 1,
      sortable: false,
      renderCell: (params) => (
        <Chip label={params.row.status.toUpperCase()} color={params.row.status === 'activated' ? 'success' : 'default'} size="small" />
      ),
    },
    {
      field: 'actions',
      type: 'actions',
      flex: 1,
      align: 'right',
      sortable: false,
      filterable: false,
      renderCell: (params) => {
        return (
          <>
            <Tooltip title="Action menu">
              <IconButton onClick={(event) => handleActionMenuClick(event, params.row)}>
                <DotsHorizontal />
              </IconButton>
            </Tooltip>
            <Menu
              anchorEl={actionMenuAnchorEl}
              open={actionMenuOpen}
              onClose={handleActionMenuClose}
              anchorOrigin={{
                vertical: 'bottom',
                horizontal: 'right',
              }}
              transformOrigin={{
                vertical: 'top',
                horizontal: 'right',
              }}
              PaperProps={{
                style: {
                  minWidth: 180,
                  boxShadow:
                    'rgb(255, 255, 255) 0px 0px 0px 0px, rgba(0, 0, 0, 0.02) 0px 0px 0px 1px, rgba(0, 0, 0, 0.01) 0px 4px 6px -2px',
                },
              }}
            >
              <MenuItem onClick={handleDialogProcessorFormOpen}>
                <ListItemIcon>
                  <Pencil />
                </ListItemIcon>
                <ListItemText>Edit</ListItemText>
              </MenuItem>
              <Link href={`processor/${selectedProcessor?.id}/gateway-config`}>
                <MenuItem onClick={handleActionMenuClose}>
                  <ListItemIcon>
                    <ApplicationCogOutline />
                  </ListItemIcon>
                  <ListItemText>Gateway Config</ListItemText>
                </MenuItem>
              </Link>
              <Divider />
              <MenuItem onClick={handleDialogProcessorDeleteOpen}>
                <ListItemIcon>
                  <TrashCanOutline color="error" />
                </ListItemIcon>
                <ListItemText>Delete</ListItemText>
              </MenuItem>
            </Menu>
          </>
        );
      },
    },
  ];

  return (
    <>
      <DataGrid
        rows={processors?.data || []}
        rowCount={processors?.meta?.totalItems || 0}
        page={page}
        pageSize={pageSize}
        paginationMode="server"
        onPageChange={(newPage) => setPage(newPage)}
        onPageSizeChange={(newPageSize) => setPageSize(newPageSize)}
        sortingMode="server"
        onSortModelChange={handleSortModelChange}
        columns={columns}
        checkboxSelection
        loading={isLoading}
      />
      <DialogProcessorForm open={dialogProcessorFormOpen} onClose={handleDialogProcessorFormClose} processor={selectedProcessor} />
      <DialogProcessorDelete open={dialogProcessorDeleteOpen} onClose={handleDialogProcessorDeleteClose} processor={selectedProcessor} />
    </>
  );
}

export default TableProcessor;
