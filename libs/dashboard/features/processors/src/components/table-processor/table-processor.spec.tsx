import { render } from '@testing-library/react';

import TableProcessor from './table-processor';

describe('TableProcessor', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<TableProcessor />);
    expect(baseElement).toBeTruthy();
  });
});
