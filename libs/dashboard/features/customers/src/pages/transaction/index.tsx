import { NextPage } from 'next';
import { Box, Breadcrumbs, Grid, Link, Typography } from '@mui/material';

import { PageHeader } from '@verifsystem/dashboard/shared';
import { useAppSelector, stateSelectedUsers } from '@verifsystem/dashboard/data-access';

import { CardTransactionHistory } from '../../components/card-transaction-history/card-transaction-history';

/* eslint-disable-next-line */
export interface TransactionProps {}

export const Transaction: NextPage = (props: TransactionProps) => {
  const selectedUser = useAppSelector(stateSelectedUsers);

  return (
    <Box>
      <PageHeader>
        <Grid container columnSpacing={3} alignItems="center">
          <Grid item xs>
            <Breadcrumbs>
              <Link underline="hover" color="inherit" href="/dashboard">
                Home
              </Link>
              <Link underline="hover" color="inherit" href="/customer">
                {selectedUser?.displayName ? selectedUser?.displayName : 'Customer'}
              </Link>
              <Typography color="text.primary">Transaction History</Typography>
            </Breadcrumbs>
          </Grid>
        </Grid>
      </PageHeader>

      <Box p={3}>
        <CardTransactionHistory />
      </Box>
    </Box>
  );
}

export default Transaction;
