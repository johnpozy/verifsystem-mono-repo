import { render } from '@testing-library/react';

import DialogVeriffUpdate from './dialog-veriff-update';

describe('DialogVeriffUpdate', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<DialogVeriffUpdate />);
    expect(baseElement).toBeTruthy();
  });
});
