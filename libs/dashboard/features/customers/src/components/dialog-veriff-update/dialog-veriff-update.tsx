import { useEffect } from 'react';
import { Button, Dialog, DialogActions, DialogTitle, Divider, Typography } from '@mui/material';
import { LoadingButton } from '@mui/lab';
import { Box } from '@mui/system';
import { useForm } from 'react-hook-form';

import { useUpdateUserVeriffMutation } from '@verifsystem/dashboard/data-access';
import { EUserVeriffStatus, IUserEcomm } from '@verifsystem/shared/data-access';

/* eslint-disable-next-line */
export interface DialogVeriffUpdateProps {
  open: boolean;
  user: IUserEcomm;
  decision: EUserVeriffStatus;
  onClose: () => void;
}

export function DialogVeriffUpdate(props: DialogVeriffUpdateProps) {
  const [updateUserVeriff, { isLoading }] = useUpdateUserVeriffMutation();
  const { user, onClose, open, decision } = props;
  const { handleSubmit, reset } = useForm({
    defaultValues: {
      decision: '',
    },
  });

  const handleClose = () => {
    onClose();
  };

  const handleFormSubmit = async (body) => {
    const veriffId = user?.user?.veriffs?.reduce((a, b) => (a.datetime > b.datetime ? a : b)).id;
    const userId = user?.user?.id;
    const result = await updateUserVeriff({ userId, veriffId, body });

    if (result['data']) {
      handleClose();
    }
  };

  useEffect(() => {
    reset({ decision });
  }, [reset, decision]);

  return (
    <Dialog onClose={handleClose} open={open} maxWidth="xs" fullWidth>
      <DialogTitle>Update Verification Status</DialogTitle>
      <Divider />
      <form onSubmit={handleSubmit(handleFormSubmit)}>
        <Box sx={{ padding: 3 }}>
          <Typography>
            Update user verification status to <strong>{decision}</strong>?
          </Typography>
        </Box>
        <DialogActions>
          <Button onClick={handleClose}>Close</Button>
          <LoadingButton variant="contained" type="submit" loading={isLoading}>
            Update
          </LoadingButton>
        </DialogActions>
      </form>
    </Dialog>
  );
}

export default DialogVeriffUpdate;
