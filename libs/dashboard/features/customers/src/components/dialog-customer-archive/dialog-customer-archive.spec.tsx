import { render } from '@testing-library/react';

import DialogCustomerArchive from './dialog-customer-archive';

describe('DialogCustomerArchive', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<DialogCustomerArchive />);
    expect(baseElement).toBeTruthy();
  });
});
