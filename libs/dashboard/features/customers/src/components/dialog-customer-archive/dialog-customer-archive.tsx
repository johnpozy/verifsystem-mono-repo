import { Button, Dialog, DialogActions, DialogTitle, Divider, Typography } from '@mui/material';
import { LoadingButton } from '@mui/lab';
import { Box } from '@mui/system';
import { useForm } from 'react-hook-form';

import { useUpdateUserEcommMutation } from '@verifsystem/dashboard/data-access';
import { EUserEcommStatus, IUserEcomm } from '@verifsystem/shared/data-access';

/* eslint-disable-next-line */
export interface DialogCustomerArchiveProps {
  open: boolean;
  user: IUserEcomm;
  onClose: () => void;
}

export function DialogCustomerArchive(props: DialogCustomerArchiveProps) {
  const [updateUserEcomm, { isLoading }] = useUpdateUserEcommMutation();
  const { user, onClose, open } = props;
  const { handleSubmit } = useForm({
    defaultValues: {
      status: EUserEcommStatus.ARCHIVED,
    },
  });

  const handleClose = () => {
    onClose();
  };

  const handleFormSubmit = async (body) => {
    const userId = user?.user?.id;
    const ecommId = user?.ecomm?.id;
    const result = await updateUserEcomm({ userId, ecommId, body });

    if (result['data']) {
      handleClose();
    }
  };

  return (
    <Dialog onClose={handleClose} open={open} maxWidth="xs" fullWidth>
      <DialogTitle>Archive Customer</DialogTitle>
      <Divider />
      <form onSubmit={handleSubmit(handleFormSubmit)}>
        <Box sx={{ padding: 3 }}>
          <Typography>
            Archive this customer?
          </Typography>
        </Box>
        <DialogActions>
          <Button onClick={handleClose}>Close</Button>
          <LoadingButton variant="contained" type="submit" color='error' loading={isLoading}>
            Archive
          </LoadingButton>
        </DialogActions>
      </form>
    </Dialog>
  );
}

export default DialogCustomerArchive;
