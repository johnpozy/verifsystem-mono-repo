import { render } from '@testing-library/react';

import DialogCustomerForm from './dialog-customer-form';

describe('DialogCustomerForm', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<DialogCustomerForm />);
    expect(baseElement).toBeTruthy();
  });
});
