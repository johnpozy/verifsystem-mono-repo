import { useCallback, useEffect, useState } from 'react';
import Link from 'next/link';
import { Avatar, Chip, Divider, IconButton, ListItemIcon, ListItemText, Menu, MenuItem, Tooltip, Typography } from '@mui/material';
import { Box } from '@mui/system';
import { GridColDef, GridColumnVisibilityModel, GridSortModel } from '@mui/x-data-grid';
import { ArchiveOutline, CheckAll, DotsHorizontal, History, Pencil } from 'mdi-material-ui';

import { DataGrid } from '@verifsystem/dashboard/shared';
import { useAppSelector, stateSelectedEcomm, useGetEcommUsersQuery } from '@verifsystem/dashboard/data-access';
import { EUserEcommStatus, EUserVeriffStatus, IUserEcomm } from '@verifsystem/shared/data-access';

import { DialogCustomerForm } from '../dialog-customer-form/dialog-customer-form';
import { DialogVeriffUpdate } from '../dialog-veriff-update/dialog-veriff-update';
import { DialogCustomerArchive } from '../dialog-customer-archive/dialog-customer-archive';

/* eslint-disable-next-line */
export interface TableCustomerListProps {}

/**
 * TableCustomerList
 *
 * @param {TableCustomerListProps} props
 * @returns JSX.Element
 */
export function TableCustomerList(props: TableCustomerListProps) {
  const selectedEcomm = useAppSelector(stateSelectedEcomm);
  const [page, setPage] = useState(0);
  const [pageSize, setPageSize] = useState(10);
  const [sortBy, setSortBy] = useState(null);
  const { data: ecommUsers, isFetching } = useGetEcommUsersQuery({
    page: page + 1,
    limit: pageSize,
    sortBy,
    ecommId: selectedEcomm?.id
  });
  const [actionMenuAnchorEl, setActionMenuAnchorEl] = useState<null | HTMLElement>(null);
  const [selectedCustomer, setSelectedCustomer] = useState<IUserEcomm>(null);
  const [selectedVeriffDecision, setSelectedVeriffDecision] = useState<EUserVeriffStatus>(null);
  const [dialogCustomerFormOpen, setDialogCustomerFormOpen] = useState(false);
  const [dialogVeriffUpdateOpen, setDialogVeriffUpdateOpen] = useState(false);
  const [dialogCustomerArchiveOpen, setDialogCustomerArchiveOpen] = useState(false);
  const [columnVisibilityModel, setColumnVisibilityModel] = useState<GridColumnVisibilityModel>({ ecomm: false });
  const actionMenuOpen = Boolean(actionMenuAnchorEl);

  const getLatestVeriffSession = (userEcomm: IUserEcomm) => {
    if (userEcomm?.user?.veriffs?.length > 0) {
      return userEcomm?.user?.veriffs?.reduce((a, b) => (a.datetime > b.datetime ? a : b));
    }

    return null;
  };

  const handleDialogCustomerFormOpen = () => {
    setDialogCustomerFormOpen(true);
    handleActionMenuClose();
  };

  const handleDialogCustomerFormClose = () => {
    setDialogCustomerFormOpen(false);
  };

  const handleDialogVeriffUpdateOpen = () => {
    setDialogVeriffUpdateOpen(true);
    handleActionMenuClose();
  };

  const handleDialogVeriffUpdateClose = () => {
    setDialogVeriffUpdateOpen(false);
  };

  const handleDialogCustomerArchiveOpen = () => {
    setDialogCustomerArchiveOpen(true);
    handleActionMenuClose();
  };

  const handleDialogCustomerArchiveClose = () => {
    setDialogCustomerArchiveOpen(false);
  };

  const handleActionMenuClick = (event: React.MouseEvent<HTMLButtonElement>, userEcomm: IUserEcomm) => {
    setSelectedCustomer(userEcomm);
    setSelectedVeriffDecision(
      getLatestVeriffSession(userEcomm)?.decision === EUserVeriffStatus.APPROVED ? EUserVeriffStatus.DECLINED : EUserVeriffStatus.APPROVED
    );
    setActionMenuAnchorEl(event.currentTarget);
  };

  const handleActionMenuClose = () => {
    setActionMenuAnchorEl(null);
  };

  const handleSortModelChange = useCallback((sortModel: GridSortModel) => {
    if (sortModel && sortModel.length > 0) {
      setSortBy(`${sortModel[0].field}:${sortModel[0].sort}`);
    }
  }, []);

  const columns: GridColDef[] = [
    {
      field: 'displayName',
      headerName: 'Display Name',
      width: 150,
      valueGetter: ({ row }) => row?.user?.displayName,
      renderCell: ({ row }) => {
        return (
          <Box sx={{ display: 'flex', alignItems: 'center' }}>
            <Avatar
              alt={row?.user?.displayName}
              sx={{ marginRight: '.5rem', width: 36, height: 36 }}
              src={row?.user?.avatar}
              variant="rounded"
            />
            <Typography>{row?.user?.displayName}</Typography>
          </Box>
        );
      },
    },
    {
      field: 'firstName',
      headerName: 'First Name',
      width: 120,
      valueGetter: ({ row }) => row?.user?.firstName,
      renderCell: ({ row }) => <Typography>{row?.user?.firstName}</Typography>,
    },
    {
      field: 'lastName',
      headerName: 'Last Name',
      width: 120,
      valueGetter: ({ row }) => row?.user?.lastName,
      renderCell: ({ row }) => <Typography>{row?.user?.lastName}</Typography>,
    },
    {
      field: 'email',
      headerName: 'Email',
      width: 200,
      valueGetter: ({ row }) => row?.user?.email,
      renderCell: ({ row }) => <Typography>{row?.user?.email}</Typography>,
    },
    {
      field: 'ecomm',
      headerName: 'Ecomm',
      flex: 1,
      hideable: false,
      valueGetter: ({ row }) => row?.ecomm?.title,
      renderCell: ({ row }) => <Typography>{row?.ecomm?.title}</Typography>,
    },
    {
      field: 'phoneNumber',
      headerName: 'Phone No.',
      width: 150,
      valueGetter: ({ row }) => row?.user?.phoneNumber,
      renderCell: ({ row }) => <Typography>{row?.user?.phoneNumber ? row?.user?.phoneNumber : 'N/A'}</Typography>,
    },
    {
      field: 'status',
      headerName: 'Status',
      flex: 1,
      renderCell: ({ row }) => (
        <Chip label={row.status.toUpperCase()} color={row?.status === EUserEcommStatus.BLOCKED ? 'error' : 'primary'} size="small" />
      ),
    },
    {
      field: 'veriffs',
      headerName: 'Verification',
      flex: 1,
      valueGetter: ({ row }) => getLatestVeriffSession(row)?.decision,
      renderCell: ({ value }) => {
        if (value) {
          return <Chip label={value?.toUpperCase()} color={value === EUserVeriffStatus.DECLINED ? 'error' : 'primary'} size="small" />;
        }

        return <Typography>N/A</Typography>;
      },
    },
    {
      field: 'actions',
      type: 'actions',
      flex: 1,
      align: 'right',
      renderCell: ({ row }) => {
        return (
          <>
            <Tooltip title="Action menu">
              <IconButton onClick={(event) => handleActionMenuClick(event, row)}>
                <DotsHorizontal />
              </IconButton>
            </Tooltip>
            <Menu
              anchorEl={actionMenuAnchorEl}
              open={actionMenuOpen}
              onClose={handleActionMenuClose}
              anchorOrigin={{
                vertical: 'bottom',
                horizontal: 'right',
              }}
              transformOrigin={{
                vertical: 'top',
                horizontal: 'right',
              }}
              PaperProps={{
                style: {
                  minWidth: 180,
                  boxShadow:
                    'rgb(255, 255, 255) 0px 0px 0px 0px, rgba(0, 0, 0, 0.02) 0px 0px 0px 1px, rgba(0, 0, 0, 0.01) 0px 4px 6px -2px',
                },
              }}
            >
              <MenuItem onClick={handleDialogVeriffUpdateOpen} disabled={!getLatestVeriffSession(row)}>
                <ListItemIcon>
                  <CheckAll />
                </ListItemIcon>
                <ListItemText>
                  Mark as{' '}
                  {getLatestVeriffSession(row)?.decision === EUserVeriffStatus.APPROVED
                    ? EUserVeriffStatus.DECLINED
                    : EUserVeriffStatus.APPROVED}
                </ListItemText>
              </MenuItem>
              <Divider />
              <Link href={`customer/${selectedCustomer?.id}/transaction`}>
                <MenuItem>
                  <ListItemIcon>
                    <History />
                  </ListItemIcon>
                  <ListItemText>Transaction History</ListItemText>
                </MenuItem>
              </Link>
              <MenuItem onClick={handleDialogCustomerFormOpen}>
                <ListItemIcon>
                  <Pencil />
                </ListItemIcon>
                <ListItemText>Edit Customer</ListItemText>
              </MenuItem>
              <MenuItem onClick={handleDialogCustomerArchiveOpen}>
                <ListItemIcon>
                  <ArchiveOutline />
                </ListItemIcon>
                <ListItemText>Archive Customer</ListItemText>
              </MenuItem>
            </Menu>
          </>
        );
      },
    },
  ];

  useEffect(() => {
    setColumnVisibilityModel({
      ecomm: !selectedEcomm,
    });
  }, [selectedEcomm]);

  return (
    <>
      <DataGrid
        rows={ecommUsers?.data || []}
        rowCount={ecommUsers?.meta?.totalItems || 0}
        page={page}
        pageSize={pageSize}
        paginationMode="server"
        onPageChange={(newPage) => setPage(newPage)}
        onPageSizeChange={(newPageSize) => setPageSize(newPageSize)}
        sortingMode="server"
        onSortModelChange={handleSortModelChange}
        columns={columns}
        checkboxSelection
        loading={isFetching}
        columnVisibilityModel={columnVisibilityModel}
        onColumnVisibilityModelChange={(newModel) => setColumnVisibilityModel(newModel)}
      />
      <DialogCustomerForm user={selectedCustomer} open={dialogCustomerFormOpen} onClose={handleDialogCustomerFormClose} />
      <DialogVeriffUpdate
        user={selectedCustomer}
        decision={selectedVeriffDecision}
        open={dialogVeriffUpdateOpen}
        onClose={handleDialogVeriffUpdateClose}
      />
      <DialogCustomerArchive user={selectedCustomer} open={dialogCustomerArchiveOpen} onClose={handleDialogCustomerArchiveClose} />
    </>
  );
}

export default TableCustomerList;
