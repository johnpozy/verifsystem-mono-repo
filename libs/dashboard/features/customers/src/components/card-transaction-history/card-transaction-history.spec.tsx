import { render } from '@testing-library/react';

import CardTransactionHistory from './card-transaction-history';

describe('CardTransactionHistory', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<CardTransactionHistory />);
    expect(baseElement).toBeTruthy();
  });
});
