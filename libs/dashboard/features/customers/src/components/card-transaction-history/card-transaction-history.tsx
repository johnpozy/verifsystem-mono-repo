import { Card, CardHeader, Divider } from '@mui/material';

import { TableTransactionList } from '../table-transaction-list/table-transaction-list';

/* eslint-disable-next-line */
export interface CardTransactionHistoryProps {}

export function CardTransactionHistory(props: CardTransactionHistoryProps) {
  return (
    <Card>
      <CardHeader titleTypographyProps={{ variant: 'h6' }} title="Transaction History" />
      <Divider />

      <TableTransactionList />
    </Card>
  );
}

export default CardTransactionHistory;
