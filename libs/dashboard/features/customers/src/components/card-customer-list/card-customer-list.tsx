import { Card, CardHeader, Divider } from '@mui/material';

import TableCustomerList from '../table-customer-list/table-customer-list';

/* eslint-disable-next-line */
export interface CardCustomerListProps {}

export function CardCustomerList(props: CardCustomerListProps) {
  return (
    <Card>
      <CardHeader titleTypographyProps={{ variant: 'h6' }} title="Customer List" />
      <Divider />

      <TableCustomerList />
    </Card>
  );
}

export default CardCustomerList;
