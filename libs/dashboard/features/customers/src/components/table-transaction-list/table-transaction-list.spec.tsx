import { render } from '@testing-library/react';

import TableTransactionList from './table-transaction-list';

describe('TableTransactionList', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<TableTransactionList />);
    expect(baseElement).toBeTruthy();
  });
});
