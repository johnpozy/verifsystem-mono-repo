import { useRouter } from 'next/router';
import { Chip, Tooltip, Typography } from '@mui/material';
import { GridColDef } from '@mui/x-data-grid';
import moment from 'moment';

import { DataGrid } from '@verifsystem/dashboard/shared';
import { useGetUserTransactionsQuery } from '@verifsystem/dashboard/data-access';

/* eslint-disable-next-line */
export interface TableTransactionListProps {}

/**
 * TableTransactionList
 *
 * @param {TableTransactionListProps} props
 * @returns JSX.Element
 */
export function TableTransactionList(props: TableTransactionListProps) {
  const router = useRouter();
  const { data: transactions = [], isLoading } = useGetUserTransactionsQuery({ userId: +router?.query?.userId });

  const columns: GridColDef[] = [
    {
      field: 'gatewayConfig.name',
      headerName: 'Payment Method',
      flex: 1,
      renderCell: (params) => <Typography>{params.row?.gatewayConfig?.name}</Typography>,
    },
    {
      field: 'total',
      headerName: 'Total',
      width: 250,
      renderCell: (params) => <Typography>${params.row.amount / 100}</Typography>,
    },
    {
      field: 'createdAt',
      headerName: 'Date',
      flex: 1,
      renderCell: (params) => <Typography>{moment(params.row.createdAt).format('ll')}</Typography>,
    },
    {
      field: 'status',
      headerName: 'Status',
      width: 250,
      renderCell: (params) => {
        return (
          <Tooltip title="Status">
            <Chip label={params.row.status.toUpperCase()} color={params.row.status === 'processing' ? 'success' : 'warning'} size="small" />
          </Tooltip>
        );
      },
    }
  ];

  return <DataGrid rows={transactions} columns={columns} checkboxSelection loading={isLoading} />;
}

export default TableTransactionList;
