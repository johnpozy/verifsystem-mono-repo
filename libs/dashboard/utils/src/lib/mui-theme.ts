import { createTheme } from '@mui/material/styles';

export const muiTheme = createTheme({
  palette: {
    primary: {
      main: '#0052cc'
    },
    success: {
      main: '#2ed573'
    },
    warning: {
      main: '#ffa502'
    },
    error: {
      main: '#ff3f34'
    },
    background: {
      default: "#f3f6f7"
    }
  },
  typography: {
    htmlFontSize: 18,
    fontFamily: [
      '"Inter"',
      '-apple-system',
      'BlinkMacSystemFont',
      '"Segoe UI"',
      'Roboto',
      '"Helvetica Neue"',
      'Arial',
      'sans-serif',
      '"Apple Color Emoji"',
      '"Segoe UI Emoji"',
      '"Segoe UI Symbol"',
    ].join(','),
  },
  components: {
    MuiChip: {
      defaultProps: {
        sx: {
          fontSize: '12px'
        }
      }
    },
    MuiBadge: {
      defaultProps: {
        sx: {
          fontSize: '12px'
        }
      }
    },
    MuiDivider: {
      defaultProps: {
        sx: {
          borderColor: 'rgba(0, 0, 0, 0.08)'
        }
      }
    },
    MuiCardContent: {
      defaultProps: {
        sx: {
          padding: '24px'
        }
      }
    },
    MuiCardHeader: {
      defaultProps: {
        sx: {
          // padding: '16px 24px',
          paddingBottom: '14px',
          // backgroundColor: 'rgba(0, 0, 0, 0.03)'
        }
      }
    },
    MuiCard: {
      defaultProps: {
        sx: {
          boxShadow: 'none',
          border: '1px solid rgba(0, 0, 0, 0.08)'
        }
      }
    },
    MuiDialogActions: {
      defaultProps: {
        sx: {
          padding: 3,
          bgcolor: 'rgba(0, 0, 0, 0.02)'
        }
      }
    },
    MuiMenu: {
      defaultProps: {
        elevation: 1
      }
    }
  }
});

export default muiTheme;
