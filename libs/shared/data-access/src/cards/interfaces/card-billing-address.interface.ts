export interface ICardBillingAddress {
  address1: string;
  address2: string;
  city: string;
  state: string;
  country: string;
  zipCode: string;
}
