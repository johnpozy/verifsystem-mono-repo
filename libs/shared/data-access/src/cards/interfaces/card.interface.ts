import { ECardStatus } from '../enums/card-status.enum';
import { ICardBillingAddress } from './card-billing-address.interface';
import { ICardHash } from './card-hash.interface';

export interface ICard {
  id: number;
  cardHolderName: string;
  billingAddress: ICardBillingAddress;
  description: string;
  cardHash: ICardHash;
  number: number;
  last4: number;
  expYear: number;
  expMonth: number;
  cvc: number;
  createdAt: Date;
  status: ECardStatus;
}
