export interface ICardHash {
  iv: string;
  hash: string;
}
