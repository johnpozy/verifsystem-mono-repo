export interface ICardGatewayConfig {
  id: number;
  cardId: string;
}
