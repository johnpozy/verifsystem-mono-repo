export enum ECardStatus {
  ACTIVATED = 'activated',
  DEACTIVATED = 'deactivated'
}
