export enum ETransactionStatus {
  Pending = 'pending',
  Processing = 'processing',
  'On Hold' = 'on-hold',
  Completed = 'completed',
  Cancelled = 'cancelled',
  Refunded = 'refunded',
  Failed = 'failed',
  Dispute = 'dispute'
}
