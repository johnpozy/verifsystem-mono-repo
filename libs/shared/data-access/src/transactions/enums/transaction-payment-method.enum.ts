export enum ETransactionPaymentMethod {
  'Smartpayment Gateway' = 'smartpayments_gateway',
  'Cheque' = 'cheque',
  'Cash On Delivery' = 'cod',
  'Direct Bank Transfer' = 'bacs'
}
