import { IEcomm } from '../../ecomms';
import { IGatewayConfig } from '../../gateway-configs';
import { IUser } from '../../users';
import { ETransactionPaymentMethod } from '../enums/transaction-payment-method.enum';
import { ETransactionStatus } from '../enums/transaction-status.enum';

export interface ITransaction {
  id: number;
  amount: number;
  orderId: number;
  createdAt: Date;
  status: ETransactionStatus;
  description: string;
  paymentMethod: ETransactionPaymentMethod
  ecomm?: IEcomm;
  user?: IUser;
  gatewayConfig?: IGatewayConfig
}
