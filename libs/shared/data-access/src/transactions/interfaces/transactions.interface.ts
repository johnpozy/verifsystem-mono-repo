import { IAPIResponse } from '../../commons/interfaces/api-response.interface';
import { ITransaction } from './transaction.interface';

export interface ITransactions extends IAPIResponse {
  data: Array<ITransaction>;
}
