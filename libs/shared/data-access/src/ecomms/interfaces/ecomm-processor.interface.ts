import { IProcessor } from '../../processors/interfaces/processor.interface';

import { IEcomm } from './ecomm.interface';
import { EEcommProcessorStatus } from '../enums/ecomm-processor-status.enum';

export interface IEcommProcessor {
  id: number;
  processor: IProcessor;
  ecomm: IEcomm;
  createdAt: Date;
  status: EEcommProcessorStatus;
}
