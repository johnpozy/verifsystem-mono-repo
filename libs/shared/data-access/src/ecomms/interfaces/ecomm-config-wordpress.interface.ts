export interface IEcommConfigWordpress {
  apiUser: string;
  apiBaseUrl: string;
  apiKey: string;
}
