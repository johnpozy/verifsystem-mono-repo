import { EEcommStatus } from '../enums/ecomm-status.enum';
import { IEcommConfig } from './ecomm-config.interface';
import { IEcommContact } from './ecomm-contact.interface';
import { IEcommProcessor } from './ecomm-processor.interface';

export interface IEcomm {
  id: number;
  title: string;
  token: string;
  description: string;
  baseUrl: string;
  contact: IEcommContact;
  processors?: Array<IEcommProcessor>;
  configs?: Array<IEcommConfig>;
  createdAt: Date;
  status: EEcommStatus;
}
