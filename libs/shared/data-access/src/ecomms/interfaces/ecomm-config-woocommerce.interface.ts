export interface IEcommConfigWoocommerce {
  consumerKey: string;
  consumerSecret: string;
}
