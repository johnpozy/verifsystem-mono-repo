import { IEcommConfigWoocommerce } from './ecomm-config-woocommerce.interface';
import { IEcommConfigWordpress } from './ecomm-config-wordpress.interface';

export interface IEcommConfig {
  id: number;
  wordpress: IEcommConfigWordpress;
  woocommerce: IEcommConfigWoocommerce;
}
