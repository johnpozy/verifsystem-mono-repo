export interface IEcommContact {
  name: string;
  phoneNumber: string;
}
