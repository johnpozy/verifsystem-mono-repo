export enum EEcommProcessorStatus {
  ACTIVATED = 'activated',
  DEACTIVATED = 'deactivated'
}
