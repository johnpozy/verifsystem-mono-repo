export enum EEcommStatus {
  ACTIVATED = 'activated',
  DEACTIVATED = 'deactivated'
}
