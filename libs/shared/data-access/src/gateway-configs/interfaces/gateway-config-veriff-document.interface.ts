export interface IGatewayConfigVeriffDocument {
  number: number;
  type: string;
  country: string;
  validFrom: Date;
  validUntil: Date;
}
