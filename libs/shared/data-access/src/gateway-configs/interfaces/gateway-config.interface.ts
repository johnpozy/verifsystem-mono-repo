import { EGatewayConfigStatus } from '../enums/gateway-config-status.enum';
import { EGatewayConfigType } from '../enums/gateway-config-type.enum';

export interface IGatewayConfig {
  id: number;
  type: EGatewayConfigType;
  name: string;
  apiKey: string;
  maxDailySessions: number;
  maxDailyTxnNumber: number;
  maxDailyTxnAmount: number;
  maxMonthlyTxnNumber: number;
  maxMonthlyTxnAmount: number;
  createdAt: Date;
  status: EGatewayConfigStatus;
}
