import { IGatewayConfigVeriffVerification } from "./gateway-config-veriff-verification.interface";

export interface IGatewayConfigVeriffDecision {
  status: string;
  verification: IGatewayConfigVeriffVerification;
  technicalData: object;
}
