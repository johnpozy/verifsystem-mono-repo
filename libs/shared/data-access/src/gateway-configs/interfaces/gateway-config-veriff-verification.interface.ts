import { IGatewayConfigVeriffDocument } from "./gateway-config-veriff-document.interface";
import { IGatewayConfigVeriffPerson } from "./gateway-config-veriff-person.interface";
import { IGatewayConfigVeriffVendordata } from "./gateway-config-veriff-vendordata.interface";

export interface IGatewayConfigVeriffVerification {
  acceptanceTime: Date;
  decisionTime: Date;
  code: number;
  id: string;
  vendorData: IGatewayConfigVeriffVendordata;
  status: string;
  reason: string;
  reasonCode: number;
  person: IGatewayConfigVeriffPerson;
  document: IGatewayConfigVeriffDocument;
  comments: Array<string>;
  additionalVerifiedData: object;
}
