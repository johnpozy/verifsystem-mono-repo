export interface IGatewayConfigVeriffPerson {
  firstName: string;
  lastName: string;
  citizenship: string;
  idNumber: number;
  gender: string;
  dateOfBirth: Date;
  yearOfBirth: number;
  placeOfBirth: string;
  nationality: string;
  pepSanctionMatch: any;
}
