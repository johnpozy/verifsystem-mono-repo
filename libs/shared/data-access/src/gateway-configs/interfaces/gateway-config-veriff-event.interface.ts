import { IGatewayConfigVeriffVendordata } from "./gateway-config-veriff-vendordata.interface";

export interface IGatewayConfigVeriffEvent {
  id: string;
  attemptId: string;
  feature: string;
  code: number;
  action: string;
  vendorData: IGatewayConfigVeriffVendordata
}
