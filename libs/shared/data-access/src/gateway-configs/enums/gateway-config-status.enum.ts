export enum EGatewayConfigStatus {
  ACTIVATED = 'activated',
  DEACTIVATE = 'deactivated',
}
