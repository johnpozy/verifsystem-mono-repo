export enum EGatewayConfigType {
  STRIPE = 'stripe',
  BAMBORA = 'bambora',
  ELAVON = 'elavon',
  VERIFF = 'veriff',
}
