import { IAPIResponse } from '../../commons/interfaces/api-response.interface';
import { IUser } from './user.interface';

export interface IUsers extends IAPIResponse {
  data: Array<IUser>;
}
