import { IEcomm } from '../../ecomms/interfaces/ecomm.interface';

import { EUserEcommStatus } from '../enums/user-ecomm-status.enum';
import { IUser } from './user.interface';

export interface IUserEcomm {
  id: number;
  ecomm?: IEcomm;
  externalId: string;
  createdAt: Date;
  status: EUserEcommStatus;
  user?: IUser;
}
