import { EUserRole } from '../enums/user-role.enum';
import { EUserStatus } from '../enums/user-status.enum';
import { IUserEcomm } from './user-ecomm.interface';
import { IUserVeriff } from './user-veriff.interface';

export interface IUser {
  id: number;
  role: EUserRole;
  displayName: string;
  firstName: string;
  lastName: string;
  password: string;
  email: string;
  phoneNumber: string;
  postcode: any;
  lastLogin: Date;
  createdAt: Date;
  status: EUserStatus;
  ecomms?: Array<IUserEcomm>;
  veriffs?: Array<IUserVeriff>;
  accessToken?: string;
}
