export interface IUserVeriff {
  id: number;
  sessionId: string;
  documentType: string;
  decision: string;
  datetime: Date;
}
