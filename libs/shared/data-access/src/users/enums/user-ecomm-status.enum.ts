export enum EUserEcommStatus {
  ACTIVATED = 'activated',
  DEACTIVATED = 'deactivated',
  BLOCKED = 'blocked',
  ARCHIVED = 'archived',
}
