export enum EUserVeriffStatus {
  APPROVED = 'approved',
  RESUBMISSION_REQUESTED = 'resubmission-requested',
  DECLINED = 'declined',
  REVIEW = 'review',
  EXPIRED = 'expired',
  ABANDONED = 'abandoned',
  STARTED = 'started',
  SUBMITTED = 'submitted'
}
