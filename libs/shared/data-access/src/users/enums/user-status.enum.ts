export enum EUserStatus {
  ACTIVATED = 'activated',
  DEACTIVATED = 'deactivated',
}
