export enum EUserRole {
  ADMINISTRATOR = 'administrator',
  PARTNER = 'partner',
  CUSTOMER = 'customer',
  APPROVED_CUSTOMER = 'approved_customer',
  DECLINED_CUSTOMER = 'declined_customer'
}
