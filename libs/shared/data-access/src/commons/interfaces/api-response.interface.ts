import { IMeta } from "./meta.interface";

export interface IAPIResponse {
  status: number;
  success: boolean;
  meta: IMeta;
}
