export enum EProcessorStatus {
  ACTIVATED = 'activated',
  DEACTIVATED = 'deactivated',
}
