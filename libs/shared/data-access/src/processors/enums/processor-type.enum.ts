export enum EProcessorType {
  VERIFICATION = 'verification',
  PAYMENT = 'payment',
}
