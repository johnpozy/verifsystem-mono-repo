import { IEcomm } from '../../ecomms';
import { EProcessorStatus } from '../enums/processor-status.enum';
import { EProcessorType } from '../enums/processor-type.enum';

export interface IProcessor {
  id: number;
  type: EProcessorType;
  title: string;
  description: string;
  baseUrl: string;
  createdAt: Date;
  status: EProcessorStatus;
  ecomm?: IEcomm;
}
