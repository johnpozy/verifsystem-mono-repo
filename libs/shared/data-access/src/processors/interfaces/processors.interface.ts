import { IAPIResponse } from '../../commons/interfaces/api-response.interface';
import { IProcessor } from './processor.interface';

export interface IProcessors extends IAPIResponse {
  data: Array<IProcessor>;
}
