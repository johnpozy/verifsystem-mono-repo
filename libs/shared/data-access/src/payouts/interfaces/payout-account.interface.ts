import { IEcomm } from '../../ecomms';
import { EPayoutStatus } from '../enums/payout-status.enum';
import { EPayoutType } from '../enums/payout-type.enum';

export interface IPayoutAccount {
  id?: number;
  name: string;
  type: EPayoutType;
  accountNumber: string;
  details: any;
  createdAt: Date;
  status: EPayoutStatus;
  ecomm?: IEcomm;
}
