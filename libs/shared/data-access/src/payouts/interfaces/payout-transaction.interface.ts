import { EPayoutTransactionStatus } from '../enums/payout-transaction-status.enum';

export interface IPayoutTransaction {
  id: number;
  amount: number;
  description: string;
  createdAt: Date;
  status: EPayoutTransactionStatus;
}
