export enum EPayoutStatus {
  ACTIVATED = 'activated',
  ARCHIVED = 'archived',
}
