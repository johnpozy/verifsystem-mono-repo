export enum EPayoutType {
  'Bill Payment' = 'bill-payment',
  'E-Transfer' = 'e-transfer',
  'Request Cheque' = 'cheque',
  'Direct Deposit' = 'direct-deposit'
}
