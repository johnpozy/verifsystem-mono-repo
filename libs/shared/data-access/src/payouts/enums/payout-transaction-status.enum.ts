export enum EPayoutTransactionStatus {
  'Pending' = 'pending',
  'Approved' = 'approved'
}
