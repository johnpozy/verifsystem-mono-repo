import { TypeOrmModuleOptions } from '@nestjs/typeorm';

export const environment = {
  production: false,
  typeORM: <TypeOrmModuleOptions>{
    type: 'mysql',
    host: '0.0.0.0',
    port: 3306,
    username: 'user',
    password: 'password',
    database: 'verifsystem',
    autoLoadEntities: true,
    synchronize: true
  },
  jwtSecret: 'loremipsumdolor',
  corsAllowedOrigins: ['http://localhost:4200', 'http://localhost:4300'],
  mailgunUrl: 'https://api.mailgun.net/v3/mg.verifsystem.com/messages',
  mailgunKey: 'YXBpOjBiNzEzZjkwM2YxMmM1ZWZkN2JjNzJkMGNiOWFmYmFlLTA2Nzc1MTdmLWVlZTJhZjU4',
};
