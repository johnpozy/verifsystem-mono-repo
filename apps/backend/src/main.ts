import { Logger } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import { IoAdapter } from '@nestjs/platform-socket.io';
import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger';
import * as cookieParser from 'cookie-parser';
import { json } from 'body-parser';

import { AppModule } from './app/app.module';

async function bootstrap() {
  const port = process.env.PORT || 3000;
  const app = await NestFactory.create(AppModule, {
    bodyParser: false
  });

  app.useWebSocketAdapter(new IoAdapter(app));
  app.use(json({ limit: '20mb' }));

  const config = new DocumentBuilder()
    .setTitle('Backend API')
    .setDescription('Backend API description')
    .setVersion('1.0')
    .build();
  const document = SwaggerModule.createDocument(app, config);

  SwaggerModule.setup('docs', app, document);

  app.enableCors({
    origin: process.env.CORS_ALLOW_ORIGIN.split(','),
    methods: ['GET', 'POST', 'DELETE', 'PATCH', 'PUT', 'OPTIONS'],
    credentials: true,
  });
  app.use(cookieParser());

  await app.listen(port, () => {
    Logger.log(`🚀 Application is running on: http://localhost:${port}`);
  });
}

bootstrap();
