import { Module } from '@nestjs/common';

import { AuthModule as AuthFeatureModule } from '@verifsystem/api/features';

import { AuthController } from './auth.controller';

@Module({
  imports: [AuthFeatureModule],
  controllers: [AuthController],
})
export class AuthModule {}
