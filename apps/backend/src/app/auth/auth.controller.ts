import { Body, Controller, Post, Request, Response, UseGuards } from '@nestjs/common';
import { ApiCreatedResponse, ApiInternalServerErrorResponse, ApiTags, ApiUnauthorizedResponse } from '@nestjs/swagger';

import { AuthLocalGuard, AuthLoginDto, AuthService } from '@verifsystem/api/features';

@ApiTags('Auth')
@Controller('auth')
export class AuthController {
  constructor(private authService: AuthService) {}

  @UseGuards(AuthLocalGuard)
  @Post('login')
  @ApiCreatedResponse({ description: 'Success' })
  @ApiInternalServerErrorResponse({ description: 'Internal Server Error' })
  @ApiUnauthorizedResponse({ description: 'Unauthorized' })
  async login(
    @Request() req,
    @Response({ passthrough: true }) res,
    @Body() authLoginDto: AuthLoginDto
  ) {
    return await this.authService.login(req, res);
  }

  @Post('logout')
  @ApiCreatedResponse({ description: 'Success' })
  @ApiInternalServerErrorResponse({ description: 'Internal Server Error' })
  @ApiUnauthorizedResponse({ description: 'Unauthorized' })
  async logout(
    @Request() req,
    @Response({ passthrough: true }) res,
  ) {
    return this.authService.logout(req, res);
  }
}
