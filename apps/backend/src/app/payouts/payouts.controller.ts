import { Body, Controller, Delete, Get, Param, Patch, Post, Query } from '@nestjs/common';
import { ApiCreatedResponse, ApiInternalServerErrorResponse, ApiOkResponse, ApiOperation, ApiParam, ApiQuery, ApiTags } from '@nestjs/swagger';

import { PayoutCreateDto, PayoutsService, PayoutTransactionCreateDto, PayoutTransactionUpdateDto, PayoutUpdateDto } from '@verifsystem/api/features';
import { EPayoutTransactionStatus } from '@verifsystem/shared/data-access';

@ApiTags('Payouts')
@Controller('payouts')
export class PayoutsController {
  constructor(private payoutsService: PayoutsService) {}

  @Get('accounts/ecomms')
  @ApiOperation({ summary: 'List all payout account' })
  @ApiOkResponse({ description: 'Ok' })
  getByEcomms() {
    return this.payoutsService.getAll();
  }

  @Get('accounts/ecomms/:ecommId')
  @ApiOperation({ summary: 'List all ecomm payout account' })
  @ApiOkResponse({ description: 'Ok' })
  @ApiParam({ name: 'ecommId', type: Number, allowEmptyValue: false })
  getByEcomm(@Param('ecommId') ecommId: number) {
    return this.payoutsService.getByEcomm(ecommId);
  }

  @Post('accounts/ecomms/:ecommId')
  @ApiOperation({ summary: 'Create payout account' })
  @ApiCreatedResponse({ description: 'Created' })
  @ApiInternalServerErrorResponse({ description: 'Internal Server Error' })
  @ApiParam({ name: 'ecommId', type: Number, allowEmptyValue: false })
  async create(
    @Param('ecommId') ecommId: number,
    @Body() payoutCreateDto: PayoutCreateDto
  ) {
    return this.payoutsService.create(ecommId, payoutCreateDto);
  }

  @Patch('accounts/:accountId')
  @ApiOperation({ summary: 'Update payout account' })
  @ApiParam({ name: 'accountId', type: Number, allowEmptyValue: false })
  @ApiCreatedResponse({ description: 'Ok' })
  @ApiInternalServerErrorResponse({ description: 'Internal Server Error' })
  update(
    @Param('accountId') accountId: number,
    @Body() payoutUpdateDto: PayoutUpdateDto
  ) {
    return this.payoutsService.update(accountId, payoutUpdateDto);
  }

  @Delete('accounts/:accountId')
  @ApiOperation({ summary: 'Delete payout account' })
  @ApiParam({ name: 'accountId', type: Number, allowEmptyValue: false })
  @ApiCreatedResponse({ description: 'Ok' })
  @ApiInternalServerErrorResponse({ description: 'Internal Server Error' })
  delete(@Param('accountId') accountId: number) {
    return this.payoutsService.deleteAccount(accountId);
  }

  @Get('accounts/:accountId/transactions')
  @ApiOperation({ summary: 'List all payout account transactions' })
  @ApiOkResponse({ description: 'Ok' })
  @ApiParam({ name: 'accountId', type: Number, allowEmptyValue: false })
  @ApiQuery({ name: 'status', required: false, type: String, enum: EPayoutTransactionStatus })
  getAccountTransactions(@Param('accountId') accountId: number, @Query('status') status: EPayoutTransactionStatus) {
    return this.payoutsService.getTransactions(accountId, { status });
  }

  @Post('accounts/:accountId/transactions')
  @ApiOperation({ summary: 'Create payout account transaction' })
  @ApiCreatedResponse({ description: 'Created' })
  @ApiInternalServerErrorResponse({ description: 'Internal Server Error' })
  @ApiParam({ name: 'accountId', type: Number, allowEmptyValue: false })
  async createTransaction(@Param('accountId') accountId: number, @Body() payoutTransactionCreateDto: PayoutTransactionCreateDto) {
    const result = await this.payoutsService.createTransaction(accountId, payoutTransactionCreateDto);

    return result;
  }

  @Patch('accounts/:accountId/transactions/:transactionId')
  @ApiOperation({ summary: 'Update payout account transaction' })
  @ApiParam({ name: 'accountId', type: Number, allowEmptyValue: false })
  @ApiParam({ name: 'transactionId', type: Number, allowEmptyValue: false })
  @ApiCreatedResponse({ description: 'Ok' })
  @ApiInternalServerErrorResponse({ description: 'Internal Server Error' })
  updateTransaction(
    @Param('accountId') accountId: number,
    @Param('transactionId') transactionId: number,
    @Body() payoutTransactionUpdateDto: PayoutTransactionUpdateDto
  ) {
    return this.payoutsService.updateTransaction(accountId, transactionId, payoutTransactionUpdateDto);
  }

  @Get('transactions')
  @ApiOperation({ summary: 'List all payout transaction' })
  @ApiOkResponse({ description: 'Ok' })
  @ApiQuery({ name: 'status', required: false, type: String, enum: EPayoutTransactionStatus })
  getTransactions(@Query('status') status: EPayoutTransactionStatus) {
    return this.payoutsService.getTransactions(null, { status });
  }

  @Get('transactions/ecomms')
  @ApiOperation({ summary: 'List all payout transaction' })
  @ApiOkResponse({ description: 'Ok' })
  @ApiQuery({ name: 'status', required: false, type: String, enum: EPayoutTransactionStatus })
  getEcommsTransactions(@Query('status') status: EPayoutTransactionStatus) {
    return this.payoutsService.getEcommTransactions(null, { status });
  }

  @Get('transactions/ecomms/:ecommId')
  @ApiOperation({ summary: 'List all payout transaction' })
  @ApiParam({ name: 'ecommId', type: Number, allowEmptyValue: false })
  @ApiOkResponse({ description: 'Ok' })
  @ApiQuery({ name: 'status', required: false, type: String, enum: EPayoutTransactionStatus })
  getEcommTransactions(@Param('ecommId') ecommId: number, @Query('status') status: EPayoutTransactionStatus) {
    return this.payoutsService.getEcommTransactions(ecommId, { status });
  }
}
