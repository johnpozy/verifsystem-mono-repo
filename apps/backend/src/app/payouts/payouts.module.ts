import { Module } from '@nestjs/common';

import { PayoutsModule as PayoutsFeatureModule } from '@verifsystem/api/features';

import { PayoutsController } from './payouts.controller';

@Module({
  imports: [PayoutsFeatureModule],
  controllers: [PayoutsController],
})
export class PayoutsModule {}
