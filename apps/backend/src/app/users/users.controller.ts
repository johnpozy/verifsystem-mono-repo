import { Body, Controller, DefaultValuePipe, Delete, Get, Param, ParseIntPipe, Patch, Post, Query } from '@nestjs/common';
import {
  ApiCreatedResponse,
  ApiInternalServerErrorResponse,
  ApiNoContentResponse,
  ApiOkResponse,
  ApiOperation,
  ApiParam,
  ApiTags,
} from '@nestjs/swagger';

import { CardsService, UsersService, UserCreateDto, UserVeriffCreateDto, CardCreateDto, UserUpdateDto, UserVeriffUpdateDto, UserEcommUpdateDto } from '@verifsystem/api/features';

@ApiTags('Users')
@Controller('users')
export class UsersController {
  constructor(private usersService: UsersService, private cardsService: CardsService) {}

  @Get()
  @ApiOperation({ summary: 'List all users' })
  @ApiOkResponse({ description: 'Ok' })
  @ApiInternalServerErrorResponse({ description: 'Internal Server Error' })
  async getAll() {
    return await this.usersService.getUsers();
  }

  @Get(':userId')
  @ApiOperation({ summary: 'Get user by id' })
  @ApiParam({ name: 'userId', type: Number, allowEmptyValue: false })
  @ApiOkResponse({ description: 'Ok' })
  @ApiNoContentResponse({ description: 'No Content' })
  @ApiInternalServerErrorResponse({ description: 'Internal Server Error' })
  async getById(@Param('userId') userId: number) {
    return await this.usersService.getUserById(userId);
  }

  @Post()
  @ApiOperation({ summary: 'Create user' })
  @ApiCreatedResponse({ description: 'The record has been successfully created.' })
  async create(@Body() userCreateDto: UserCreateDto) {
    return await this.usersService.create(userCreateDto);
  }

  @Patch(':userId')
  @ApiOperation({ summary: 'Update user' })
  @ApiParam({ name: 'userId', type: Number, allowEmptyValue: false })
  @ApiOkResponse({ description: 'Ok' })
  @ApiNoContentResponse({ description: 'No Content' })
  @ApiInternalServerErrorResponse({ description: 'Internal Server Error' })
  async update(@Body() userUpdateDto: UserUpdateDto, @Param('userId') userId: number) {
    return await this.usersService.update(userUpdateDto, userId);
  }

  @Get('ecomms/customers')
  @ApiOperation({ summary: 'Get ecomm customers' })
  @ApiOkResponse({ description: 'Ok' })
  @ApiNoContentResponse({ description: 'No Content' })
  @ApiInternalServerErrorResponse({ description: 'Internal Server Error' })
  async getEcommCustomers(
    @Query('page', new DefaultValuePipe(1), ParseIntPipe) page: number,
    @Query('limit', new DefaultValuePipe(10), ParseIntPipe) limit: number,
    @Query('sortBy') sortBy: [string, string][]
  ) {
    return this.usersService.getEcommCustomers({
      limit,
      page,
      sortBy,
      path: ''
    });
  }

  @Get('ecomms/:ecommId/customers')
  @ApiOperation({ summary: 'Get ecomm customers' })
  @ApiParam({ name: 'ecommId', type: Number, allowEmptyValue: false })
  @ApiOkResponse({ description: 'Ok' })
  @ApiNoContentResponse({ description: 'No Content' })
  @ApiInternalServerErrorResponse({ description: 'Internal Server Error' })
  async getSpecificEcommCustomers(
    @Query('page', new DefaultValuePipe(1), ParseIntPipe) page: number,
    @Query('limit', new DefaultValuePipe(10), ParseIntPipe) limit: number,
    @Query('sortBy') sortBy: [string, string][],
    @Param('ecommId') ecommId: number
  ) {
    return this.usersService.getEcommCustomers({
      limit,
      page,
      sortBy,
      path: ''
    }, ecommId);
  }

  @Get(':externalId/ecomms/:ecommId')
  @ApiOperation({ summary: 'Get ecomm user, used by wp plugin' })
  @ApiParam({ name: 'externalId', type: String, allowEmptyValue: false })
  @ApiParam({ name: 'ecommId', type: Number, allowEmptyValue: false })
  @ApiOkResponse({ description: 'Ok' })
  @ApiNoContentResponse({ description: 'No Content' })
  @ApiInternalServerErrorResponse({ description: 'Internal Server Error' })
  async getEcommUser(@Param('externalId') externalId: string, @Param('ecommId') ecommId: number) {
    const user = await this.usersService.findEcommUser(externalId, ecommId);

    return user;
  }

  @Post(':externalId/ecomms/:ecommId')
  @ApiOperation({ summary: 'Create ecomm user, used by wp plugin' })
  @ApiParam({ name: 'externalId', type: String, allowEmptyValue: false })
  @ApiParam({ name: 'ecommId', type: Number, allowEmptyValue: false })
  @ApiOkResponse({ description: 'Ok' })
  @ApiNoContentResponse({ description: 'No Content' })
  @ApiInternalServerErrorResponse({ description: 'Internal Server Error' })
  async createEcommUser(@Body() userCreateDto: UserCreateDto, @Param('externalId') externalId: string, @Param('ecommId') ecommId: number) {
    const user = await this.usersService.createEcommUser(userCreateDto, externalId, ecommId);

    return user;
  }

  @Patch(':userId/ecomms/:ecommId')
  @ApiOperation({ summary: 'Update ecomm user' })
  @ApiParam({ name: 'userId', type: Number, allowEmptyValue: false })
  @ApiParam({ name: 'ecommId', type: Number, allowEmptyValue: false })
  @ApiOkResponse({ description: 'Ok' })
  @ApiNoContentResponse({ description: 'No Content' })
  @ApiInternalServerErrorResponse({ description: 'Internal Server Error' })
  async updateEcommUser(
    @Body() userEcommUpdateDto: UserEcommUpdateDto,
    @Param('userId') userId: number,
    @Param('ecommId') ecommId: number
  ) {
    const user = await this.usersService.updateEcommUser(userEcommUpdateDto, userId, ecommId);

    return user;
  }

  @Post(':userId/ecomms/:ecommId/assign')
  @ApiOperation({ summary: 'Assign user to ecomm' })
  @ApiParam({ name: 'userId', type: Number, allowEmptyValue: false })
  @ApiParam({ name: 'ecommId', type: Number, allowEmptyValue: false })
  @ApiOkResponse({ description: 'Ok' })
  @ApiNoContentResponse({ description: 'No Content' })
  @ApiInternalServerErrorResponse({ description: 'Internal Server Error' })
  async assignEcommUser(
    @Param('userId') userId: number,
    @Param('ecommId') ecommId: number
  ) {
    return await this.usersService.assignEcommUser(userId, ecommId);
  }

  @Get(':userId/veriffs')
  @ApiOperation({ summary: 'Get user veriff session' })
  @ApiParam({ name: 'userId', type: Number, allowEmptyValue: false })
  @ApiCreatedResponse({ description: 'Created' })
  @ApiInternalServerErrorResponse({ description: 'Internal Server Error' })
  async getUserVeriff(@Param('userId') userId: number) {
    const result = await this.usersService.getUserVeriffs(userId);

    return result;
  }

  @Post(':userId/veriffs')
  @ApiOperation({ summary: 'Create user veriff session' })
  @ApiParam({ name: 'userId', type: Number, allowEmptyValue: false })
  @ApiCreatedResponse({ description: 'Created' })
  @ApiInternalServerErrorResponse({ description: 'Internal Server Error' })
  async createUserVeriff(@Body() userVeriffCreateDto: UserVeriffCreateDto, @Param('userId') userId: number) {
    const user = await this.usersService.getUserById(userId);
    const result = await this.usersService.createUserVeriff(userVeriffCreateDto, user);

    return result;
  }

  @Get(':userId/veriffs/:veriffId')
  @ApiOperation({ summary: 'Get user veriff session detail' })
  @ApiParam({ name: 'userId', type: Number, allowEmptyValue: false })
  @ApiParam({ name: 'veriffId', type: String, allowEmptyValue: false })
  @ApiCreatedResponse({ description: 'Created' })
  @ApiInternalServerErrorResponse({ description: 'Internal Server Error' })
  async getUserVeriffSession(
    @Param('userId') userId: number,
    @Param('veriffId') veriffId: number
  ) {
    const result = await this.usersService.getUserVeriff(userId, veriffId);

    return result;
  }

  @Patch(':userId/veriffs/:veriffId')
  @ApiOperation({ summary: 'Patch user veriff session detail' })
  @ApiParam({ name: 'userId', type: Number, allowEmptyValue: false })
  @ApiParam({ name: 'veriffId', type: String, allowEmptyValue: false })
  @ApiCreatedResponse({ description: 'Created' })
  @ApiInternalServerErrorResponse({ description: 'Internal Server Error' })
  async patchUserVeriffSession(
    @Body() userVeriffUpdateDto: UserVeriffUpdateDto,
    @Param('userId') userId: number,
    @Param('veriffId') veriffId: number
  ) {
    return await this.usersService.patchUserVeriff(userVeriffUpdateDto, veriffId);
  }

  @Post(':userId/cards')
  @ApiOperation({ summary: 'Add user card, used by wp plugin' })
  @ApiParam({ name: 'userId', type: Number, allowEmptyValue: false })
  @ApiCreatedResponse({ description: 'Created' })
  @ApiInternalServerErrorResponse({ description: 'Internal Server Error' })
  async addCard(@Body() cardCreateDto: CardCreateDto, @Param('userId') userId: number) {
    const user = await this.usersService.getUserById(userId);
    const card = await this.cardsService.create(cardCreateDto, user);

    return card;
  }

  @Delete(':userId/cards/:cardId')
  @ApiOperation({ summary: 'Delete user card, used by wp plugin' })
  @ApiParam({ name: 'userId', type: Number, allowEmptyValue: false })
  @ApiParam({ name: 'cardId', type: Number, allowEmptyValue: false })
  @ApiCreatedResponse({ description: 'Created' })
  @ApiInternalServerErrorResponse({ description: 'Internal Server Error' })
  async deleteCard(@Param('userId') userId: number, @Param('cardId') cardId: number) {
    const user = await this.usersService.getUserById(userId);

    await this.cardsService.delete(cardId);

    return user;
  }
}
