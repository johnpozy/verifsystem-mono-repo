import { Module } from '@nestjs/common';

import { UsersModule as UsersFeatureModule, CardsModule as CardsFeatureModule } from '@verifsystem/api/features';

import { UsersController } from './users.controller';

@Module({
  imports: [UsersFeatureModule, CardsFeatureModule],
  controllers: [UsersController],
})
export class UsersModule {}
