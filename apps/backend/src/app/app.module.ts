import { APP_FILTER, APP_INTERCEPTOR } from '@nestjs/core';
import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { TypeOrmModule } from '@nestjs/typeorm';

import { AllExceptionsFilter, LoggingInterceptor, TimeoutInterceptor, TransformInterceptor } from '@verifsystem/api/utils';

import { ProcessorsModule } from './processors/processors.module';
import { EcommsModule } from './ecomms/ecomms.module';
import { UsersModule } from './users/users.module';
import { CardsModule } from './cards/cards.module';
import { GatewayConfigsModule } from './gateway-configs/gateway-configs.module';
import { TransactionsModule } from './transactions/transactions.module';
import { PayoutsModule } from './payouts/payouts.module';
import { WebhooksModule } from './webhooks/webhooks.module';
import { AuthModule } from './auth/auth.module';
import { ReportsModule } from './reports/reports.module';

@Module({
  imports: [
    ConfigModule.forRoot(),
    TypeOrmModule.forRoot({
      type: 'mysql',
      host: process.env.MYSQL_HOST || '0.0.0.0',
      port: +process.env.MYSQL_PORT || 3306,
      username: process.env.MYSQL_USER || 'user',
      password: process.env.MYSQL_PASS || 'password',
      database: process.env.MYSQL_DB || 'verifsystem',
      autoLoadEntities: true,
      synchronize: true,
    }),
    CardsModule,
    ProcessorsModule,
    EcommsModule,
    UsersModule,
    TransactionsModule,
    GatewayConfigsModule,
    PayoutsModule,
    WebhooksModule,
    AuthModule,
    ReportsModule,
  ],
  providers: [
    {
      provide: APP_FILTER,
      useClass: AllExceptionsFilter,
    },
    {
      provide: APP_INTERCEPTOR,
      useClass: LoggingInterceptor,
    },
    {
      provide: APP_INTERCEPTOR,
      useClass: TransformInterceptor,
    },
    {
      provide: APP_INTERCEPTOR,
      useClass: TimeoutInterceptor,
    },
  ],
})
export class AppModule {}
