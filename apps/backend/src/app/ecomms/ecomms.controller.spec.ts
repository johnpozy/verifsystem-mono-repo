import { Test, TestingModule } from '@nestjs/testing';
import { EcommsController } from './ecomms.controller';

describe('EcommsController', () => {
  let controller: EcommsController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [EcommsController],
    }).compile();

    controller = module.get<EcommsController>(EcommsController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
