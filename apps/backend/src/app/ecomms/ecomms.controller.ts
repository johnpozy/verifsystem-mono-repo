import { Body, Controller, DefaultValuePipe, Get, Param, ParseIntPipe, Patch, Post, Query, UseGuards } from '@nestjs/common';
import { ApiCreatedResponse, ApiInternalServerErrorResponse, ApiOkResponse, ApiOperation, ApiParam, ApiTags } from '@nestjs/swagger';

import { EProcessorType } from '@verifsystem/shared/data-access';
import {
  EcommsService,
  ProcessorCreateDto,
  EcommCreateDto,
  AuthJwtGuard,
  EcommConfigCreateDto,
  EcommConfigUpdateDto,
} from '@verifsystem/api/features';
import { SetRequestTimeout } from '@verifsystem/api/utils';

@ApiTags('Ecomms')
@Controller('ecomms')
export class EcommsController {
  constructor(private ecommsService: EcommsService) {}

  @UseGuards(AuthJwtGuard)
  @Get()
  @ApiOperation({ summary: 'List all ecomm' })
  async getAll() {
    const result = await this.ecommsService.getEcomms();

    return result;
  }

  @Get(':ecommId')
  @ApiOperation({ summary: 'Get ecomm by id' })
  @ApiParam({ name: 'ecommId', type: Number, allowEmptyValue: false })
  async getEcomm(@Param('ecommId') id: number) {
    const result = await this.ecommsService.getEcommById(id);

    return result;
  }

  @Get(':ecommId/processors/:processorType')
  @ApiOperation({ summary: 'Get available ecomm processor, used by wp plugin' })
  @ApiParam({ name: 'ecommId', type: Number, allowEmptyValue: false })
  @ApiParam({ name: 'processorType', type: String, enum: EProcessorType, allowEmptyValue: false })
  async pickEcommProcessor(@Param('ecommId') ecommId: number, @Param('processorType') processorType: EProcessorType) {
    const processor = await this.ecommsService.pickProcessor(ecommId, processorType);

    return processor;
  }

  @Post()
  @ApiOperation({ summary: 'Create ecomm' })
  @ApiCreatedResponse({ description: 'Created' })
  @ApiInternalServerErrorResponse({ description: 'Internal Server Error' })
  async create(@Body() ecommCreateDto: EcommCreateDto) {
    const result = await this.ecommsService.create(ecommCreateDto);

    return result;
  }

  @Get('verify/:token')
  @ApiOperation({ summary: 'Verify ecomm' })
  @ApiParam({ name: 'token', type: String, allowEmptyValue: false })
  @ApiOkResponse({ description: 'Ok' })
  @ApiInternalServerErrorResponse({ description: 'Internal Server Error' })
  async verify(@Param('token') token: string) {
    const ecomm = await this.ecommsService.verify(token);

    return ecomm;
  }

  @Post(':ecommId/processors')
  @ApiOperation({ summary: 'Create ecomm processor' })
  @ApiParam({ name: 'ecommId', type: Number, allowEmptyValue: false })
  async createProcessor(@Body() processorCreateDto: ProcessorCreateDto, @Param('ecommId') ecommId: number) {
    const processor = await this.ecommsService.createProcessor(processorCreateDto, ecommId);

    return processor;
  }

  @Get('all/processors')
  @ApiOperation({ summary: 'Get ecomm processors' })
  @ApiOkResponse({ description: 'Ok' })
  getProcessors() {
    return this.ecommsService.getProcessors();
  }

  @Get(':ecommId/processors')
  @ApiOperation({ summary: 'Get specific ecomm processors' })
  @ApiOkResponse({ description: 'Ok' })
  @ApiParam({ name: 'ecommId', type: Number, allowEmptyValue: false })
  async getEcommProcessors(@Param('ecommId') ecommId: number) {
    const processor = await this.ecommsService.getEcommProcessors(ecommId);

    return processor;
  }

  @Get(':ecommId/customers')
  @ApiOperation({ summary: 'Get ecomm customer' })
  @ApiOkResponse({ description: 'Ok' })
  @ApiParam({ name: 'ecommId', type: Number, allowEmptyValue: false })
  async getCustomers(@Param('ecommId') ecommId: number) {
    return this.ecommsService.getCustomers(ecommId);
  }

  @Get(':ecommId/transactions')
  @ApiOperation({ summary: 'Get ecomm transactions' })
  @ApiOkResponse({ description: 'Ok' })
  @ApiParam({ name: 'ecommId', type: Number, allowEmptyValue: false })
  async getTransactions(@Param('ecommId') ecommId: number) {
    const transactions = await this.ecommsService.getTransactions(ecommId);

    return transactions;
  }

  @Get(':ecommId/configs')
  @ApiOperation({ summary: 'Get ecomm configs' })
  @ApiParam({ name: 'ecommId', type: Number, allowEmptyValue: false })
  async getConfigs(@Param('ecommId') ecommId: number) {
    return this.ecommsService.getConfigs(ecommId);
  }

  @Post(':ecommId/configs')
  @ApiOperation({ summary: 'Create ecomm config' })
  @ApiParam({ name: 'ecommId', type: Number, allowEmptyValue: false })
  async createConfig(@Body() ecommConfigCreateDto: EcommConfigCreateDto, @Param('ecommId') ecommId: number) {
    return this.ecommsService.createConfig(ecommConfigCreateDto, ecommId);
  }

  @Patch(':ecommId/configs/:configId')
  @ApiOperation({ summary: 'Update ecomm config' })
  @ApiParam({ name: 'ecommId', type: Number, allowEmptyValue: false })
  async updateConfig(
    @Body() ecommConfigUpdateDto: EcommConfigUpdateDto,
    @Param('ecommId') ecommId: number,
    @Param('configId') configId: number
  ) {
    return this.ecommsService.updateConfig(ecommConfigUpdateDto, ecommId, configId);
  }

  @Get(':ecommId/users/import')
  @ApiOperation({ summary: 'Get ecomm external users' })
  @ApiParam({ name: 'ecommId', type: Number, allowEmptyValue: false })
  async getExternalEcommUsers(@Param('ecommId') ecommId: number) {
    return this.ecommsService.getExternalEcommUsers(ecommId);
  }

  @SetRequestTimeout(60 * 60000)
  @Post(':ecommId/users/import')
  @ApiOperation({ summary: 'Get ecomm external users' })
  @ApiParam({ name: 'ecommId', type: Number, allowEmptyValue: false })
  async saveExternalEcommUsers(@Body() users: Array<any>, @Param('ecommId') ecommId: number) {
    return this.ecommsService.saveExternalEcommUsers(ecommId, users);
  }
}
