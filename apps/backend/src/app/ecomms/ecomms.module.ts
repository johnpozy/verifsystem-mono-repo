import { Module } from '@nestjs/common';

import { EcommsModule as EcommsFeatureModule } from '@verifsystem/api/features';

import { EcommsController } from './ecomms.controller';

@Module({
  imports: [EcommsFeatureModule],
  controllers: [EcommsController],
})
export class EcommsModule {}
