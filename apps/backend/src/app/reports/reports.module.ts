import { Module } from '@nestjs/common';

import {ReportsModule as ReportsFeatureModule } from '@verifsystem/api/features';

import { ReportsController } from './reports.controller';

@Module({
  imports: [ReportsFeatureModule],
  controllers: [ReportsController],
})
export class ReportsModule {}
