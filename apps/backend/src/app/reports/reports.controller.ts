import { Controller, Get, Query } from '@nestjs/common';
import { ApiOkResponse, ApiOperation, ApiQuery, ApiTags } from '@nestjs/swagger';

import { ReportsService } from '@verifsystem/api/features';

@ApiTags('Reports')
@Controller('reports')
export class ReportsController {
  constructor(private reportsService: ReportsService) {}

  @Get('orders')
  @ApiOperation({ summary: 'Count transaction based on status' })
  @ApiQuery({ name: 'startDate', required: false, type: String, description: 'Date format is "YYYY-MM-DD"' })
  @ApiQuery({ name: 'endDate', required: false, type: String, description: 'Date format is "YYYY-MM-DD"' })
  @ApiOkResponse({ description: 'Ok' })
  countOrders(@Query('startDate') startDate: Date, @Query('endDate') endDate: Date) {
    return this.reportsService.countOrders({ startDate, endDate });
  }

  @Get('sales')
  @ApiOperation({ summary: 'Get all sales count' })
  @ApiQuery({ name: 'startDate', required: false, type: String, description: 'Date format is "YYYY-MM-DD"' })
  @ApiQuery({ name: 'endDate', required: false, type: String, description: 'Date format is "YYYY-MM-DD"' })
  @ApiOkResponse({ description: 'Ok' })
  calculateSales(@Query('startDate') startDate: Date, @Query('endDate') endDate: Date) {
    return this.reportsService.calculateSales({ startDate, endDate });
  }

  @Get('export')
  @ApiOperation({ summary: 'Export data to csv' })
  @ApiQuery({ name: 'startDate', required: false, type: String, description: 'Date format is "YYYY-MM-DD"' })
  @ApiQuery({ name: 'endDate', required: false, type: String, description: 'Date format is "YYYY-MM-DD"' })
  @ApiOkResponse({ description: 'Ok' })
  export(@Query('startDate') startDate: Date, @Query('endDate') endDate: Date) {
    return this.reportsService.export({ startDate, endDate });
  }

  @Get('chart')
  @ApiOperation({ summary: 'Generate chart data' })
  @ApiQuery({ name: 'startDate', required: false, type: String, description: 'Date format is "YYYY-MM-DD"' })
  @ApiQuery({ name: 'endDate', required: false, type: String, description: 'Date format is "YYYY-MM-DD"' })
  @ApiOkResponse({ description: 'Ok' })
  chart(@Query('startDate') startDate: Date, @Query('endDate') endDate: Date) {
    return this.reportsService.chart({ startDate, endDate });
  }
}
