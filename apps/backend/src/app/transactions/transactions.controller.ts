import { Body, Controller, DefaultValuePipe, Get, Param, ParseIntPipe, Patch, Query } from '@nestjs/common';
import { ApiOkResponse, ApiOperation, ApiParam, ApiTags } from '@nestjs/swagger';

import { TransactionsService, TransactionUpdateDto } from '@verifsystem/api/features';

@ApiTags('Transactions')
@Controller('transactions')
export class TransactionsController {
  constructor(private transactionsService: TransactionsService) {}

  @Get()
  @ApiOperation({ summary: 'List all transaction' })
  @ApiOkResponse({ description: 'Ok' })
  getAll() {
    return this.transactionsService.getAll();
  }

  @Patch(':transactionId')
  @ApiOperation({ summary: 'Update transaction' })
  @ApiParam({ name: 'transactionId', type: Number, allowEmptyValue: false })
  @ApiOkResponse({ description: 'Ok' })
  update(@Param('transactionId') transactionId: number, @Body() transactionUpdateDto: TransactionUpdateDto) {
    return this.transactionsService.update(transactionId, transactionUpdateDto);
  }

  @Get('users/:userId')
  @ApiOperation({ summary: 'List all transaction by user' })
  @ApiParam({ name: 'userId', type: Number, allowEmptyValue: false })
  @ApiOkResponse({ description: 'Ok' })
  getByUser(@Param('userId') userId: number) {
    return this.transactionsService.getByUser(userId);
  }

  @Get('ecomms/all')
  @ApiOperation({ summary: 'List all transaction by all ecomm' })
  @ApiOkResponse({ description: 'Ok' })
  getByEcomms(
    @Query('page', new DefaultValuePipe(1), ParseIntPipe) page: number,
    @Query('limit', new DefaultValuePipe(10), ParseIntPipe) limit: number,
    @Query('filter') filter: { [column: string]: string | string[]; },
    @Query('sortBy') sortBy?: [string, string][],
  ) {
    return this.transactionsService.getByEcomm(null, {
      limit,
      page,
      sortBy,
      filter,
      path: ''
    });
  }

  @Get('ecomms/:ecommId')
  @ApiOperation({ summary: 'List all transaction by ecomm' })
  @ApiParam({ name: 'ecommId', type: Number, allowEmptyValue: false })
  @ApiOkResponse({ description: 'Ok' })
  getByEcomm(
    @Param('ecommId') ecommId: number,
    @Query('page', new DefaultValuePipe(1), ParseIntPipe) page: number,
    @Query('limit', new DefaultValuePipe(10), ParseIntPipe) limit: number,
    @Query('filter') filter: { [column: string]: string | string[]; },
    @Query('sortBy') sortBy?: [string, string][]
  ) {
    return this.transactionsService.getByEcomm(ecommId, {
      limit,
      page,
      sortBy,
      filter,
      path: ''
    });
  }
}
