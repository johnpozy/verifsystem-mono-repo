import { Module } from '@nestjs/common';

import { TransactionsModule as TransactionsFeatureModule } from '@verifsystem/api/features';

import { TransactionsController } from './transactions.controller';

@Module({
  imports: [TransactionsFeatureModule],
  controllers: [TransactionsController],
})
export class TransactionsModule {}
