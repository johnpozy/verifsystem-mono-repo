import { Body, Controller, DefaultValuePipe, Delete, Get, Param, ParseIntPipe, Patch, Post, Query } from '@nestjs/common';
import { ApiCreatedResponse, ApiInternalServerErrorResponse, ApiOkResponse, ApiOperation, ApiParam, ApiTags } from '@nestjs/swagger';

import { ProcessorsService, UsersService, ProcessorCreateDto, ProcessorUpdateDto, GatewayConfigCreateDto } from '@verifsystem/api/features';
import { EProcessorType } from '@verifsystem/shared/data-access';

@ApiTags('Processors')
@Controller('processors')
export class ProcessorsController {
  constructor(private processorService: ProcessorsService, private userService: UsersService) {}

  @Get()
  @ApiOperation({ summary: 'List all processor' })
  @ApiOkResponse({ description: 'Ok' })
  getAll() {
    return this.processorService.findAll();
  }

  @Get(':processorType')
  @ApiOperation({ summary: 'List all processor by its type' })
  @ApiParam({ name: 'processorType', enum: EProcessorType, required: true, allowEmptyValue: false })
  @ApiOkResponse({ description: 'Ok' })
  @ApiInternalServerErrorResponse({ description: 'Internal Server Error' })
  getByType(@Param('processorType') processorType: EProcessorType) {
    return this.processorService.findByType(processorType);
  }

  @Post()
  @ApiOperation({ summary: 'Create processor' })
  @ApiCreatedResponse({ description: 'Ok' })
  @ApiInternalServerErrorResponse({ description: 'Internal Server Error' })
  create(@Body() processorCreateDto: ProcessorCreateDto) {
    return this.processorService.create(processorCreateDto);
  }

  @Patch(':processorId')
  @ApiOperation({ summary: 'Update processor' })
  @ApiParam({ name: 'processorId', type: Number, allowEmptyValue: false })
  @ApiCreatedResponse({ description: 'Ok' })
  @ApiInternalServerErrorResponse({ description: 'Internal Server Error' })
  update(@Body() processorUpdateDto: ProcessorUpdateDto, @Param('processorId') processorId: number) {
    return this.processorService.update(processorUpdateDto, processorId);
  }

  @Delete(':processorId')
  @ApiOperation({ summary: 'Delete processor' })
  @ApiParam({ name: 'processorId', type: Number, allowEmptyValue: false })
  @ApiCreatedResponse({ description: 'Ok' })
  @ApiInternalServerErrorResponse({ description: 'Internal Server Error' })
  delete(@Param('processorId') processorId: number) {
    return this.processorService.delete(processorId);
  }

  @Get(':processorId/gateway-configs')
  @ApiOperation({ summary: 'Get processor gateway configs' })
  @ApiParam({ name: 'processorId', type: Number, allowEmptyValue: false })
  @ApiOkResponse({ description: 'Ok' })
  @ApiInternalServerErrorResponse({ description: 'Internal Server Error' })
  getGatewayConfig(@Param('processorId') processorId: number) {
    return this.processorService.getGatewayConfigs(processorId);
  }

  @Post(':processorId/gateway-configs')
  @ApiOperation({ summary: 'Create processor gateway configs' })
  @ApiParam({ name: 'processorId', type: Number, allowEmptyValue: false })
  @ApiCreatedResponse({ description: 'Ok' })
  @ApiInternalServerErrorResponse({ description: 'Internal Server Error' })
  createGatewayConfig(@Body() gatewayConfigCreateDto: GatewayConfigCreateDto, @Param('processorId') processorId: number) {
    return this.processorService.createGatewayConfig(gatewayConfigCreateDto, processorId);
  }

  @Get('ecomms/all')
  @ApiOperation({ summary: 'Get all ecomms processors' })
  @ApiOkResponse({ description: 'Ok' })
  @ApiInternalServerErrorResponse({ description: 'Internal Server Error' })
  getEcommsProcessors(
    @Query('page', new DefaultValuePipe(1), ParseIntPipe) page: number,
    @Query('limit', new DefaultValuePipe(10), ParseIntPipe) limit: number,
    @Query('sortBy') sortBy: [string, string][],
  ) {
    return this.processorService.getEcommsProcessors({
      limit,
      page,
      sortBy,
      path: ''
    });
  }

  @Get('ecomms/:ecommId')
  @ApiOperation({ summary: 'Get all ecomms processors' })
  @ApiParam({ name: 'ecommId', type: Number, allowEmptyValue: false })
  @ApiOkResponse({ description: 'Ok' })
  @ApiInternalServerErrorResponse({ description: 'Internal Server Error' })
  getEcommProcessors(
    @Query('page', new DefaultValuePipe(1), ParseIntPipe) page: number,
    @Query('limit', new DefaultValuePipe(10), ParseIntPipe) limit: number,
    @Query('sortBy') sortBy: [string, string][],
    @Param('ecommId') ecommId: number
  ) {
    return this.processorService.getEcommProcessors(ecommId, {
      limit,
      page,
      sortBy,
      path: ''
    });
  }
}
