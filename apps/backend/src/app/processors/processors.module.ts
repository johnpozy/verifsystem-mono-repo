import { Module } from '@nestjs/common';

import { ProcessorsModule as ProcessorsFeatureModule, UsersModule as UsersFeatureModule } from '@verifsystem/api/features';

import { ProcessorsController } from './processors.controller';

@Module({
  imports: [ProcessorsFeatureModule, UsersFeatureModule],
  controllers: [ProcessorsController],
})
export class ProcessorsModule {}
