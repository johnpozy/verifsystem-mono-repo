import { Test, TestingModule } from '@nestjs/testing';
import { GatewayConfigsController } from './gateway-configs.controller';

describe('GatewayConfigsController', () => {
  let controller: GatewayConfigsController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [GatewayConfigsController],
    }).compile();

    controller = module.get<GatewayConfigsController>(GatewayConfigsController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
