import { Body, Controller, Delete, Param, Patch, Post } from '@nestjs/common';
import { ApiCreatedResponse, ApiInternalServerErrorResponse, ApiOkResponse, ApiOperation, ApiParam, ApiTags } from '@nestjs/swagger';

import {
  GatewayConfigsService,
  GatewayConfigUpdateDto,
  GatewayConfigVeriffDecisionDto,
  GatewayConfigVeriffEventDto,
} from '@verifsystem/api/features';

@ApiTags('Gateway Configs')
@Controller('gateway-configs')
export class GatewayConfigsController {
  constructor(private gatewayConfigsService: GatewayConfigsService) {}

  @Post('veriff/hooks/events')
  @ApiOperation({ summary: 'Gateway config veriff event hooks' })
  @ApiCreatedResponse({ description: 'Ok' })
  @ApiInternalServerErrorResponse({ description: 'Internal Server Error' })
  eventsHook(@Body() gatewayConfigVeriffEventDto: GatewayConfigVeriffEventDto) {
    return this.gatewayConfigsService.veriffEvent(gatewayConfigVeriffEventDto);
  }

  @Post('veriff/hooks/decisions')
  @ApiOperation({ summary: 'Gateway config veriff decision hooks' })
  @ApiCreatedResponse({ description: 'Ok' })
  @ApiInternalServerErrorResponse({ description: 'Internal Server Error' })
  decisionsHook(@Body() gatewayConfigVeriffDecisionDto: GatewayConfigVeriffDecisionDto) {
    return this.gatewayConfigsService.veriffDecision(gatewayConfigVeriffDecisionDto);
  }

  @Patch(':gatewayConfigId')
  @ApiOperation({ summary: 'Update gateway configs' })
  @ApiParam({ name: 'gatewayConfigId', type: Number, allowEmptyValue: false })
  @ApiOkResponse({ description: 'Ok' })
  @ApiInternalServerErrorResponse({ description: 'Internal Server Error' })
  update(@Body() gatewayConfigUpdateDto: GatewayConfigUpdateDto, @Param('gatewayConfigId') gatewayConfigId: number) {
    return this.gatewayConfigsService.update(gatewayConfigUpdateDto, gatewayConfigId);
  }

  @Delete(':gatewayConfigId')
  @ApiOperation({ summary: 'Delete gateway configs' })
  @ApiParam({ name: 'gatewayConfigId', type: Number, allowEmptyValue: false })
  @ApiOkResponse({ description: 'Ok' })
  @ApiInternalServerErrorResponse({ description: 'Internal Server Error' })
  delete(@Param('gatewayConfigId') gatewayConfigId: number) {
    return this.gatewayConfigsService.delete(gatewayConfigId);
  }
}
