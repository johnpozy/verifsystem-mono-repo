import { Body, Controller, Delete, Get, Param, Post } from '@nestjs/common';
import { ApiCreatedResponse, ApiInternalServerErrorResponse, ApiOkResponse, ApiOperation, ApiParam, ApiTags } from '@nestjs/swagger';

import { CardsService, CardCreateDto } from '@verifsystem/api/features';

@ApiTags('Cards')
@Controller('cards')
export class CardsController {
  constructor(private cardsService: CardsService) {}

  @Get()
  @ApiOperation({ summary: 'List all card' })
  @ApiOkResponse({ description: 'Ok' })
  @ApiInternalServerErrorResponse({ description: 'Internal Server Error' })
  async getAll() {
    const result = await this.cardsService.findAll();

    return result;
  }

  @Get(':cardId')
  @ApiOperation({ summary: 'Get card by id' })
  @ApiParam({ name: 'cardId', type: Number, allowEmptyValue: false })
  @ApiOkResponse({ description: 'Ok' })
  @ApiInternalServerErrorResponse({ description: 'Internal Server Error' })
  async getById(@Param('cardId') cardId: number) {
    const result = await this.cardsService.findById(cardId);

    return result;
  }

  @Post()
  @ApiOperation({ summary: 'Create card' })
  @ApiCreatedResponse({ description: 'Created' })
  @ApiInternalServerErrorResponse({ description: 'Internal Server Error' })
  async create(@Body() cardCreateDto: CardCreateDto) {
    const result = await this.cardsService.create(cardCreateDto);

    return result;
  }

  @Delete(':cardId')
  @ApiOperation({ summary: 'Delete card' })
  @ApiParam({ name: 'cardId', type: Number, allowEmptyValue: false })
  @ApiOkResponse({ description: 'Ok' })
  @ApiInternalServerErrorResponse({ description: 'Internal Server Error' })
  async delete(@Param('cardId') cardId: number) {
    const result = await this.cardsService.delete(cardId);

    return result;
  }
}
