import { Module } from '@nestjs/common';

import { CardsModule as CardsFeatureModule } from '@verifsystem/api/features';

import { CardsController } from './cards.controller';

@Module({
  imports: [CardsFeatureModule],
  controllers: [CardsController],
})
export class CardsModule {}
