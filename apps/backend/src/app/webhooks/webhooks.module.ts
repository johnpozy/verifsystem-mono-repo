import { Module } from '@nestjs/common';
import { WebhooksController } from './webhooks.controller';

import { WebhooksModule as WebhooksFeatureModule} from '@verifsystem/api/features';

@Module({
  imports: [WebhooksFeatureModule],
  controllers: [WebhooksController],
})
export class WebhooksModule {}
