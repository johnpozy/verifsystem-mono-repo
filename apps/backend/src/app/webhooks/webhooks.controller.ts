import { Body, Controller, Post, Put } from '@nestjs/common';
import { ApiCreatedResponse, ApiOperation, ApiTags } from '@nestjs/swagger';

import { WebhooksService, WebhookWoocommerceCustomerDto, WebhookWoocommerceOrderUpdateDto } from '@verifsystem/api/features';

@ApiTags('Webhooks')
@Controller('webhooks')
export class WebhooksController {
  constructor(private webhooksService: WebhooksService) {}

  @Post('woocommerce/customer')
  @ApiOperation({ summary: 'Woocommerce customer update' })
  @ApiCreatedResponse({ description: 'The record has been successfully created.' })
  async woocommerceCustomer(@Body() webhookWoocommerceCustomerDto: WebhookWoocommerceCustomerDto) {
    return this.webhooksService.woocommerceCustomer(webhookWoocommerceCustomerDto);
  }

  @Post('woocommerce/order')
  @ApiOperation({ summary: 'Woocommerce order create' })
  @ApiCreatedResponse({ description: 'The record has been successfully created.' })
  async woocommerceOrder(@Body() webhookWoocommerceOrderUpdateDto: WebhookWoocommerceOrderUpdateDto) {
    return this.webhooksService.woocommerceUpdateOrder(webhookWoocommerceOrderUpdateDto);
  }

  @Put('woocommerce/order')
  @ApiOperation({ summary: 'Woocommerce order update' })
  @ApiCreatedResponse({ description: 'The record has been successfully updated.' })
  async woocommerceOrderUpdat(@Body() webhookWoocommerceOrderUpdateDto: WebhookWoocommerceOrderUpdateDto) {
    return this.webhooksService.woocommerceUpdateOrder(webhookWoocommerceOrderUpdateDto);
  }
}
