import { APP_FILTER, APP_INTERCEPTOR } from '@nestjs/core';
import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { TypeOrmModule } from '@nestjs/typeorm';

import { AllExceptionsFilter, LoggingInterceptor, TransformInterceptor } from '@verifsystem/api/utils';

import { GatewayConfigsModule } from './gateway-configs/gateway-configs.module';

@Module({
  imports: [
    ConfigModule.forRoot(),
    TypeOrmModule.forRoot({
      type: 'mysql',
      host: process.env.MYSQL_HOST || '0.0.0.0',
      port: +process.env.MYSQL_PORT || 3306,
      username: process.env.MYSQL_USER || 'user',
      password: process.env.MYSQL_PASS || 'password',
      database: process.env.MYSQL_DB || 'verifsystem',
      autoLoadEntities: true,
      synchronize: true
    }),
    GatewayConfigsModule
  ],
  providers: [
    {
      provide: APP_FILTER,
      useClass: AllExceptionsFilter,
    },
    {
      provide: APP_INTERCEPTOR,
      useClass: LoggingInterceptor,
    },
    {
      provide: APP_INTERCEPTOR,
      useClass: TransformInterceptor,
    },
  ],
})
export class AppModule {}
