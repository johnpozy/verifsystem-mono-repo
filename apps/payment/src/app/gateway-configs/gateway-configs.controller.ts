import { Body, Controller, HttpException, HttpStatus, Post } from '@nestjs/common';
import { ApiCreatedResponse, ApiInternalServerErrorResponse, ApiOperation, ApiTags } from '@nestjs/swagger';

import { CardChargeDto, GatewayConfigsService } from '@verifsystem/api/features';
import { EGatewayConfigType } from '@verifsystem/shared/data-access';

@ApiTags('Gateway Configs')
@Controller('gateway-configs')
export class GatewayConfigsController {
  constructor(private gatewayConfigsService: GatewayConfigsService) {}

  @Post('charge')
  @ApiOperation({ summary: 'Gateway config payment charge' })
  @ApiCreatedResponse({ description: 'Created' })
  @ApiInternalServerErrorResponse({ description: 'Internal Server Error' })
  async chargeCard(@Body() cardChargeDto: CardChargeDto) {
    if (cardChargeDto.gateway.type === EGatewayConfigType.STRIPE) {
      return await this.gatewayConfigsService.stripeCharge(cardChargeDto);
    }

    throw new HttpException('Internal Server Error', HttpStatus.INTERNAL_SERVER_ERROR);
  }
}
