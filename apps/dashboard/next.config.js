// eslint-disable-next-line @typescript-eslint/no-var-requires
const withNx = require('@nrwl/next/plugins/with-nx');

/**
 * @type {import('@nrwl/next/plugins/with-nx').WithNxOptions}
 **/
const nextConfig = {
  // assetPrefix: process.env.NODE_ENV === 'production' ? '/verifsystem-mono-repo' : '',
  // basePath: process.env.NODE_ENV === 'production' ? '/verifsystem-mono-repo' : '',
  nx: {
    // Set this to true if you would like to to use SVGR
    // See: https://github.com/gregberge/svgr
    svgr: false,
  },
  images: {
    loader: 'custom'
  }
};

module.exports = withNx(nextConfig);
