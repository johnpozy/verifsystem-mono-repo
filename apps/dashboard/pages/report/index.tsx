import Error from 'next/error';
import dynamic from 'next/dynamic';

import { Layout } from '@verifsystem/dashboard/shared';
import { stateLoggedUser, useAppSelector } from '@verifsystem/dashboard/data-access';
import { accessControl } from '@verifsystem/shared/utils';

const ReportView = dynamic(() => import('@verifsystem/dashboard/features/reports').then((m) => m.Report));

// eslint-disable-next-line @typescript-eslint/no-empty-interface
interface ReportProps {}

/**
 * Report
 *
 * @param {ReportProps} props
 * @returns JSX.Element
 */
export const Report = (props: ReportProps) => {
  const loggedUser = useAppSelector(stateLoggedUser);

  if (!loggedUser || !accessControl.can(loggedUser?.role).readAny('reports').granted) {
    return <Error statusCode={403} title='You are not authorized to view this page' />;
  }

  return (
    <Layout drawerWidth={280} htmlTitle="Reports">
      <ReportView />
    </Layout>
  );
};

export default Report;
