import { NextPage } from 'next';
import dynamic from 'next/dynamic';

import { Layout } from '@verifsystem/dashboard/shared';

const TransactionView = dynamic(() => import('@verifsystem/dashboard/features/customers').then((m) => m.Transaction));

// eslint-disable-next-line @typescript-eslint/no-empty-interface
interface TransactionProps {}

/**
 * Transaction
 *
 * @param {TransactionProps} props
 * @returns JSX.Element
 */
export const Transaction: NextPage = (props: TransactionProps) => {
  return (
    <Layout drawerWidth={280} htmlTitle="Transactions History">
      <TransactionView {...props} />
    </Layout>
  );
};

export default Transaction;
