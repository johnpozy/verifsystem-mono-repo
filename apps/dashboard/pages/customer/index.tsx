import Error from 'next/error';
import dynamic from 'next/dynamic';

import { accessControl } from '@verifsystem/shared/utils';
import { Layout } from '@verifsystem/dashboard/shared';
import { stateLoggedUser, useAppSelector } from '@verifsystem/dashboard/data-access';

const CustomerView = dynamic(() => import('@verifsystem/dashboard/features/customers').then((m) => m.Customer));

// eslint-disable-next-line @typescript-eslint/no-empty-interface
interface CustomerProps {}

/**
 * Customer
 *
 * @param {CustomerProps} props
 * @returns JSX.Element
 */
export const Customer = (props: CustomerProps) => {
  const loggedUser = useAppSelector(stateLoggedUser);

  if (!loggedUser || !accessControl.can(loggedUser?.role).readAny('customers').granted) {
    return <Error statusCode={403} title='You are not authorized to view this page' />;
  }

  return (
    <Layout drawerWidth={280} htmlTitle="Customers">
      <CustomerView />
    </Layout>
  );
};

export default Customer;
