import dynamic from 'next/dynamic';
import Error from 'next/error';

import { Layout } from '@verifsystem/dashboard/shared';
import { stateLoggedUser, useAppSelector } from '@verifsystem/dashboard/data-access';
import { accessControl } from '@verifsystem/shared/utils';

const TransactionView = dynamic(() => import('@verifsystem/dashboard/features/orders').then((m) => m.Order));

// eslint-disable-next-line @typescript-eslint/no-empty-interface
interface OrdersProps {}

/**
 * Transaction
 *
 * @param {OrdersProps} props
 * @returns JSX.Element
 */
export const Transaction = (props: OrdersProps) => {
  const loggedUser = useAppSelector(stateLoggedUser);

  if (!loggedUser || !accessControl.can(loggedUser?.role).readAny('transactions').granted) {
    return <Error statusCode={403} title='You are not authorized to view this page' />;
  }

  return (
    <Layout drawerWidth={280} htmlTitle="Transactions">
      <TransactionView />
    </Layout>
  );
};

export default Transaction;
