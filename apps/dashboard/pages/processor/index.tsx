import { NextPage } from 'next';
import dynamic from 'next/dynamic';
import Error from 'next/error';

import { Layout } from '@verifsystem/dashboard/shared';
import { stateLoggedUser, useAppSelector } from '@verifsystem/dashboard/data-access';
import { accessControl } from '@verifsystem/shared/utils';

const ProcessorView = dynamic(() => import('@verifsystem/dashboard/features/processors').then((m) => m.Processor));

// eslint-disable-next-line @typescript-eslint/no-empty-interface
interface ProcessorProps {}

/**
 * Processor
 *
 * @param {ProcessorProps} props
 * @returns JSX.Element
 */
export const Processor: NextPage = (props: ProcessorProps) => {
  const loggedUser = useAppSelector(stateLoggedUser);

  if (!loggedUser || !accessControl.can(loggedUser?.role).readAny('processors').granted) {
    return <Error statusCode={403} title='You are not authorized to view this page' />;
  }

  return (
    <Layout drawerWidth={280} htmlTitle="Processors">
      <ProcessorView {...props} />
    </Layout>
  );
};

export default Processor;
