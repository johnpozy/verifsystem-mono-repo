import dynamic from 'next/dynamic';
import { Layout } from '@verifsystem/dashboard/shared';
import { NextPage } from 'next';

const GatewayConfigPage = dynamic(() => import('@verifsystem/dashboard/features/gateway-config').then((m) => m.GatewayConfig));

// eslint-disable-next-line @typescript-eslint/no-empty-interface
interface GatewayConfigProps {}

/**
 * GatewayConfig
 *
 * @param {GatewayConfigProps} props
 * @returns JSX.Element
 */
export const GatewayConfig: NextPage = (props: GatewayConfigProps) => {
  return (
    <Layout drawerWidth={280} htmlTitle="Gateway Config">
      <GatewayConfigPage {...props} />
    </Layout>
  );
};

export default GatewayConfig;
