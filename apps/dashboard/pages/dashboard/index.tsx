import Error from 'next/error';
import dynamic from 'next/dynamic';

import { accessControl } from '@verifsystem/shared/utils';
import { Layout } from '@verifsystem/dashboard/shared';
import { stateLoggedUser, useAppSelector } from '@verifsystem/dashboard/data-access';

const DashboardView = dynamic(() => import('@verifsystem/dashboard/features/dashboard').then((m) => m.DashboardView));

// eslint-disable-next-line @typescript-eslint/no-empty-interface
interface DashboardProps {}

/**
 * Dashboard
 *
 * @param {DashboardProps} props
 * @returns JSX.Element
 */
export const Dashboard = (props: DashboardProps) => {
  const loggedUser = useAppSelector(stateLoggedUser);

  if (!loggedUser || !accessControl.can(loggedUser?.role).readAny('dashboard').granted) {
    return <Error statusCode={403} title='You are not authorized to view this page' />;
  }

  return (
    <Layout drawerWidth={280} htmlTitle="Home">
      <DashboardView />
    </Layout>
  );
};

export default Dashboard;
