import dynamic from 'next/dynamic';

import { Layout } from '@verifsystem/dashboard/shared';

const DashboardOverviewView = dynamic(() => import('@verifsystem/dashboard/features/dashboard').then((m) => m.DashboardOverviewView));

// eslint-disable-next-line @typescript-eslint/no-empty-interface
interface DashboardOverviewProps {}

/**
 * Dashboard
 *
 * @param {DashboardOverviewProps} props
 * @returns JSX.Element
 */
export const DashboardOverview = (props: DashboardOverviewProps) => {
  return (
    <Layout drawerWidth={280} htmlTitle="Orders & Chargeback">
      <DashboardOverviewView />
    </Layout>
  );
};

export default DashboardOverview;
