import Error from 'next/error';
import dynamic from 'next/dynamic';

import { Layout } from '@verifsystem/dashboard/shared';
import { stateLoggedUser, useAppSelector } from '@verifsystem/dashboard/data-access';
import { accessControl } from '@verifsystem/shared/utils';

const DisputeView = dynamic(() => import('@verifsystem/dashboard/features/disputes').then((m) => m.Dispute));

// eslint-disable-next-line @typescript-eslint/no-empty-interface
interface DisputeProps {}

/**
 * Dispute
 *
 * @param {DisputeProps} props
 * @returns JSX.Element
 */
export const Dispute = (props: DisputeProps) => {
  const loggedUser = useAppSelector(stateLoggedUser);

  if (!loggedUser || !accessControl.can(loggedUser?.role).readAny('disputes').granted) {
    return <Error statusCode={403} title='You are not authorized to view this page' />;
  }

  return (
    <Layout drawerWidth={280} htmlTitle="Disputes">
      <DisputeView />
    </Layout>
  );
};

export default Dispute;
