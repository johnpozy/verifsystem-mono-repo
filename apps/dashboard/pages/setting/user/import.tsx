import dynamic from 'next/dynamic';
import Error from 'next/error';

import { Layout } from '@verifsystem/dashboard/shared';
import { NextPage } from 'next';
import { stateLoggedUser, useAppSelector } from '@verifsystem/dashboard/data-access';
import { accessControl } from '@verifsystem/shared/utils';

const UserImportView = dynamic(() => import('@verifsystem/dashboard/features/setting/user').then((m) => m.UserImport));

// eslint-disable-next-line @typescript-eslint/no-empty-interface
interface UserImportProps {}

/**
 * User Import
 *
 * @param {UserImportProps} props
 * @returns JSX.Element
 */
export const UserImport: NextPage = (props: UserImportProps) => {
  const loggedUser = useAppSelector(stateLoggedUser);

  if (!loggedUser || !accessControl.can(loggedUser?.role).readAny('settings').granted) {
    return <Error statusCode={403} title='You are not authorized to view this page' />;
  }

  return (
    <Layout drawerWidth={280} htmlTitle="Import User">
      <UserImportView {...props} />
    </Layout>
  );
};

export default UserImport;
