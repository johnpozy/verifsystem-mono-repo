import dynamic from 'next/dynamic';
import Error from 'next/error';

import { Layout } from '@verifsystem/dashboard/shared';
import { NextPage } from 'next';
import { stateLoggedUser, useAppSelector } from '@verifsystem/dashboard/data-access';
import { accessControl } from '@verifsystem/shared/utils';

const PayoutView = dynamic(() => import('@verifsystem/dashboard/features/payouts').then((m) => m.Payout));

// eslint-disable-next-line @typescript-eslint/no-empty-interface
interface PayoutProps {}

/**
 * Payout
 *
 * @param {PayoutProps} props
 * @returns JSX.Element
 */
export const Payout: NextPage = (props: PayoutProps) => {
  const loggedUser = useAppSelector(stateLoggedUser);

  if (!loggedUser || !accessControl.can(loggedUser?.role).readAny('payouts').granted) {
    return <Error statusCode={403} title='You are not authorized to view this page' />;
  }

  return (
    <Layout drawerWidth={280} htmlTitle="Payouts">
      <PayoutView {...props} />
    </Layout>
  );
};

export default Payout;
