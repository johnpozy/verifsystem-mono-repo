import { NextPage } from 'next';
import dynamic from 'next/dynamic';

import { Layout } from '@verifsystem/dashboard/shared';

const HistoryView = dynamic(() => import('@verifsystem/dashboard/features/payouts').then((m) => m.History));

// eslint-disable-next-line @typescript-eslint/no-empty-interface
interface HistoryProps {}

/**
 * History
 *
 * @param {HistoryProps} props
 * @returns JSX.Element
 */
export const History: NextPage = (props: HistoryProps) => {
  return (
    <Layout drawerWidth={280} htmlTitle="Payouts">
      <HistoryView {...props} />
    </Layout>
  );
};

export default History;
