import { useRouter } from "next/router";
import { useEffect } from "react";

// eslint-disable-next-line @typescript-eslint/no-empty-interface
interface IndexProps {}

/**
 * Index
 *
 * @param {IndexProps} props
 * @returns JSX.Element
 */
export const Index = (props: IndexProps) => {
  const router = useRouter();

  useEffect(() => {
    router.push('/auth/login');
  }, [router])

  return (
    <>
      <div>Redirecting...</div>
    </>
  );
}

export default Index;
