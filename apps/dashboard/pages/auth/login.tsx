import dynamic from 'next/dynamic';
import Head from 'next/head';

const LoginView = dynamic(() => import('@verifsystem/dashboard/features/auth').then((m) => m.Login));

// eslint-disable-next-line @typescript-eslint/no-empty-interface
interface IndexProps {}

/**
 * Index
 *
 * @param {IndexProps} props
 * @returns JSX.Element
 */
export const Index = (props: IndexProps) => {
  return (
    <>
      <Head>
        <title>Verifsystem - Login</title>
      </Head>
      <LoginView />
    </>
  );
};

export default Index;
