import dynamic from 'next/dynamic';
import Head from 'next/head';

const LogoutView = dynamic(() => import('@verifsystem/dashboard/features/auth').then((m) => m.Logout));

// eslint-disable-next-line @typescript-eslint/no-empty-interface
interface IndexProps {}

/**
 * Index
 *
 * @param {IndexProps} props
 * @returns JSX.Element
 */
export const Index = (props: IndexProps) => {
  return (
    <>
      <Head>
        <title>Verifsystem - Logout</title>
      </Head>
      <LogoutView />
    </>
  );
};

export default Index;
