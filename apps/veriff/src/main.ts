import { Logger } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger';
import * as cookieParser from 'cookie-parser';
import { json } from 'body-parser';

import { AppModule } from './app/app.module';

async function bootstrap() {
  const port = process.env.PORT || 3001;
  const app = await NestFactory.create(AppModule, {
    bodyParser: false
  });

  app.use(json({ limit: '20mb' }));
  // app.setGlobalPrefix('veriff');

  const config = new DocumentBuilder()
    .setTitle('Veriff API')
    .setDescription('Veriff API description')
    .setVersion('1.0')
    .build();
  const document = SwaggerModule.createDocument(app, config);

  // SwaggerModule.setup('veriff/docs', app, document);
  SwaggerModule.setup('docs', app, document);

  app.enableCors({
    origin: process.env.CORS_ALLOW_ORIGIN.split(','),
    methods: ['GET', 'POST', 'DELETE', 'PATCH', 'PUT', 'OPTIONS'],
    credentials: true,
  });
  app.use(cookieParser());

  await app.listen(port, () => {
    Logger.log(`🚀 Application is running on: http://localhost:${port}`);
  });
}

bootstrap();
