import { TypeOrmModuleOptions } from '@nestjs/typeorm';

export const environment = {
  production: true,
  typeORM: <TypeOrmModuleOptions>{
    type: 'mysql',
    host: '0.0.0.0',
    port: 3306,
    username: 'user',
    password: 'password',
    database: 'verifsystem',
    autoLoadEntities: true,
    synchronize: true
  }
};
