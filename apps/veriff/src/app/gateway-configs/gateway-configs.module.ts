import { Module } from '@nestjs/common';

import { GatewayConfigsModule as GatewayConfigsFeatureModule } from '@verifsystem/api/features';

import { GatewayConfigsController } from './gateway-configs.controller';

@Module({
  imports: [GatewayConfigsFeatureModule],
  controllers: [GatewayConfigsController],
})
export class GatewayConfigsModule {}
