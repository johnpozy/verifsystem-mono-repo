import { Body, Controller, Post } from '@nestjs/common';
import { ApiInternalServerErrorResponse, ApiOkResponse, ApiOperation, ApiTags } from '@nestjs/swagger';

import { GatewayConfigsService, GatewayConfigVeriffDecisionDto, GatewayConfigVeriffEventDto } from '@verifsystem/api/features';

@ApiTags('Gateway Configs')
@Controller('gateway-configs')
export class GatewayConfigsController {
  constructor(private gatewayConfigsService: GatewayConfigsService) {}

  @Post('veriff/events')
  @ApiOperation({ summary: 'Veriff events hook' })
  @ApiOkResponse({ description: 'Ok' })
  @ApiInternalServerErrorResponse({ description: 'Internal Server Error' })
  events(@Body() gatewayConfigVeriffEventDto: GatewayConfigVeriffEventDto) {
    // TODO: find a way to serialize this using `class-transformer` package
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    gatewayConfigVeriffEventDto.vendorData = JSON.parse(gatewayConfigVeriffEventDto.vendorData as any);

    return this.gatewayConfigsService.veriffEvent(gatewayConfigVeriffEventDto);
  }

  @Post('veriff/decisions')
  @ApiOperation({ summary: 'Veriff decisions hook' })
  @ApiOkResponse({ description: 'Ok' })
  @ApiInternalServerErrorResponse({ description: 'Internal Server Error' })
  decisions(@Body() gatewayConfigVeriffDecisionDto: GatewayConfigVeriffDecisionDto) {
    // TODO: find a way to serialize this using `class-transformer` package
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    gatewayConfigVeriffDecisionDto.verification.vendorData = JSON.parse(gatewayConfigVeriffDecisionDto.verification.vendorData as any);

    return this.gatewayConfigsService.veriffDecision(gatewayConfigVeriffDecisionDto);
  }
}
