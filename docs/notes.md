admin-dashboard (https://app.verifsystem.com) — ssh -i ./verifsystem-dev.pem ec2-user@3.131.213.82 - DONE
main-backend:3000 (https://api.verifsystem.com) — ssh -i ./verifsystem-dev.pem ec2-user@3.15.48.18 - DONE
stripe-processor:3001 (https://api.nsbeautysupply.com) — ssh -i ./verifsystem-dev.pem ec2-user@3.131.101.234 - DONE
veriff-processor:3002 (https://api3.vancityscooter.com) — ssh -i ./verifsystem-dev.pem ec2-user@18.190.90.36 - DONE

https://devcoops.com/install-nginx-on-aws-ec2-amazon-linux/
sudo yum install nginx
sudo yum install -y certbot python2-certbot-apache
sudo certbot --nginx -d app.verifsystem.com

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

18.141.57.23
ssh root@165.22.100.172
ssh -i ./verifsystem-dev.pem ec2-user@18.190.90.36
ssh -i ./cosmic-gummies-ap-southeast-1.pem ubuntu@13.215.252.99
ssh root@165.22.100.172

rsync -avL --progress --delete -e "ssh -i ./verifsystem-dev.pem" \
--exclude ".husky" \
--exclude ".vscode" \
--exclude "docker" \
--exclude "docs" \
--exclude "node_modules" \
--exclude "verifsystem-dev.pem" \
--exclude "docker-compose.yml" \
--exclude "tunnelto" \
--exclude ".env*" \
./* ec2-user@3.15.48.18:app/

rsync -avL --progress --delete -e "ssh -i ./verifsystem-dev.pem" ./dist/apps/dashboard/exported/* ec2-user@3.131.213.82:app/
rsync -avL --progress --delete -e "ssh -i ./verifsystem-dev.pem" ./pm2/* ec2-user@3.15.48.18:app/pm2/

pm2 startOrReload pm2/aws.config.js --only "payment,veriff"

sudo chmod 655 /home/ec2-user/app/dist/apps/dashboard/exported
sudo cp -R app/* /usr/share/nginx/html/dashboard/

/usr/share/nginx/html

location /dashboard {
root /home/ec2-user/app/dist/apps/dashboard/exported/;
index index.html;
autoindex on;
}

location ^~ /backend {
proxy_http_version 1.1;
proxy_set_header Host $host;
proxy_set_header X-Real-IP $remote_addr;
proxy_pass http://127.0.0.1:3000;
}

location ^~ /payment {
proxy_http_version 1.1;
proxy_set_header Host $host;
proxy_set_header X-Real-IP $remote_addr;
proxy_pass http://127.0.0.1:3001;
}

location ^~ /veriff {
proxy_http_version 1.1;
proxy_set_header Host $host;
proxy_set_header X-Real-IP $remote_addr;
proxy_pass http://127.0.0.1:3002;
}

location /dashboard {
proxy_set_header Upgrade $http_upgrade;
    proxy_set_header Connection $connection_upgrade;
    proxy_set_header X-Forwarded-for $proxy_add_x_forwarded_for;
    proxy_set_header Host $host;
    alias /home/ec2-user/app/dist/apps/dashboard/exported;
    try_files $uri $uri$args $uri$args/ /dashboard/index.html;
location ~\* \.(jpg|jpeg|png|gif|ico|js|woff|woff2|ttf)$ {
access_log off;
expires max;
}
}

location /backend {
proxy_pass http://127.0.0.1:3001;
proxy_http_version 1.1;
proxy_set_header Upgrade $http_upgrade;
proxy_set_header Connection 'upgrade';
proxy_set_header Host $host;
proxy_cache_bypass $http_upgrade;
}

location ^~ /backend/ {
proxy_http_version 1.1;
proxy_set_header Host $host;
proxy_set_header X-Real-IP $remote_addr;
proxy_pass http://127.0.0.1:3000;
}

location ^~ /payment/ {
proxy_http_version 1.1;
proxy_set_header Host $host;
proxy_set_header X-Real-IP $remote_addr;
proxy_pass http://127.0.0.1:3001;
}

location ^~ /veriff/ {
proxy_http_version 1.1;
proxy_set_header Host $host;
proxy_set_header X-Real-IP $remote_addr;
proxy_pass http://127.0.0.1:3002;
}

server {
listen 80;
server_name local;

    location /backend {
        proxy_pass http://127.0.0.1:3001;
        proxy_set_header Host $host;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Proto $scheme;
    }

}

# For more information on configuration, see:

# \* Official English Documentation: http://nginx.org/en/docs/

# \* Official Russian Documentation: http://nginx.org/ru/docs/

user nginx;
worker_processes auto;
error_log /var/log/nginx/error.log;
pid /run/nginx.pid;

# Load dynamic modules. See /usr/share/doc/nginx/README.dynamic.

include /usr/share/nginx/modules/\*.conf;

events {
worker_connections 1024;
}

http {
log_format main '$remote_addr - $remote_user [$time_local] "$request" '
                      '$status $body_bytes_sent "$http_referer" '
'"$http_user_agent" "$http_x_forwarded_for"';

    access_log  /var/log/nginx/access.log  main;

    sendfile            on;
    tcp_nopush          on;
    tcp_nodelay         on;
    keepalive_timeout   65;
    types_hash_max_size 4096;

    include             /etc/nginx/mime.types;
    default_type        application/octet-stream;

    # Load modular configuration files from the /etc/nginx/conf.d directory.
    # See http://nginx.org/en/docs/ngx_core_module.html#include
    # for more information.
    include /etc/nginx/conf.d/*.conf;

    server {
        server_name  api.nsbeautysupply.com;
        root         /usr/share/nginx/html;

        # Load configuration files for the default server block.
        include /etc/nginx/default.d/*.conf;

        location / {
            proxy_http_version 1.1;
            proxy_set_header Host $host;
            proxy_set_header X-Real-IP $remote_addr;
            proxy_pass http://127.0.0.1:3002;
        }
    }

}
