# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

## [1.2.0](https://gitlab.com/onseen.yt/verifsystem-mono-repo/compare/v1.1.3...v1.2.0) (2022-08-27)


### Features

* **ecomm:** import external users into database ([9400885](https://gitlab.com/onseen.yt/verifsystem-mono-repo/commit/94008858ec07c2831007a577979b7c8793eb5a52))

### [1.1.3](https://gitlab.com/onseen.yt/verifsystem-mono-repo/compare/v1.1.2...v1.1.3) (2022-08-23)

### [1.1.2](https://gitlab.com/onseen.yt/verifsystem-mono-repo/compare/v1.1.1...v1.1.2) (2022-08-23)


### Bug Fixes

* **dashboard:** remove version number in sidebar ([7bc7b9e](https://gitlab.com/onseen.yt/verifsystem-mono-repo/commit/7bc7b9edbaa63b95b954854ad516cb8349c94bbc))

### [1.1.1](https://gitlab.com/onseen.yt/verifsystem-mono-repo/compare/v1.1.0...v1.1.1) (2022-08-23)

## 1.1.0 (2022-08-23)


### Features

* **global:** add conventional commit message ([4cda15d](https://gitlab.com/onseen.yt/verifsystem-mono-repo/commit/4cda15db0a5425afacb677293d0f3751af0f2c9d))
* initial commit ([25d2800](https://gitlab.com/onseen.yt/verifsystem-mono-repo/commit/25d280031bae138b9f5336f1a67022ca6e09a0cf))
* initial commit ([0189920](https://gitlab.com/onseen.yt/verifsystem-mono-repo/commit/0189920c92b68b6f9f19ac2fa0526fc71e6132ee))
* initial commit ([c947422](https://gitlab.com/onseen.yt/verifsystem-mono-repo/commit/c947422afb3cb63943bfc8ceeed123fba26498e7))
* initial commit ([669a1f9](https://gitlab.com/onseen.yt/verifsystem-mono-repo/commit/669a1f9c4360063fcf8c5c3ee756f29044f35a49))
* initial commit ([002e367](https://gitlab.com/onseen.yt/verifsystem-mono-repo/commit/002e3672bec66460ec59ebd43057614c0dcc93b5))
* initial commit ([fd66a52](https://gitlab.com/onseen.yt/verifsystem-mono-repo/commit/fd66a521ad79633a8b385c071968dbe6a03ced7a))
* initial commit ([2381217](https://gitlab.com/onseen.yt/verifsystem-mono-repo/commit/2381217663fac84305374fb851ba6a015b15761a))
* initial commit ([e8b41dc](https://gitlab.com/onseen.yt/verifsystem-mono-repo/commit/e8b41dc834b9d1bb02599ee30bc70543fe0983cf))
* initial commit ([eca7dbe](https://gitlab.com/onseen.yt/verifsystem-mono-repo/commit/eca7dbeb21b1a3e87d6fdfba06b69524f278ed27))
* initial commit ([b5a915d](https://gitlab.com/onseen.yt/verifsystem-mono-repo/commit/b5a915d3925b2784c952a06a812e61ebbf272335))
* initial commit ([4b6bfc0](https://gitlab.com/onseen.yt/verifsystem-mono-repo/commit/4b6bfc018419d4c9f2cf489fb1de271719c72250))
* initial commit ([74df23e](https://gitlab.com/onseen.yt/verifsystem-mono-repo/commit/74df23ef982c781b616b1332176e45dd27fb41ce))
* initial commit ([60885c8](https://gitlab.com/onseen.yt/verifsystem-mono-repo/commit/60885c8af9e693b39088b790debdae47ed15adad))
* initial commit ([820c4cc](https://gitlab.com/onseen.yt/verifsystem-mono-repo/commit/820c4cc5d0cc6a3e605e59eb295eaf530c44adcf))
* initial commit ([d599af6](https://gitlab.com/onseen.yt/verifsystem-mono-repo/commit/d599af685fc23aaa038e48b85908cb477938c527))
* initial commit ([42b9be7](https://gitlab.com/onseen.yt/verifsystem-mono-repo/commit/42b9be77bb75f867ab4619d63dbd0a5be9a8dc45))
* initial commit ([99dfdcb](https://gitlab.com/onseen.yt/verifsystem-mono-repo/commit/99dfdcb48fa52fb64fbe8a08af794809be8bffea))
* initial commit ([2cd596d](https://gitlab.com/onseen.yt/verifsystem-mono-repo/commit/2cd596d26c192bca57fec15dd5916361466dbb57))
* initial commit ([be48d71](https://gitlab.com/onseen.yt/verifsystem-mono-repo/commit/be48d71ef3e6ae8685a4bbe208d074ea33d460a5))
* initial commit ([61b9625](https://gitlab.com/onseen.yt/verifsystem-mono-repo/commit/61b9625d490cef6af9079646138f60036180d3cd))
* initial commit ([307daf3](https://gitlab.com/onseen.yt/verifsystem-mono-repo/commit/307daf3895d92c9efcab18641cf04ed02ab39412))
* initial commit ([b1a32a7](https://gitlab.com/onseen.yt/verifsystem-mono-repo/commit/b1a32a7f2fd2bf3f4947554d0072662aeeecdc63))
* initial commit ([d0b0582](https://gitlab.com/onseen.yt/verifsystem-mono-repo/commit/d0b0582d1d230d2b7f3a4c7cdfffc38b5006264c))
* initial commit ([840e216](https://gitlab.com/onseen.yt/verifsystem-mono-repo/commit/840e216ce5ff357139890654189cc31e31021c96))
* initial commit ([74ba894](https://gitlab.com/onseen.yt/verifsystem-mono-repo/commit/74ba894c816f6b7de754f257736531f158938277))
* initial commit ([235d73a](https://gitlab.com/onseen.yt/verifsystem-mono-repo/commit/235d73af818f9359d698c202fbd9b4c5b9f3341f))
* initial commit ([454596a](https://gitlab.com/onseen.yt/verifsystem-mono-repo/commit/454596a47a78aa8b7f2ae3fdaa103359f6a9855c))
* initial commit ([d13d21a](https://gitlab.com/onseen.yt/verifsystem-mono-repo/commit/d13d21abe5f9412daa4217563a40df05d32e5590))
* initial commit ([74cef13](https://gitlab.com/onseen.yt/verifsystem-mono-repo/commit/74cef13737c648794700eeed764da72d6dfec102))
* initial commit ([1bafd32](https://gitlab.com/onseen.yt/verifsystem-mono-repo/commit/1bafd329e6c199eb9b9969eea365485d49eb092c))
* initial commit ([6bdf985](https://gitlab.com/onseen.yt/verifsystem-mono-repo/commit/6bdf9856830bc204f74d7d19df2f2bf7dd95b988))
* initial commit ([5bdfcd7](https://gitlab.com/onseen.yt/verifsystem-mono-repo/commit/5bdfcd72ebc1a92526c732c0f0ba6f28d13d04af))
* initial commit ([9643a26](https://gitlab.com/onseen.yt/verifsystem-mono-repo/commit/9643a2692aa8c2d9d75652618de2658c24d5b47a))
* initial commit ([f2e003d](https://gitlab.com/onseen.yt/verifsystem-mono-repo/commit/f2e003d30bd946f35773ac942b7a409554571e9c))
* initial commit ([87f2f5d](https://gitlab.com/onseen.yt/verifsystem-mono-repo/commit/87f2f5d1012c79b19573ab601c2753c22e38da2a))
* initial commit ([44a60ba](https://gitlab.com/onseen.yt/verifsystem-mono-repo/commit/44a60bab0cc96287741061e8a9cb45c702e48b48))
* initial commit ([dc86ef0](https://gitlab.com/onseen.yt/verifsystem-mono-repo/commit/dc86ef0a172aa20d5d7d78d4839978d814c2061c))
* initial commit ([26572b1](https://gitlab.com/onseen.yt/verifsystem-mono-repo/commit/26572b1eeca304d3e6f8e9e95452f8eb7e534749))
* initial commit ([5db2df3](https://gitlab.com/onseen.yt/verifsystem-mono-repo/commit/5db2df3de08a17f5349decedd62c42a7ed287a2b))
* initial commit ([9839267](https://gitlab.com/onseen.yt/verifsystem-mono-repo/commit/983926711d5a61029779c5488734b50c4bc35936))
* initial commit ([e012e52](https://gitlab.com/onseen.yt/verifsystem-mono-repo/commit/e012e5292501924ae890c49349613699bba0997f))
* initial commit ([810615f](https://gitlab.com/onseen.yt/verifsystem-mono-repo/commit/810615ff5b017109eb2c94eaad56450a96fdad07))
* initial commit ([3690263](https://gitlab.com/onseen.yt/verifsystem-mono-repo/commit/3690263c9934a5834d414989608026b1de4ad69f))
* initial commit ([80485a6](https://gitlab.com/onseen.yt/verifsystem-mono-repo/commit/80485a6cf5cbb4208b6454bb447eb1146f023ad3))
* initial commit ([0ff0d28](https://gitlab.com/onseen.yt/verifsystem-mono-repo/commit/0ff0d288bb68dec8f1411f5293d15b37919cb655))
* initial commit ([3648b23](https://gitlab.com/onseen.yt/verifsystem-mono-repo/commit/3648b23a7ce738fa0683f429af9fbc0221fc7616))
* initial commit ([f003920](https://gitlab.com/onseen.yt/verifsystem-mono-repo/commit/f0039200f36f9be389ff1b7d8ea80c08d7b8841d))
* initial commit ([1da3dc3](https://gitlab.com/onseen.yt/verifsystem-mono-repo/commit/1da3dc38a947644a786bdd5be2d51ba1dc47a014))
* initial commit ([23d59a5](https://gitlab.com/onseen.yt/verifsystem-mono-repo/commit/23d59a5366c7d9af665c8b0ab05fb43e279b85e0))
* initial commit ([e73834e](https://gitlab.com/onseen.yt/verifsystem-mono-repo/commit/e73834edc8d3784be4a8854a4c466d22d79d9da0))
* initial commit ([1ad5f4e](https://gitlab.com/onseen.yt/verifsystem-mono-repo/commit/1ad5f4eb338ca5989f3b19bea6c59c315b71716f))
* initial commit ([3b17ecc](https://gitlab.com/onseen.yt/verifsystem-mono-repo/commit/3b17ecc72ae9d23f37952a3a089ee753b3107783))
* initial commit ([adf7cc0](https://gitlab.com/onseen.yt/verifsystem-mono-repo/commit/adf7cc0bf1068db5aeb34985802e997f13739f1c))
* initial commit ([341500e](https://gitlab.com/onseen.yt/verifsystem-mono-repo/commit/341500ecb4849d9a92a3812864667fbf93abfe33))
* initial commit ([7f2ac21](https://gitlab.com/onseen.yt/verifsystem-mono-repo/commit/7f2ac216b0e4aaca5d4af958b20b6aaf20805a73))
* initial commit ([7694757](https://gitlab.com/onseen.yt/verifsystem-mono-repo/commit/769475766a9a4a8b6a949ea29c8b1a0f29df734c))
* initial commit ([7c76361](https://gitlab.com/onseen.yt/verifsystem-mono-repo/commit/7c76361e98788c915ee8f71b3c532282191edbcc))
* initial commit ([0c50942](https://gitlab.com/onseen.yt/verifsystem-mono-repo/commit/0c50942e124f9af66e1900a93b3a9bb71fe3db85))
* initial commit ([86e431e](https://gitlab.com/onseen.yt/verifsystem-mono-repo/commit/86e431e9661f4a6181fd8939d6fa4890fbebf408))
* initial commit ([f13314f](https://gitlab.com/onseen.yt/verifsystem-mono-repo/commit/f13314f273de47764f1affe31ca5cdd5726b060d))
* initial commit ([560389c](https://gitlab.com/onseen.yt/verifsystem-mono-repo/commit/560389c7a2dbc8634921d8beb400b5e793b352cf))
* initial commit ([e559655](https://gitlab.com/onseen.yt/verifsystem-mono-repo/commit/e559655dbac84ca5e1bf383cba8bf44cb54b90f2))
* initial commit ([12654ef](https://gitlab.com/onseen.yt/verifsystem-mono-repo/commit/12654ef88305623f700fb8d82a1d1ea551de8036))
* initial commit ([5bf57f6](https://gitlab.com/onseen.yt/verifsystem-mono-repo/commit/5bf57f67e9d827e0ffacdede27ddcec099b6d64d))
* initial commit ([3c79643](https://gitlab.com/onseen.yt/verifsystem-mono-repo/commit/3c796436ee5368c9cf7e442a9fc68f096d51c9c6))
* initial commit ([bc461d6](https://gitlab.com/onseen.yt/verifsystem-mono-repo/commit/bc461d696b8eb960d9bfb7902044f4aa6e9d3d02))
* initial commit ([1ae5fcd](https://gitlab.com/onseen.yt/verifsystem-mono-repo/commit/1ae5fcded824be08e982c98475e17ef6f696846c))
* initial commit ([0023ea5](https://gitlab.com/onseen.yt/verifsystem-mono-repo/commit/0023ea5aef6a3e37376439954a80a8234935fa50))
* initial commit ([c517618](https://gitlab.com/onseen.yt/verifsystem-mono-repo/commit/c517618f048f246ccb2efa57be5ffbbe6a0415ad))
* initial commit ([f85442e](https://gitlab.com/onseen.yt/verifsystem-mono-repo/commit/f85442e8da574f31c4f879baa558b674c1aef93d))
* initial commit ([316dc97](https://gitlab.com/onseen.yt/verifsystem-mono-repo/commit/316dc97db10a6b8d9514ec9340422e8fda145af5))
* initial commit ([466497d](https://gitlab.com/onseen.yt/verifsystem-mono-repo/commit/466497d91c9086ca706074d13018ad8d6b43be17))
* initial commit ([dc1cc6f](https://gitlab.com/onseen.yt/verifsystem-mono-repo/commit/dc1cc6f530b3faf70a66568e3394d85b15bced49))
* initial commit ([26bd0e4](https://gitlab.com/onseen.yt/verifsystem-mono-repo/commit/26bd0e4361524c5b26638141fed30207bc880c8a))
* initial commit ([e45167f](https://gitlab.com/onseen.yt/verifsystem-mono-repo/commit/e45167f9cb515a85726c7f6dbb3fd990810c8256))
* initial commit ([cb6b2b3](https://gitlab.com/onseen.yt/verifsystem-mono-repo/commit/cb6b2b36f844c5a04cba52987542681c9caafc77))
* initial commit ([e1df41c](https://gitlab.com/onseen.yt/verifsystem-mono-repo/commit/e1df41c198d8ca042b1f77456f6c453ea2af1927))
* initial commit ([b1dc03f](https://gitlab.com/onseen.yt/verifsystem-mono-repo/commit/b1dc03f937a9d2d326feaa47b9917ebc7d0fcc65))
* initial commit ([e77bb8b](https://gitlab.com/onseen.yt/verifsystem-mono-repo/commit/e77bb8b12fd3f84807c4c4e01309167c07f80d78))
* initial commit ([ed1e96b](https://gitlab.com/onseen.yt/verifsystem-mono-repo/commit/ed1e96b78937d238304298a73129f718d930b85c))
* initial commit ([9d0a3a1](https://gitlab.com/onseen.yt/verifsystem-mono-repo/commit/9d0a3a1a3eb89b53108106c90aa973d8695c7a5f))
* initial commit ([bc9ed4c](https://gitlab.com/onseen.yt/verifsystem-mono-repo/commit/bc9ed4c7f9bb84d5ef1de810139d2ef6b127900f))
* initial commit ([b98f469](https://gitlab.com/onseen.yt/verifsystem-mono-repo/commit/b98f469c5b838ab7ca6dc7f2c7266dc9ca4fc293))
* initial commit ([dfeb3db](https://gitlab.com/onseen.yt/verifsystem-mono-repo/commit/dfeb3db40456ab650220f2361f8d481cf40e9a50))
* initial commit ([2e9fd90](https://gitlab.com/onseen.yt/verifsystem-mono-repo/commit/2e9fd9050373fc2c0a778939bbcc2f239dc25c7b))
* initial commit ([15139ed](https://gitlab.com/onseen.yt/verifsystem-mono-repo/commit/15139ed68047125dc498db83c31b1dc9a4f7becb))
* initial commit ([d39a1c8](https://gitlab.com/onseen.yt/verifsystem-mono-repo/commit/d39a1c8e31665328247e9f957949a502c6246eec))
* initial commit ([18462ab](https://gitlab.com/onseen.yt/verifsystem-mono-repo/commit/18462ab5cc012ee62100c31f468f4e1c570aa278))
* initial commit ([c981634](https://gitlab.com/onseen.yt/verifsystem-mono-repo/commit/c981634658fabafc20046f18fef897e2bb390826))
* initial commit ([0d890c0](https://gitlab.com/onseen.yt/verifsystem-mono-repo/commit/0d890c0e32939f51d3212726ccf71630642d3883))
* initial commit ([eb3cdff](https://gitlab.com/onseen.yt/verifsystem-mono-repo/commit/eb3cdff386336bb5758688ff51ef618b379c2fce))
* initial commit ([518bab7](https://gitlab.com/onseen.yt/verifsystem-mono-repo/commit/518bab764be911fbe85febfcdacea81b1c46a254))
* initial commit ([a399d92](https://gitlab.com/onseen.yt/verifsystem-mono-repo/commit/a399d927a0dd484dde396472274657b6596f5599))
* initial commit ([31de5ef](https://gitlab.com/onseen.yt/verifsystem-mono-repo/commit/31de5ef1b3aba2972e40a04fe22628456f065477))
* initial commit ([1efe3dd](https://gitlab.com/onseen.yt/verifsystem-mono-repo/commit/1efe3dd8919827ff2b9f592b830d038fd571896b))
* initial commit ([e91f32c](https://gitlab.com/onseen.yt/verifsystem-mono-repo/commit/e91f32c5f26725dca57e4ee82b213f1fe08a3af7))
* initial commit ([79bc470](https://gitlab.com/onseen.yt/verifsystem-mono-repo/commit/79bc4705ba7d3abb04f77a5bd20b1a030d6d7a4b))
* initial commit ([e07a2cc](https://gitlab.com/onseen.yt/verifsystem-mono-repo/commit/e07a2ccd407407c9ef5fdb88063ef9b0cd2bfdfe))
* initial commit ([cbb2da6](https://gitlab.com/onseen.yt/verifsystem-mono-repo/commit/cbb2da616e0003e32342fe8ffeaa70d93d1f245f))
* initial commit ([61a7c38](https://gitlab.com/onseen.yt/verifsystem-mono-repo/commit/61a7c38f727b72cffce290ee7431ad6ae3e6aea6))
* initial commit ([1e76255](https://gitlab.com/onseen.yt/verifsystem-mono-repo/commit/1e762559149eec6e5ed1f4a0fd2ba9a9a722d137))
* initial commit ([ac076d2](https://gitlab.com/onseen.yt/verifsystem-mono-repo/commit/ac076d20e60cba71982cae58d8c68ebde1bd8cf0))
* initial commit ([e447dae](https://gitlab.com/onseen.yt/verifsystem-mono-repo/commit/e447dae60ae819f39c02c8a0e1d5e0d40aac4930))
* initial commit ([70624c2](https://gitlab.com/onseen.yt/verifsystem-mono-repo/commit/70624c2ba1b5c90c17d8291b84d77b4efec6418a))
* initial commit ([19a938d](https://gitlab.com/onseen.yt/verifsystem-mono-repo/commit/19a938de031ec3182e0f4095ebdb85ad1063a8ab))
* initial commit ([ef2b4f1](https://gitlab.com/onseen.yt/verifsystem-mono-repo/commit/ef2b4f1fe8a88cb7f1f75e81f3ad2a0d7bcdb33e))
* initial commit ([faada13](https://gitlab.com/onseen.yt/verifsystem-mono-repo/commit/faada132fe5653494af94b545d3ceb2b01220b93))
* initial commit ([be97d4c](https://gitlab.com/onseen.yt/verifsystem-mono-repo/commit/be97d4c8803d02b841a83bca152070edebe3e858))
* initial commit ([c09bb15](https://gitlab.com/onseen.yt/verifsystem-mono-repo/commit/c09bb150f2a1a5e652396e11c0b3f57ee64f1fc4))
* initial commit ([e273354](https://gitlab.com/onseen.yt/verifsystem-mono-repo/commit/e273354a60b8c912475433c47849f3a656615e25))
* initial commit ([7d69305](https://gitlab.com/onseen.yt/verifsystem-mono-repo/commit/7d69305c809f71f0ad55668de3db7d0fd9ac2af6))
* initial commit ([2b3b785](https://gitlab.com/onseen.yt/verifsystem-mono-repo/commit/2b3b78577fd4c01282793e745467a7309864c2fc))
* initial commit ([2e2d495](https://gitlab.com/onseen.yt/verifsystem-mono-repo/commit/2e2d495d809439c09a396bdd05578b288b9061e0))
* initial commit ([0629c4f](https://gitlab.com/onseen.yt/verifsystem-mono-repo/commit/0629c4f0b2a1f5fca7f98a1ceb02470b3dfc969e))
* initial commit ([1aeeb47](https://gitlab.com/onseen.yt/verifsystem-mono-repo/commit/1aeeb47a37db27cf5f819d6cabc729e1b8ef75b3))
* initial commit ([d560ab4](https://gitlab.com/onseen.yt/verifsystem-mono-repo/commit/d560ab40ff9d7369cfa769936e1f572c3a3d97fd))
* initial commit ([08197af](https://gitlab.com/onseen.yt/verifsystem-mono-repo/commit/08197afe7c899c084c7c687684bf712cf4087593))
* initial commit ([433e230](https://gitlab.com/onseen.yt/verifsystem-mono-repo/commit/433e230bc2ed0099d28dc78b3b56ffcead196093))
* initial commit ([4135c43](https://gitlab.com/onseen.yt/verifsystem-mono-repo/commit/4135c4378001f46edbac1ba4609f851391df6c99))
* initial commit ([20f57c0](https://gitlab.com/onseen.yt/verifsystem-mono-repo/commit/20f57c09bdd493e402d9902ecf3bce605f13ccb7))
* initial commit ([74a3a30](https://gitlab.com/onseen.yt/verifsystem-mono-repo/commit/74a3a30c5e86d3bf8d18084663b803fd3e52845c))
* initial commit ([39b351d](https://gitlab.com/onseen.yt/verifsystem-mono-repo/commit/39b351daa9de57a4f222b9f8ebd97bcf445317ef))
* initial commit ([eb54e56](https://gitlab.com/onseen.yt/verifsystem-mono-repo/commit/eb54e5671b574c813ed571b43abbf559ac88fcbf))
* initial commit ([ab8ce61](https://gitlab.com/onseen.yt/verifsystem-mono-repo/commit/ab8ce6128810953e6b5b7d4d6ab8a7b6796f4626))
* initial commit ([00ffd4f](https://gitlab.com/onseen.yt/verifsystem-mono-repo/commit/00ffd4f1968a59d705ca06551ccd521a449dc701))
* initial commit ([4f95f4d](https://gitlab.com/onseen.yt/verifsystem-mono-repo/commit/4f95f4d8c4d376ba54a20155393fd1f559d1cc96))
* initial commit ([1fb8716](https://gitlab.com/onseen.yt/verifsystem-mono-repo/commit/1fb8716780c70fd112068fb3645650f5ca3eb8a0))
* initial commit ([68d1658](https://gitlab.com/onseen.yt/verifsystem-mono-repo/commit/68d165843483256bd427e47c911a4ce316d8c856))
* initial commit ([2a4acb9](https://gitlab.com/onseen.yt/verifsystem-mono-repo/commit/2a4acb9934baba65d3eccb469d7ded3d8f2edeed))
* initial commit ([aae0f84](https://gitlab.com/onseen.yt/verifsystem-mono-repo/commit/aae0f84ca2ee2b6259719cba859ec9a54d01ec17))
* initial commit ([4d06308](https://gitlab.com/onseen.yt/verifsystem-mono-repo/commit/4d0630860af293ebd361abc42ddf9cc29389d24a))
* initial commit ([ffb55ac](https://gitlab.com/onseen.yt/verifsystem-mono-repo/commit/ffb55ac2e0d4bd00deb4ed2bc8ca4dfb97b9498e))
* initial commit ([60a60e2](https://gitlab.com/onseen.yt/verifsystem-mono-repo/commit/60a60e2c7188caea7f6a6742ec431ae07cf2b41a))
* initial commit ([a354f36](https://gitlab.com/onseen.yt/verifsystem-mono-repo/commit/a354f3642230dc437ee8fd0ee7bf98942cd8eadd))
* initial commit ([e8d3a8b](https://gitlab.com/onseen.yt/verifsystem-mono-repo/commit/e8d3a8bb034e7f57a0ed9d0f382f9447cbd778c9))
* initial commit ([1ba9693](https://gitlab.com/onseen.yt/verifsystem-mono-repo/commit/1ba96934364278cc1e505c70dc4c881d3e078194))
* initial commit ([9cdea53](https://gitlab.com/onseen.yt/verifsystem-mono-repo/commit/9cdea53d17c27428ca5a07e067db6f987bc54c30))
* initial commit ([bc07eca](https://gitlab.com/onseen.yt/verifsystem-mono-repo/commit/bc07ecab60624b1dc62f72bf4907d335afe9dc44))
* initial commit ([7cd980c](https://gitlab.com/onseen.yt/verifsystem-mono-repo/commit/7cd980c13193e145014b16657c0de5b7bc56604d))
* initial commit ([4449fb8](https://gitlab.com/onseen.yt/verifsystem-mono-repo/commit/4449fb896e20e6958f10a1285b7825220bf7e1af))
* initial commit ([1ac061a](https://gitlab.com/onseen.yt/verifsystem-mono-repo/commit/1ac061a95c3c29d6e0060a5811fb632e63c50129))
* initial commit ([26a8df6](https://gitlab.com/onseen.yt/verifsystem-mono-repo/commit/26a8df6270978da0f74d149855c50c7e1ed1a500))
* initial commit ([ac770eb](https://gitlab.com/onseen.yt/verifsystem-mono-repo/commit/ac770ebecb0b0bd627a7bb07db7f580ba39a0000))
* initial commit ([ddc88f7](https://gitlab.com/onseen.yt/verifsystem-mono-repo/commit/ddc88f79a76a2fffa3a46b445948346e99f09aca))
* initial commit ([45cbdbb](https://gitlab.com/onseen.yt/verifsystem-mono-repo/commit/45cbdbb52e7c12cabc66d2353dd1c3efc54165c6))
* initial commit ([a9be65d](https://gitlab.com/onseen.yt/verifsystem-mono-repo/commit/a9be65df0f6ad5c64b79425a7f877687f2869395))
* initial commit ([62489da](https://gitlab.com/onseen.yt/verifsystem-mono-repo/commit/62489da095dd0f8d63ecb88dcab625da6ab0b829))
* initial commit ([003674b](https://gitlab.com/onseen.yt/verifsystem-mono-repo/commit/003674bee3e93669783404ce481c23b71d78428e))
* initial commit ([4bf74a9](https://gitlab.com/onseen.yt/verifsystem-mono-repo/commit/4bf74a91f4645b010df3d6eacee97bd13c18a78c))
* initial commit ([059db63](https://gitlab.com/onseen.yt/verifsystem-mono-repo/commit/059db6321478d1b70ef22326dd59fb9a271c709b))
* initial commit ([d8af190](https://gitlab.com/onseen.yt/verifsystem-mono-repo/commit/d8af190afa76c0d201b118e3a9f5d967c314f1e5))
* initial commit ([de95943](https://gitlab.com/onseen.yt/verifsystem-mono-repo/commit/de9594343dffecbd8ebe9a75cfaf9a4394354b0f))
* initial commit ([ae7e326](https://gitlab.com/onseen.yt/verifsystem-mono-repo/commit/ae7e32670bf15b1b140b20c3e4eba6d64120c534))
* initial commit ([db1bbb0](https://gitlab.com/onseen.yt/verifsystem-mono-repo/commit/db1bbb07af0a104a9eeb11e3d2666af4c4a8fbf6))
* initial commit ([15b3058](https://gitlab.com/onseen.yt/verifsystem-mono-repo/commit/15b30588fa4b3d1eac419da29674976f3af3a732))
* initial commit ([ff8736c](https://gitlab.com/onseen.yt/verifsystem-mono-repo/commit/ff8736c0f3e808a7c3c8ed491325af843f94d233))
* initial commit ([7ffb6f9](https://gitlab.com/onseen.yt/verifsystem-mono-repo/commit/7ffb6f9371616a2c6a1cd5386513baff2628a0a2))
* initial commit ([69e1e3e](https://gitlab.com/onseen.yt/verifsystem-mono-repo/commit/69e1e3eedda26117654372513f9c1ddee650252e))
* initial commit ([d5804e4](https://gitlab.com/onseen.yt/verifsystem-mono-repo/commit/d5804e4578d31685566bdd44d55fdda18e50d34d))
* initial commit ([de54121](https://gitlab.com/onseen.yt/verifsystem-mono-repo/commit/de54121a266fa8102553fcd7a90c0b3e476cae15))
* initial commit ([3269d16](https://gitlab.com/onseen.yt/verifsystem-mono-repo/commit/3269d163d5f5d47c0083a146921c6c1ba26534b3))
* initial commit ([bbcaaec](https://gitlab.com/onseen.yt/verifsystem-mono-repo/commit/bbcaaec4e37abb8c0641a7a355931ed77581bd63))
* initial commit ([d0a754f](https://gitlab.com/onseen.yt/verifsystem-mono-repo/commit/d0a754fe90600074ca117736603c9d00c6c0450c))
* initial commit ([2a6ee7e](https://gitlab.com/onseen.yt/verifsystem-mono-repo/commit/2a6ee7ef1dbd20033e3396eca334755d55e3b1b0))
* initial commit ([dd36e27](https://gitlab.com/onseen.yt/verifsystem-mono-repo/commit/dd36e27616dba82e5f79977dfff56785a6a24c4c))
* initial commit ([5b6d154](https://gitlab.com/onseen.yt/verifsystem-mono-repo/commit/5b6d154609f7833ec599c864fd556726a81a2d5f))
* initial commit ([f8868d1](https://gitlab.com/onseen.yt/verifsystem-mono-repo/commit/f8868d105dd78165154693272eb69eaaa22b5b24))
* initial commit ([2a7cd26](https://gitlab.com/onseen.yt/verifsystem-mono-repo/commit/2a7cd26c7834cc120be8e963ea9fbe3b8ff2a35a))
* initial commit ([90e90f6](https://gitlab.com/onseen.yt/verifsystem-mono-repo/commit/90e90f6b3c6ed453fafd9a51d90934da1c237cda))
* initial commit ([4efc9c2](https://gitlab.com/onseen.yt/verifsystem-mono-repo/commit/4efc9c2f3fdea09c62059e2d08fd2f129d8a4da1))
* initial commit ([df355b0](https://gitlab.com/onseen.yt/verifsystem-mono-repo/commit/df355b0a676657c6e2e254b279cdd63f97717711))
* initial commit ([996fda9](https://gitlab.com/onseen.yt/verifsystem-mono-repo/commit/996fda968760cc0cb14506ba5ec963a485da8e7b))
* initial commit ([ae783a7](https://gitlab.com/onseen.yt/verifsystem-mono-repo/commit/ae783a73c242e43d288e248e7cfdf2bdf2edeb33))
* initial commit ([0429703](https://gitlab.com/onseen.yt/verifsystem-mono-repo/commit/0429703bc516819e839a696e0b6ff11b893ddd46))
* initial commit ([7ec65ed](https://gitlab.com/onseen.yt/verifsystem-mono-repo/commit/7ec65edb465a3bd15d2786585a2e5061db345fc4))
* initial commit ([256c7e7](https://gitlab.com/onseen.yt/verifsystem-mono-repo/commit/256c7e76569e9e0ed5723af415a2dc57361d9abb))
* initial commit ([ca2cfd2](https://gitlab.com/onseen.yt/verifsystem-mono-repo/commit/ca2cfd2d6efe270a8bb63e59640935d9e2682727))
* initial commit ([745a091](https://gitlab.com/onseen.yt/verifsystem-mono-repo/commit/745a091eb2626517ef26d2eb961db779fb6be629))
* initial commit ([f56d0a1](https://gitlab.com/onseen.yt/verifsystem-mono-repo/commit/f56d0a16033ba0f76a89537bb31be880d1fb6eb0))
* initial commit ([006b341](https://gitlab.com/onseen.yt/verifsystem-mono-repo/commit/006b341521a3ba745a474b84d769b1dac00a9aee))
* initial commit ([d8bf0f3](https://gitlab.com/onseen.yt/verifsystem-mono-repo/commit/d8bf0f344e2cb1d270631d4c44bb7db9a488e5c9))
* initial commit ([06d74f6](https://gitlab.com/onseen.yt/verifsystem-mono-repo/commit/06d74f66f035592f1da930e09f66ed2702418538))
* initial commit ([d43080f](https://gitlab.com/onseen.yt/verifsystem-mono-repo/commit/d43080f8e051334b7648d76fd417f41223a0e6c8))
* initial commit ([4c310bd](https://gitlab.com/onseen.yt/verifsystem-mono-repo/commit/4c310bd314ecd51ee158e4c3527162d46b22d18a))
* initial commit ([194a5b0](https://gitlab.com/onseen.yt/verifsystem-mono-repo/commit/194a5b0ac5dbf86e3468880bb8eb07c5fd87a102))
* initial commit ([0aa58b2](https://gitlab.com/onseen.yt/verifsystem-mono-repo/commit/0aa58b2fd863835363d7428c6f1ef31efad9a3b3))
* initial commit ([85f3646](https://gitlab.com/onseen.yt/verifsystem-mono-repo/commit/85f364653ee2214fd2237c5e987ea28eb2407091))
* initial commit ([fdc84b7](https://gitlab.com/onseen.yt/verifsystem-mono-repo/commit/fdc84b7136326b4884b6845fa878b19b2cb5f7e5))
* initial commit ([f970766](https://gitlab.com/onseen.yt/verifsystem-mono-repo/commit/f97076661fdea4be679a9864c5247752c76bd780))
* initial commit ([79d19ae](https://gitlab.com/onseen.yt/verifsystem-mono-repo/commit/79d19ae4e9eb8be9fd385cd96a57c67036249fa1))
* intial commit ([54f6d09](https://gitlab.com/onseen.yt/verifsystem-mono-repo/commit/54f6d09dd7735624db07eb313c951e3a1e333078))
* intial commit ([58fec63](https://gitlab.com/onseen.yt/verifsystem-mono-repo/commit/58fec63448f854c5a7cfd73622b6347fdda500ff))


### Bug Fixes

* build issue on vercel ([02621ec](https://gitlab.com/onseen.yt/verifsystem-mono-repo/commit/02621ec08cb1163b592973f276a3988431b91283))
* missing gateway config type ([a235916](https://gitlab.com/onseen.yt/verifsystem-mono-repo/commit/a235916b368bc54c044f845c5b6a14c9e719203e))
* processor query on relational table ([3dc9200](https://gitlab.com/onseen.yt/verifsystem-mono-repo/commit/3dc920054707a2bda5684951fc80faa69bdd7020))
* progree bar throw error ([5e0b8bf](https://gitlab.com/onseen.yt/verifsystem-mono-repo/commit/5e0b8bf6ab7de6fae904caec3c3923995f6b7737))
* pull payout transactions based on ecomm ([898f46a](https://gitlab.com/onseen.yt/verifsystem-mono-repo/commit/898f46a2d23e2a5facaff6b78da255502e9bb38f))
* wrong id being pass to update processor API ([8420b3b](https://gitlab.com/onseen.yt/verifsystem-mono-repo/commit/8420b3b4d5716ae90b966301e75cef21ad8d2221))
* wrong order id reference ([b011241](https://gitlab.com/onseen.yt/verifsystem-mono-repo/commit/b01124102aa89fe12c7b98bdc4d100c73f38dfb5))
